const express = require('express');
const regeneratorRuntime = require('regenerator-runtime');
//const router = express.Router();
const fs = require('fs');
const path = require('path');
const dbpool = require('./dbpool');
const enums = require('../middleware/enums');
const tools = require('../middleware/tools');
const vendorFunctions = require('../middleware/api.vendor.functions');
const treosFunctions = require('../middleware/treos.functions');

//router.use(express.json());

module.exports =  {

    getCryptoWithdrawals_Report: async function(symbol, transaction_type_id, date_from, date_to, forExport, record_offset, record_counter) {
        'use strict';
    
        let commandText = '';
        let countSQL = '';
        let criteria = '';
        let parameters = [];
        let dbResult = null;
        let proceed = true;

        let response = {
            result: {
                code: enums.ResponseCodeEnum.INTERNAL_ERROR,
                message: 'Internal Error'
            },
            total_records: 0,
            months: []
        }
        
        try {
            if (proceed) {
                commandText = "SELECT `year`, `month`" +
                    " , ANY_VALUE(IF(`year` = YEAR(CURRENT_DATE) AND `month` = MONTH(CURRENT_DATE), 1, 0)) AS CurrentMonth";

                countSQL = "SELECT COUNT(*) AS TotalMonths" +
                    " FROM ( SELECT COUNT(*) AS Total"


                criteria = " FROM treos_corporate_transactions";

                criteria += " LEFT JOIN treos_transaction_type ON (treos_corporate_transactions.transaction_type_id = treos_transaction_type.type_id)"

                criteria += " WHERE (`date` BETWEEN ? AND ?)"
                parameters.push(date_from + ' 00:00:00');
                parameters.push(date_to + ' 23:59:59');

                if (symbol !== '*') {
                    criteria += " AND symbol = ?";
                    parameters.push(symbol);
                }

                if (transaction_type_id !== '*') {
                    criteria += " AND transaction_type_id = ?";
                    parameters.push(transaction_type_id);
                }

                criteria += " GROUP BY `year`, `month`";

                commandText += criteria;
                countSQL += criteria;

                commandText += " ORDER BY `year` DESC, `month` DESC";

                if (!forExport) {
                    commandText += " LIMIT ?, ?";
                }

                countSQL += ") T1";

                parameters.push(record_offset);
                parameters.push(record_counter);

                dbResult = await dbpool.query(commandText, parameters);

                if (dbResult.success) {
                    for (let i = 0; i < dbResult.result[0].length; i++) {
                        let item = dbResult.result[0][i];

                        let period = {
                            year: null,
                            month: null,
                            current_month: true,
                            currencies: []
                        }

                        period.year = dbResult.result[0][i].year;
                        period.month = dbResult.result[0][i].month;

                        if (dbResult.result[0][i].CurrentMonth === 0) {
                            period.current_month = false;
                        } else {
                            period.current_month = true;
                        }

                        period.currencies = await this.populateCryptoWithdrawalsCurrencies(period.year, period.month, transaction_type_id, symbol);

                        response.months.push(period);
                    }

                    //response.result.code = enums.ResponseCodeEnum.SUCCESS;
                    //response.result.message = null;
                } else {
                    console.log(dbResult.error);
                    response.result.message = "Internal Error";
                    proceed = false;
                }
            }


            if (proceed) {
                dbResult = null;
                
                dbResult = await dbpool.query(countSQL, parameters);

                if (dbResult.success) {
                    response.total_records = dbResult.result[0][0].TotalMonths;

                    response.result.code = enums.ResponseCodeEnum.SUCCESS;
                    response.result.message = null;
                } else {
                    console.log(dbResult.error);
                    response.result.message = "Internal Error";
                    proceed = false;
                }
            }
            
        } catch (err) {
            response.months.length = 0;
            response.result.message = "Internal Error";
            console.log(err);
        }
        return response;
    },   // getCryptoWithdrawals_Report

    getEosWalletCreationHistory: async function(request_id, symbol, member_name, date_from, date_to, forExport, record_offset, record_counter) {
        'use strict';
    
        let commandText = '';
        let countSQL = '';
        let criteria = '';
        let parameters = [];
        let dbResult = null;
        let proceed = true;

        let response = {
            result: {
                code: enums.ResponseCodeEnum.INTERNAL_ERROR,
                message: 'Internal Error'
            },
            total_records: 0,
            requests: []
        }
        
        try {
            if (proceed) {
                commandText = "SELECT T1.*" +
                    ", IFNULL(CONCAT(xc_profiles.firstName, ' ', xc_profiles.lastName), '') AS profile_name" +
                    ", IFNULL((SELECT SUM(balance) FROM treos_profile_token_balance WHERE profile_id = T1.profile_id AND token = T1.symbol), 0) AS current_balance" +  // Using SUM until unique index is applied
                    ", treos_eos_account_create_status.name AS status_name"

                countSQL = "SELECT COUNT(*) AS Total";


                criteria = " FROM treos_eos_account_create T1" +
                    " LEFT JOIN xc_profiles ON (T1.profile_id = xc_profiles.profile_id)" +
                    " LEFT JOIN treos_eos_account_create_status ON (T1.status_id = treos_eos_account_create_status.status_id)";

                criteria += " WHERE T1.status_id <> 1";  // Exclude New
                
                criteria += " AND (date BETWEEN ? AND ?)"
                parameters.push(date_from + ' 00:00:00');
                parameters.push(date_to + ' 23:59:59');

                if (symbol !== '*') {
                    criteria += " AND T1.symbol = ?";
                    parameters.push(symbol);
                }

                if (member_name !== undefined && member_name !== null && member_name.trim().length > 0) {
                    if (!isNaN(member_name.trim())) {
                        criteria += " AND (T1.profile_id = ?)";
                        parameters.push(member_name.trim());
                    } else {
                        criteria += " AND (CONCAT(xc_profiles.firstName, ' ', xc_profiles.lastName) LIKE CONCAT('%', ?, '%'))";
                        parameters.push(member_name.trim());
                    }
                }

                if (request_id && !isNaN(request_id)) {
                    criteria += " AND T1.id = ?";
                    parameters.push(request_id);
                }

                commandText += criteria;
                countSQL += criteria;

                commandText += " ORDER BY `date` DESC";
                
                if (!forExport) {
                    commandText += " LIMIT ?, ?";
                    parameters.push(record_offset);
                    parameters.push(record_counter);
                }

                dbResult = await dbpool.query(commandText, parameters);

                if (dbResult.success) {
                    for (let i = 0; i < dbResult.result[0].length; i++) {
                        let item = dbResult.result[0][i];

                        let request = {
                            request_id: null,
                            profile_id: null,
                            profile_name: null,
                            date_request: null,
                            date: null,
                            status_id: null,
                            status: null,
                            account_name: null,
                            symbol: null,
                            owner_public_key: null,
                            active_public_key: null,
                            fee: null,
                            current_balance: null,
                            reason: null,
                            txn_reference: null,
                            email: null
                        }

                        request.request_id = dbResult.result[0][i].id;
                        request.profile_id = dbResult.result[0][i].profile_id;
                        request.profile_name = dbResult.result[0][i].profile_name;
                        request.date_request = dbResult.result[0][i].date_request;
                        request.date = dbResult.result[0][i].date;
                        request.status_id = dbResult.result[0][i].status_id;
                        request.status = dbResult.result[0][i].status_name;
                        request.account_name = dbResult.result[0][i].account_name;
                        request.symbol = dbResult.result[0][i].symbol;
                        request.owner_public_key = dbResult.result[0][i].owner_public_key;
                        request.active_public_key = dbResult.result[0][i].active_public_key;
                        request.fee = parseFloat(dbResult.result[0][i].fee);
                        request.current_balance = parseFloat(dbResult.result[0][i].current_balance);
                        request.reason = dbResult.result[0][i].reason || '';
                        request.txn_reference = dbResult.result[0][i].txn_reference || '';
                        request.email = dbResult.result[0][i].email || '';

                        response.requests.push(request);
                    }

                    //response.result.code = enums.ResponseCodeEnum.SUCCESS;
                    //response.result.message = null;
                } else {
                    console.log(dbResult.error);
                    response.result.message = "Internal Error";
                    proceed = false;
                }
            }


            if (proceed) {
                dbResult = null;
                
                dbResult = await dbpool.query(countSQL, parameters);

                if (dbResult.success) {
                    response.total_records = dbResult.result[0][0].Total;

                    response.result.code = enums.ResponseCodeEnum.SUCCESS;
                    response.result.message = null;
                } else {
                    console.log(dbResult.error);
                    response.result.message = "Internal Error";
                    proceed = false;
                }
            }
            
        } catch (err) {
            response.withdrawals.length = 0;
            response.result.message = "Internal Error";
            console.log(err);
        }
        return response;
    },   // getEosWalletCreationHistory

    getEosWalletCreationRequests: async function(request_id, symbol, member_name, record_offset, record_counter) {
        'use strict';
    
        let commandText = '';
        let countSQL = '';
        let criteria = '';
        let parameters = [];
        let dbResult = null;
        let proceed = true;

        let response = {
            result: {
                code: enums.ResponseCodeEnum.INTERNAL_ERROR,
                message: 'Internal Error'
            },
            total_records: 0,
            requests: []
        }
        
        try {
            if (proceed) {
                commandText = "SELECT T1.*" +
                    ", IFNULL(CONCAT(xc_profiles.firstName, ' ', xc_profiles.lastName), '') AS profile_name" +
                    ", IFNULL((SELECT SUM(balance) FROM treos_profile_token_balance WHERE profile_id = T1.profile_id AND token = T1.symbol), 0) AS current_balance" +  // Using SUM until unique index is applied
                    ", xc_profiles.login AS email" +
                    ", treos_eos_account_create_status.name AS status_name";
                    

                countSQL = "SELECT COUNT(*) AS Total";

                criteria = " FROM treos_eos_account_create T1" +
                    " LEFT JOIN xc_profiles ON (T1.profile_id = xc_profiles.profile_id)" +
                    " LEFT JOIN treos_eos_account_create_status ON (T1.status_id = treos_eos_account_create_status.status_id)" +
                    " WHERE T1.status_id = 1"; // new

                if (symbol && symbol !== '*') {
                    criteria += " AND T1.symbol = ?";
                    parameters.push(symbol);
                }

                if (member_name !== undefined && member_name !== null && member_name.trim().length > 0) {
                    if (!isNaN(member_name.trim())) {
                        criteria += " AND (T1.profile_id = ?)";
                        parameters.push(member_name.trim());
                    } else {
                        criteria += " AND (CONCAT(xc_profiles.firstName, ' ', xc_profiles.lastName) LIKE CONCAT('%', ?, '%'))";
                        parameters.push(member_name.trim());
                    }
                }

                if (request_id && !isNaN(request_id)) {
                    criteria += " AND T1.id = ?";
                    parameters.push(request_id);
                }

                commandText += criteria;
                countSQL += criteria;

                commandText += " ORDER BY `date` DESC";
                commandText += " LIMIT ?, ?";

                parameters.push(record_offset);
                parameters.push(record_counter);

                dbResult = await dbpool.query(commandText, parameters);

                if (dbResult.success) {

                    for (let i = 0; i < dbResult.result[0].length; i++) {
                        let item = dbResult.result[0][i];

                        let request = {
                            request_id: null,
                            profile_id: null,
                            profile_name: null,
                            date_request: null,
                            date: null,
                            status_id: null,
                            status: null,
                            account_name: null,
                            symbol: null,
                            owner_public_key: null,
                            active_public_key: null,
                            fee: null,
                            current_balance: null,
                            email: null
                        }

                        request.request_id = dbResult.result[0][i].id;
                        request.profile_id = dbResult.result[0][i].profile_id;
                        request.profile_name = dbResult.result[0][i].profile_name;
                        request.date_request = dbResult.result[0][i].date_request;
                        request.date = dbResult.result[0][i].date;
                        request.status_id = dbResult.result[0][i].status_id;
                        request.status = dbResult.result[0][i].status_name;
                        request.account_name = dbResult.result[0][i].account_name;
                        request.symbol = dbResult.result[0][i].symbol;
                        request.owner_public_key = dbResult.result[0][i].owner_public_key;
                        request.active_public_key = dbResult.result[0][i].active_public_key;
                        request.fee = parseFloat(dbResult.result[0][i].fee);
                        request.current_balance = parseFloat(dbResult.result[0][i].current_balance);
                        request.email = dbResult.result[0][i].email;

                        response.requests.push(request);
                    }

                    response.result.code = enums.ResponseCodeEnum.SUCCESS;
                    response.result.message = null;
                } else {
                    console.log(dbResult.error);
                    response.result.message = "Internal Error";
                    proceed = false;
                }
            }


            if (proceed) {
                dbResult = null;
                
                dbResult = await dbpool.query(countSQL, parameters);

                if (dbResult.success) {
                    response.total_records = dbResult.result[0][0].Total;

                    response.result.code = enums.ResponseCodeEnum.SUCCESS;
                    response.result.message = null;
                } else {
                    console.log(dbResult.error);
                    response.result.message = "Internal Error";
                    proceed = false;
                }
            }
        } catch (err) {
            response.requests.length = 0;
            response.result.message = "Internal Error";
            console.log(err);
        }
        return response;
    },   // getEosWalletCreationRequests

    cancelEosAccountCreationRequest: async function(request_id, reason, email) {
        'use strict';
    
        let conn = null;
        let commandText = '';
        let parameters = [];
        let dbResult = null;
        let proceed = true;

        let profile_id = -1;
        let symbol = '';
        let fee = 0.0;

        let response = {
            result: {
                code: enums.ResponseCodeEnum.INTERNAL_ERROR,
                message: 'Internal Error'
            }
        }
        
        try {

            if (proceed) {
                if (request_id !== undefined && request_id !== null && !isNaN(parseInt(request_id))   ) {
                    // withdrawalID is considered to be valid
                } else {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = "Invalid Request ID";
                    proceed = false;
                }
            }

            if (proceed) {
                if (reason !== undefined && reason !== null) {
                    // reason is considered to be valid
                } else {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = "Invalid Reason.";
                    proceed = false;
                }
            }

            if (proceed) {
                if (reason.toString().length > 0) {
                    // reason is considered to be valid
                } else {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_REQUIRED;
                    response.result.message = "You must specify a reason for cancelling the withdrawal request.";
                    proceed = false;
                }
            }

            if (proceed) {
                if (email !== undefined && email !== null) {
                    // reason is considered to be valid
                } else {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = "Invalid Email.";
                    proceed = false;
                }
            }

            if (proceed) {
                if (email.toString().length > 0) {
                    // reason is considered to be valid
                } else {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_REQUIRED;
                    response.result.message = "You must specify an email address.";
                    proceed = false;
                }
            }

            if (proceed) {
                conn = await dbpool.getConnection();    // Create connection to be reused
                dbResult = await dbpool.query('START TRANSACTION', [], conn);
                if (!dbResult.success) {
                    console.log(dbResult.error);
                    response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                    response.result.message = "Database Error";
                    proceed = false;
                }
            }

            
            if (proceed) {
                commandText = '';
                parameters.length = 0;

                commandText = "SELECT *" +
                    " FROM treos_eos_account_create" +
                    " WHERE id = ?" +
                    " AND status_id = 1";  // New

                parameters.push(request_id);

                dbResult = await dbpool.query(commandText, parameters, conn);

                if (dbResult.success) {
                    if (dbResult.result[0].length > 0) {
                        profile_id = dbResult.result[0][0].profile_id;
                        symbol = dbResult.result[0][0].symbol;
                        fee = dbResult.result[0][0].fee;
                    } else {
                        response.result.code = enums.ResponseCodeEnum.TRANSACTION_NOT_FOUND;
                        response.result.message = "New Request Not Found";
                        proceed = false;
                    }
                } else {
                    console.log(dbResult.error);
                    response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                    response.result.message = "Database Error";
                    proceed = false;
                }
            }


            /* Update wallet balance */
            if (proceed) {
                let alterTokenResponse = await treosFunctions.alterTokenAmount(conn,
                    profile_id,
                    symbol,
                    'ADD_CREATE_EOS_ACCOUNT_CANCEL',
                    fee);

                //console.log(alterTokenResponse);

                if (alterTokenResponse.result.code !== enums.ResponseCodeEnum.SUCCESS) {
                    response.result = alterTokenResponse.result;
                    proceed = false;
                }
            }


            if (proceed) {
                commandText = '';
                parameters.length = 0;

                commandText = "UPDATE treos_eos_account_create";

                commandText += " SET status_id = 2";
                commandText += ", date = NOW()";
                commandText += ", reason = ?"; parameters.push(reason);

                commandText += " WHERE id = ?";             parameters.push(request_id);

                dbResult = await dbpool.query(commandText, parameters, conn);

                if (dbResult.success) {
                    if (dbResult.result[0].affectedRows === 0) {
                        response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                        response.result.message = "No rows affected";
                        proceed = false;
                    }
                } else {
                    console.log(dbResult.error);
                    response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                    response.result.message = "Database Error";
                    proceed = false;
                }
            }

        } catch (err) {
            proceed = false;
            response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
            response.result.message = "Internal Error";
            console.log(err);
        } finally {
            if (conn !== null) {
                if (proceed) {
                    dbResult = await dbpool.query('COMMIT', [], conn);
                    if (dbResult.success) {
                        response.result.code = enums.ResponseCodeEnum.SUCCESS;
                        response.result.message = null;
                    } else {
                        console.log(dbResult.error);
                        response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                        response.result.message = "Database Error";
                        proceed = false;
                    }
                } else {
                    dbResult = await dbpool.query('ROLLBACK', [], conn);
                    if (!dbResult.success) {
                        console.log(dbResult.error);
                    }
                }

                conn.release();
                conn = null;
            }
        }
        return response;
    },  // cancelEosAccountCreationRequest

    completeEosAccountCreationRequest: async function(request_id, email, txn_reference) {
        'use strict';
    
        let conn = null;
        let commandText = '';
        let parameters = [];
        let dbResult = null;
        let proceed = true;

        let profile_id = -1;
        let symbol = '';
        let fee = 0.0;

        let response = {
            result: {
                code: enums.ResponseCodeEnum.INTERNAL_ERROR,
                message: 'Internal Error'
            }
        }
        
        try {

            if (proceed) {
                if (request_id !== undefined && request_id !== null && !isNaN(parseInt(request_id))   ) {
                    // withdrawalID is considered to be valid
                } else {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = "Invalid Request ID";
                    proceed = false;
                }
            }

            if (proceed) {
                if (email !== undefined && email !== null) {
                    // reason is considered to be valid
                } else {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = "Invalid Email.";
                    proceed = false;
                }
            }

            if (proceed) {
                if (email.toString().length > 0) {
                    // reason is considered to be valid
                } else {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_REQUIRED;
                    response.result.message = "You must specify an email address.";
                    proceed = false;
                }
            }

            if (proceed) {
                conn = await dbpool.getConnection();    // Create connection to be reused
                dbResult = await dbpool.query('START TRANSACTION', [], conn);
                if (!dbResult.success) {
                    console.log(dbResult.error);
                    response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                    response.result.message = "Database Error";
                    proceed = false;
                }
            }

            
            if (proceed) {
                commandText = '';
                parameters.length = 0;

                commandText = "SELECT *" +
                    " FROM treos_eos_account_create" +
                    " WHERE id = ?" +
                    " AND status_id = 1";  // New

                parameters.push(request_id);

                dbResult = await dbpool.query(commandText, parameters, conn);

                if (dbResult.success) {
                    if (dbResult.result[0].length > 0) {
                        profile_id = dbResult.result[0][0].profile_id;
                        symbol = dbResult.result[0][0].symbol;
                        fee = dbResult.result[0][0].fee;
                    } else {
                        response.result.code = enums.ResponseCodeEnum.TRANSACTION_NOT_FOUND;
                        response.result.message = "New Request Not Found";
                        proceed = false;
                    }
                } else {
                    console.log(dbResult.error);
                    response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                    response.result.message = "Database Error";
                    proceed = false;
                }
            }


            if (proceed) {
                commandText = '';
                parameters.length = 0;

                commandText = "INSERT INTO treos_corporate_transactions";

                commandText += " SET date=NOW()";    // Complete
                commandText += ", year = YEAR(CURRENT_DATE)";
                commandText += ", month = MONTH(CURRENT_DATE)";
                commandText += ", profile_id = ?";              parameters.push(profile_id);
                commandText += ", transaction_type_id = 3";     // Account Creation
                commandText += ", symbol = ?";                  parameters.push(symbol);
                commandText += ", amount = ?";                  parameters.push(fee);
                commandText += ", parent_id = ?";               parameters.push(request_id);

                dbResult = await dbpool.query(commandText, parameters, conn);

                if (dbResult.success) {
                    if (dbResult.result[0].affectedRows === 0) {
                        response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                        response.result.message = "No rows affected";
                        proceed = false;
                    }
                } else {
                    console.log(dbResult.error);
                    response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                    response.result.message = "Database Error";
                    proceed = false;
                }
            }
            


            if (proceed) {
                commandText = '';
                parameters.length = 0;

                commandText = "UPDATE treos_eos_account_create";

                commandText += " SET status_id = 3";    // Created
                commandText += ", date = NOW()";
                commandText += ", email = ?";               parameters.push(email);
                commandText += ", txn_reference = ?";               parameters.push(txn_reference);

                commandText += " WHERE id = ?";             parameters.push(request_id);

                dbResult = await dbpool.query(commandText, parameters, conn);

                if (dbResult.success) {
                    if (dbResult.result[0].affectedRows === 0) {
                        response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                        response.result.message = "No rows affected";
                        proceed = false;
                    }
                } else {
                    console.log(dbResult.error);
                    response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                    response.result.message = "Database Error";
                    proceed = false;
                }
            }

            if (proceed) {
                let addBountyRewardResponse = await treosFunctions.addBountyReward(conn, profile_id, 'EOS Account Creation', 'bounty_eos_account_create', 'bounty_max_eos');

                if (addBountyRewardResponse.result.code !== enums.ResponseCodeEnum.SUCCESS) {
                    response.result = addBountyRewardResponse.result;
                    proceed = false;
                }
            }

        } catch (err) {
            proceed = false;
            response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
            response.result.message = "Internal Error";
            console.log(err);
        } finally {
            if (conn !== null) {
                if (proceed) {
                    dbResult = await dbpool.query('COMMIT', [], conn);
                    if (dbResult.success) {
                        response.result.code = enums.ResponseCodeEnum.SUCCESS;
                        response.result.message = null;
                    } else {
                        console.log(dbResult.error);
                        response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                        response.result.message = "Database Error";
                        proceed = false;
                    }
                } else {
                    dbResult = await dbpool.query('ROLLBACK', [], conn);
                    if (!dbResult.success) {
                        console.log(dbResult.error);
                    }
                }

                conn.release();
                conn = null;
            }
        }
        return response;
    },  // completeEosAccountCreationRequest

    getWithdrawalRequests: async function(timeframe, currency, member_name, record_offset, record_counter) {
        'use strict';
    
        let commandText = '';
        let countSQL = '';
        let criteria = '';
        let parameters = [];
        let dbResult = null;
        let proceed = true;

        let response = {
            result: {
                code: enums.ResponseCodeEnum.INTERNAL_ERROR,
                message: 'Internal Error'
            },
            total_records: 0,
            requests: []
        }
        
        try {
            let iTimeframe = parseInt(timeframe);

            if (isNaN(iTimeframe)) {
                iTimeframe = 0;
            }

            if (proceed) {
                commandText = "SELECT T1.*" +
                    ", IFNULL(CONCAT(xc_profiles.firstName, ' ', xc_profiles.lastName), '') AS profile_name" +
                    ", HOUR(TIMEDIFF(NOW(), `date`)) AS pending_hours" +
                    ", IFNULL((SELECT SUM(balance) FROM treos_profile_token_balance WHERE profile_id = T1.member_id AND token = T1.currency), 0) AS current_balance" +  // Using SUM until unique index is applied
                    ", IFNULL(fee_withdrawal, 0) AS fee_withdrawal";
                    

                countSQL = "SELECT COUNT(*) AS Total";

                criteria = " FROM treos_withdrawal T1" +
                    " LEFT JOIN xc_profiles ON (T1.member_id = xc_profiles.profile_id)" +
                    " LEFT JOIN treos_fiat_rates ON (T1.currency = treos_fiat_rates.currency)" +
                    " WHERE T1.currency <> 'GCR' AND T1.status IN ('PENDING', 'IN_PROGRESS')";

                if (iTimeframe > 0) {
                    criteria += " AND date <= DATE_ADD(NOW(), INTERVAL (-1 * ?) HOUR)";
                    parameters.push(iTimeframe);
                }

                if (currency !== '*') {
                    criteria += " AND T1.currency = ?";
                    parameters.push(currency);
                }

                // if (member_name !== undefined && member_name !== null && member_name.trim().length > 0) {
                //     commandText += " AND (T1.member_id = ?";
                //     commandText += " OR CONCAT(xc_profiles.firstName, ' ', xc_profiles.lastName) LIKE CONCAT('%', ?, '%'))";
                //     parameters.push(member_name.trim());
                //     parameters.push(member_name.trim());
                // }

                if (member_name !== undefined && member_name !== null && member_name.trim().length > 0) {
                    if (!isNaN(member_name.trim())) {
                        criteria += " AND (T1.member_id = ?)";
                        parameters.push(member_name.trim());
                    } else {
                        criteria += " AND (CONCAT(xc_profiles.firstName, ' ', xc_profiles.lastName) LIKE CONCAT('%', ?, '%'))";
                        parameters.push(member_name.trim());
                    }
                }

                commandText += criteria;
                countSQL += criteria;

                commandText += " ORDER BY `date` DESC";
                commandText += " LIMIT ?, ?";

                parameters.push(record_offset);
                parameters.push(record_counter);

                let addressTypeCount = 0;

                // if (isBilling !== undefined && isBilling !== null && isBilling === true) {
                //     commandText += " AND (xc_profile_addresses.is_billing = ?";
                //     let boolValue = isBilling == true ? 1 : 0;
                //     parameters.push(boolValue);
                //     addressTypeCount++;
                // }

                dbResult = await dbpool.query(commandText, parameters);

                if (dbResult.success) {

                    for (let i = 0; i < dbResult.result[0].length; i++) {
                        let item = dbResult.result[0][i];

                        let request = {
                            withdrawal_id: null,
                            profile_id: null,
                            profile_name: null,
                            date: null,
                            pending_hours: null,
                            address_to: null,
                            currency: null,
                            amount: null,
                            fee_withdrawal: null,
                            current_balance: null,
                            fee: null,
                            status: null,
                            txn_reference: null,
                            memo: null
                        }

                        request.withdrawal_id = dbResult.result[0][i].id;
                        request.profile_id = dbResult.result[0][i].member_id;
                        request.profile_name = dbResult.result[0][i].profile_name;
                        request.date = dbResult.result[0][i].date;
                        request.pending_hours = dbResult.result[0][i].pending_hours;
                        request.address_to = dbResult.result[0][i].address_to;
                        request.currency = dbResult.result[0][i].currency;
                        request.amount = parseFloat(dbResult.result[0][i].amount);
                        request.fee_withdrawal = parseFloat(dbResult.result[0][i].fee_withdrawal);
                        request.current_balance = parseFloat(dbResult.result[0][i].current_balance);
                        request.fee = parseFloat(dbResult.result[0][i].fee);
                        request.status = dbResult.result[0][i].status;
                        request.txn_reference = dbResult.result[0][i].txn_reference;
                        request.memo = dbResult.result[0][i].memo || '';

                        response.requests.push(request);
                    }

                    response.result.code = enums.ResponseCodeEnum.SUCCESS;
                    response.result.message = null;
                } else {
                    console.log(dbResult.error);
                    response.result.message = "Internal Error";
                    proceed = false;
                }
            }


            if (proceed) {
                dbResult = null;
                
                dbResult = await dbpool.query(countSQL, parameters);

                if (dbResult.success) {
                    response.total_records = dbResult.result[0][0].Total;

                    response.result.code = enums.ResponseCodeEnum.SUCCESS;
                    response.result.message = null;
                } else {
                    console.log(dbResult.error);
                    response.result.message = "Internal Error";
                    proceed = false;
                }
            }
        } catch (err) {
            response.requests.length = 0;
            response.result.message = "Internal Error";
            console.log(err);
        }
        return response;
    },   // getWithdrawalRequests

    getWithdrawalHistory: async function(currency, member_name, date_from, date_to, forExport, record_offset, record_counter) {
        'use strict';
    
        let commandText = '';
        let countSQL = '';
        let criteria = '';
        let parameters = [];
        let dbResult = null;
        let proceed = true;

        let response = {
            result: {
                code: enums.ResponseCodeEnum.INTERNAL_ERROR,
                message: 'Internal Error'
            },
            total_records: 0,
            withdrawals: []
        }
        
        try {
            if (proceed) {
                commandText = "SELECT T1.*" +
                    ", IFNULL(CONCAT(xc_profiles.firstName, ' ', xc_profiles.lastName), '') AS profile_name" +
                    ", IFNULL((SELECT SUM(balance) FROM treos_profile_token_balance WHERE profile_id = T1.member_id AND token = T1.currency), 0) AS current_balance" +  // Using SUM until unique index is applied
                    ", T1.reason"

                countSQL = "SELECT COUNT(*) AS Total";


                criteria = " FROM treos_withdrawal T1" +
                    " LEFT JOIN xc_profiles ON (T1.member_id = xc_profiles.profile_id)" +
                    " LEFT JOIN treos_fiat_rates ON (T1.currency = treos_fiat_rates.currency)";

                criteria += " WHERE T1.status NOT IN ('PENDING', 'IN_PROGRESS')";
                
                criteria += " AND (date BETWEEN ? AND ?)"
                parameters.push(date_from + ' 00:00:00');
                parameters.push(date_to + ' 23:59:59');

                if (currency !== '*') {
                    criteria += " AND T1.currency = ?";
                    parameters.push(currency);
                }

                if (member_name !== undefined && member_name !== null && member_name.trim().length > 0) {
                    if (!isNaN(member_name.trim())) {
                        criteria += " AND (T1.member_id = ?)";
                        parameters.push(member_name.trim());
                    } else {
                        criteria += " AND (CONCAT(xc_profiles.firstName, ' ', xc_profiles.lastName) LIKE CONCAT('%', ?, '%'))";
                        parameters.push(member_name.trim());
                    }
                }

                commandText += criteria;
                countSQL += criteria;

                commandText += " ORDER BY `date` DESC";

                if (!forExport) {
                    commandText += " LIMIT ?, ?";
                }

                parameters.push(record_offset);
                parameters.push(record_counter);

                dbResult = await dbpool.query(commandText, parameters);

                if (dbResult.success) {
                    for (let i = 0; i < dbResult.result[0].length; i++) {
                        let item = dbResult.result[0][i];

                        let withdrawal = {
                            date: null,
                            withdrawal_id: null,
                            status: null,
                            profile_id: null,
                            profile_name: null,
                            currency: null,
                            amount: null,
                            fee: null,
                            fee_spent: null,
                            address_to: null,
                            memo: null,
                            txn_reference: null,
                            reason: null
                        }

                        withdrawal.withdrawal_id = dbResult.result[0][i].id;
                        withdrawal.profile_id = dbResult.result[0][i].member_id;
                        withdrawal.profile_name = dbResult.result[0][i].profile_name;
                        withdrawal.date = dbResult.result[0][i].date;
                        withdrawal.address_to = dbResult.result[0][i].address_to;
                        withdrawal.currency = dbResult.result[0][i].currency;
                        withdrawal.amount = parseFloat(dbResult.result[0][i].amount);
                        withdrawal.status = dbResult.result[0][i].status;
                        withdrawal.fee = parseFloat(dbResult.result[0][i].fee);
                        withdrawal.fee_spent = parseFloat(dbResult.result[0][i].fee_spent);
                        withdrawal.txn_reference = dbResult.result[0][i].txn_reference;
                        withdrawal.memo = dbResult.result[0][i].memo || '';
                        withdrawal.reason = dbResult.result[0][i].reason || '';

                        response.withdrawals.push(withdrawal);
                    }

                    //response.result.code = enums.ResponseCodeEnum.SUCCESS;
                    //response.result.message = null;
                } else {
                    console.log(dbResult.error);
                    response.result.message = "Internal Error";
                    proceed = false;
                }
            }


            if (proceed) {
                dbResult = null;
                
                dbResult = await dbpool.query(countSQL, parameters);

                if (dbResult.success) {
                    response.total_records = dbResult.result[0][0].Total;

                    response.result.code = enums.ResponseCodeEnum.SUCCESS;
                    response.result.message = null;
                } else {
                    console.log(dbResult.error);
                    response.result.message = "Internal Error";
                    proceed = false;
                }
            }
            
        } catch (err) {
            response.withdrawals.length = 0;
            response.result.message = "Internal Error";
            console.log(err);
        }
        return response;
    },   // getWithdrawalHistory

    getWithdrawFeesHistory: async function(currency, date_from, date_to, forExport, record_offset, record_counter) {
        'use strict';
    
        let commandText = '';
        let countSQL = '';
        let criteria = '';
        let parameters = [];
        let dbResult = null;
        let proceed = true;

        let response = {
            result: {
                code: enums.ResponseCodeEnum.INTERNAL_ERROR,
                message: 'Internal Error'
            },
            total_records: 0,
            months: []
        }
        
        try {
            if (proceed) {
                commandText = "SELECT YEAR(date) AS Year" +
                    ", MONTH(date) AS Month" +
                    " , ANY_VALUE(IF(YEAR(`date`) = YEAR(CURRENT_DATE) AND MONTH(`date`) = MONTH(CURRENT_DATE), 1, 0)) AS CurrentMonth";

                countSQL = "SELECT COUNT(*) AS TotalMonths" +
                    " FROM ( SELECT COUNT(*) AS Total"


                criteria = " FROM treos_withdrawal";

                criteria += " WHERE status='DONE'";

                criteria += " AND (date BETWEEN ? AND ?)"
                parameters.push(date_from + ' 00:00:00');
                parameters.push(date_to + ' 23:59:59');

                if (currency !== '*') {
                    criteria += " AND currency = ?";
                    parameters.push(currency);
                }

                criteria += " GROUP BY YEAR(date), MONTH(date)";

                commandText += criteria;
                countSQL += criteria;

                commandText += " ORDER BY `Year` DESC, `Month` DESC";

                if (!forExport) {
                    commandText += " LIMIT ?, ?";
                }

                countSQL += ") T1";

                parameters.push(record_offset);
                parameters.push(record_counter);

                dbResult = await dbpool.query(commandText, parameters);

                if (dbResult.success) {
                    for (let i = 0; i < dbResult.result[0].length; i++) {
                        let item = dbResult.result[0][i];

                        let feesmonth = {
                            year: null,
                            month: null,
                            current_month: true,
                            currencies: []
                        }

                        feesmonth.year = dbResult.result[0][i].Year;
                        feesmonth.month = dbResult.result[0][i].Month;

                        if (dbResult.result[0][i].CurrentMonth === 0) {
                            feesmonth.current_month = false;
                        } else {
                            feesmonth.current_month = true;
                        }

                        feesmonth.currencies = await this.populateWithdrawFeeCurrencies(feesmonth.year, feesmonth.month, currency);

                        response.months.push(feesmonth);
                    }

                    //response.result.code = enums.ResponseCodeEnum.SUCCESS;
                    //response.result.message = null;
                } else {
                    console.log(dbResult.error);
                    response.result.message = "Internal Error";
                    proceed = false;
                }
            }


            if (proceed) {
                dbResult = null;
                
                dbResult = await dbpool.query(countSQL, parameters);

                if (dbResult.success) {
                    response.total_records = dbResult.result[0][0].TotalMonths;

                    response.result.code = enums.ResponseCodeEnum.SUCCESS;
                    response.result.message = null;
                } else {
                    console.log(dbResult.error);
                    response.result.message = "Internal Error";
                    proceed = false;
                }
            }
            
        } catch (err) {
            response.months.length = 0;
            response.result.message = "Internal Error";
            console.log(err);
        }
        return response;
    },   // getWithdrawFeesHistory


    getFeesWithdrawalHistory: async function(currency, date_from, date_to, forExport, record_offset, record_counter) {
        'use strict';
    
        let commandText = '';
        let countSQL = '';
        let criteria = '';
        let parameters = [];
        let dbResult = null;
        let proceed = true;

        let response = {
            result: {
                code: enums.ResponseCodeEnum.INTERNAL_ERROR,
                message: 'Internal Error'
            },
            total_records: 0,
            withdrawals: []
        }
        
        try {
            if (proceed) {
                commandText = "SELECT *";

                countSQL = "SELECT COUNT(*) AS Total";

                criteria = " FROM treos_withdrawal_fees";

                criteria += " WHERE (date BETWEEN ? AND ?)"
                parameters.push(date_from);
                parameters.push(date_to);

                if (currency !== '*') {
                    criteria += " AND currency = ?";
                    parameters.push(currency);
                }

                commandText += criteria;
                countSQL += criteria;

                commandText += " ORDER BY `date` DESC";

                if (!forExport) {
                    commandText += " LIMIT ?, ?";
                }

                parameters.push(record_offset);
                parameters.push(record_counter);

                dbResult = await dbpool.query(commandText, parameters);

                if (dbResult.success) {
                    for (let i = 0; i < dbResult.result[0].length; i++) {
                        
                        let item = dbResult.result[0][i];

                        let withdrawal = {
                            withdrawal_id: null,
                            date: null,
                            year: null,
                            month: null,
                            currency: null,
                            amount: null,
                            fee_spent: null,
                            address_to: null,
                            txn_reference: null
                        }

                        withdrawal.withdrawal_id = dbResult.result[0][i].id;
                        withdrawal.date = dbResult.result[0][i].date;
                        withdrawal.year = dbResult.result[0][i].year;
                        withdrawal.month = dbResult.result[0][i].month;
                        withdrawal.currency = dbResult.result[0][i].currency;
                        withdrawal.amount = parseFloat(dbResult.result[0][i].amount);
                        withdrawal.fee_spent = parseFloat(dbResult.result[0][i].fee_spent);
                        withdrawal.address_to = dbResult.result[0][i].address_to;
                        withdrawal.txn_reference = dbResult.result[0][i].txn_reference;

                        response.withdrawals.push(withdrawal);
                    }

                    //response.result.code = enums.ResponseCodeEnum.SUCCESS;
                    //response.result.message = null;
                } else {
                    console.log(dbResult.error);
                    response.result.message = "Internal Error";
                    proceed = false;
                }
            }


            if (proceed) {
                dbResult = null;
                
                dbResult = await dbpool.query(countSQL, parameters);

                if (dbResult.success) {
                    response.total_records = dbResult.result[0][0].TotalMonths;

                    response.result.code = enums.ResponseCodeEnum.SUCCESS;
                    response.result.message = null;
                } else {
                    console.log(dbResult.error);
                    response.result.message = "Internal Error";
                    proceed = false;
                }
            }
            
        } catch (err) {
            response.months.length = 0;
            response.result.message = "Internal Error";
            console.log(err);
        }
        return response;
    },   // getFeesWithdrawalHistory

    getCryptoWithdrawalHistory_Report: async function(symbol, transaction_type_id, date_from, date_to, record_offset, record_counter, forExport) {
        'use strict';
    
        let commandText = '';
        let countSQL = '';
        let criteria = '';
        let parameters = [];
        let dbResult = null;
        let proceed = true;

        let response = {
            result: {
                code: enums.ResponseCodeEnum.INTERNAL_ERROR,
                message: 'Internal Error'
            },
            total_records: 0,
            withdrawals: []
        }
        
        try {
            if (proceed) {
                commandText = "SELECT treos_corporate_withdrawals.*" +
                    ", treos_transaction_type.type AS transaction_type";

                countSQL = "SELECT COUNT(*) AS Total";

                criteria = " FROM treos_corporate_withdrawals" +
                    " LEFT JOIN treos_transaction_type ON (treos_corporate_withdrawals.transaction_type_id = treos_transaction_type.type_id)";

                criteria += " WHERE (date BETWEEN ? AND ?)"
                parameters.push(date_from);
                parameters.push(date_to);

                if (symbol !== '*') {
                    criteria += " AND symbol = ?";
                    parameters.push(symbol);
                }

                if (transaction_type_id !== '*') {
                    criteria += " AND transaction_type_id = ?";
                    parameters.push(transaction_type_id);
                }

                commandText += criteria;
                countSQL += criteria;

                commandText += " ORDER BY `date` DESC";

                if (!forExport) {
                    commandText += " LIMIT ?, ?";
                }

                parameters.push(record_offset);
                parameters.push(record_counter);

                dbResult = await dbpool.query(commandText, parameters);

                if (dbResult.success) {
                    for (let i = 0; i < dbResult.result[0].length; i++) {
                        
                        let item = dbResult.result[0][i];

                        let withdrawal = {
                            withdrawal_id: null,
                            date: null,
                            year: null,
                            month: null,
                            transaction_type_id: null,
                            transaction_type: null,
                            symbol: null,
                            amount: null,
                            fee_spent: null,
                            withdraw_address: null,
                            withdraw_memo: null,
                            txn_reference: null
                        }

                        withdrawal.withdrawal_id = dbResult.result[0][i].id;
                        withdrawal.date = dbResult.result[0][i].date;
                        withdrawal.year = dbResult.result[0][i].year;
                        withdrawal.month = dbResult.result[0][i].month;
                        withdrawal.transaction_type_id = dbResult.result[0][i].transaction_type_id;
                        withdrawal.transaction_type = dbResult.result[0][i].transaction_type;
                        withdrawal.symbol = dbResult.result[0][i].symbol;
                        withdrawal.amount = parseFloat(dbResult.result[0][i].amount);
                        withdrawal.fee_spent = parseFloat(dbResult.result[0][i].fee_spent);
                        withdrawal.withdraw_address = dbResult.result[0][i].withdraw_address;
                        withdrawal.withdraw_memo = dbResult.result[0][i].withdraw_memo;
                        withdrawal.txn_reference = dbResult.result[0][i].txn_reference;

                        response.withdrawals.push(withdrawal);
                    }

                    //response.result.code = enums.ResponseCodeEnum.SUCCESS;
                    //response.result.message = null;
                } else {
                    console.log(dbResult.error);
                    response.result.message = "Internal Error";
                    proceed = false;
                }
            }


            if (proceed) {
                dbResult = null;
                
                dbResult = await dbpool.query(countSQL, parameters);

                if (dbResult.success) {
                    response.total_records = dbResult.result[0][0].TotalMonths;

                    response.result.code = enums.ResponseCodeEnum.SUCCESS;
                    response.result.message = null;
                } else {
                    console.log(dbResult.error);
                    response.result.message = "Internal Error";
                    proceed = false;
                }
            }
            
        } catch (err) {
            response.months.length = 0;
            response.result.message = "Internal Error";
            console.log(err);
        }
        return response;
    },   // getCryptoWithdrawalHistory_Report

    completeWithdrawalRequest: async function(withdrawalID, transactionID, fee, fee_spent) {
        'use strict';
    
        let conn = null;
        let commandText = '';
        let parameters = [];
        let dbResult = null;
        let proceed = true;

        let profile_id = -1;
        let finalFee = 0.0;
        let finalFee_Spent = 0.0;
        let amount = 0.0;
        let currencyCode = '';

        let response = {
            result: {
                code: enums.ResponseCodeEnum.INTERNAL_ERROR,
                message: 'Internal Error'
            }
        }
        
        try {

            if (proceed) {
                conn = await dbpool.getConnection();    // Create connection to be reused
                dbResult = await dbpool.query('START TRANSACTION', [], conn);
                if (!dbResult.success) {
                    console.log(dbResult.error);
                    response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                    response.result.message = "Database Error";
                    proceed = false;
                }
            }

            if (proceed) {
                if (fee !== undefined && fee !== null && !isNaN(parseFloat(fee))) {
                    finalFee = parseFloat(fee);
                } else {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = "Invalid Fee";
                    proceed = false;
                }
            }

            if (proceed) {
                if (fee_spent !== undefined && fee_spent !== null && !isNaN(parseFloat(fee_spent))) {
                    finalFee_Spent = parseFloat(fee_spent);
                } else {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = "Invalid Fee Spent";
                    proceed = false;
                }
            }

            if (proceed) {
                if (transactionID !== undefined && transactionID !== null && transactionID.trim().length > 0) {
                    // transactionID is considered to be valid
                } else {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = "Invalid Transaction ID";
                    proceed = false;
                }
            }

            if (proceed) {
                commandText = '';
                parameters.length = 0;

                commandText = "SELECT *" +
                    " FROM treos_withdrawal" +
                    " WHERE id = ?" +
                    " AND status = 'IN_PROGRESS'";

                parameters.push(withdrawalID);

                dbResult = await dbpool.query(commandText, parameters, conn);

                if (dbResult.success) {
                    if (dbResult.result[0].length > 0) {
                        profile_id = dbResult.result[0][0].member_id;
                        amount = parseFloat(dbResult.result[0][0].amount);
                        currencyCode = dbResult.result[0][0].currency;
                    } else {
                        response.result.code = enums.ResponseCodeEnum.TRANSACTION_NOT_FOUND;
                        response.result.message = "Pending Withdrawal Not Found";
                        proceed = false;
                    }
                } else {
                    console.log(dbResult.error);
                    response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                    response.result.message = "Database Error";
                    proceed = false;
                }
            }


            if (proceed) {
                commandText = '';
                parameters.length = 0;

                commandText = "INSERT INTO treos_profile_token_updates" +
                    " SET profile_id = ?" +
                    ", date = NOW()" +
                    ", token = ?" +
                    ", action = 'DEDUCT_'" +
                    ", amount = ?" +
                    ", amount_before = (SELECT balance FROM treos_profile_token_balance WHERE profile_id = ? AND token = ?)" +
                    ", parent_id = 0";

                parameters.push(profile_id);
                parameters.push(currencyCode);
                parameters.push(amount);  // Make sure is +ve number
                parameters.push(profile_id);
                parameters.push(currencyCode);

                let dbResponse = await dbpool.query(commandText, parameters, conn);

                if (!dbResponse.success) {
                    response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                    response.result.message = 'Database Error';
                    proceed = false;
                }
            }


            if (proceed) {
                commandText = '';
                parameters.length = 0;

                commandText = "UPDATE treos_withdrawal";

                commandText += " SET status = 'DONE'";
                commandText += ", date = NOW()";
                commandText += ", txn_reference = ?";       parameters.push(transactionID.trim());
                commandText += ", fee = ?";                 parameters.push(finalFee.toFixed(8));
                commandText += ", fee_spent = ?";           parameters.push(finalFee_Spent.toFixed(8));

                commandText += " WHERE id = ?";             parameters.push(withdrawalID);

                dbResult = await dbpool.query(commandText, parameters, conn);

                if (dbResult.success) {
                    if (dbResult.result[0].affectedRows === 0) {
                        response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                        response.result.message = "No rows affected";
                        proceed = false;
                    }
                } else {
                    console.log(dbResult.error);
                    response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                    response.result.message = "Database Error";
                    proceed = false;
                }
            }

        } catch (err) {
            proceed = false;
            response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
            response.result.message = "Internal Error";
            console.log(err);
        } finally {
            if (conn !== null) {
                if (proceed) {
                    dbResult = await dbpool.query('COMMIT', [], conn);
                    if (dbResult.success) {
                        response.result.code = enums.ResponseCodeEnum.SUCCESS;
                        response.result.message = null;
                    } else {
                        console.log(dbResult.error);
                        response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                        response.result.message = "Database Error";
                        proceed = false;
                    }
                } else {
                    dbResult = await dbpool.query('ROLLBACK', [], conn);
                    if (!dbResult.success) {
                        console.log(dbResult.error);
                    }
                }

                conn.release();
                conn = null;
            }
        }
        return response;
    },  // completeWithdrawalRequest


    withdrawalEdit_Update: async function(withdrawalID, transactionID, fee, fee_spent) {
        'use strict';
    
        let conn = null;
        let commandText = '';
        let parameters = [];
        let dbResult = null;
        let proceed = true;

        let finalFee = 0.0;
        let finalFee_Spent = 0.0;

        let response = {
            result: {
                code: enums.ResponseCodeEnum.INTERNAL_ERROR,
                message: 'Internal Error'
            }
        }
        
        try {
            if (proceed) {
                if (transactionID !== undefined && transactionID !== null && transactionID.trim().length > 0) {
                    // transactionID is considered to be valid
                } else {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = "Invalid Transaction ID";
                    proceed = false;
                }
            }
            
            if (proceed) {
                if (fee !== undefined && fee !== null && !isNaN(parseFloat(fee))) {
                    finalFee = parseFloat(fee);
                } else {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = "Invalid Fee";
                    proceed = false;
                }
            }

            if (proceed) {
                if (fee_spent !== undefined && fee_spent !== null && !isNaN(parseFloat(fee_spent))) {
                    finalFee_Spent = parseFloat(fee_spent);
                } else {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = "Invalid Fee Spent";
                    proceed = false;
                }
            }

            
            if (proceed) {
                commandText = '';
                parameters.length = 0;

                commandText = "UPDATE treos_withdrawal";

                commandText += " SET txn_reference = ?";    parameters.push(transactionID.trim());
                commandText += ", fee = ?";                 parameters.push(finalFee.toFixed(8));
                commandText += ", fee_spent = ?";           parameters.push(finalFee_Spent.toFixed(8));

                commandText += " WHERE id = ?";             parameters.push(withdrawalID);

                dbResult = await dbpool.query(commandText, parameters, conn);

                if (dbResult.success) {
                    if (dbResult.result[0].affectedRows === 0) {
                        response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                        response.result.message = "No rows affected";
                        console.log(parameters);
                        proceed = false;
                    } else {
                        response.result.code = enums.ResponseCodeEnum.SUCCESS;
                        response.result.message = null;
                    }
                } else {
                    console.log(dbResult.error);
                    response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                    response.result.message = "Database Error";
                    proceed = false;
                }
            }
        } catch (err) {
            proceed = false;
            response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
            response.result.message = "Internal Error";
            console.log(err);
        } finally {
            if (conn !== null) {
                conn.release();
                conn = null;
            }
        }
        return response;
    },  // withdrawalEdit_Update


    cancelWithdrawalRequest: async function(withdrawalID, reason) {
        'use strict';
    
        let conn = null;
        let commandText = '';
        let parameters = [];
        let dbResult = null;
        let proceed = true;

        let profile_id = -1;
        let finalFee = 0.0;
        let finalFee_Spent = 0.0;
        let amount = 0.0;
        let currencyCode = '';

        let response = {
            result: {
                code: enums.ResponseCodeEnum.INTERNAL_ERROR,
                message: 'Internal Error'
            }
        }
        
        try {

            if (proceed) {
                if (withdrawalID !== undefined && withdrawalID !== null && !isNaN(parseInt(withdrawalID))   ) {
                    // withdrawalID is considered to be valid
                } else {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = "Invalid Withdrawal ID";
                    proceed = false;
                }
            }

            if (proceed) {
                if (reason !== undefined && reason !== null) {
                    // reason is considered to be valid
                } else {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = "Invalid Reason.";
                    proceed = false;
                }
            }

            if (proceed) {
                if (reason.toString().length > 0) {
                    // reason is considered to be valid
                } else {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = "You must specify a reason for cancelling the withdrawal request.";
                    proceed = false;
                }
            }

            if (proceed) {
                conn = await dbpool.getConnection();    // Create connection to be reused
                dbResult = await dbpool.query('START TRANSACTION', [], conn);
                if (!dbResult.success) {
                    console.log(dbResult.error);
                    response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                    response.result.message = "Database Error";
                    proceed = false;
                }
            }

            
            if (proceed) {
                commandText = '';
                parameters.length = 0;

                commandText = "SELECT *" +
                    " FROM treos_withdrawal" +
                    " WHERE id = ?" +
                    " AND status = 'IN_PROGRESS'";

                parameters.push(withdrawalID);

                dbResult = await dbpool.query(commandText, parameters, conn);

                if (dbResult.success) {
                    if (dbResult.result[0].length > 0) {
                        profile_id = dbResult.result[0][0].member_id;
                        amount = parseFloat(dbResult.result[0][0].amount);
                        currencyCode = dbResult.result[0][0].currency;
                    } else {
                        response.result.code = enums.ResponseCodeEnum.TRANSACTION_NOT_FOUND;
                        response.result.message = "Pending Withdrawal Not Found";
                        proceed = false;
                    }
                } else {
                    console.log(dbResult.error);
                    response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                    response.result.message = "Database Error";
                    proceed = false;
                }
            }

            /* Update wallet balance */
            // if (proceed) {
            //     let alterTokenResponse = await treosFunctions.alterTokenAmount(conn,
            //         profile_id,
            //         currencyCode,
            //         'ADD_WITHDRAWAL_CANCEL',
            //         amount);

            //     //console.log(alterTokenResponse);

            //     if (alterTokenResponse.result.code !== enums.ResponseCodeEnum.SUCCESS) {
            //         response.result = alterTokenResponse.result;
            //         proceed = false;
            //     }
            // }


            /* Need to replace this with the above Update Wallet Balance section so as to add the right action */
            if (proceed) {
                commandText = '';
                parameters.length = 0;

                commandText = "UPDATE treos_profile_token_balance";

                commandText += " SET balance = balance + (SELECT amount FROM treos_withdrawal WHERE id = ?)";    parameters.push(withdrawalID);

                commandText += " WHERE profile_id = ?";         parameters.push(profile_id);
                commandText += " AND token = ?";                parameters.push(currencyCode);

                dbResult = await dbpool.query(commandText, parameters, conn);

                if (dbResult.success) {
                    if (dbResult.result[0].affectedRows === 0) {
                        response.result.code = enums.ResponseCodeEnum.INSUFFICIENT_BALANCE;
                        response.result.message = "Error - Balance Not Updated";
                        proceed = false;
                    }
                } else {
                    console.log(dbResult.error);
                    response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                    response.result.message = "Database Error";
                    proceed = false;
                }
            }


            if (proceed) {
                commandText = '';
                parameters.length = 0;

                commandText = "UPDATE treos_withdrawal";

                commandText += " SET status = 'CANCELLED'";
                commandText += ", date = NOW()";
                commandText += ", txn_reference = ''";
                commandText += ", fee = 0";
                commandText += ", fee_spent = 0";
                commandText += ", reason = ?"; parameters.push(reason);

                commandText += " WHERE id = ?";             parameters.push(withdrawalID);

                dbResult = await dbpool.query(commandText, parameters, conn);

                if (dbResult.success) {
                    if (dbResult.result[0].affectedRows === 0) {
                        response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                        response.result.message = "No rows affected";
                        proceed = false;
                    }
                } else {
                    console.log(dbResult.error);
                    response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                    response.result.message = "Database Error";
                    proceed = false;
                }
            }

        } catch (err) {
            proceed = false;
            response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
            response.result.message = "Internal Error";
            console.log(err);
        } finally {
            if (conn !== null) {
                if (proceed) {
                    dbResult = await dbpool.query('COMMIT', [], conn);
                    if (dbResult.success) {
                        response.result.code = enums.ResponseCodeEnum.SUCCESS;
                        response.result.message = null;
                    } else {
                        console.log(dbResult.error);
                        response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                        response.result.message = "Database Error";
                        proceed = false;
                    }
                } else {
                    dbResult = await dbpool.query('ROLLBACK', [], conn);
                    if (!dbResult.success) {
                        console.log(dbResult.error);
                    }
                }

                conn.release();
                conn = null;
            }
        }
        return response;
    },  // cancelWithdrawalRequest


    reserveWithdrawalRequest: async function(withdrawalID, overrideInProgress) {
        'use strict';
    
        let conn = null;
        let commandText = '';
        let parameters = [];
        let dbResult = null;
        let proceed = true;

        let profile_id = -1;
        let finalFee = 0.0;
        let amount = 0.0;
        let currencyCode = '';

        let response = {
            result: {
                code: enums.ResponseCodeEnum.INTERNAL_ERROR,
                message: 'Internal Error'
            },
            amount: null,
            fee_withdrawal: null,
            currency: null,
            address_to: null,
            memo: null
        }
        
        try {

            if (proceed) {
                conn = await dbpool.getConnection();    // Create connection to be reused
                dbResult = await dbpool.query('START TRANSACTION', [], conn);
                if (!dbResult.success) {
                    console.log(dbResult.error);
                    response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                    response.result.message = "Database Error";
                    proceed = false;
                }
            }

            if (proceed) {
                if (withdrawalID !== undefined && withdrawalID !== null && !isNaN(parseInt(withdrawalID))) {
                    // Use the withdrawal ID
                } else {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = "Invalid Withdrawal ID";
                    proceed = false;
                }
            }

            if (proceed) {
                commandText = '';
                parameters.length = 0;

                commandText = "SELECT member_id, amount" +
                    ", IFNULL(fee_withdrawal, 0) AS fee_withdrawal" +
                    ", treos_withdrawal.currency, address_to" +
                    ", IFNULL(memo, '') AS memo" +
                    " FROM treos_withdrawal" +
                    " LEFT JOIN treos_fiat_rates ON (treos_withdrawal.currency = treos_fiat_rates.currency)" +
                    " WHERE treos_withdrawal.id = ?" +
                    " AND (status = 'PENDING'";
                if (overrideInProgress) {
                    commandText += " OR status = 'IN_PROGRESS'"
                }
                commandText += ") FOR UPDATE";

                parameters.push(withdrawalID);

                dbResult = await dbpool.query(commandText, parameters, conn);

                if (dbResult.success) {
                    if (dbResult.result[0].length > 0) {
                        profile_id = dbResult.result[0][0].member_id;
                        response.amount = parseFloat(dbResult.result[0][0].amount).toFixed(8);
                        response.fee_withdrawal = parseFloat(dbResult.result[0][0].fee_withdrawal).toFixed(8);
                        response.currency = dbResult.result[0][0].currency;
                        response.address_to = dbResult.result[0][0].address_to;
                        response.memo = dbResult.result[0][0].memo;
                    } else {
                        response.result.code = enums.ResponseCodeEnum.TRANSACTION_NOT_FOUND;
                        response.result.message = "Pending Withdrawal Not Found<br/><br/>It may have already been processed.";
                        proceed = false;
                    }
                } else {
                    console.log(dbResult.error);
                    response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                    response.result.message = "Database Error";
                    proceed = false;
                }
            }


            if (proceed) {
                commandText = '';
                parameters.length = 0;

                commandText = "UPDATE treos_withdrawal";

                commandText += " SET status = 'IN_PROGRESS'";

                commandText += " WHERE id = ?";             parameters.push(withdrawalID);

                dbResult = await dbpool.query(commandText, parameters, conn);

                if (dbResult.success) {
                    if (dbResult.result[0].affectedRows === 0) {
                        response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                        response.result.message = "No rows affected";
                        proceed = false;
                    }
                } else {
                    console.log(dbResult.error);
                    response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                    response.result.message = "Database Error";
                    proceed = false;
                }
            }

        } catch (err) {
            proceed = false;
            response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
            response.result.message = "Internal Error";
            console.log(err);
        } finally {
            if (conn !== null) {
                if (proceed) {
                    dbResult = await dbpool.query('COMMIT', [], conn);
                    if (dbResult.success) {
                        response.result.code = enums.ResponseCodeEnum.SUCCESS;
                        response.result.message = null;
                    } else {
                        console.log(dbResult.error);
                        response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                        response.result.message = "Database Error";
                        proceed = false;
                    }
                } else {
                    dbResult = await dbpool.query('ROLLBACK', [], conn);
                    if (!dbResult.success) {
                        console.log(dbResult.error);
                    }
                }

                conn.release();
                conn = null;
            }
        }
        return response;
    },  // reserveWithdrawalRequest


    unreserveWithdrawalRequest: async function(withdrawalID) {
        'use strict';
    
        let conn = null;
        let commandText = '';
        let parameters = [];
        let dbResult = null;
        let proceed = true;

        let finalFee = 0.0;
        let amount = 0.0;
        let currencyCode = '';

        let response = {
            result: {
                code: enums.ResponseCodeEnum.INTERNAL_ERROR,
                message: 'Internal Error'
            },
        }
        
        try {
            if (proceed) {
                conn = await dbpool.getConnection();    // Create connection to be reused
                dbResult = await dbpool.query('START TRANSACTION', [], conn);
                if (!dbResult.success) {
                    console.log(dbResult.error);
                    response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                    response.result.message = "Database Error";
                    proceed = false;
                }
            }

            if (proceed) {
                if (withdrawalID !== undefined && withdrawalID !== null && !isNaN(parseInt(withdrawalID))) {
                    // Use the withdrawal ID
                } else {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = "Invalid Withdrawal ID";
                    proceed = false;
                }
            }

            if (proceed) {
                commandText = '';
                parameters.length = 0;

                commandText = "SELECT *" +
                    " FROM treos_withdrawal" +
                    " WHERE id = ?" +
                    " AND status = 'IN_PROGRESS'" +
                    " FOR UPDATE";

                parameters.push(withdrawalID);

                dbResult = await dbpool.query(commandText, parameters, conn);

                if (dbResult.success) {
                    if (dbResult.result[0].length > 0) {
                        // Carry on
                    } else {
                        response.result.code = enums.ResponseCodeEnum.TRANSACTION_NOT_FOUND;
                        response.result.message = "In Progress Withdrawal Not Found<br/><br/>It may have already been processed.";
                        proceed = false;
                    }
                } else {
                    console.log(dbResult.error);
                    response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                    response.result.message = "Database Error";
                    proceed = false;
                }
            }

            if (proceed) {
                commandText = '';
                parameters.length = 0;

                commandText = "UPDATE treos_withdrawal";

                commandText += " SET status = 'PENDING'";

                commandText += " WHERE id = ?";             parameters.push(withdrawalID);

                dbResult = await dbpool.query(commandText, parameters, conn);

                if (dbResult.success) {
                    if (dbResult.result[0].affectedRows === 0) {
                        response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                        response.result.message = "No rows affected";
                        proceed = false;
                    }
                } else {
                    console.log(dbResult.error);
                    response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                    response.result.message = "Database Error";
                    proceed = false;
                }
            }

        } catch (err) {
            proceed = false;
            response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
            response.result.message = "Internal Error";
            console.log(err);
        } finally {
            if (conn !== null) {
                if (proceed) {
                    dbResult = await dbpool.query('COMMIT', [], conn);
                    if (dbResult.success) {
                        response.result.code = enums.ResponseCodeEnum.SUCCESS;
                        response.result.message = null;
                    } else {
                        console.log(dbResult.error);
                        response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                        response.result.message = "Database Error";
                        proceed = false;
                    }
                } else {
                    dbResult = await dbpool.query('ROLLBACK', [], conn);
                    if (!dbResult.success) {
                        console.log(dbResult.error);
                    }
                }

                conn.release();
                conn = null;
            }
        }
        return response;
    },  // unreserveWithdrawalRequest


    populateWithdrawFeeCurrencies: async function(fees_year, fees_month, currency) {
        'use strict';
        let result = [];

        let commandText = '';
        let parameters = [];
        let dbResult = null;

        try {
            commandText = "SELECT T1.currency, SUM(fee) AS Fee, SUM(fee_spent) AS FeeSpent, SUM(fee - fee_spent) AS FeeDiff" +
                ", ANY_VALUE(SUM(fee - fee_spent) - IFNULL((SELECT SUM(amount) FROM treos_withdrawal_fees WHERE `year` = YEAR(T1.date) AND `month` = MONTH(T1.date) AND currency = T1.currency), 0)) AS FeeAvailable" +
                ", treos_fiat_rates.fee_withdrawal_address" +
                " FROM treos_withdrawal T1" +
                " LEFT JOIN treos_fiat_rates ON (T1.currency = treos_fiat_rates.currency)" +
                " WHERE status='DONE' AND YEAR(date) = ? AND MONTH(date) = ?";

            parameters.push(fees_year);
            parameters.push(fees_month);

            if (currency !== '*') {
                commandText += " AND T1.currency = ?";
                parameters.push(currency);
            }

            commandText += " GROUP BY T1.currency" +
                " ORDER BY currency ASC";

            dbResult = await dbpool.query(commandText, parameters);

            if (dbResult.success) {
                for (let i = 0; i < dbResult.result[0].length; i++) {
                    let item = dbResult.result[0][i];

                    let feesmonthcurrency = {
                        currency: null,
                        fee: null,
                        fee_spent: null,
                        fee_diff: null,
                        fee_available: null,
                        fee_withdrawal_address: null
                    }

                    feesmonthcurrency.currency = dbResult.result[0][i].currency;
                    feesmonthcurrency.fee = parseFloat(dbResult.result[0][i].Fee);
                    feesmonthcurrency.fee_spent = parseFloat(dbResult.result[0][i].FeeSpent);
                    feesmonthcurrency.fee_diff = parseFloat(dbResult.result[0][i].FeeDiff);
                    feesmonthcurrency.fee_available = parseFloat(dbResult.result[0][i].FeeAvailable);
                    feesmonthcurrency.fee_withdrawal_address = dbResult.result[0][i].fee_withdrawal_address;

                    result.push(feesmonthcurrency);
                }
            } else {
                console.log(dbResult.error);
            }
        } catch (err) {
            result.length = 0;
            console.error(err);
        }
        return result;
    }, // populateWithdrawFeeCurrencies

    populateCryptoWithdrawalsCurrencies: async function(period_year, period_month, transaction_type_id, symbol) {
        'use strict';
        let result = [];

        let commandText = '';
        let parameters = [];
        let dbResult = null;

        try {
            commandText = "SELECT T1.transaction_type_id, treos_transaction_type.`type` AS transaction_type, T1.symbol, SUM(amount) AS amount" +
                ", ANY_VALUE(SUM(amount) - IFNULL((SELECT SUM(amount) FROM treos_corporate_withdrawals WHERE `year` = T1.`year` AND `month` = T1.`month` AND symbol = T1.symbol), 0)) AS crypto_available" +
                //", IFNULL(treos_transaction_address.address, '') AS withdrawal_address, IFNULL(treos_transaction_address.memo, '') AS withdrawal_memo" +
                " FROM treos_corporate_transactions T1" +
                " LEFT JOIN treos_transaction_type ON (T1.transaction_type_id = treos_transaction_type.type_id)" +
                //" LEFT JOIN treos_transaction_address ON (T1.transaction_type_id = treos_transaction_address.type_id AND T1.symbol = token)" +
                " WHERE (`year` = ? AND `month` = ?)";

            parameters.push(period_year);
            parameters.push(period_month);

            if (symbol !== '*') {
                commandText += " AND T1.symbol = ?";
                parameters.push(symbol);
            }

            if (transaction_type_id !== '*') {
                commandText += " AND T1.transaction_type_id = ?";
                parameters.push(transaction_type_id);
            }

            commandText += " GROUP BY T1.symbol, T1.transaction_type_id" +
                " ORDER BY symbol ASC, treos_transaction_type.type ASC";

            dbResult = await dbpool.query(commandText, parameters);

            if (dbResult.success) {
                for (let i = 0; i < dbResult.result[0].length; i++) {
                    let item = dbResult.result[0][i];

                    let period_currency = {
                        transaction_type_id: null,
                        transaction_type: null,
                        symbol: null,
                        amount: null,
                        crypto_available: null,
                        //withdrawal_address: null,
                        //withdrawal_memo: null
                    }

                    period_currency.transaction_type_id = dbResult.result[0][i].transaction_type_id;
                    period_currency.transaction_type = dbResult.result[0][i].transaction_type;
                    period_currency.symbol = dbResult.result[0][i].symbol;
                    period_currency.amount = parseFloat(dbResult.result[0][i].amount);
                    period_currency.crypto_available = parseFloat(dbResult.result[0][i].crypto_available);
                    //period_currency.withdrawal_address = dbResult.result[0][i].withdrawal_address;
                    //period_currency.withdrawal_memo = dbResult.result[0][i].withdrawal_memo;

                    result.push(period_currency);
                }
            } else {
                console.log(dbResult.error);
            }
        } catch (err) {
            result.length = 0;
            console.error(err);
        }
        return result;
    }, // populateCryptoWithdrawalsCurrencies

    withdrawFees_Insert: async function(fees_year, fees_month, currency, amount, fee_spent, address_to, txn_reference) {
        'use strict';
    
        let conn = null;
        let commandText = '';
        let parameters = [];
        let dbResult = null;
        let proceed = true;

        let finalAmount = 0.0;
        let finalFee_Spent = 0.0;

        let response = {
            result: {
                code: enums.ResponseCodeEnum.INTERNAL_ERROR,
                message: 'Internal Error'
            }
        }
        
        try {
            if (proceed) {
                if (fees_year !== undefined && fees_year !== null && !isNaN(parseInt(fees_year))   ) {
                    // fees_year is considered to be valid
                } else {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = "Invalid Year";
                    proceed = false;
                }
            }

            if (proceed) {
                if (fees_month !== undefined && fees_month !== null && !isNaN(parseInt(fees_month))   ) {
                    // fees_month is considered to be valid
                } else {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = "Invalid Month";
                    proceed = false;
                }
            }

            if (proceed) {
                if (currency !== undefined && currency !== null && currency.trim().length > 0) {
                    // currency is considered to be valid
                } else {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = "Invalid Currency";
                    proceed = false;
                }
            }

            if (proceed) {
                if (amount !== undefined && amount !== null && !isNaN(parseFloat(amount))) {
                    finalAmount = parseFloat(amount);
                } else {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = "Invalid Fee Spent";
                    proceed = false;
                }
            }

            if (proceed) {
                if (address_to !== undefined && address_to !== null && address_to.trim().length > 0) {
                    // address_to is considered to be valid
                } else {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = "Invalid Address";
                    proceed = false;
                }
            }

            if (proceed) {
                if (fee_spent !== undefined && fee_spent !== null && !isNaN(parseFloat(fee_spent))) {
                    finalFee_Spent = parseFloat(fee_spent);
                } else {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = "Invalid Fee Spent";
                    proceed = false;
                }
            }

            if (proceed) {
                if (txn_reference !== undefined && txn_reference !== null && txn_reference.trim().length > 0) {
                    // transactionID is considered to be valid
                } else {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = "Invalid Transaction ID";
                    proceed = false;
                }
            }

            
            if (proceed) {
                commandText = '';
                parameters.length = 0;

                commandText = "INSERT INTO treos_withdrawal_fees";

                commandText += " SET date = NOW()";
                commandText += ", `year` = ?";                 parameters.push(fees_year);
                commandText += ", `month` = ?";                 parameters.push(fees_month);
                commandText += ", currency = ?";                 parameters.push(currency);
                commandText += ", amount = ?";                 parameters.push(finalAmount.toFixed(8));
                commandText += ", fee_spent = ?";           parameters.push(finalFee_Spent.toFixed(8));
                commandText += ", address_to = ?";                 parameters.push(address_to);
                commandText += ", txn_reference = ?";    parameters.push(txn_reference.trim());

                dbResult = await dbpool.query(commandText, parameters, conn);

                if (dbResult.success) {
                    if (dbResult.result[0].affectedRows === 0) {
                        response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                        response.result.message = "No rows affected";
                        console.log(parameters);
                        proceed = false;
                    } else {
                        response.result.code = enums.ResponseCodeEnum.SUCCESS;
                        response.result.message = null;
                    }
                } else {
                    console.log(dbResult.error);
                    response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                    response.result.message = "Database Error";
                    proceed = false;
                }
            }
        } catch (err) {
            proceed = false;
            response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
            response.result.message = "Internal Error";
            console.log(err);
        } finally {
            if (conn !== null) {
                conn.release();
                conn = null;
            }
        }
        return response;
    },  // withdrawFees_Insert

    withdrawCrypto_Insert: async function(year, month, transaction_type_id, symbol, amount, fee_spent, withdraw_address, withdraw_memo, txn_reference) {
        'use strict';
    
        let conn = null;
        let commandText = '';
        let parameters = [];
        let dbResult = null;
        let proceed = true;

        let finalAmount = 0.0;
        let finalFee_Spent = 0.0;

        let response = {
            result: {
                code: enums.ResponseCodeEnum.INTERNAL_ERROR,
                message: 'Internal Error'
            }
        }
        
        try {
            if (proceed) {
                if (year !== undefined && year !== null && !isNaN(parseInt(year))   ) {
                    // year is considered to be valid
                } else {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = "Invalid Year";
                    proceed = false;
                }
            }

            if (proceed) {
                if (month !== undefined && month !== null && !isNaN(parseInt(month))   ) {
                    // fees_month is considered to be valid
                } else {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = "Invalid Month";
                    proceed = false;
                }
            }

            if (proceed) {
                if (transaction_type_id !== undefined && transaction_type_id !== null && !isNaN(parseInt(transaction_type_id))   ) {
                    // transaction_type_id is considered to be valid
                } else {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = "Invalid Transaction Type ID";
                    proceed = false;
                }
            }

            if (proceed) {
                if (symbol !== undefined && symbol !== null && symbol.trim().length > 0) {
                    // symbol is considered to be valid
                } else {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = "Invalid Currency";
                    proceed = false;
                }
            }

            if (proceed) {
                if (amount !== undefined && amount !== null && !isNaN(parseFloat(amount))) {
                    finalAmount = parseFloat(amount);
                } else {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = "Invalid Amount";
                    proceed = false;
                }
            }

            if (proceed) {
                if (withdraw_address !== undefined && withdraw_address !== null && withdraw_address.trim().length > 0) {
                    // withdraw_address is considered to be valid
                } else {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = "Invalid Address";
                    proceed = false;
                }
            }

            if (proceed) {
                if (withdraw_memo !== undefined && withdraw_memo !== null) {
                    // withdraw_memo is considered to be valid
                } else {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = "Invalid Memo";
                    proceed = false;
                }
            }

            if (proceed) {
                if (fee_spent !== undefined && fee_spent !== null && !isNaN(parseFloat(fee_spent))) {
                    finalFee_Spent = parseFloat(fee_spent);
                } else {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = "Invalid Fee Spent";
                    proceed = false;
                }
            }

            if (proceed) {
                if (finalAmount - finalFee_Spent <= 0.0) {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = "Invalid Fee Spent";
                    proceed = false;
                }
            }

            if (proceed) {
                if (txn_reference !== undefined && txn_reference !== null && txn_reference.trim().length > 0) {
                    // transactionID is considered to be valid
                } else {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = "Invalid Transaction ID";
                    proceed = false;
                }
            }

            
            if (proceed) {
                commandText = '';
                parameters.length = 0;

                commandText = "INSERT INTO treos_corporate_withdrawals";

                commandText += " SET date = NOW()";
                commandText += ", `year` = ?";                  parameters.push(year);
                commandText += ", `month` = ?";                 parameters.push(month);
                commandText += ", transaction_type_id = ?";     parameters.push(transaction_type_id);
                commandText += ", symbol = ?";                  parameters.push(symbol);
                commandText += ", amount = ?";                  parameters.push(finalAmount.toFixed(8));
                commandText += ", fee_spent = ?";               parameters.push(finalFee_Spent.toFixed(8));
                commandText += ", withdraw_address = ?";        parameters.push(withdraw_address);
                commandText += ", withdraw_memo = ?";           parameters.push(withdraw_memo);
                commandText += ", txn_reference = ?";           parameters.push(txn_reference.trim());

                dbResult = await dbpool.query(commandText, parameters, conn);

                if (dbResult.success) {
                    if (dbResult.result[0].affectedRows === 0) {
                        response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                        response.result.message = "No rows affected";
                        console.log(parameters);
                        proceed = false;
                    } else {
                        response.result.code = enums.ResponseCodeEnum.SUCCESS;
                        response.result.message = null;
                    }
                } else {
                    console.log(dbResult.error);
                    response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                    response.result.message = "Database Error";
                    proceed = false;
                }
            }
        } catch (err) {
            proceed = false;
            response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
            response.result.message = "Internal Error";
            console.log(err);
        } finally {
            if (conn !== null) {
                conn.release();
                conn = null;
            }
        }
        return response;
    },  // withdrawCrypto_Insert

}   // module.exports






async function getBlockchainDetails_Txn(currencyCode, transactionID) {
    'use strict';

    let response = {
        result: {
            code: enums.ResponseCodeEnum.INTERNAL_ERROR,
            message: 'Internal Error'
        },
        fee: 0
    }
    
    try {
        let url = '';
        const axios = require("axios");

        if (currencyCode.toUpperCase() === 'BCH') {
            // https://bch.btc.com/
            url = 'https://bch-chain.api.btc.com/v3/tx/' + transactionID;
            let result = await axios.get(url);

            if (result.status === 200) {    // OK
                let data = result.data;

                console.log(data);

                if (data.err_no === 0) {
                    data = result.data.data;

                    let fee = data.fee;
                    if (fee !== null && fee !== null && !isNaN(fee)) {
                        response.fee = fee / 100000000.0;   // Convert satoshi to BCH

                        response.result.code = enums.ResponseCodeEnum.SUCCESS;
                        response.result.message = 'Success';
                    }
                } else {
                    response.result.code = enums.INTERNAL_ERROR;
                    response.result.message = data.err_no + ' - ' + data.err_msg;
                }
            } else {
                response.result.code = enums.INTERNAL_ERROR;
                response.result.message = result.status + ' - ' + result.statusText
            }
        } else if (currencyCode.toUpperCase() === 'BTC' ||
                currencyCode.toUpperCase() === 'ETH' ||
                currencyCode.toUpperCase() === 'LTC') {
            // //https://btc.com/api-doc#Transaction
            // //https://chain.api.btc.com/v3/tx/0eab89a271380b09987bcee5258fca91f28df4dadcedf892658b9bc261050d96
            // url = 'https://blockchain.info/q/txfee/' + transactionID;
            // let result = await axios.get(url);
            // //console.log(result);
            // if (result.status === 200) {    // OK
            //     let fee = parseFloat(result.data);
            //     if (fee !== null && fee !== null && !isNaN(fee)) {
            //         response.fee = fee / 100000000.0;   // Convert satoshi to BTC

            //         response.result.code = enums.ResponseCodeEnum.SUCCESS;
            //         response.result.message = 'Success';
            //     }
            // }

            url = 'https://api.blockcypher.com/v1/' + currencyCode.toLowerCase() + '/main/txs/' + transactionID;

            const instance = axios.create({
                headers: {'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0'}
            });

            try {
                let result = await instance.get(url);

                if (result.status === 200) {    // OK
                    let data = result.data;

                    let fee = data.fees;
                    if (fee !== null && fee !== null && !isNaN(fee)) {
                        if (currencyCode.toUpperCase() === 'ETH') {
                            response.fee = fee / 1000000000000000000.0;   // Convert satoshi to ETH
                        } else {
                            response.fee = fee / 100000000.0;   // Convert satoshi to BTC
                        }
                        

                        response.result.code = enums.ResponseCodeEnum.SUCCESS;
                        response.result.message = 'Success';
                    }
                } else {
                    response.result.code = enums.INTERNAL_ERROR;
                    response.result.message = result.status + ' - ' + result.statusText
                }
            } catch(err) {
                response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
                if (err.response !== undefined && err.response !== null) {
                    response.result.message = err.response.status + ' - ' + err.response.statusText;
                }
            }
            
        } else {
            response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
            response.result.message = 'No API Configured for this currency.';
        }

        

    } catch (err) {
        response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
        response.result.message = "Internal Error";
        console.error(err);
    }
    return response;
}  // getBlockchainDetails_Txn

module.exports.getBlockchainDetails_Txn = getBlockchainDetails_Txn;
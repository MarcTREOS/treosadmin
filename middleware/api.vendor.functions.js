const express = require('express');
//const router = express.Router();
const fs = require('fs');
const path = require('path');
const dbpool = require('./dbpool');
const enums = require('../middleware/enums');
const ipfs = require('../middleware/ipfs');
const tools = require('../middleware/tools');
const Papa = require('../public/js/csv/PapaParse-5.0.2/papaparse');
const CSV_Shopify = require('../middleware/csv_shopify');
const api_dfuse = require('../middleware/dfuse_api');
const treosFunctions = require('../middleware/treos.functions');

//router.use(express.json());

module.exports = {

    getRole: async function (profile_id) {
        'use strict';
    
        let commandText = '';
        let parameters = [];
        let dbResult = null;

        let response = {
            result: {
                code: enums.ResponseCodeEnum.INTERNAL_ERROR,
                message: 'Internal Error'
            },
            role_id: -1
        }
        
        try {
            commandText = "SELECT role_id" +
                " FROM xc_profile_roles" +
                " WHERE profile_id = ?";

            parameters.push(profile_id);

            dbResult = await dbpool.query(commandText, parameters);

            if (dbResult.success) {

                if (dbResult.result[0].length > 0) {
                    response.role_id = dbResult.result[0][0].role_id;

                    response.result.code = enums.ResponseCodeEnum.SUCCESS;
                    response.result.message = "Success";
                }
            } else {
                console.log(dbResult.error);
                response.result.message = "Internal Error";
            }
        } catch (err) {
            response.result.message = "Internal Error";
            console.log(err);
        }
        return response;
    },  // getRole

    eosAccountExists: async function(account_name) {
        'use strict';

        let commandText = '';
        let parameters = [];
        let dbResult = null;

        let proceed = true;

        let response = {
            result: {
                code: enums.ResponseCodeEnum.INTERNAL_ERROR,
                message: 'Internal Error'
            },
            accountExists: false,
            requestExists: false
        }

        try {
            if (proceed) {
                if (account_name === undefined || account_name === null) {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = "Account Name Missing";
                    proceed = false;
                } else {
                    if (account_name.trim().length === 0) {
                        response.result.code = enums.ResponseCodeEnum.FORM_FIELD_REQUIRED;
                        response.result.message = "Account Name is Required";
                        proceed = false;
                    }
                }
            }

            if (proceed) {
                commandText = "SELECT COUNT(*) AS Total" +
                    " FROM treos_eos_account_create" +
                    " WHERE account_name = ?" +
                    " AND status_id = 1";   // New

                parameters.push(account_name);

                dbResult = await dbpool.query(commandText, parameters);

                if (dbResult.success) {

                    if (dbResult.result[0][0].Total > 0) {
                        response.requestExists = true;
                    }
                } else {
                    response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                    response.result.message = "Database Error";
                    proceed = false;
                }
            }

            if (proceed && !response.requestExists) {
                response.accountExists = await api_dfuse.accountExists(account_name);
            }
            
            if (proceed) {
                response.result.code = enums.ResponseCodeEnum.SUCCESS;
                response.result.message = "Success";
            }
        } catch (err) {
            response.result.message = "Internal Error";
            console.error(err);
        }
        return response;
    },  // eosAccountExists

    eosCreateAccount: async function (profile_id, account_name, ownerPublicKey, activePublicKey, symbol, fee) {
        'use strict';
        let conn = null;
        let commandText = '';
        let parameters = [];
        let dbResult = null;

        let proceed = true;

        let response = {
            result: {
                code: enums.ResponseCodeEnum.INTERNAL_ERROR,
                message: 'Internal Error'
            }
        }
        
        try {
            if (proceed) {
                //console.log('req.body', req.body);
                let validationResponse = await this.eosCreateAccount_Validate(conn, profile_id, account_name, ownerPublicKey, activePublicKey, symbol, fee);
                
                if (validationResponse.result.code !== enums.ResponseCodeEnum.SUCCESS) {
                    response.result = validationResponse.result;
                    proceed = false;
                }
            }

            if (proceed) {
                conn = await dbpool.getConnection();    // Create connection to be reused
                dbResult = await dbpool.query('START TRANSACTION', [], conn);
                if (!dbResult.success) {
                    console.log(dbResult.error);
                    response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                    response.result.message = "Database Error";
                    proceed = false;
                }
            }

            /* Add Account Create Request */
            if (proceed) {
                commandText = '';
                parameters.length = 0;

                commandText = "INSERT INTO treos_eos_account_create" +
                    " SET profile_id = ?" +
                    ", date_request = NOW()" +
                    ", date = NOW()" +
                    ", status_id = 1" + // New
                    ", account_name = ?" +
                    ", owner_public_key = ?" +
                    ", active_public_key = ?" +
                    ", symbol = ?" +
                    ", fee = ?";

                parameters.push(profile_id);
                parameters.push(account_name);
                parameters.push(ownerPublicKey);
                parameters.push(activePublicKey);
                parameters.push(symbol);
                parameters.push(fee);

                let dbResponse = await dbpool.query(commandText, parameters, conn);

                if (!dbResponse.success) {
                    response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                    response.result.message = 'Database Error';
                    proceed = false;
                }
            }

            /* Update wallet balance */
            if (proceed) {
                let alterTokenResponse = await treosFunctions.alterTokenAmount(conn,
                    profile_id,
                    symbol,
                    'DEDUCT_CREATE_EOS_ACCOUNT',
                    fee * -1.0);

                //console.log(alterTokenResponse);

                if (alterTokenResponse.result.code !== enums.ResponseCodeEnum.SUCCESS) {
                    response.result = alterTokenResponse.result;
                    proceed = false;
                }
            }

            
        } catch (err) {
            response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
            response.result.message = "Internal Error";
            proceed = false;
            console.error(err);
        } finally {
            if (conn !== null) {
                if (proceed) {
                    await dbpool.query('COMMIT', [], conn);
                    response.result.code = enums.ResponseCodeEnum.SUCCESS;
                    response.result.message = null;
                } else {
                    await dbpool.query('ROLLBACK', [], conn);
                }

                conn.release();
                conn = null;
            }
        }
        
        return response;
    },  // eosCreateAccount

    eosCreateAccount_Validate: async function (conn, profile_id, accountName, ownerPublicKey, activePublicKey, symbol, fee) {
        'use strict';
        let commandText = '';
        let parameters = [];
        let dbResult = null;

        let proceed = true;
        let profile = null;
        let balance = 0.0;

        let response = {
            result: {
                code: enums.ResponseCodeEnum.INTERNAL_ERROR,
                message: 'Internal Error'
            }
        }
        
        try {
            if (proceed) {
                let accountExistsResponse = await this.eosAccountExists(accountName);

                if (accountExistsResponse.result.code === enums.ResponseCodeEnum.SUCCESS) {
                    if (accountExistsResponse.accountExists) {
                        response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                        response.result.message = "Account already exists.";
                        proceed = false;
                    } else if (accountExistsResponse.requestExists) {
                        response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                        response.result.message = "Account creation request already exists for that username.";
                        proceed = false;
                    }
                } else {
                    response.result.code = accountExistsResponse.result.code;
                    response.result.message = accountExistsResponse.result.message;
                    proceed = false;
                }
            }

            if (proceed) {
                if (ownerPublicKey === undefined || ownerPublicKey === null || ownerPublicKey.length < 20) {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_REQUIRED;
                    response.result.message = "Owner Public Key is required.";
                    proceed = false;
                } else if (ownerPublicKey.substring(0, 3) !== 'EOS') {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = "Invalid Owner Public Key.";
                    proceed = false;
                }
            }

            if (proceed) {
                if (activePublicKey === undefined || activePublicKey === null || activePublicKey.length < 20) {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_REQUIRED;
                    response.result.message = "Owner Active Key is required.";
                    proceed = false;
                } else if (activePublicKey.substring(0, 3) !== 'EOS') {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = "Invalid Active Public Key.";
                    proceed = false;
                }
            }

            if (proceed) {
                if (symbol === undefined || symbol === null) {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_REQUIRED;
                    response.result.message = "Currency is required.";
                    proceed = false;
                }
            }

            if (proceed) {
                let profileResponse = await this.getVendorProfile(profile_id);

                if (profileResponse.result.code === enums.ResponseCodeEnum.SUCCESS) {
                    profile = profileResponse.profile;
                } else {
                    response.result.code = profileResponse.result.code;
                    response.result.message = profileResponse.result.message;
                    proceed = false;
                }
            }

            if (proceed) {
                if (isNaN(fee)) {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = "Invalid Fee.";
                    proceed = false;
                }
            }

            if (proceed) {
                if (profile.balances[symbol] !== undefined) {
                    balance = parseFloat(profile.balances[symbol].balance);

                    if (balance < fee) {
                        response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                        response.result.message = "Insufficient Balance.";
                        proceed = false;
                    }
                } else {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = "Insufficient Balance.";
                    proceed = false;
                }
            }

            if (proceed) {
                response.result.code = enums.ResponseCodeEnum.SUCCESS;
                response.result.message = null;
            }
            
        } catch (err) {
            response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
            response.result.message = "Internal Error";
            proceed = false;
            console.error(err);
        }
        
        return response;
    },  // eosCreateAccount_Validate
    
    getAddresses: async function(profile_id, isBilling, isShipping, isWork) {
        'use strict';
    
        let commandText = '';
        let parameters = [];
        let dbResult = null;

        let response = {
            result: {
                code: enums.ResponseCodeEnum.INTERNAL_ERROR,
                message: 'Internal Error'
            },
            addresses: []
        }
        
        try {
            commandText = "SELECT xc_address_field_value.address_id, xc_address_field.id AS address_field_id, serviceName, xc_address_field_value.value" +
                " FROM xc_address_field" +
                " RIGHT JOIN xc_address_field_value ON (xc_address_field.id = xc_address_field_value.address_field_id)" +
                " LEFT JOIN xc_profile_addresses ON (xc_address_field_value.address_id = xc_profile_addresses.address_id)" +
                " WHERE xc_address_field.enabled = 1";

            let addressTypeCount = 0;

            if (isBilling !== undefined && isBilling !== null && isBilling === true) {
                commandText += " AND (xc_profile_addresses.is_billing = ?";
                let boolValue = isBilling == true ? 1 : 0;
                parameters.push(boolValue);
                addressTypeCount++;
            }

            if (isShipping !== undefined && isShipping !== null && isShipping === true) {
                if (addressTypeCount > 0) {
                    commandText += ' OR ';
                } else {
                    commandText += ' AND (';
                }
                commandText += " xc_profile_addresses.is_shipping = ?";
                let boolValue = isShipping == true ? 1 : 0;
                parameters.push(boolValue);
                addressTypeCount++;
            }

            if (isWork !== undefined && isWork !== null && isWork === true) {
                if (addressTypeCount > 0) {
                    commandText += ' OR ';
                } else {
                    commandText += ' AND (';
                }
                commandText += " xc_profile_addresses.isWork = ?";
                let boolValue = isWork == true ? 1 : 0;
                parameters.push(boolValue);
                addressTypeCount++;
            }

            if (addressTypeCount > 0) {
                commandText += ')';
            }

            commandText += " AND xc_profile_addresses.profile_id = ?" +
                " ORDER BY address_id ASC, address_field_id ASC";

            parameters.push(profile_id);

            dbResult = await dbpool.query(commandText, parameters);

            if (dbResult.success) {

                let address = {};
                let prev_address_id = null;

                for (let i = 0; i < dbResult.result[0].length; i++) {
                    let item = dbResult.result[0][i];

                    if (prev_address_id === null || item.address_id !== prev_address_id) {
                        if (prev_address_id !== null) {
                            response.addresses.push(address);
                        }

                        address = {
                            address_id: item.address_id,
                            title: null,
                            firstname: null,
                            lastname: null,
                            street: null,
                            city: null,
                            state_id: null,
                            custom_state: null,
                            zipcode: null,
                            country_code: null,
                            country: null,
                            phone: null
                        }

                        prev_address_id = item.address_id;
                    }

                    switch (item.address_field_id) {
                        case 2:  address.firstname      = item.value; break;
                        case 3:  address.lastname       = item.value; break;
                        case 4:  address.street         = item.value; break;
                        case 5:  address.city           = item.value; break;
                        case 6: {
                            address.country_code = item.value;

                            let respGetCountry = await this.getCountry(item.value);

                            if (respGetCountry.result.code === enums.ResponseCodeEnum.SUCCESS && respGetCountry.country !== null && respGetCountry.country !== undefined) {
                                address.country = respGetCountry.country;
                            }
                            break;
                        }
                        case 7:  address.state_id       = item.value; break;
                        case 8:  address.custom_state   = item.value; break;
                        case 9:  address.zipcode        = item.value; break;
                        case 10: address.phone          = item.value; break;
                    }
                }

                if (prev_address_id !== null) {
                    response.addresses.push(address);
                }

                response.result.code = enums.ResponseCodeEnum.SUCCESS;
                response.result.message = null;
            } else {
                console.log(dbResult.error);
                response.result.message = "Internal Error";


                // let address1 = {
                //     address_id: 10,
                //     title: null,
                //     firstname: 'Marc',
                //     lastname: 'Dixon',
                //     street: '5 Waimanawa Lane',
                //     city: null,
                //     state_id: null,
                //     custom_state: null,
                //     zipcode: null,
                //     country_code: null,
                //     country: null,
                //     phone: null
                // }

                // let address2 = {
                //     address_id: 10,
                //     title: null,
                //     firstname: 'Marc',
                //     lastname: 'Dixon',
                //     street: '3 Ancona Lane',
                //     city: null,
                //     state_id: null,
                //     custom_state: null,
                //     zipcode: null,
                //     country_code: null,
                //     country: null,
                //     phone: null
                // }

                // response.addresses.push(address1);
                // response.addresses.push(address2);
                // response.success = true;
                // response.errorMessage = null;
            }
        } catch (err) {
            response.result.message = "Internal Error";
            console.error(err);
        }
        return response;
    },   // getAddresses


    getCountry: async function(country_code) {
        'use strict';
        let commandText = '';
        let parameters = [];
        let dbResult = null;
    
        let response = {
            result: {
                code: enums.ResponseCodeEnum.INTERNAL_ERROR,
                message: 'Internal Error'
            },
            country_code: null,
            country: null
        }
        
        try {
            commandText = "SELECT id AS country_code, country" +
                " FROM xc_country_translations" +
                " WHERE id = ?";
    
            parameters.push(country_code);
    
            dbResult = await dbpool.query(commandText, parameters);
            
            if (dbResult.success) {
                if (dbResult.result[0].length > 0) {
                    response.country_code = dbResult.result[0][0].country_code;
                    response.country = dbResult.result[0][0].country;
                }
    
                response.result.code = enums.ResponseCodeEnum.SUCCESS;
                response.result.message = null;
            } else {
                console.log(dbResult.error);
                response.result.message = "Internal Error";
            }
        } catch (err) {
            response.result.message = "Internal Error";
            console.log(err);
        }
        return response;
    },   // getCountry

    getConfigSettings: async function(settingNames) {
        'use strict';
        let commandText = '';
        let parameters = [];
        let dbResult = null;
    
        let response = {
            result: {
                code: enums.ResponseCodeEnum.INTERNAL_ERROR,
                message: 'Internal Error'
            },
            settings: {}
        }
        
        try {
            commandText = "SELECT option_name, option_value" +
                " FROM treos_config_v2" +
                " WHERE option_name IN (";
            
            for (let i = 0; i < settingNames.length; i++) {
                if (i > 0) {
                    commandText += ",";
                }
                commandText += "?";
                parameters.push(settingNames[i]);
            }

            commandText += ")" +
                " ORDER BY option_name ASC"
    
            dbResult = await dbpool.query(commandText, parameters);
            
            if (dbResult.success) {
                for (let i = 0; i < dbResult.result[0].length; i++) {
                    let option_name = dbResult.result[0][i].option_name;
                    let option_value = dbResult.result[0][i].option_value;
                    
                    response.settings[option_name] = option_value;  // Dictionary Object
                }

                response.result.code = enums.ResponseCodeEnum.SUCCESS;
                response.result.message = null;
            } else {
                console.log(dbResult.error);
                response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
                response.result.message = "Internal Error";
            }
        } catch (err) {
            response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
            response.result.message = "Internal Error";
            console.log(err);
        }
        return response;
    },  // getConfigSettings

    getFiatCurrencies: async function() {
        'use strict';
        let commandText = '';
        let parameters = [];
        let dbResult = null;
    
        let response = {
            result: {
                code: enums.ResponseCodeEnum.INTERNAL_ERROR,
                message: 'Internal Error'
            },
            currencies: {}
        }
        
        try {
            commandText = "SELECT currency_id, xc_currencies.code AS currency_code" +
                ", symbol AS currency_symbol, name AS currency_name, prefix, suffix, e AS exponent" +
                ", decimalDelimiter, thousandDelimiter, IF(roundUp = 'Y', 1, 0) AS roundUp" +
                ", tro_rate" +
                " FROM treos_fiat_rates" +
                " LEFT JOIN xc_currencies ON (treos_fiat_rates.currency = xc_currencies.code)" +
                " LEFT JOIN xc_currency_translations ON (currency_id = xc_currency_translations.id AND xc_currency_translations.code = 'en')" +
                " HAVING xc_currencies.code IS NOT NULL" +
                " ORDER BY xc_currency_translations.name ASC";
                //" ORDER BY xc_currencies.code ASC";
    
            dbResult = await dbpool.query(commandText, parameters);
            
            if (dbResult.success) {
                for (let i = 0; i < dbResult.result[0].length; i++) {
                    let currency = {
                        id: dbResult.result[0][i].currency_id,
                        code: dbResult.result[0][i].currency_code,
                        symbol: dbResult.result[0][i].currency_symbol,
                        name: dbResult.result[0][i].currency_name,
                        prefix: dbResult.result[0][i].prefix,
                        suffix: dbResult.result[0][i].suffix,
                        exponent: dbResult.result[0][i].exponent,
                        decimalDelimiter: dbResult.result[0][i].decimalDelimiter,
                        thousandDelimiter: dbResult.result[0][i].thousandDelimiter,
                        roundUp: (dbResult.result[0][i].roundUp === 1),
                        tro_rate: parseFloat(dbResult.result[0][i].tro_rate)    // Decimal returned as string from the database
                    }

                    //response.currencies.push(currency);
                    response.currencies[currency.code] = currency;  // Dictionary Object
                }

                response.result.code = enums.ResponseCodeEnum.SUCCESS;
                response.result.message = null;
            } else {
                console.log(dbResult.error);
                response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
                response.result.message = "Internal Error";
            }
        } catch (err) {
            response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
            response.result.message = "Internal Error";
            console.log(err);
        }
        return response;
    },   // getFiatCurrencies

    getPlatformCategories: async function() {
        'use strict';
        let commandText = '';
        let parameters = [];
        let dbResult = null;
    
        let response = {
            result: {
                code: enums.ResponseCodeEnum.INTERNAL_ERROR,
                message: 'Internal Error'
            },
            categories: []
        }
        
        try {
            commandText = "SELECT category_id, `name`, parent_id" +
                " FROM xc_categories" +
                " LEFT JOIN xc_category_translations ON (xc_categories.category_id = xc_category_translations.id)" +
                " WHERE parent_id IS NOT NULL" +
                " ORDER BY parent_id ASC, `name` ASC";
    
            dbResult = await dbpool.query(commandText, parameters);
            
            if (dbResult.success) {
                for (let i = 0; i < dbResult.result[0].length; i++) {
                    if (dbResult.result[0][i].parent_id === 1) {
                        // root node
                        response.categories.push(dbResult.result[0][i].name);
                    }

                    for (let j = i; j < dbResult.result[0].length; j++) {
                        if (dbResult.result[0][j].category_id === dbResult.result[0][i].parent_id) {
                            
                        }
                    }
                }

                response.result.code = enums.ResponseCodeEnum.SUCCESS;
                response.result.message = null;
            } else {
                console.log(dbResult.error);
                response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
                response.result.message = "Internal Error";
            }
        } catch (err) {
            response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
            response.result.message = "Internal Error";
            console.log(err);
        }
        return response;
    },   // getPlatformCategories

    getPlatformCurrencies: async function() {
        'use strict';
        let commandText = '';
        let parameters = [];
        let dbResult = null;
    
        let response = {
            result: {
                code: enums.ResponseCodeEnum.INTERNAL_ERROR,
                message: 'Internal Error'
            },
            currencies: {}
        }
        
        try {
            commandText = "SELECT treos_fiat_rates.*" +
                //", CAST(IF(isFiat = 1, tro_rate, (SELECT tro_rate FROM treos_fiat_rates WHERE currency = 'USD') / tro_rate) AS DECIMAL(64,8)) AS actual_tro_rate" +
                ", CAST(CASE" +
                    " WHEN isFiat = 1 THEN tro_rate" +
                    " WHEN currency = 'TRO' THEN 1.0" +
                    " WHEN currency = 'TRO_UBI' THEN 1.0" +
                    " WHEN currency = 'TRE' THEN 10.0" +
                    " WHEN currency = 'TREOS' THEN 100.0" +
                    " ELSE (SELECT tro_rate FROM treos_fiat_rates WHERE currency = 'USD') / tro_rate" +
                " END AS DECIMAL(64,8)) AS actual_tro_rate" +
                " FROM treos_fiat_rates" +
                " WHERE isActive = 1" +
                " ORDER BY currency ASC";
    
            dbResult = await dbpool.query(commandText, parameters);
            
            if (dbResult.success) {
                for (let i = 0; i < dbResult.result[0].length; i++) {
                    let currency = {
                        id: dbResult.result[0][i].id,
                        symbol: dbResult.result[0][i].currency,
                        name: dbResult.result[0][i].name,
                        fee_withdrawal: parseFloat(dbResult.result[0][i].fee_withdrawal),
                        exponent: dbResult.result[0][i].exponent,
                        tro_rate: parseFloat(dbResult.result[0][i].actual_tro_rate),    // Decimal returned as string from the database
                        qr_code_prefix: dbResult.result[0][i].qr_code_prefix,
                        useScatter: dbResult.result[0][i].useScatter,
                        isFiat: dbResult.result[0][i].isFiat
                    }

                    //response.currencies.push(currency);
                    response.currencies[currency.symbol] = currency;  // Dictionary Object
                }

                response.result.code = enums.ResponseCodeEnum.SUCCESS;
                response.result.message = null;
            } else {
                console.log(dbResult.error);
                response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
                response.result.message = "Internal Error";
            }
        } catch (err) {
            response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
            response.result.message = "Internal Error";
            console.log(err);
        }
        return response;
    },   // getPlatformCurrencies


    getPlatformTransactionTypes: async function(onlyActive) {
        'use strict';
        let commandText = '';
        let parameters = [];
        let dbResult = null;
    
        let response = {
            result: {
                code: enums.ResponseCodeEnum.INTERNAL_ERROR,
                message: 'Internal Error'
            },
            transaction_types: {}
        }
        
        try {
            commandText = "SELECT type_id AS transaction_type_id, `type` as transaction_type" +
                " FROM treos_transaction_type";

            if (onlyActive) {
                commandText += " WHERE is_active = 1";
            }
                
            commandText += " ORDER BY transaction_type ASC";
    
            dbResult = await dbpool.query(commandText, parameters);
            
            if (dbResult.success) {
                for (let i = 0; i < dbResult.result[0].length; i++) {
                    let transaction_type = {
                        transaction_type_id: dbResult.result[0][i].transaction_type_id,
                        transaction_type: dbResult.result[0][i].transaction_type
                    }

                    response.transaction_types[transaction_type.transaction_type_id] = transaction_type;  // Dictionary Object
                }

                response.result.code = enums.ResponseCodeEnum.SUCCESS;
                response.result.message = null;
            } else {
                console.log(dbResult.error);
                response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
                response.result.message = "Internal Error";
            }
        } catch (err) {
            response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
            response.result.message = "Internal Error";
            console.log(err);
        }
        return response;
    },   // getPlatformTransactionTypes


    getVendorProfile: async function(profile_id) {
        'use strict';
    
        let commandText = '';
        let parameters = [];
        let dbResult = null;

        let proceed = true;
        

        let response = {
            result: {
                code: enums.ResponseCodeEnum.INTERNAL_ERROR,
                message: 'Internal Error'
            },
            profile: {
                profile_id: null,
                login: null,
                language: null,
                firstName: null,
                lastName: null,
                balances: {},   // Dictionary
                settings: {}    // Dictionary
            }
        }
        
        try {
            if (proceed) {
                commandText = "SELECT login, language, firstName, lastName" +
                    " FROM xc_profiles" +
                    " WHERE profile_id = ?";

                parameters.length = 0;
                parameters.push(profile_id);

                dbResult = await dbpool.query(commandText, parameters);

                if (dbResult.success) {
                    if(dbResult.result[0].length > 0) {
                        let item = dbResult.result[0][0];

                        response.profile.profile_id = profile_id;
                        response.profile.login = item.login;
                        response.profile.language = item.language;
                        response.profile.firstName = item.firstName;
                        response.profile.lastName = item.lastName;
                    }
                } else {
                    console.log(dbResult.error);
                    response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                    response.result.message = "Database Error";
                    proceed = false;
                }
            }
            

            if (proceed) {
                let currenciesResponse = await this.getPlatformCurrencies();

                if (currenciesResponse.result.code === enums.ResponseCodeEnum.SUCCESS) {
                    console.log('Getting Balances');
                    commandText = "SELECT token, balance" +
                        " FROM treos_profile_token_balance" +
                        " WHERE profile_id = ?" +
                        " ORDER BY token ASC";

                    parameters.length = 0;
                    parameters.push(profile_id);

                    dbResult = await dbpool.query(commandText, parameters);

                    if (dbResult.success) {
                        for (let i = 0; i < dbResult.result[0].length; i++) {
                            let item = dbResult.result[0][i];

                            let coin = {
                                balance: item.balance,
                                currency: currenciesResponse.currencies[item.token]
                            }
                            response.profile.balances[item.token] = coin;
                        }
                    } else {
                        console.log(dbResult.error);
                        response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                        response.result.message = "Database Error";
                        proceed = false;
                    }
                } else {

                }

                
            }

            if (proceed) {
                let getConfigSettingsResponse = await this.getConfigSettings(['listing_fee_wishlist']);
                if (getConfigSettingsResponse.result.code === enums.ResponseCodeEnum.SUCCESS) {
                    response.profile.settings = getConfigSettingsResponse.settings;
                } else {
                    response.result = getConfigSettingsResponse.result;
                    proceed = false;
                }
            }
        } catch (err) {
            response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
            response.result.message = "Internal Error";
            console.log(err);
            proceed = false;
        } finally {
            if (proceed) {
                response.result.code = enums.ResponseCodeEnum.SUCCESS;
                response.result.message = null;
            }
        }
        return response;
    },   // getVendorProfile


    webScrapeProductInfo: async function(url) {
        'use strict';
    
        let commandText = '';
        let parameters = [];
        let dbResult = null;

        let proceed = true;
        

        let response = {
            result: {
                code: enums.ResponseCodeEnum.INTERNAL_ERROR,
                message: 'Internal Error'
            },
            scrapeResult: {
                imageURLs: [],
                headings: [],
                prices: []
            }
        }
        
        try {
            const scraper = require('./webScraper');

            let scrapeResponse = await scraper.scrapePage(url);
            if (scrapeResponse.result.code === enums.ResponseCodeEnum.SUCCESS) {
                response.scrapeResult.imageURLs = scraper.imageURLs;
                response.scrapeResult.headings = scraper.headings;
                response.scrapeResult.prices = scraper.prices;

                response.result.code = enums.ResponseCodeEnum.SUCCESS;
                response.result.message = null;
            } else {
                response.result = scrapeResponse.result;
            }

        } catch (err) {
            response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
            response.result.message = "Internal Error";
            console.log(err);
        }
        return response;
    },   // webScrapeProductInfo


    uploadWishlistImage: async function(profile_id, files) {
        'use strict';
    
        let commandText = '';
        let parameters = [];
        let dbResult = null;

        let proceed = true;
        

        let response = {
            result: {
                code: enums.ResponseCodeEnum.INTERNAL_ERROR,
                message: 'Internal Error'
            },
            imageURL: null
        }
        
        try {
            if (proceed) {
                if (files === null || files === undefined || Object.keys(files).length === 0) {

                }
            }
            
            if (proceed) {
                //let file = files['wishlistImageFile'];  // FormFieldName 
                let file = files.wishlistImageFile;  // FormFieldName 
                console.log(file);

                //let blah = await ipfs.addFileFromStream(file.data);
                let blah = await ipfs.addFile('/treos/wishlist/' + file.name, file.data);

                let newPath = path.join(appRoot, '/public/var/www/html/treos/uploads/wishlist', file.name);

                console.log('moving file');
                file.mv(newPath, function(err) {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log('Successful Move to ' + newPath);
                        response.result.code = enums.ResponseCodeEnum.SUCCESS;
                        response.result.message = null;
                    }
                });
                console.log('end of moving file');

                // try {
                //     fs.renameSync(file.name, newPath);
                //     console.log('Successful Move to ' + newPath);
                //     response.result.code = enums.ResponseCodeEnum.SUCCESS;
                //     response.result.message = null;
                // } catch (err) {
                //     console.log(err);
                // }
                
            }

        } catch (err) {
            response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
            response.result.message = "Internal Error";
            console.log(err);
        }
        return response;
    }, // uploadWishlistImage


    addWishlistItem: async function(profile_id, req) {
        'use strict';
    
        let conn = null;
        let commandText = '';
        let parameters = [];
        let dbResult = null;

        let proceed = true;

        let shippingAddress = null;
        let digitalEmailAddress = null;
        let collectionLocation = null;
        let exchangeDetails = null;



        let deliveryMethod = null;
        let shippingAddressId = null;
        let itemWebsiteURL = null;
        let itemTitle = null;
        let fiatCurrencyCode = null;
        let fiatItemPrice = null;
        let fiatShipping = null;
        let bonusPercentage = null;
        let totalTRO = null;
        let fundingWallet = null;
        let deliveryComment = null;
        
        let shippingAddresses = [];
        let wishlist_item_id = null;
        let uploadedFileHash = null;

        let response = {
            result: {
                code: enums.ResponseCodeEnum.INTERNAL_ERROR,
                message: 'Internal Error'
            }
        }
        
        try {
            if (proceed) {
                conn = await dbpool.getConnection();    // Create connection to be reused
                await dbpool.query('START TRANSACTION', [], conn);
            }

            if (proceed) {
                //console.log('req.body', req.body);
                let validationResponse = await this.addWishlistItem_Validate(conn, profile_id, req);
                if (validationResponse.result.code !== enums.ResponseCodeEnum.SUCCESS) {
                    response.result = validationResponse.result;
                    proceed = false;
                }
            }

            if (proceed) {
                switch (req.body.deliveryMethod) {
                    case 'standard': {
                        let getAddressesResponse = await this.getAddresses(profile_id, false, true, false);
                        if (getAddressesResponse.result.code === enums.ResponseCodeEnum.SUCCESS) {
                            for (let i = 0; i < getAddressesResponse.addresses.length; i++) {
                                if (getAddressesResponse.addresses[i].address_id == req.body.shippingAddressId) {
                                    shippingAddress = getAddressesResponse.addresses[i];
                                    break;
                                }
                            }
                            if (shippingAddress === null) {
                                response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
                                response.result.message = 'Unable to find shipping address.';
                                proceed = false;
                            }
                        } else {
                            response.result = getAddressesResponse.result;
                            proceed = false;
                        }
                        break;
                    }
                    case 'digital': {
                        digitalEmailAddress = req.body.digitalEmailAddress;
                        break;
                    }
                    case 'collection': {
                        collectionLocation = req.body.collectionLocation;
                        break;
                    }
                    case 'exchange': {
                        exchangeDetails = req.body.exchangeDetails;
                        break;
                    }
                    default: {
                        response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
                        response.result.message = 'Unknown delivery method';
                        proceed = false;
                        break;
                    }
                }
            }


            /* Add the Wishlist item to the database */
            if (proceed) {
                let url = new URL(req.body.itemWebsiteURL);

                commandText = "INSERT INTO treos_wishlist_item";

                commandText += " SET user_id = ?";                  parameters.push(profile_id);
                commandText += ", url = ?";                         parameters.push(req.body.itemWebsiteURL);
                commandText += ", site_domain = ?";                 parameters.push(url.hostname);
                commandText += ", preview_image = ?";               parameters.push('');
                commandText += ", heading = ?";                     parameters.push(req.body.itemTitle);
                commandText += ", description = ?";                 parameters.push('');
                commandText += ", value_range_start = ?";           parameters.push(1.0);
                commandText += ", value_range_end = ?";             parameters.push(100.0);
                commandText += ", value = ?";                       parameters.push(req.body.fiatItemPrice);
                commandText += ", shipping_value = ?";              parameters.push(req.body.fiatShipping);
                commandText += ", ships_to = ?";                    parameters.push('Ships World Wide');
                commandText += ", offer_value = ?";                 parameters.push(req.body.totalTRO);
                commandText += ", comment = ?";                     parameters.push(req.body.deliveryComment);
                commandText += ", shipping_first_name = ?";         parameters.push(shippingAddress === null ? null : shippingAddress.firstname);
                commandText += ", shipping_last_name = ?";          parameters.push(shippingAddress === null ? null : shippingAddress.lastname);
                commandText += ", shipping_address = ?";            parameters.push(shippingAddress === null ? null : shippingAddress.street);
                commandText += ", shipping_street = ?";             parameters.push('');
                commandText += ", shipping_state = ?";              parameters.push(shippingAddress === null ? null : shippingAddress.custom_state);
                commandText += ", shipping_country = ?";            parameters.push(shippingAddress === null ? null : shippingAddress.country_code.toUpperCase());
                commandText += ", shipping_phone_country_code = ?"; parameters.push('');
                commandText += ", shipping_phone_number = ?";       parameters.push(shippingAddress === null ? null : shippingAddress.phone);
                commandText += ", shipping_city = ?";               parameters.push(shippingAddress === null ? null : shippingAddress.city);
                commandText += ", shipping_zip = ?";                parameters.push(shippingAddress === null ? null : shippingAddress.zipcode);
                commandText += ", shipping_email = (SELECT login FROM xc_profiles WHERE profile_id = ?)";   parameters.push(profile_id);
                commandText += ", status = ?";                      parameters.push('LISTED');
                commandText += ", value_currency = ?";              parameters.push(req.body.fiatCurrencyCode);
                commandText += ", shipping_value_currency = ?";     parameters.push(req.body.fiatCurrencyCode);
                commandText += ", bonus_percentage = ?";            parameters.push(req.body.bonusPercentage);
                commandText += ", create_date = NOW()";
                commandText += ", upload_photo = ?";                parameters.push('');
                commandText += ", upload_proof = ?";                parameters.push('');
                commandText += ", cxl_item_price = ?";              parameters.push(0.0);
                commandText += ", cxl_item_shipping = ?";           parameters.push(0.0);
                commandText += ", ship_type = ?";                   parameters.push(req.body.deliveryMethod);
                commandText += ", ship_type_detail = ?";            parameters.push('');
                commandText += ", tro_wallet = ?";                  parameters.push(req.body.fundingWallet);

                let dbResponse = await dbpool.query(commandText, parameters, conn);

                if (dbResponse.success) {
                    wishlist_item_id = dbResponse.result[0].insertId;
                } else {
                    response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                    response.result.message = 'Database Error';
                    proceed = false;
                }
            }


            /* Add Escrow information */
            if (proceed) {
                commandText = '';
                parameters.length = 0;

                commandText = "INSERT INTO treos_wishlist_escrow" +
                    " SET date = NOW()" +
                    ", item_id = ?" +
                    ", tro_value = ?" +
                    ", tro_wallet = ?" +
                    ", fee = ?" +
                    ", status = ?";

                let calculator = require('../middleware/calculator_wishlist');
                calculator.calculate(parseInt(req.body.bonusPercentage) , parseFloat(req.body.totalTRO));

                parameters.push(wishlist_item_id);          // item_id
                parameters.push(tools.roundDown(calculator.totalTRO - calculator.listingFeeTRO, 4));         // tro_value    price + shipping + bonus + fee ???
                parameters.push(req.body.fundingWallet);                        // tro_wallet
                parameters.push(calculator.listingFeeTRO);     // fee
                parameters.push('NEW');                        // status

                let dbResponse = await dbpool.query(commandText, parameters, conn);

                if (!dbResponse.success) {
                    response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                    response.result.message = 'Database Error';
                    proceed = false;
                }
            }


            /* Update wallet balance */
            if (proceed) {
                let alterTokenResponse = await treosFunctions.alterTokenAmount(conn,
                    profile_id,
                    req.body.fundingWallet,
                    'ADD_WISHLIST',
                    Math.abs(parseFloat(req.body.totalTRO)));

                console.log(alterTokenResponse);

                if (alterTokenResponse.result.code !== enums.ResponseCodeEnum.SUCCESS) {
                    response.result = alterTokenResponse.result;
                    proceed = false;
                }
            }


            /* Upload image to IPFS */
            if (proceed) {
                if (Object.keys(req.files).length > 0) {
                    let file = req.files.itemImageUploaded;  // FormFieldName
                    try {
                        let ipfsResponse = await ipfs.addFile('/treos/wishlist/' + wishlist_item_id, file.data);
                        uploadedFileHash = ipfsResponse.hash;
                    } catch (err) {
                        console.log(err);
                        response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
                        response.result.message = "Failed to upload image file to IPFS";
                        proceed = false;
                    }
                } else {
                    console.log('No image file found to upload.')
                }
            }
            

            /* Update wishlist item with file hash */
            if (proceed) {
                commandText = '';
                parameters.length = 0;

                commandText = "UPDATE treos_wishlist_item" +
                    " SET preview_image = ?" +
                    " WHERE id = ?";

                parameters.push(uploadedFileHash);
                parameters.push(wishlist_item_id);

                let dbResponse = await dbpool.query(commandText, parameters, conn);

                if (!dbResponse.success) {
                    response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                    response.result.message = 'Database Error';
                    proceed = false;
                }
            }
        } catch (err) {
            response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
            response.result.message = "Internal Error";
            console.error(err);
            proceed = false;
        } finally {
            if (conn !== null) {
                if (proceed) {
                    await dbpool.query('COMMIT', [], conn);
                    response.result.code = enums.ResponseCodeEnum.SUCCESS;
                    response.result.message = null;
                } else {
                    await dbpool.query('ROLLBACK', [], conn);
                }

                conn.release();
                conn = null;
            }
        }
        return response;
    }, // addWishlistItem

    addWishlistItem_Validate: async function(conn, profile_id, req) {
        let response = {
            result: {
                code: enums.ResponseCodeEnum.INTERNAL_ERROR,
                message: 'Internal Error'
            }
        }
    
        let proceed = true;
        let validWallets = ['TRO', 'TRO_UBI'];
    
        try {
            /* ### Delivery Options ###*/
            if (proceed) {
                if (req.body.deliveryMethod === 'standard') {
                    let shippingAddress = null;
                    if (tools.isNullOrEmpty(req.body.shippingAddressId)) {
                        response.result.code = enums.ResponseCodeEnum.FORM_FIELD_REQUIRED;
                        response.result.message = 'Shipping Address must be specified';
                        proceed = false;
                    } else if (isNaN(req.body.shippingAddressId)) {
                        response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                        response.result.message = 'Invalid Shipping Address';
                        proceed = false;
                    } else {
                        let getAddressesResponse = await this.getAddresses(profile_id, false, true, false);
                        if (getAddressesResponse.result.code === enums.ResponseCodeEnum.SUCCESS) {
                            for (let i = 0; i < getAddressesResponse.addresses.length; i++) {
                                if (getAddressesResponse.addresses[i].address_id == req.body.shippingAddressId) {
                                    shippingAddress = getAddressesResponse.addresses[i];
                                    break;
                                }
                            }
                            if (shippingAddress === null) {
                                response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                                response.result.message = 'Invalid Shipping Address';
                                proceed = false;
                            } else {
                                if (tools.isNullOrEmpty(shippingAddress.firstname) && tools.isNullOrEmpty(shippingAddress.lastname)) {
                                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_REQUIRED;
                                    response.result.message = '<strong>Name</strong> is a required field in the shipping address.';
                                    proceed = false;
                                } else  if (tools.isNullOrEmpty(shippingAddress.street)) {
                                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_REQUIRED;
                                    response.result.message = '<strong>Address</strong> is a required field in the shipping address.';
                                    proceed = false;
                                } else if (tools.isNullOrEmpty(shippingAddress.city)) {
                                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_REQUIRED;
                                    response.result.message = '<strong>City</strong> is a required field in the shipping address.';
                                    proceed = false;
                                } else if (tools.isNullOrEmpty(shippingAddress.zipcode)) {
                                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_REQUIRED;
                                    response.result.message = '<strong>Zip Code</strong> is a required field in the shipping address.';
                                    proceed = false;
                                } else if (tools.isNullOrEmpty(shippingAddress.country)) {
                                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_REQUIRED;
                                    response.result.message = '<strong>Country</strong> is a required field in the shipping address.';
                                    proceed = false;
                                } else if (tools.isNullOrEmpty(shippingAddress.phone)) {
                                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_REQUIRED;
                                    response.result.message = '<strong>Phone Number</strong> is a required field in the shipping address.';
                                    proceed = false;
                                }
                            }
                        } else {
                            response.result = getAddressesResponse.result;
                            proceed = false;
                        }
    
                    }
    
                } else if (req.body.deliveryMethod === 'digital') {
                    if (tools.isNullOrEmpty(req.body.digitalEmailAddress)) {
                        response.result.code = enums.ResponseCodeEnum.FORM_FIELD_REQUIRED;
                        response.result.message = '<strong>Email Address</strong> must be specified.';
                        proceed = false;
                    }
                } else if (req.body.deliveryMethod === 'collection') {
                    if (tools.isNullOrEmpty(req.body.collectionLocation)) {
                        response.result.code = enums.ResponseCodeEnum.FORM_FIELD_REQUIRED;
                        response.result.message = '<strong>Collection Location</strong> must be specified.';
                        proceed = false;
                    }
                } else if (req.body.deliveryMethod === 'exchange') {
                    if (tools.isNullOrEmpty(req.body.exchangeDetails)) {
                        response.result.code = enums.ResponseCodeEnum.FORM_FIELD_REQUIRED;
                        response.result.message = '<strong>Exchange Details</strong> must be specified.';
                        proceed = false;
                    }
                } else {
                        response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                        response.result.message = 'Unknown Delivery Method "' + req.body.deliveryMethod + '"';
                        proceed = false;
                }
            } /* End of Delivery Options */
    
    
            /* ### Product Information ### */
            if (proceed) {
                if(tools.isNullOrEmpty(req.body.itemWebsiteURL)) {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_REQUIRED;
                    response.result.message = 'The website URL of the purchase item must be specified.';
                    proceed = false;
                } else if (Object.keys(req.files).length === 0) {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_REQUIRED;
                    response.result.message = 'An image must be selected or uploaded.';
                    proceed = false;
                } else if (tools.isNullOrEmpty(req.files.itemImageUploaded)) {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_REQUIRED;
                    response.result.message = 'An image must be selected or uploaded.';
                    proceed = false;
                } else if (tools.isNullOrEmpty(req.body.itemTitle)) {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_REQUIRED;
                    response.result.message = 'A title must be specified.';
                    proceed = false;
                }
            }   /* End of Product Information */
    
    
            /* ### Product Pricing ### */
            if (proceed) {
                let getFiatCurrenciesResponse = await this.getFiatCurrencies();
                let currency = getFiatCurrenciesResponse.currencies[req.body.fiatCurrencyCode];
                
                if (getFiatCurrenciesResponse.result.code !== enums.ResponseCodeEnum.SUCCESS) {
                    response.result = getFiatCurrenciesResponse.result
                    proceed = false;
                } else if (tools.isNullOrEmpty(req.body.fiatCurrencyCode)) {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_REQUIRED;
                    response.result.message = 'A currency code must be specified.';
                    proceed = false;
                } else if (tools.isNullOrEmpty(currency)) {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = 'Unknown fiat currency code "' + req.body.fiatCurrencyCode + '"';
                    proceed = false;
                } else if (tools.isNullOrEmpty(req.body.fiatItemPrice)) {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_REQUIRED;
                    response.result.message = 'The products price must be specified.';
                    proceed = false;
                } else if (isNaN(req.body.fiatItemPrice)) {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = 'Invalid product price.';
                    proceed = false;
                } else if (parseFloat(req.body.fiatItemPrice) <= 0.0) {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_REQUIRED;
                    response.result.message = 'The products price must greater than 0.';
                    proceed = false;
                } else if (tools.isNullOrEmpty(req.body.fiatShipping)) {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_REQUIRED;
                    response.result.message = 'The products shipping price must be specified.';
                    proceed = false;
                } else if (isNaN(req.body.fiatShipping)) {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = 'Invalid product shipping price.';
                    proceed = false;
                } else if (parseFloat(req.body.fiatShipping) < 0.0) {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = 'The product shipping price must be positive.';
                    proceed = false;
                } else if (tools.isNullOrEmpty(req.body.bonusPercentage)) {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_REQUIRED;
                    response.result.message = 'The bonus percentage must be specified.';
                    proceed = false;
                } else if (isNaN(req.body.bonusPercentage)) {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = 'Invalid bonus percentage.';
                    proceed = false;
                } else if (parseInt(req.body.bonusPercentage) < 3 || parseInt(req.body.bonusPercentage) > 8) {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = 'Bonus percentage outside of accepted range.';
                    proceed = false;
                } else if (tools.isNullOrEmpty(req.body.totalTRO)) {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_REQUIRED;
                    response.result.message = 'The Total Amount of TRO must be specified.';
                    proceed = false;
                } else if (isNaN(req.body.totalTRO)) {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = 'Invalid value for Total Amount of TRO.';
                    proceed = false;
                } else if (parseFloat(req.body.totalTRO) <= 0.0) {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = 'Invalid value for Total Amount of TRO.';
                    proceed = false;
                } else {
                    /*
                        Check to see if totalTRO passed from form at least covers the item and shipping price.
                        Trying to detect manipulation
                        Not including bonus or listing fee to allow a grace window in case tro_rate has changed.
                    */
                    let tro_rate = currency.tro_rate;
                    let totalFiat = parseFloat(req.body.fiatItemPrice) + parseFloat(req.body.fiatShipping);
                    let troValue = totalFiat * (1.0/tro_rate);
                    troValue = tools.roundUp(troValue, 4);

                    if (parseFloat(req.body.totalTRO) < troValue) {
                        response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                        response.result.message = 'Invalid value for Total Amount of TRO.';
                        proceed = false;
                    }
                }
            }   /* End of Product Pricing */
    
    
            /* ### Funding Wallet ### */
            if (proceed) {
                
                if (tools.isNullOrEmpty(req.body.fundingWallet)){
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_REQUIRED;
                    response.result.message = 'A funding wallet must be specified.';
                    proceed = false;
                } else if (!validWallets.includes(req.body.fundingWallet)) {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                    response.result.message = 'Invalid funding wallet specified "' + req.body.fundingWallet + '"';
                    proceed = false;
                } else {
                    let balance = 0.0;

                    commandText = "SELECT balance" +
                        " FROM treos_profile_token_balance" +
                        " WHERE profile_id = ?" +
                        " AND token = ?" +
                        " FOR UPDATE";  // Use FOR UPDATE to lock the token balance until we commit any changes. Prevent double spend.

                    let parameters = [];
                    parameters.push(profile_id);
                    parameters.push(req.body.fundingWallet);

                    let dbResponse = await dbpool.query(commandText, parameters, conn);

                    if (dbResponse.success) {
                        if (dbResponse.result[0].length > 0) {
                            balance = dbResponse.result[0][0].balance;
                        } else {
                            balance = 0.0;
                        }

                        if (balance < parseFloat(req.body.totalTRO)) {
                            response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                            response.result.message = 'Insufficient Funds';
                            proceed = false;
                        }
                    } else {
                        response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                        response.result.message = 'Database Error';
                        proceed = false;
                    }
                }
            }   /* End of Funding Wallet */
    
    
            /* ### Terms and Condition ### */
            if (proceed) {
                if (req.body.acceptTerms !== 'agree') {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_REQUIRED;
                    response.result.message = 'You must accept the terms and conditions';
                    proceed = false;
                }
            }
    
    
            if (proceed) {
                response.result.code = enums.ResponseCodeEnum.SUCCESS;
                response.result.message = null;
            }
        } catch (err) {
            console.error(err);
        }
        return response;
    },  // addWishlistItem_Validate

    convertCSV: async function(profile_id, req) {
        let proceed = true;
        let csvContents = null;
        let csv_site = null;

        let fileType = null;
        let currency = null;
        let fileContents = null;
        let tempFilePath = null;
        let knownCategories = {};   // Dictionary of known categories
        let platformCurrencies = null;

        let response = {
            result: {
                code: enums.ResponseCodeEnum.INTERNAL_ERROR,
                message: 'Internal Error'
            },
            tmpFilename: null,
            unknownCategories: []  // Array of unknown categories
        }
        
        try {
            if (proceed) {
                if (!tools.isNullOrEmpty(req.body.fileType)) {
                    fileType = req.body.fileType;
                } else {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_REQUIRED;
                    response.result.message = 'File Type not specified.';
                    proceed = false;
                }
            }

            if (proceed) {
                if (!tools.isNullOrEmpty(req.body.currency)) {
                    currency = req.body.currency;
                } else {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_REQUIRED;
                    response.result.message = 'Currency not specified.';
                    proceed = false;
                }
            }

            if (proceed) {
                if (req.files && Object.keys(req.files).length > 0) {
                    //let file = req.files.csvFile;  // FormFieldName
                    tempFilePath = req.files.csvFile.tempFilePath;
                } else {
                    if (!tools.isNullOrEmpty(req.body.tmpFilename)) {
                        tempFilePath = path.join(global.tempFileDir,req.body.tmpFilename);
                    } else {
                        response.result.code = enums.ResponseCodeEnum.FORM_FIELD_REQUIRED;
                        response.result.message = 'File not specified.';
                        proceed = false;
                    }
                }
            }

            if (proceed) {
                if (fs.existsSync(tempFilePath)) {
                    //file exists
                    response.tmpFilename = path.basename(tempFilePath);
                } else {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_REQUIRED;
                    response.result.message = 'Cached file not found. Please start again.';
                    proceed = false;
                }
            }

            if (proceed) {
                if (!tools.isNullOrEmpty(req.body.knownCategories)) {
                    knownCategories = JSON.parse(req.body.knownCategories);
                } else {
                    response.result.code = enums.ResponseCodeEnum.FORM_FIELD_REQUIRED;
                    response.result.message = 'Known Categories not specified.';
                    proceed = false;
                }
            }

            if (proceed) {
                // PapaParse does not have async await so need to implement Promise
                const parseFile = function(rawFile) {
                    return new Promise(function(resolve) {
                        let response = {
                            result: {
                                code: enums.ResponseCodeEnum.INTERNAL_ERROR,
                                message: 'Internal Error'
                            },
                            csv: null
                        }

                        let file = fs.createReadStream(tempFilePath);

                        Papa.parse(file, {
                            delimiter: "",	// auto-detect
                            newline: "",	// auto-detect
                            quoteChar: '"',
                            escapeChar: '"',
                            //header: false,
                            header: true,
                            transformHeader: undefined,
                            //dynamicTyping: false,
                            dynamicTyping: true,
                            preview: 0,
                            encoding: "utf8",
                            worker: false,
                            comments: false,
                            step: undefined,
                            download: false,
                            //download: true,
                            downloadRequestHeaders: undefined,
                            skipEmptyLines: true,
                            chunk: undefined,
                            fastMode: undefined,
                            beforeFirstChunk: undefined,
                            withCredentials: undefined,
                            transform: undefined,
                            delimitersToGuess: [',', '\t', '|', ';', Papa.RECORD_SEP, Papa.UNIT_SEP],
                            //complete: undefined,
                            complete: function(results) {
                                response.result.code = enums.ResponseCodeEnum.SUCCESS;
                                response.result.message = null;
                                response.csv = results.data;
                                console.log('success parse');
                                resolve(response);
                            },
                            //error: undefined,
                            error: function(err) {
                                response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
                                response.result.message = 'Error parsing csv file';
                                console.error(err);
                                console.log('error parsing csv file');
                            }
                        })
                    });
                }

                let parsedResponse = await parseFile(tempFilePath);

                //console.log(parsedResponse);

                if (parsedResponse.result.code === enums.ResponseCodeEnum.SUCCESS) {
                    fileContents = parsedResponse.csv;
                } else {
                    response.result.code = parsedResponse.result.code;
                    response.result.message = parsedResponse.result.message;
                    proceed = false;
                }
            }

            if (proceed) {
                let currenciesResponse = await this.getPlatformCurrencies();
                if (currenciesResponse.result.code === enums.ResponseCodeEnum.SUCCESS) {
                    platformCurrencies = currenciesResponse.currencies;
                } else {

                }
            }

            if (proceed) {
                switch (req.body.fileType) {
                    case '2': { // Shopify
                        csv_site = new CSV_Shopify();
                        break;
                    }
                    default: {
                        response.result.code = enums.ResponseCodeEnum.FORM_FIELD_INVALID;
                        response.result.message = 'Unknown File Type.';
                        proceed = false;
                        break;
                    }
                }
            }

            if (proceed) {
                csv_site.Currencies = platformCurrencies;
                csv_site.ProcessCSVFile(fileContents, knownCategories, currency);
                response.unknownCategories = csv_site.UnknownCategories;

                if (response.unknownCategories.length === 0) {
                    response.csv = await csv_site.GenerateCSVObject();
                }
            }


            // Clean up the tmp directory
            let tempFileDir = global.tempFileDir;
            fs.readdir(tempFileDir, function (err, files) {
                //handling error
                if (err) {
                    console.log('Unable to scan temp directory "' + tempFileDir + '": ' + err);
                } else {
                    console.log(files);
                    files.forEach(function (file) {
                        let filePath = path.join(tempFileDir,file);

                        const { mtime, ctime } = fs.statSync(filePath)

                        let timeNow = new Date().getTime();
                        let endTime = new Date(ctime).getTime() + 3600000;  // Older than an hour

                        if (timeNow > endTime) {
                            try {
                                fs.unlinkSync(filePath);
                                console.log('Deleted temp file: ' + file); 
                            } catch(err) {
                                console.error(err)
                            }
                        }
                    });
                }
            });
            
        } catch(err) {
            console.error(err);
            response.csv = null;
            proceed = false;
        } finally {
            if (proceed) {
                response.result.code = enums.ResponseCodeEnum.SUCCESS;
                response.result.message = null;
            }

        }

        return response;
    }   // convertCSV


}   // module.exports







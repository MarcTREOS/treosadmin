const enums = require('../middleware/enums');
const tools = require('../middleware/tools');
const api_vendor_functions = require('../middleware/api.vendor.functions');

module.exports = {
    /* Properties */
    bonusPercentage: 0,
    bonusTRO: 0.0,
    listingFeePercentage: 2,
    listingFeeTRO: 0.0,
    totalTRO: 0.0,

    /* Methods */
    calculate: async function(bonusPercentage, totalTRO) {
        let getConfigSettingsResponse = await api_vendor_functions.getConfigSettings(['listing_fee_wishlist']);
        if (getConfigSettingsResponse.result.code === enums.ResponseCodeEnum.SUCCESS) {
            this.listingFeePercentage = getConfigSettingsResponse.settings['listing_fee_wishlist'];
            if (!tools.isNullOrEmpty(this.listingFeePercentage)) {
                this.listingFeePercentage = tools.roundDown(this.listingFeePercentage * 100.0, 0);
            } else {
                this.listingFeePercentage = 2.0;
            }
        }

        this.bonusPercentage = bonusPercentage;
        this.totalTRO = totalTRO;

        let totalPercentage = 100.0 + bonusPercentage + this.listingFeePercentage;

        let tro100Percent = totalTRO / (totalPercentage / 100); // 110 / 1.1 = 100 <-- before fees
        tro100Percent = tools.roundUp(tro100Percent, 4);
    
        let totalFees = totalTRO - tro100Percent;
        totalFees = tools.roundDown(totalFees, 4);    // roundDown to avoid float errors
    
        this.listingFeeTRO = tro100Percent * (this.listingFeePercentage / 100);
        this.listingFeeTRO = tools.roundUp(this.listingFeeTRO, 4);    // Round up to make sure we always get our listing fee
    
        this.bonusTRO = totalFees - this.listingFeeTRO;
        this.bonusTRO = tools.roundDown(this.bonusTRO, 4);    // roundDown to avoid float errors
    }   // calculate
}   // module.exports


const tools = require('../middleware/tools');

module.exports = class CSV_Master {
    constructor() {
        this.XCartItems = [];
        this.KnownCategories = {};  // Dictionary
        this.UnknownCategories = [];
        this.CurrencyCode = null;
        this.Currencies = null;
    } 

    // set name(name) {
    //     this._name = name.charAt(0).toUpperCase() + name.slice(1);
    // }

    // get name() {
    //     return this._name;
    // }

    /* Properties */
    // XCartItems : [],

    /* Methods */

    // Convert weights to kg
    ConvertToKg(weight, weightUnit) {
        let result = null;
        if (weight !== undefined && weight !== null && !isNaN(weight)) {

            if (weightUnit.toLowerCase() === 'g') {
                result = parseFloat(weight) / 1000.0;
            }

            // Roundup to 2 dp
            if (result !== null) {
                result = result * 100;
                result = Math.ceil(result);
                result = result / 100;
            }
        }
        return result;
    }

    ClearCSVfile() {
        this.XCartItems.length = 0;
        this.UnknownCategories.length = 0;
        this.CurrencyCode = null;

        // Remove each property in the dictionary
        tools.clearObject(this.KnownCategories);
        // for (var prop in this.KnownCategories) {
        //     if (this.KnownCategories.hasOwnProperty(prop)) {
        //         delete this.KnownCategories[prop];
        //     }
        // }
    }

    AddToXCartArray(UniqueProductIdentifier = null,
        SkuCommon = null,       // If there are variants, this is the common SKU between them
        Sku = null,
        Name = null,
        Price = null,
        MarketPrice = null,
        Description = null,
        BriefDescription = null,
        Weight = null,
        Shippable = null,
        Images = null,
        VariantImage = null,    // Only to be used when it is a Variant
        ImagesAlt = null,
        InventoryTrackingEnabled = null,
        StockLevel = null,
        Attributes = null,
        Sale = null,
        UpcISBN = null,
        Manufacturer = null,
        RelatedProducts = null,
        ShipForFree = null,     // Enables/disables the 'Free shipping' option for a product
        FreeShipping = null,    // Enables/disables the 'Exclude from shipping cost calculation' option for a product
        FreightFixedFee = null,
        UseSeparateBox = null,
        BoxWidth = null,        // Width of box in mm
        BoxHeight = null,       // Height of box in mm
        BoxLength = null,       // Length of box in mm
        ItemsPerBox = null) {
        
            let xCartItem = {};

            xCartItem.sku = UniqueProductIdentifier || '';
            xCartItem.price = Price || '';
            xCartItem.weight = Weight || '';
            xCartItem.shippable = Shippable || '';
            xCartItem.images = Images || '';
            xCartItem.imagesAlt = ImagesAlt || '';
            xCartItem.inventoryTrackingEnabled = InventoryTrackingEnabled || '';
            xCartItem.stockLevel = StockLevel || '';
            xCartItem.useSeparateBox = UseSeparateBox || '';
            xCartItem.boxWidth = BoxWidth || '';
            xCartItem.boxLength = BoxLength || '';
            xCartItem.boxHeight = BoxHeight || '';
            xCartItem.itemsPerBox = ItemsPerBox || '';
            xCartItem.name = Name || '';
            xCartItem.description = Description || '';
            xCartItem.briefDescription = BriefDescription || '';

            xCartItem.shipForFree = ShipForFree || '';
            xCartItem.freeShipping = FreeShipping || '';
            xCartItem.freightFixedFee = FreightFixedFee || '';
            xCartItem.sale = Sale || '';
            xCartItem.marketPrice = MarketPrice || '';
        
            this.XCartItems.push(xCartItem);
    }

    GetArributeColumnName(attribute) {
        let result = '';
        switch (attribute.toLowerCase()) {
            case 'color': {
                result = attribute.toLowerCase() + ' (field:global)';
                break;
            }
            case 'size': {
                result = attribute.toLowerCase() + ' (field:global)';
                break;
            }
            default: {
                result = attribute + ' (field:product)';
                break;
            }
        }
        return result;
    }

    GenerateCSVRow(attributes) {
        let result = {};

        result.sku = null;
        result.price = null;
        result.weight = null;
        result.shippable = null;
        result.images = null;
        result.imagesAlt = null;
        result.categories = null;
        result.inventoryTrackingEnabled = null;
        result.stockLevel = null;
        result.name = null;
        result.description = null;
        result.briefDescription = null;

        for (let i = 0; i < attributes.length; i++) {
            result[this.GetArributeColumnName(attributes[i])] = null;
        }

        //result.variantID = null;
        result.variantSKU = null;
        result.variantPrice = null;
        result.variantQuantity = null;
        result.variantWeight = null;
        result.variantImage = null;
        result.variantImageAlt = null;

        result.shipForFree = null;
        result.freeShipping = null;
        result.freightFixedFee = null;
        result.marketPrice = null;

        return result;
    }

    // Get all of the attribute names for every product
    GetAllAttributeColumns() {
        let result = [];

        for (let productIndex = 0; productIndex < this.XCartItems.length; productIndex++) {
            let product = this.XCartItems[productIndex];

            for (let variantIndex = 0; variantIndex < product.Variants.length; variantIndex++) {
                let variant = product.Variants[variantIndex];

                for (let attributeIndex = 0; attributeIndex < variant.Attributes.length; attributeIndex++) {
                    let foundAttribute = false;
                    for (let resultIndex = 0; resultIndex < result.length; resultIndex++) {
                        if (result[resultIndex] === variant.Attributes[attributeIndex].Name) {
                            foundAttribute = true;
                            break;
                        }
                    }

                    if (!foundAttribute) {
                        result.push(variant.Attributes[attributeIndex].Name);
                    }
                }

                
            }
        }

        return result;
    }

    async CalculateFinalPrice(basePrice) {
        'use strict';
        let result = null;

        if (basePrice !== undefined && basePrice !== null && !isNaN(basePrice)) {
            if (this.CurrencyCode && this.Currencies) {
                let currency = this.Currencies[this.CurrencyCode];

                if (currency) {
                    let troRate = currency.tro_rate;

                    result = parseFloat(basePrice);

                    result = result / troRate;

                    result = tools.roundUp(result, 4);
                }
                
                // if (MarginPercentage > 0.0m) {
                //     result = result + (result * (MarginPercentage / 100.0m));
                // }

                // if (MarginFixedAmount > 0.0m) {
                //     result = result + MarginFixedAmount;
                // }

                // if (TROExchangeRate != 0.0m) {
                //     result = result / TROExchangeRate;  // Convert to TRO
                // }
            }
        }

        return result;
    }

    async GenerateCSVObject() {
        let result = [];
        let allAttributes = [];
        try {
            allAttributes = this.GetAllAttributeColumns();

            for (let productIndex = 0; productIndex < this.XCartItems.length; productIndex++) {
                let product = this.XCartItems[productIndex];
    
                for (let variantIndex = 0; variantIndex < product.Variants.length; variantIndex++) {
                    let variant = product.Variants[variantIndex];

                    let row = this.GenerateCSVRow(allAttributes);

                    row.sku = product.Sku.toString();

                    for (let attributeIndex = 0; attributeIndex < variant.Attributes.length; attributeIndex++) {
                        row[this.GetArributeColumnName(variant.Attributes[attributeIndex].Name)] = variant.Attributes[attributeIndex].Value;
                    }

                    if (variantIndex === 0) {
                        row.briefDescription = product.BriefDescription;
                        row.description = product.Description;
                        row.marketPrice = await this.CalculateFinalPrice(variant.MarketPrice);
                        row.name = product.Name;
                        row.price = await this.CalculateFinalPrice(variant.Price);
                        if (product.ShipForFree === true) {
                            row.shipForFree = 'Yes'
                        } else {
                            row.shipForFree = 'No'
                        }
                        row.stockLevel = variant.Quantity;
                        row.weight = variant.Weight;
                        
                        for (let imageIndex = 0; imageIndex < product.Images.length; imageIndex++) {
                            if (row.images === null) {
                                row.images = '';    // remove null
                            }
                            if (imageIndex > 0) {
                                row.images += '&&';
                            }
                            row.images += product.Images[imageIndex];
                        }
                    }

                    if (product.Variants.length > 1) {
                        row.variantSKU = variant.Sku.toString();
                        row.variantPrice = await this.CalculateFinalPrice(variant.Price);
                        row.variantQuantity = variant.Quantity;
                        row.variantWeight = variant.Weight;

                        for (let imageIndex = 0; imageIndex < variant.Images.length; imageIndex++) {
                            if (row.variantImage === null) {
                                row.variantImage = '';
                            }
                            if (imageIndex > 0) {
                                row.variantImage += '&&';
                            }
                            row.variantImage += variant.Images[imageIndex];
                        }

                        // TODO: Variant Images Alt

                    }

                    result.push(row);
                }
    
            }
        } catch(err) {
            console.error(err);
            result.length = 0;
        }

        return result;
    }

}   // module.exports
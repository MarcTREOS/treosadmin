const tools = require('../middleware/tools');
const enums = require('../middleware/enums');
const CSV_Master = require('../middleware/csv_master');
const CSV_XCartProduct = require('../middleware/csv_xcart_product');
const CSV_XCartVariant = require('../middleware/csv_xcart_variant');
const CSV_XCartAttribute = require('../middleware/csv_xcart_attribute');


module.exports = class CSV_Shopify extends CSV_Master {

    /* Methods */
    ProcessCSVFile(items, knownCategories, currencyCode) {
        this.ClearCSVfile();
        this.CurrencyCode = currencyCode;

        for (let row = 0; row < items.length; row++) {
            let item = items[row];
            
            if (item.Handle !== null && item.Handle !== undefined) {
                this.AddToXCartArray(item, knownCategories);
            }
        }
    }


    AddToXCartArray(item, knownCategories) {
        let productIndex = null;

        for (let i = 0; i < this.XCartItems.length; i++) {
            if (this.XCartItems[i].UniqueProductIdentifier === item.Handle.toString()) {
                // Found existing product
                productIndex = i;
                break;
            }
        }

        let variant = new CSV_XCartVariant();
        variant.Sku = item['Variant SKU'];  // Max length 32 characters
        variant.Price = item['Variant Price'];
        if (item['Variant Requires Shipping'] != 'TRUE') {
            variant.Shippable = false;
        }
        if (!tools.isNullOrEmpty(item['Variant Image'])) {
            variant.Images.push(item['Variant Image']);
        }

        if (!tools.isNullOrEmpty(item['Option1 Value'])) {
            if (productIndex !== null) {
                // Has a parent
                let parentAttribute = this.XCartItems[productIndex].Variants[0].Attributes[0];

                let attribute = new CSV_XCartAttribute();
                attribute.Name = parentAttribute.Name;
                attribute.Value = item['Option1 Value'];
                variant.Attributes.push(attribute);
            } else {
                // Does not have a parent
                if (!tools.isNullOrEmpty(item['Option1 Name'])) {
                    let attribute = new CSV_XCartAttribute();
                    attribute.Name = item['Option1 Name'];
                    attribute.Value = item['Option1 Value'];
                    variant.Attributes.push(attribute);
                }
            }
        }

        variant.Weight = this.ConvertToKg(item['Variant Grams'], 'g');

        if (productIndex !== null) {
            let product = this.XCartItems[productIndex];

            // Add additional images
            if (!tools.isNullOrEmpty(item['Image Src'])) {
                product.Images.push(item['Image Src']);
            }

            if (!tools.isNullOrEmpty(variant.Sku)) {
                product.Variants.push(variant);
            } else {
                // Is additional information (Images etc) for the product
                // Do not add
            }
        } else {
            let product = new CSV_XCartProduct();

            product.UniqueProductIdentifier = item.Handle.toString();
            
            // Sku Max length 32 characters
            product.Sku = item['Variant SKU'].split('-')[0]; // Tries to parse a unique SKU from the Variant_SKU field
            product.Description = item['Body (HTML)'];
            
            product.Name = item.Title;

            if (!tools.isNullOrEmpty(item['Image Src'])) {
                product.Images.push(item['Image Src']);
            }

            if (item['Variant Requires Shipping'] === 'TRUE') {
                product.Shippable = 'Yes';
            }
            
            product.Variants.push(variant);

            // this.AddToXCartArray(
            //     UniqueProductIdentifier = item.Handle.toString(),
            //     Description = item['Body (HTML)'],
            //     Name = item.Title,
            // );

            this.XCartItems.push(product);
        }
    }
}   // module.exports

/* ------------- END OF MODULE ------------- */
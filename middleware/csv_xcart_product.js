module.exports = class CSV_XCartProduct {
    constructor() {
        this.UniqueProductIdentifier = null;
        this.Sku = null;    // Max length 32 characters
        this.Name = null;
        this.BriefDescription = null;
        this.Description = null;
        this.ShipForFree = null;
        this.Shippable = null;
        this.Images = [];   // Images specific to the product family.
        this.Variants = [];
    } 
}
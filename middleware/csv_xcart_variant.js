module.exports = class CSV_XCartVariant {
    constructor() {
        this.Sku = null;    // Max length 32 characters
        this.MarketPrice = null;
        this.Price = null;
        this.Quantity = null;
        this.Shippable = true;
        this.Images = [];   // Images specific to the variant (Only 1 will be used).
        this.Weight = null;
        this.Attributes = [];
    } 
}
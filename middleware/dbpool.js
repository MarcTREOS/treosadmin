const mysql = require('mysql2/promise');
const dotenv = require('dotenv');

dotenv.config();

const pool = mysql.createPool({
    host: process.env.DB_HOST || 'localhost',
    user: process.env.DB_USER || 'myuser',
    password: process.env.DB_PASSWORD || "mypass",
    database: process.env.DB_DATABASE || 'TRO_db',
    waitForConnections: true,
    connectionLimit: process.env.DB_CONNECTIONLIMIT || 10,
    queueLimit: 0
});

module.exports = {
    // Properties
    //connection: null,

    // Methods
    getConnection: async function() {
        'use strict';
        return await pool.getConnection();
    },  // getConnection


    query: async function(
            commandText,    // SQL statement string
            parameters,     // array of parameters.  If no parameters, use []
            connection      // if re-using a connection, pass the existing connection here.  Otherwise ignore or pass nul.
        ) {
        'use strict';
        let conn = null;
        let isLocalConnection = true;
        let proceed = true;

        let response = {
            success: false,
            result: null,
            error: null
        }

        try {
            if (connection !== undefined && connection !== null) {
                // Use connection that is passed in.  It will not be released here.
                conn = connection;
                isLocalConnection = false;
            } else {
                try {
                    // Create local connection which will be released afterwards
                    isLocalConnection = true;
                    conn = await pool.getConnection();
                } catch (ex) {
                    response.error = 'Database Error';
                    proceed = false;

                    let errMsg = 'Error getting connection from pool. ';
                    errMsg += ex;
                    console.log(errMsg);
                }
            }

            if (proceed) {
                //const [rows, fields] = await conn.query(commandText, parameters);
                response.result = await conn.query(commandText, parameters);
                response.success = true;
            }
        } catch (err) {
            let errMsg = 'Error processing query "' + commandText + '".';
            errMsg += ' With parameters: [' + parameters + '].';
            errMsg += err;
            response.error = errMsg;
            console.log(errMsg);
        } finally {
            // If the connection was created locally, we need to release it.
            if (isLocalConnection && conn !== null) {
                conn.release();
                conn = null;
            }
        }

        return response;
    }   // query
} // module.exports



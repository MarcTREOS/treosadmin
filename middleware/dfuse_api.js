const { createDfuseClient, waitFor, dynamicMessageDispatcher } = require("@dfuse/client")
const fetch = require('node-fetch');

const { Api, JsonRpc, RpcError } = require('eosjs');
const { JsSignatureProvider } = require('eosjs/dist/eosjs-jssig');      // development only
const { TextEncoder, TextDecoder } = require('util');                   // node only; native TextEncoder/Decoder
//const { TextEncoder, TextDecoder } = require('text-encoding');                   // node only; native TextEncoder/Decoder
//const { TextEncoder, TextDecoder } = require('text-encoding');

const path = require('path');

const enums = require('../middleware/enums');
const tools = require('../middleware/tools');


module.exports = {
    // Properties
    //profile_id: null,
    //token: null,
    jwt_token: null,
    jwt_expires_at: null,

    client: null,

    // Methods
    createClient() {
        if (this.client === null) {
            this.client = createDfuseClient({
                //apiKey: 'server_80c2f549261a1ac3be775829517d97b7',  // TREOS
                //apiKey: 'server_af7e47baa964b247803c396002ece5c9',  // Marc
                apiKey: process.env.DFUSE_API_KEY || '',
                network: 'mainnet'
            })
        }
    },

    jwtHasExpired: function() {
        let result = true;
        if (this.jwt_expires_at !== null) {
            let d = new Date(0);
            let dateNow = new Date(0);
            d.setUTCSeconds(jwt_expires_at);
            dateNow.setUTCMilliseconds(Date.now());
            let dateExpires = dateNow.setMinutes(dateNow.getMinutes() - 10);

            if (Date.now() <= dateExpires) {
                reult = false;
            }
        }
        return result;
    },

    getJWT: async function() {
        if (this.jwtHasExpired()) {
            var tokenResponse = await fetch("https://auth.dfuse.io/v1/auth/issue", {
                method: "POST",
                body: JSON.stringify({
                    //api_key: "server_af7e47baa964b247803c396002ece5c9"    // Marc
                    api_key: "server_80c2f549261a1ac3be775829517d97b7"      // TREOS
                }),
                headers: {
                    "Content-Type": "application/json"
                }
            }); 

            if (tokenResponse.status === 200 /* OK */) {
                let jsonResponse = await tokenResponse.json();
                this.jwt_token = jsonResponse.token;
                this.jwt_expires_at = jsonResponse.expires_at;   // Cache JWT (for up to 24 hours)
                console.log('expires_at=' + this.jwt_expires_at);
            } else {
                console.log('token Response=' + tokenResponse.status + ' - ' + tokenResponse.statusText);
            }
        }
        return this.jwt_token;
    },

    accountExists: async function(accountName) {
        'use strict';
        let response = true;    // Default to true just in case
        //try {
            if (accountName !== undefined && accountName !== null) {
                this.createClient();

                /*
                    stateTable<T = unknown>(account: string, scope: string, table: string, options?: {
                        blockNum?: number;
                        json?: boolean;
                        keyType?: StateKeyType;
                        withBlockNum?: boolean;
                        withAbi?: boolean;
                    }): Promise<StateResponse<T>>;
                */

                // https://github.com/dfuse-io/client-js/blob/master/examples/advanced/has-account.ts
                let resp = await this.client.stateTable('eosio', accountName, 'userres');

                // if we get at least one row, the account exists.  Otherwise it doesn't.
                if (resp.rows.length > 0) {
                    response = true;
                } else {
                    response = false;
                }
            } else {
                console.log('dfuse_api.accountExists - EOS accountName = ' + accountName);
            }
        // ### Disabled tryCatch so that error could be trapped and return proper response.
        // } catch (err) {
        //     console.log('dfuse_api.accountExists - Error in accountExists.');
        //     console.error(err);
        // }
        return response;
    },

    accountBalances: async function(account, symbol) {
        'use strict';
        // https://docs.dfuse.io/reference/eosio/graphql/#query-accountbalances
        // https://www.dfuse.io/en/blog/how-to-get-token-lists-and-balances-on-eosio
        // https://docs.dfuse.io/reference/eosio/graphql/#query-tokenbalances
        // https://mainnet.eos.dfuse.io/graphiql/?query=cXVlcnkgKCRhY2NvdW50OiBTdHJpbmchLCAkbGltaXQ6IFVpbnQzMiwgJG9wdHM6IFtBQ0NPVU5UX0JBTEFOQ0VfT1BUSU9OIV0pIHsKICBhY2NvdW50QmFsYW5jZXMoYWNjb3VudDogJGFjY291bnQsIGxpbWl0OiAkbGltaXQsIG9wdGlvbnM6ICRvcHRzLCB0b2tlblN5bWJvbHM6IFsiVFJPIiwgIkVPUyJdKSB7CiAgICBibG9ja1JlZiB7CiAgICAgIGlkCiAgICAgIG51bWJlcgogICAgfQogICAgcGFnZUluZm8gewogICAgICBzdGFydEN1cnNvcgogICAgICBlbmRDdXJzb3IKICAgIH0KICAgIGVkZ2VzIHsKICAgICAgbm9kZSB7CiAgICAgICAgYWNjb3VudAogICAgICAgIGNvbnRyYWN0CiAgICAgICAgc3ltYm9sCiAgICAgICAgcHJlY2lzaW9uCiAgICAgICAgYmFsYW5jZQogICAgICB9CiAgICB9CiAgfQp9Cg==&variables=ewogICJhY2NvdW50IjogImp1bmdsZW1vbmtleSIsCiAgIm9wdHMiOiBbIkVPU19JTkNMVURFX1NUQUtFRCJdLAogICJsaW1pdCI6IDAKfQ==
        try {
            if (account !== undefined && account !== null) {
                this.createClient();

                // https://docs.dfuse.io/reference/eosio/graphql/#query-accountbalances
                // https://github.com/dfuse-io/client-js
                // https://docs.dfuse.io/guides/ethereum/getting-started/javascript-quickstart/
                const operation = `query ($account: String!, $tokenSymbols: [String!], $limit: Uint32, $opts: [ACCOUNT_BALANCE_OPTION!]) {
                      accountBalances(account: $account, limit: $limit, tokenSymbols: $tokenSymbols, options: $opts) {
                      blockRef {
                        id
                        number
                      }
                      pageInfo {
                        startCursor
                        endCursor
                      }
                      edges {
                        node {
                          account
                          contract
                          symbol
                          precision
                          balance
                        }
                      }
                    }
                  }`;

                var variables = {
                    variables: {
                        account: 'junglemonkey',
                        tokenSymbols: ['TRO', 'EOS']
                    }
                }

                // https://docs.dfuse.io/guides/ethereum/tutorials/search/
                let response = await this.client.graphql(operation, variables);
                //console.log(response);

                if (!response.errors) {
                    let accountBalances = response.data.accountBalances;
                    let edges = accountBalances.edges;
                    for (let i = 0; i < edges.length; i++) {
                        let edge = edges[i];
                        let details = edge.node;
                        console.log('account=' + details.account + ', symbol=' + details.symbol + ', balance=' + details.balanc);
                    }
                } else {
                    for (let i = 0; i < response.errors.length; i++) {
                        console.log('Error: ' + response.errors[i].message);
                    }
                }

                // , function(message) {
                //     if (message.type === 'data') {
                //         console.log(message.data);

                //         // Mark stream at cursor location, on re-connect, we will start back at cursor
                //         stream.mark({ cursor });
                //     }

                //     if (message.type === 'error') {
                //         console.log('An error occurred', message.errors, message.terminal);
                //     }

                //     if (message.type === 'complete') {
                //         console.log('Completed');
                //     }
                // })
                    
                // Waits until the stream completes, or forever
                // await stream.join();
                // await this.client.release;
                


                // var response = await client.graphql(query, (message, stream) => {
                //     if (message.type === "error") {
                //         console.log("An error occurred", message.errors, message.terminal)
                //     }

                //     if (message.type === "data") {
                //         const data = message.data.searchTransactionsForward
                //         const actions = data.trace.matchingActions

                //         actions.forEach(({ json }: any) => {
                //             const { from, to, quantity, memo } = json
                //             console.log(`Transfer [${from} -> ${to}, ${quantity}] (${memo})`)
                //         })

                //         stream.mark({ cursor: data.cursor })
                //     }

                //     if (message.type === "complete") {
                //         console.log("Stream completed")
                //     }
                // });

            } else {
                console.log('dfuse_api.accountBalances - EOS accountName = ' + account);
            }
        } catch (err) {
            console.log('dfuse_api.accountBalances - Error in accountBalances.');
            console.error(err);
        }
    },


    tokenBalances: async function(contract, symbol) {
        'use strict';
        // https://docs.dfuse.io/reference/eosio/graphql/#query-tokenbalances
        // https://www.dfuse.io/en/blog/how-to-get-token-lists-and-balances-on-eosio
        // https://mainnet.eos.dfuse.io/graphiql/?query=cXVlcnkoJGNvbnRyYWN0OiBTdHJpbmchLCRzeW1ib2w6U3RyaW5nISwkbGltaXQ6CVVpbnQzMiwgJG9wdHM6IFtBQ0NPVU5UX0JBTEFOQ0VfT1BUSU9OIV0pIHsKICB0b2tlbkJhbGFuY2VzKGNvbnRyYWN0OiAkY29udHJhY3QsIHN5bWJvbDogJHN5bWJvbCxsaW1pdDogJGxpbWl0LCBvcHRpb25zOiAkb3B0cykgewogICAgYmxvY2tSZWYgewogICAgICBpZAogICAgICBudW1iZXIKICAgIH0KICAgIHBhZ2VJbmZvIHsKICAgICAgc3RhcnRDdXJzb3IKICAgICAgZW5kQ3Vyc29yCiAgICB9CiAgICBlZGdlcyB7CiAgICAgIG5vZGUgewogICAgICAgIGFjY291bnQKICAgICAgICBjb250cmFjdAogICAgICAgIHN5bWJvbAogICAgICAgIHByZWNpc2lvbgogICAgICAgIGFtb3VudAogICAgICAgIGJhbGFuY2UKICAgICAgfQogICAgfQogIH0KfQ==&variables=ewogICJjb250cmFjdCI6ICJlb3Npby50b2tlbiIsCiAgInN5bWJvbCI6ICJFT1MiLAogICJvcHRzIjogWyJFT1NfSU5DTFVERV9TVEFLRUQiXSwKICAibGltaXQiOiAxMAp9
        
        let response = {
            result: {
                code: enums.ResponseCodeEnum.INTERNAL_ERROR,
                message: 'Internal Error'
            },
            accounts: []
        };

        try {
            if (contract !== undefined && contract !== null && symbol !== undefined && symbol !== null) {
                this.createClient();

                let cursor = '';
                let startCursor1 = '';
                let startCursor2 = '';
                let loopCounter = 0;
                let totalAccounts = 0;

                do {
                    //console.log('### Loop Start');
                    loopCounter++;

                    if (cursor !== '') {
                        startCursor1 = ', $cursor: String!';
                        startCursor2 = ', cursor: $cursor';
                    } else {
                        startCursor1 = '';
                        startCursor2 = '';
                    }

                    const operation = `query($contract: String!, $symbol:String!, $tokenHolders: [String!]` + startCursor1 + `, $limit: Uint32, $opts: [ACCOUNT_BALANCE_OPTION!]) {
                        tokenBalances(contract: $contract, symbol: $symbol, tokenHolders: $tokenHolders, ` + startCursor2 + `, limit: $limit, options: $opts) {
                          blockRef {
                            id
                            number
                          }
                          pageInfo {
                            startCursor
                            endCursor
                          }
                          edges {
                            node {
                              account
                              contract
                              symbol
                              precision
                              balance
                            }
                          }
                        }
                      }`;

                    let variables = {
                        variables: {
                            contract: contract,
                            symbol: symbol,
                            //tokenHolders: ['junglemonkey'],   // List of TokenHolders to filter the results against
                            opts: ['EOS_INCLUDE_STAKED'],
                            limit: 100  // Max 100
                        }
                    }

                    if (cursor !== '') {
                        variables.variables.cursor = cursor;
                    } else {
                        variables.variables.cursor = undefined;
                    }
    
                    // https://docs.dfuse.io/guides/ethereum/tutorials/search/
                    let response = await this.client.graphql(operation, variables);
                    //console.log(response);
    
                    if (!response.errors) {
                        let tokenBalances = response.data.tokenBalances;

                        let pageInfo = tokenBalances.pageInfo;

                        if (pageInfo !== null) {
                            cursor = pageInfo.endCursor;
                            console.log('### cursor = ' + cursor);

                            let edges = tokenBalances.edges;
                            for (let i = 0; i < edges.length; i++) {
                                let edge = edges[i];
                                //console.log(edge);
                                let details = edge.node;
                                //console.log(details);
                                let balance = parseFloat(details.balance);
                                console.log('account=' + details.account + ', symbol=' + details.symbol + ', balance=' + balance.toFixed(details.precision));
                                response.accounts.push({ account: details.account, balance: balance });
                                totalAccounts++;
                            }
                        } else {
                            cursor = '';
                        }
                    } else {
                        response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
                        response.result.message = '';
                        cursor = '';
                        for (let i = 0; i < response.errors.length; i++) {
                            console.log('Error: ' + response.errors[i].message);
                            if (response.result.message.length > 0) {
                                response.result.message += '\r\n';
                            }
                            response.result.message += response.errors[i].message;
                            
                        }
                        break;
                    }
                } while (cursor !== '');

                response.result.code = enums.ResponseCodeEnum.SUCCESS;
                response.result.message = null;
                
            } else {
                console.log('dfuse_api.tokenBalances - EOS contract = ' + contract + ', symbol = ' + symbol);
            }
        } catch (err) {
            response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
            response.result.message = "Internal Error";

            console.log('dfuse_api.tokenBalances - Error in tokenBalances.');
            console.error(err);
        }
    },

    customizedFetch: async function(init) {
        if (init === undefined) {
            init = {}
          }
        
          if (init.headers === undefined) {
            init.headers = {}
          }
        
          // This is highly optimized and cached, so while the token is fresh, this is very fast
          const apiTokenInfo = await this.client.getTokenInfo();
          
          init.headers["Authorization"] = 'Bearer ' + apiTokenInfo.token;
          init.headers['X-Eos-Push-Guarantee'] = 'in-block'; // Or "irreversible", "handoff:1", "handoffs:2", "handoffs:3"
        
          console.log('init', init);

          //mainnet.eos.dfuse.io
          return fetch('https://auth.dfuse.io/v1/auth/issue', init);
    },

    sendToken: async function(account_to, amount) {
        // let contract = 'eosio.token';
        // let symbol = 'EOS';

        let contract = 'gilsertience';
        let symbol = 'GIL';

        let account_from = process.env.EOSIO_ACCOUNT_FROM;
        account_to = 'mdixonwallee';
        amount = 0.0001;
        
        let response = {
            success: false,
            errorMessage: null,
            error: null,
            apiResponse: null,
            transaction_id: null
        }

        try {
            // https://docs.dfuse.io/guides/eosio/tutorials/write-chain/
            // https://github.com/EOSIO/eosjs

            this.createClient();

            const privateKey = process.env.EOSIO_ACCOUNT_FROM_PRIVATE_KEY; // junglemonkey@active
            const signatureProvider = new JsSignatureProvider([privateKey]); // need a way to securely do this
    
            let token = await this.getJWT();

            // https://docs.dfuse.io/guides/eosio/tutorials/write-chain/
            // https://eosio.stackexchange.com/questions/4065/unable-to-send-transaction
            const customFetch = async function(input, init) {
                'use strict';
                // input = https://mainnet.eos.dfuse.io/v1/chain/get_info

                if (init === undefined) {
                    init = {};
                }

                if (init.headers === undefined) {
                    init.headers = {};
                }

                init.headers['Authorization'] = 'Bearer ' + token;
                init.headers['X-Eos-Push-Guarantee'] = 'in-block'; // Or "in-block", "irreversible", "handoff:1", "handoffs:2", "handoffs:3"]

                return fetch(input, init);
            }

            //const rpc = new JsonRpc('http://127.0.0.1:8888', { fetch });
            //const rpc = new JsonRpc('https://nodes.get-scatter.com', { fetch });  // Works
            const rpc = new JsonRpc(this.client.endpoints.restUrl, { fetch : customFetch });    // Work with dfuse
    
            const api = new Api({ rpc, signatureProvider, textDecoder: new TextDecoder(), textEncoder: new TextEncoder() });

            try {
                const result = await api.transact({
                    actions: [{
                        account: contract,
                        name: 'transfer',
                        authorization: [{
                            actor: account_from,
                            permission: 'active',   // active, owner?
                        }],
                        data: {
                            from: account_from,
                            to: account_to,
                            quantity: amount.toFixed(4) + ' ' + symbol,
                            memo: '',
                        },
                    }]
                    }, {
                        blocksBehind: 3,
                        expireSeconds: 30,
                    }
                );

                response.apiResponse = result;

                if (result !== null && result.transaction_id !== null && result.transaction_id !== undefined) {
                    response.success = true;
                    response.errorMessage = null;
                    response.transaction_id = result.transaction_id;
                }
    
            } catch(error) {
                if (error instanceof RpcError) {
                    console.log(JSON.stringify(error.json, null, 2));
                } else {
                    console.error(error);
                }

                tools.saveObjectToFile(error, path.join(global.tempFileDir, 'sendToken_error.json'))
                response.error = error;
                response.success = false;

                if (error.message.includes('no balance object found')) {
                    response.errorMessage = 'Insufficient Funds';
                } else if (error.message.includes('overdrawn balance')) {
                    response.errorMessage = 'Insufficient Funds';
                } else if (error.message.includes('User rejected the signature request')) {
                    response.errorMessage = 'User Cancelled';
                } else if (error.message.includes('account does not exist')) {
                    response.errorMessage = 'Account does not exist';
                } else {
                    response.errorMessage = error.message;
                }
            }
        } catch (error) {
            if (error instanceof RpcError) {
                console.log(JSON.stringify(error.json, null, 2));
            } else {
                console.error(error);
            }

            response.success = false;
            response.error = error;
            response.errorMessage = error.ReferenceError;

            tools.saveObjectToFile(response.error, path.join(global.tempFileDir, 'sendToken_error.json'))
            
        }
        return response;
    }


    // Create new account
    // https://github.com/EOSIO/eosjs/blob/master/docs/2.-Transaction-Examples.md

    // const result = await api.transact({
    //     actions: [{
    //       account: 'eosio',
    //       name: 'newaccount',
    //       authorization: [{
    //         actor: 'useraaaaaaaa',
    //         permission: 'active',
    //       }],
    //       data: {
    //         creator: 'useraaaaaaaa',
    //         name: 'mynewaccount',
    //         owner: {
    //           threshold: 1,
    //           keys: [{
    //             key: 'PUB_R1_6FPFZqw5ahYrR9jD96yDbbDNTdKtNqRbze6oTDLntrsANgQKZu',
    //             weight: 1
    //           }],
    //           accounts: [],
    //           waits: []
    //         },
    //         active: {
    //           threshold: 1,
    //           keys: [{
    //             key: 'PUB_R1_6FPFZqw5ahYrR9jD96yDbbDNTdKtNqRbze6oTDLntrsANgQKZu',
    //             weight: 1
    //           }],
    //           accounts: [],
    //           waits: []
    //         },
    //       },
    //     },
    //     {
    //       account: 'eosio',
    //       name: 'buyrambytes',
    //       authorization: [{
    //         actor: 'useraaaaaaaa',
    //         permission: 'active',
    //       }],
    //       data: {
    //         payer: 'useraaaaaaaa',
    //         receiver: 'mynewaccount',
    //         bytes: 8192,
    //       },
    //     },
    //     {
    //       account: 'eosio',
    //       name: 'delegatebw',
    //       authorization: [{
    //         actor: 'useraaaaaaaa',
    //         permission: 'active',
    //       }],
    //       data: {
    //         from: 'useraaaaaaaa',
    //         receiver: 'mynewaccount',
    //         stake_net_quantity: '1.0000 SYS',
    //         stake_cpu_quantity: '1.0000 SYS',
    //         transfer: false,
    //       }
    //     }]
    //   }, {
    //     blocksBehind: 3,
    //     expireSeconds: 30,
    //   });

}   // module.exports
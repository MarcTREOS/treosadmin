/* https://www.npmjs.com/package/ipfs-http-client */

const ipfsClient = require('ipfs-http-client');

const ipfs_host = process.env.IPFS_HOST || 'localhost';
const ipfs_port = process.env.IPFS_PORT || '5001';
const ipfs_protocol = process.env.IPFS_PROTOCOL || 'http';

module.exports = {
    // Properties
    //profile_id: null,
    //token: null,

    // Methods
    addFile: async function(path, fileData) {
        'use strict';
        let response = {
            path: null,
            hash: null,
            size: null
        }
        try {
            let ipfs = ipfsClient({ host: ipfs_host, port: ipfs_port, protocol: ipfs_protocol });

            var files = [
                {
                    path: path,         // The file path
                    content: fileData   // A Buffer, Readable Stream, Pull Stream or File with the contents of the file
                }
            ]
            var result = await ipfs.add(files);
            
            response = result[0];
            var hash = response.hash; // "m..WW"
        } catch (err) {
            console.log(err);
            throw err;
        }
        return response;
    },

    // addFile: async function(req) {
    //     'use strict';
    //     let response = {
    //         profile_id: null,
    //         token: null
    //     }
    //     try {
    //         let ipfs = ipfsClient({ host: ipfs_host, port: ipfs_port, protocol: ipfs_protocol });

    //         var files = [
    //             {
    //                 path: '/tmp/myfile.txt',
    //                 content: ipfs.Buffer.from('ABC')
    //             }
    //         ]
    //         var content = ipfs.Buffer.from('ABC');
    //         var results = await ipfs.add(content);
    //         var hash = results[0].hash; // "m..WW"
    //     } catch (err) {
    //         console.log(err);
    //     }
    //     return response;
    // },

    addFileFromFs: async function(path) {
        try {
            let ipfs = ipfsClient({ host: ipfs_host, port: ipfs_port, protocol: ipfs_protocol });

            var result = await ipfs.add(path, { recursive: false });
            console.log(result);
            var hash = result[0].hash; // "m..WW"
        } catch (err) {
            console.log(err);
        }
    },


    addFileFromStream: async function(stream) {
        let response = {
            path: null,
            hash: null,
            size: null
        }
        try {
            let ipfs = ipfsClient({ host: ipfs_host, port: ipfs_port, protocol: ipfs_protocol });

            var result = await ipfs.addFromStream(stream);
            console.log(result);
            response = result[0];
            var hash = response.hash; // "m..WW"
        } catch (err) {
            console.log(err);
            throw err;
        }
        return response;
    },



}   // module.exports
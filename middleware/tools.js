const fs = require('fs');

module.exports = {
    /* Properties */

    /* Methods */
    isNullOrEmpty: function(value) {
        'use strict';
        let result = false;
        if (value === null || value === undefined || value.toString().trim() === '') {
            result = true;
        }
        return result;
    },
    
    roundDown: function(value, decimalPlaces) {
        'use strict';
        let result = value;
        result = value * Math.pow(10, decimalPlaces);
        result = Math.floor(result);
        result = result / Math.pow(10, decimalPlaces);
        return result;
    },
    
    roundUp: function(value, decimalPlaces) {
        'use strict';
        let result = value;
        result = value * Math.pow(10, decimalPlaces);
        result = Math.ceil(result);
        result = result / Math.pow(10, decimalPlaces);
        return result;
    },

    parseCookies (req) {
        'use strict';
        let list = {},
            rc = req.headers.cookie;
    
        rc && rc.split(';').forEach(function( cookie ) {
            let parts = cookie.split('=');
    
            list[parts.shift().trim()] = decodeURI(parts.join('='));
        });
    
        return list;
    },

    clearObject(obj) {
        // Remove each property in the dictionary
        for (var prop in obj) {
            if (obj.hasOwnProperty(prop)) {
                delete obj[prop];
            }
        }
    },

    saveObjectToFile(obj, filename) {
        'use strict';
        let outputString = '';
        try {
            //console.log('### Object Type: ' + typeof obj);

            switch (typeof obj) {
                case 'object': {
                    outputString = JSON.stringify(obj, null, 3);
                    break;
                }
                default: {
                    outputString = obj;
                    break;
                }
            }
            fs.writeFileSync(filename, outputString);
        } catch (err) {
            console.error(err)
        }
    }


}   // module.exports


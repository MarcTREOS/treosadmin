const dbpool = require('./dbpool');
const enums = require('../middleware/enums');

module.exports =  {

    alterTokenAmount: async function(conn, profile_id, token, action, amount) {
        let commandText = '';
        let parameters = [];
        let proceed = true;

        let response = {
            result: {
                code: enums.ResponseCodeEnum.INTERNAL_ERROR,
                message: 'Internal Error'
            },
            token: null,
            balance: null
        }
        
        try {
            if (proceed) {
                if (conn === undefined || conn === null) {
                    response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
                    response.result.message = 'A persistent database connection must be defined.';
                    proceed = false;
                }
            }

            /* Update wallet history */
            if (proceed) {
                commandText = '';
                parameters.length = 0;

                commandText = "INSERT INTO treos_profile_token_updates" +
                    " SET profile_id = ?" +
                    ", date = NOW()" +
                    ", token = ?" +
                    ", action = ?" +        // Start with ADD_ or DEDUCT_
                    ", amount = ABS(?)" +   // Make sure is +ve number
                    ", amount_before = (SELECT balance FROM treos_profile_token_balance WHERE profile_id = ? AND token = ?)" +
                    ", parent_id=0";

                parameters.push(profile_id);
                parameters.push(token);
                parameters.push(action);
                parameters.push(amount);
                parameters.push(profile_id);
                parameters.push(token);

                let dbResponse = await dbpool.query(commandText, parameters, conn);

                if (!dbResponse.success) {
                    response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                    response.result.message = 'Database Error';
                    proceed = false;
                }
            }

            /* Update wallet balance */
            if (proceed) {
                commandText = '';
                parameters.length = 0;

                commandText = "INSERT INTO treos_profile_token_balance" +
                    " (profile_id, token, balance)" +
                    " VALUES(?, ?, ?)" +
                    " ON DUPLICATE KEY UPDATE balance = balance + ?";

                parameters.push(profile_id);
                parameters.push(token);
                parameters.push(amount);
                parameters.push(amount);

                let dbResponse = await dbpool.query(commandText, parameters, conn);

                if (!dbResponse.success) {
                    response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                    response.result.message = 'Database Error';
                    proceed = false;
                } else if (dbResponse.success && dbResponse.result[0].affectedRows === 0) {
                    response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                    response.result.message = 'Failed to update token balance';
                    proceed = false;
                }
            }

            /* Return updated balance */
            if (proceed) {
                commandText = '';
                parameters.length = 0;

                commandText = "SELECT token, balance FROM treos_profile_token_balance" +
                    " WHERE profile_id = ? AND token = ?";

                parameters.push(profile_id);
                parameters.push(token);

                let dbResponse = await dbpool.query(commandText, parameters, conn);
                
                if (dbResponse.success) {
                    if (dbResponse.result[0].length > 0) {
                        response.token = dbResponse.result[0][0].token;
                        response.balance = dbResponse.result[0][0].balance;
                    }
                } else {
                    response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                    response.result.message = 'Database Error';
                    proceed = false;
                }
            }
        } catch(error) {
            console.error(error);
            proceed = false;
        } finally {
            if (proceed) {
                response.result.code = enums.ResponseCodeEnum.SUCCESS;
                response.result.message = null;
            }
        }

        return response;
    },   // alterTokenAmount

    addBountyReward: async function(conn, profile_id, bounty_task_name, amount_option_name, max_times_receive_option_name) {
        let commandText = '';
        let parameters = [];
        let proceed = true;

        let bounty_task_id = null;
        let bounty_reward_amount = null;
        let bounty_reward_max_times_receive = null;
        let exceeded_max_times_receive = true;

        let response = {
            result: {
                code: enums.ResponseCodeEnum.INTERNAL_ERROR,
                message: 'Internal Error'
            },
            token: null,
            balance: null
        }
        
        try {
            
            if (proceed) {
                if (!bounty_task_name) {
                    response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
                    response.result.message = 'bounty_task_name is not defined';
                    proceed = false;
                } else if (!amount_option_name) {
                    response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
                    response.result.message = 'amount_option_name is not defined';
                    proceed = false;
                }
            }

            if (proceed) {
                if (conn === undefined || conn === null) {
                    response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
                    response.result.message = 'A persistent database connection must be defined.';
                    proceed = false;
                }
            }

            if (proceed) {
                if (max_times_receive_option_name === undefined || max_times_receive_option_name === null) {
                    bounty_reward_max_times_receive = -1;   // infinite
                }
            }

            /* Bounty Task ID to Insert */
            if (proceed) {
                commandText = '';
                parameters.length = 0;

                commandText = "SELECT id" +
                    " FROM treos_bounty_task" +
                    " WHERE name = ?";

                parameters.push(bounty_task_name);

                let dbResponse = await dbpool.query(commandText, parameters, conn);
                
                if (dbResponse.success) {
                    if (dbResponse.result[0].length > 0) {
                        bounty_task_id = dbResponse.result[0][0].id;
                    } else {
                        response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                        response.result.message = 'Database Error - bounty_task id not found.';
                        console.error(response.result.message);
                        proceed = false;
                    }
                } else {
                    response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                    response.result.message = 'Database Error';
                    proceed = false;
                }
            }

            if (proceed) {
                commandText = '';
                parameters.length = 0;

                commandText = "SELECT option_name, option_value" +
                    " FROM treos_config_v2" +
                    " WHERE option_name = ?";
                parameters.push(amount_option_name);
                    
                if (max_times_receive_option_name !== undefined && max_times_receive_option_name !== null) {
                    commandText += " OR option_name = ?";
                    parameters.push(max_times_receive_option_name);
                }
                
                let dbResponse = await dbpool.query(commandText, parameters, conn);
                
                if (dbResponse.success) {
                    for (let i = 0; i < dbResponse.result[0].length; i++) {
                        if (amount_option_name && dbResponse.result[0][i].option_name === amount_option_name) {
                            let option_value = dbResponse.result[0][i].option_value;
                            if (!isNaN(option_value)) {
                                bounty_reward_amount = parseFloat(dbResponse.result[0][i].option_value);
                            }
                        } else if (max_times_receive_option_name && dbResponse.result[0][i].option_name === max_times_receive_option_name) {
                            let option_value = dbResponse.result[0][i].option_value;
                            if (!isNaN(option_value)) {
                                bounty_reward_max_times_receive = parseInt(dbResponse.result[0][i].option_value);
                            }
                        }
                    }
                } else {
                    response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                    response.result.message = 'Database Error';
                    proceed = false;
                }
            }

            /* Validate database response */
            if (proceed) {
                if (bounty_reward_amount === null) {
                    response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                    response.result.message = 'Database Error - bounty_reward_amount not returned from database.';
                    console.error(response.result.message);
                    proceed = false;
                } else if (max_times_receive_option_name && bounty_reward_max_times_receive === null) {
                    response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                    response.result.message = 'Database Error - bounty_reward_max_times_receive not returned from database.';
                    console.error(response.result.message);
                    proceed = false;
                }
            }


            /* Have they exceeded the maximum amount of receives */
            if (proceed) {
                if (bounty_reward_max_times_receive >= 0) { /* -1 unlimited, 0 disabled, > 0 carry on */
                    commandText = '';
                    parameters.length = 0;

                    commandText = "SELECT COUNT(*) AS Total" +
                        " FROM treos_profile_bounty_rewards" +
                        " WHERE profile_id = ?" +
                        " AND bounty_task_id = ?";
                    
                    parameters.push(profile_id);
                    parameters.push(bounty_task_id);
                        
                    let dbResponse = await dbpool.query(commandText, parameters, conn);
                    
                    if (dbResponse.success) {
                        if (dbResponse.result[0].length > 0) {
                            if (dbResponse.result[0][0].Total === -1) { /* unlimited */
                                exceeded_max_times_receive = false;
                            } else if (dbResponse.result[0][0].Total >= bounty_reward_max_times_receive) {
                                exceeded_max_times_receive = true;
                            } else {
                                exceeded_max_times_receive = false;
                            }
                        } else {
                            exceeded_max_times_receive = false;
                        }
                    } else {
                        response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                        response.result.message = 'Database Error';
                        proceed = false;
                    }
                } else {
                    // unlimited
                    exceeded_max_times_receive = false;
                }
            }

            /* Add reward */
            if (proceed) {
                if (!exceeded_max_times_receive) {
                    commandText = '';
                    parameters.length = 0;

                    commandText = "INSERT INTO treos_profile_bounty_rewards" +
                        " SET profile_id = ?" +
                        ", bounty = NULL" +
                        ", date = NOW()" +
                        ", points = ?" +        // Start with ADD_ or DEDUCT_
                        ", bounty_task_id = ?" +   // Make sure is +ve number
                        ", purchase_id = NULL" +
                        ", is_complete = 1";

                    parameters.push(profile_id);
                    parameters.push(bounty_reward_amount);
                    parameters.push(bounty_task_id);

                    let dbResponse = await dbpool.query(commandText, parameters, conn);

                    if (!dbResponse.success) {
                        response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
                        response.result.message = 'Database Error';
                        proceed = false;
                    }
                }
            }
            
        } catch(error) {
            console.error(error);
            proceed = false;
        } finally {
            if (proceed) {
                response.result.code = enums.ResponseCodeEnum.SUCCESS;
                response.result.message = null;
            }
        }

        return response;
    }

} // module.exports
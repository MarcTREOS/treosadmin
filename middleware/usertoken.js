const dbpool = require('./dbpool');

module.exports = {
    // Properties
    //profile_id: null,
    //token: null,

    // Methods
    authenticateToken: async function(req) {
        'use strict';
        let response = {
            profile_id: null,
            token: null
        }
        try {
            let cookies = parseCookies(req);
            //console.log('Cookies = ', cookies);

            if (cookies.treos_jwt !== undefined) {
                let parts = cookies.treos_jwt.split(':');
                if (parts.length === 2) {
                    let profile_id = parts[0];
                    let token = parts[1];
                    if (await tokenExistsInDatabase(profile_id, token)) {
                        response.profile_id = profile_id;
                        response.token = token;
                    }
                    else {
                        console.log('Token Doesn\'t Exist in database');
                    }
                } else {
                    console.log('cookie token length != 2');
                }
            }
        } catch (err) {
            console.log(err);
        }
        return response;
    }

}   // module.exports


async function tokenExistsInDatabase(profile_id, token) {
    'use strict';
    let result = false;

    let commandText = '';
    let parameters = [];
    let dbResult = null;
    try {
        
        commandText = "SELECT COUNT(*) AS Total" +
            " FROM treos_profile_session_token" +
            " WHERE profile_id = ? AND token = ?";

        parameters.push(profile_id);
        parameters.push(token);

        dbResult = await dbpool.query(commandText, parameters);

        if (dbResult.success) {
            if (dbResult.result[0].length > 0) {
                if (dbResult.result[0][0].Total > 0) {
                    result = true;
                } 
            }
        } else {
            console.log(dbResult.error);
        }
    } catch (err) {
        console.log(err);
    }

    return result;
}


function parseCookies (req) {
    'use strict';
    let list = {},
        rc = req.headers.cookie;

    rc && rc.split(';').forEach(function( cookie ) {
        let parts = cookie.split('=');

        list[parts.shift().trim()] = decodeURI(parts.join('='));
    });

    return list;
}
const request = require('request-promise');
const cheerio = require('cheerio'); // JQuery for node.js
const puppeteer = require('puppeteer');
const enums = require('../middleware/enums');
const urlParser = require('url');
const fs = require('fs');

module.exports = {
    /* Properties */
    imageURLs: [],
    headings: [],
    prices: [],

    /* Methods */
    scrapePage: async function(url) {
        let images = [];
        let headings = [];
        let prices = [];

        let response = {
            result: {
                code: enums.ResponseCodeEnum.INTERNAL_ERROR,
                message: 'Internal Error'
            },
        }

        process.setMaxListeners(Infinity);  // Added to try and fix crashing

        let browser = null;
        let sites = null;
        try {
            this.imageURLs.length = 0;
            this.headings.length = 0;
            this.prices.length = 0;

            try {
                let rawdata = fs.readFileSync('public/assets/wishlist_add_css_selectors.json');
                sites = JSON.parse(rawdata);
            } catch (err) {
                console.log('Error parsing JSON selectors file - ', err.message);
            }
            

            // Puppeteer UserAgent
            //'--no-sandbox',
            // const args = [
            //     '--ignore-certificate-errors',
            //     '--ignore-certifcate-errors-spki-list',
            //     '--window-size=1920,1080',
            //     '--lang=en-US,en',
            //     '--user-agent="Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36 Edge/18.17763"'
            // ];

            const args = [
                '--no-sandbox',
                '--disable-setuid-sandbox',
                '--disable-infobars',
                '--window-position=0,0',
                '--ignore-certificate-errors',
                '--ignore-certifcate-errors-spki-list',
                '--window-size=1920,1080',
                '--lang=en-US,en',
                '--user-agent="Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36 Edge/18.17763"'
            ];


            const options = {
                args,
                headless: true,
                //headless: false,
                ignoreHTTPSErrors: true,
                defaultViewport: null
            }

            browser = await puppeteer.launch(options);
            const page = await browser.newPage();

            await page._client.send( 'Emulation.clearDeviceMetricsOverride' );

            await page.setViewport({
                width  : 1920,
                height : 1080
            });

            await page.setExtraHTTPHeaders({
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                'Accept-Language': 'en-US,en;q=0.5'
            });

            const preloadFile = fs.readFileSync('middleware/webScraper_preload.js', 'utf8');
            await page.evaluateOnNewDocument(preloadFile);

            let q = urlParser.parse(url, true);

            

            await page.goto(url, { waitUntil: 'networkidle0', timeout: 30000 });
            
            //await page.screenshot({path: 'digg.png', fullPage: true});
            /*
                console.log(q.host);        //returns 'localhost:8080'
                console.log(q.pathname);    //returns '/default.htm'
                console.log(q.search);      //returns '?year=2017&month=february'
            */

            if (sites !== null) {
                let foundSite = false;
                for (let siteIndex = 0; siteIndex < sites.length; siteIndex++) {
                    let site = sites[siteIndex];
                    let siteURLs = [];
                    let siteWaitForSelector = null;
                    let siteImageSelectors = [];
                    let siteTitleSelectors = [];
                    let sitePriceSelectors = [];

                    if (site.urls !== undefined && site.urls !== null) {
                        siteURLs = site.urls;
                    }

                    if (site.waitForSelector !== undefined && site.waitForSelector !== null) {
                        siteWaitForSelector = site.waitForSelector;
                    }

                    if (site.imageSelectors !== undefined && site.imageSelectors !== null) {
                        siteImageSelectors = site.imageSelectors;
                    }

                    if (site.titleSelectors !== undefined && site.titleSelectors !== null) {
                        siteTitleSelectors = site.titleSelectors;
                    }

                    if (site.priceSelectors !== undefined && site.priceSelectors !== null) {
                        sitePriceSelectors = site.priceSelectors;
                    }

                    for (let urlIndex = 0; urlIndex < siteURLs.length; urlIndex++) {
                        if (q.host.toLowerCase().includes(siteURLs[urlIndex].toLowerCase())) {
                            foundSite = true;
                            console.log('Found matching site URL ' + siteURLs[urlIndex]);
                            
                            if (siteWaitForSelector !== null) {
                                console.log('Found waitForSelector ' + site.waitForSelector);
                                try {
                                    await page.waitForSelector(site.waitForSelector, { timeout: 10000 });
                                } catch(err) {
                                    console.log('Error while waiting for selector ' + site.waitForSelector + ' - ' + err);
                                }
                            }

                            for (let imageSelectorIndex = 0; imageSelectorIndex < siteImageSelectors.length; imageSelectorIndex++) {
                                images = await getProductImages_QuerySelector(page, siteImageSelectors[imageSelectorIndex]);

                                console.log('Found ' + images.length + ' matching site Images using CSS Selector ' + siteImageSelectors[imageSelectorIndex]);

                                if (images.length > 0) {
                                    break;
                                }
                            }

                            for (let titleSelectorIndex = 0; titleSelectorIndex < siteTitleSelectors.length; titleSelectorIndex++) {
                                headings = await getProductHeadings_QuerySelector(page, siteTitleSelectors[titleSelectorIndex]);

                                console.log('Found ' + headings.length + ' matching site Headings using CSS Selector ' + siteTitleSelectors[titleSelectorIndex]);

                                if (headings.length > 0) {
                                    break;
                                }
                            }

                            for (let priceSelectorIndex = 0; priceSelectorIndex < sitePriceSelectors.length; priceSelectorIndex++) {
                                prices = await getProductPrices_QuerySelector(page, sitePriceSelectors[priceSelectorIndex]);

                                console.log('Found ' + prices.length + ' matching site Pricings using CSS Selector ' + sitePriceSelectors[priceSelectorIndex]);

                                if (prices.length > 0) {
                                    break;
                                }
                            }
                        }

                        if (foundSite) {
                            break;
                        }
                    }

                    if (foundSite) {
                        break;
                    }
                }
            }

            if (images.length === 0) {
                // Default to getting 20 largest images
                console.log('No images found - defaulting to getting all images from page.');
                images = await getProductImages(page);
            }

            if (headings.length === 0) {
                console.log('No headings found - defaulting to getting all headings from page.');
                headings = await getProductHeadings(page);
                console.log('back again ' + headings.length);
            }

            
            this.imageURLs = images;
            this.headings = headings;
            this.prices = prices;

            response.result.code = enums.ResponseCodeEnum.SUCCESS;
            response.result.message = null;
        } catch (err) {
            console.log('Caught Error. err.message - ', err.message);
            if (err.message.includes('invalid URL') ||
                    err.message.includes('ERR_NAME_NOT_RESOLVED')) {
                response.result.code = enums.ResponseCodeEnum.INVALID_URL;
                response.result.message = err.message;
            } else if (err.message.includes('Navigation Timeout Exceeded') ||
                        err.message.includes('ERR_CONNECTION_TIMED_OUT')) {
                    response.result.code = enums.ResponseCodeEnum.TIMEOUT_ERROR;
                    response.result.message = err.message;
            } else {
                response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
                response.result.message = "Internal Error";
                console.log('webScraper - ', err);
            }
        } finally {
            if (browser !== null) {
                await browser.close();
            }
        }
        return response;
    }
}   // module.exports

async function getLargestImage(page) {
    let result = null;
    try {
        const largest_image = await page.evaluate(() => {
            return [...document.getElementsByTagName('img')].sort((a, b) => b.naturalWidth * b.naturalHeight - a.naturalWidth * a.naturalHeight)[0].src;
        });
    } catch (err) {
        console.log(err);
    }
    return result;
}


async function getProductImages(page) {
    let result = [];
    try {
        
        result = await page.evaluate(() => {
            // Everything in here should be classic javascript
            let result = [];

            let images = [...document.getElementsByTagName('img')].sort((a, b) => b.naturalWidth * b.naturalHeight - a.naturalWidth * a.naturalHeight);
            //let images = document.getElementsByTagName('img');

            // Sort with largest image at the top
            //images.sort((a, b) => b.naturalWidth * b.naturalHeight - a.naturalWidth * a.naturalHeight);

            for (let i = 0; i < images.length; i++) {
                // img.src will always return a fully qualified url.   e.g.   ./images/blah.jpg will return as http://www.amazon.com/images/blah.jpg
                let src = images[i].src;
                
                // Confine it to just images on the domain
                

                result.push(images[i].src);

                // Only keep top 10 results
                if (result.length >= 20) {
                    break;
                }
            }

            return result;
        });
    } catch (err) {
        console.log(err);
        //throw err;
    }
    return result;
}


async function getProductImages_QuerySelector(page, cssSelector) {
    let result = [];
    try {
        result = await page.evaluate((selector) => {
            // Everything in here should be classic javascript
            let result = [];
            
            let images = document.querySelectorAll(selector);

            for (let i = 0; i < images.length; i++) {
                if (images[i].src !== undefined && images[i].src !== null) {
                    result.push(images[i].src);
                }

                // Only keep top 20 results
                if (result.length >= 20) {
                    break;
                }
            }

            return result;
        }, cssSelector);
    } catch (err) {
        console.log(err);
        //throw err;
    }
    return result;
}

async function getProductHeadings_QuerySelector(page, cssSelector) {
    let result = [];
    try {
        result = await page.evaluate((selector) => {
            // Everything in here should be classic javascript
            let result = [];
            
            let headings = document.querySelectorAll(selector);

            for (let i = 0; i < headings.length; i++) {
                if (headings[i].innerText !== undefined && headings[i].innerText !== null) {
                    result.push(headings[i].innerText.trim());
                }

                // Only keep top 20 results
                if (result.length >= 20) {
                    break;
                }
            }

            return result;
        }, cssSelector);
    } catch (err) {
        console.log(err);
        //throw err;
    }
    return result;
}


async function getProductPrices_QuerySelector(page, cssSelector) {
    let result = [];
    try {
        result = await page.evaluate((selector) => {
            // Everything in here should be classic javascript
            let result = [];
            
            let prices = document.querySelectorAll(selector);

            for (let i = 0; i < prices.length; i++) {
                if (prices[i].innerText !== undefined && prices[i].innerText !== null) {
                    let validChars = '0123456789.,';
                    let parsedPrice = '';

                    let finalPrice = prices[i].innerText;
                    for (let i = 0; i < finalPrice.length; i++) {
                        if (validChars.includes(finalPrice.charAt(i))) {
                            parsedPrice += finalPrice.charAt(i);
                        }
                    }
                    result.push(parsedPrice);
                }

                // Only keep top 20 results
                if (result.length >= 20) {
                    break;
                }
            }

            return result;
        }, cssSelector);
    } catch (err) {
        console.log(err);
        //throw err;
    }
    return result;
}


async function getProductHeadings(page) {
    let result = [];
    try {
        console.log('Inside getProductHeadings');
        result = await page.evaluate(() => {
            // Everything in here should be classic javascript
            let result = [];

            console.log('About to get all headings');

            let headings = [...document.getElementsByTagName('h1')].sort((a, b) => b.naturalWidth * b.naturalHeight - a.naturalWidth * a.naturalHeight);

            console.log('found ' + headings.length + ' headings');
            // Sort with largest image at the top
            //images.sort((a, b) => b.naturalWidth * b.naturalHeight - a.naturalWidth * a.naturalHeight);

            for (let i = 0; i < headings.length; i++) {
                // img.src will always return a fully qualified url.   e.g.   ./images/blah.jpg will return as http://www.amazon.com/images/blah.jpg
                let heading = headings[i];
                
                // Confine it to just images on the domain
                

                result.push(heading.innerText);

                // Only keep top 20 results
                if (result.length >= 20) {
                    break;
                }
            }

            return result;
        });
    } catch (err) {
        console.log(err);
        //throw err;
    }
    return result;
}




// var getAbsoluteUrl = (function() {
// 	var a;

// 	return function(url) {
// 		if(!a) a = document.createElement('a');
// 		a.href = url;

// 		return a.href;
// 	};
// })();


// No matter how you pass in the URL string, the URL will come out absolute.  Of course strings like `javascript:;` wont come out any different, but real qualified URLs will come out as absolute!
var dictionary_currencies = undefined;

var CryptoWithdrawal = (function () {
    'use strict';
    // private variables
    var convTarget = null;
    var balances = null;
    var currencies = null;

    var test = 'test';

    function CryptoWithdrawal_btnApplyFilter_Click() {
        'use strict';
        $('#CryptoWithdrawal_RecordOffset').val('0');
    }   // CryptoWithdrawal_btnApplyFilter_Click

    function CryptoWithdrawal_Withdraw_Withdraw_btnSubmit_Click(event) {
        'use strict';
        $(event.target).blur();
        $('#CryptoWithdrawal_Form_Withdrawal').trigger('submit');
    }

    function Form_Report_Submit(event) {
        'use strict';
        $('#CryptoWithdrawal_btnApplyFilter').blur();
        event.preventDefault();
        clearNotifyMessages();
    
        RefreshReport();
        return false;
    }   // Form_Report_Submit
    
    function CryptoWithdrawal_btnExport_Click() {
        'use strict';
        event.target.blur();
        event.preventDefault();

        GetReport(true, function(data) {
            /* success */
            if (data.result.code === ResponseCodeEnum.SUCCESS) {
                if (data.months.length > 0) {

                    let columnHeadings = ['Year',
                        'Month',
                        'Transaction Type',
                        'Currency',
                        'Amount',
                        'Available to Withdraw' ];
                    let exportArray = [];

                    for (let i = 0; i < data.months.length; i++) {
                        let month = data.months[i];
    
                        for (let j = 0; j < month.currencies.length; j++) {
                            let currency = month.currencies[j];

                            let item = {
                                year: month.year,
                                month: month.month,
                                transaction_type: currency.transaction_type,
                                symbol: currency.symbol,
                                amount: currency.amount,
                                crypto_available: currency.crypto_available
                            }
                            exportArray.push(item);

                        }
                    }
                    
                    downloadArrayAsCSV(exportArray, 'crypto_withdrawal_monthly_summary.csv', columnHeadings);
                } else {
                    alert('no records found.');
                }
            } else {
                switch (data.result.code) {
                    default: {
                        showErrorMessage(data.result.message);
                        break;
                    }
                }
            }
        },
        function(jqXHR, textStatus, errorThrown) {
            /* fail */
            showErrorMessage('fail - ' + errorThrown);
        },
        function() {
            /* always */
        });
    } // CryptoWithdrawal_btnExport_Click

    function RefreshReport() {
        'use strict';
    
        GetReport(false, function(data) {
            /* success */
            if (data.result.code === ResponseCodeEnum.SUCCESS) {
                let total_records = data.total_records;
    
                let table = $('#CryptoWithdrawal_ReportTable table');
                table.find('tbody tr').remove();
    
                if (data.months.length > 0) {
                    for (let i = 0; i < data.months.length; i++) {
                        let feesmonth = data.months[i];
    
                        for (let j = 0; j < feesmonth.currencies.length; j++) {
                            let feecurrency = feesmonth.currencies[j];
        
                            let imageFilename = feecurrency.symbol.toUpperCase() + '.png';
    
                            let $row = $('<tr></tr>');
    
                            $row.attr('data-year', feesmonth.year).data('year', feesmonth.year);
                            $row.attr('data-month', feesmonth.month).data('month', feesmonth.month);
                            $row.attr('data-transaction_type', feecurrency.transaction_type_id).data('transaction_type', feecurrency.transaction_type_id);
                            $row.attr('data-symbol', feecurrency.symbol).data('symbol', feecurrency.symbol);
                            $row.attr('data-available', feecurrency.crypto_available).data('available', feecurrency.crypto_available);
                            //$row.attr('data-withdrawal_address', feecurrency.withdrawal_address).data('withdrawal_address', feecurrency.withdrawal_address);
                            //$row.attr('data-withdrawal_memo', feecurrency.withdrawal_memo).data('withdrawal_memo', feecurrency.withdrawal_memo);
    
                            let $cell_year = $('<td></td>').addClass('year');
                            let $cell_month = $('<td></td>').addClass('month');
                            let $cell_transaction_type = $('<td></td>').addClass('transaction_type');
                            let $cell_symbol = $('<td></td>').addClass('currency');
                            let $cell_amount = $('<td></td>').addClass('amount');
                            let $cell_crypto_available = $('<td></td>').addClass('crypto_available');
                            let $cell_available_usd = $('<td></td>').addClass('available_usd');
                            let $cell_buttons = $('<td></td>');
    
                            if (j > 0) {
                                $cell_year.html('&nbsp;');
                                $cell_month.html('&nbsp;');
                            } else {
                                $cell_year.text(feesmonth.year);
                                $cell_month.text(feesmonth.month);
                            }
                            
                            $cell_transaction_type.html(feecurrency.transaction_type).attr('style', 'text-align: left;');

                            let $img_symbol = $('<img/>').attr('src', '/images/wallets/' + imageFilename).attr('style', 'height:20px; width: auth; margin-right: 10px;');
                            $cell_symbol.append($img_symbol);
                            $cell_symbol.append(feecurrency.symbol);
    
                            $cell_amount.html(formatTokenBalance(feecurrency.symbol, feecurrency.amount)).attr('style', 'text-align: right;');

                            $cell_crypto_available.html(formatTokenBalance(feecurrency.symbol, feecurrency.crypto_available)).attr('style', 'text-align: right;');
    
                            let tro_rate = dictionary_currencies[feecurrency.symbol].tro_rate;
                            let usd_rate = dictionary_currencies['USD'].tro_rate;

                            if (!isNaN(tro_rate) && !isNaN(usd_rate)) {
                                let usd_value = (feecurrency.crypto_available / tro_rate) * usd_rate;
                                 $cell_available_usd.html(formatTokenBalance('USD', usd_value)).attr('style', 'text-align: right;');
                            } else {
                                $cell_available_usd.html('isNAN');
                            }
                            

                            if (feecurrency.crypto_available > 0.0) {
                                let $btn_withdraw = $('<button/>').text('Withdraw').on('click', btnWithdraw_Click);
    
                                $cell_buttons.append($btn_withdraw);
                            } else {
                                $cell_buttons.html('&nbsp;');
                            }
    
                            $row.append($cell_year);
                            $row.append($cell_month);
                            $row.append($cell_transaction_type);
                            $row.append($cell_symbol);
                            $row.append($cell_amount);
                            $row.append($cell_crypto_available);
                            $row.append($cell_available_usd);
                            $row.append($cell_buttons);
    
                            table.find('tbody').append($row);
                        }
                    }
    
                    generateReportPagination($('#CryptoWithdrawal_NavTop'), $('#CryptoWithdrawal_RecordOffset'), $('#CryptoWithdrawal_RecordCounter'), total_records);
                    generateReportPagination($('#CryptoWithdrawal_NavBottom'), $('#CryptoWithdrawal_RecordOffset'), $('#CryptoWithdrawal_RecordCounter'), total_records);
                } else {
                    let row = '<tr>';
                    row += '<td colspan="8">No Records Found</td>';
                    table.find('tbody').append(row);
                }
            } else {
                switch (data.result.code) {
                    default: {
                        showErrorMessage(data.result.message);
                        break;
                    }
                }
            }
        },
        function(jqXHR, textStatus, errorThrown) {
            /* fail */
            showErrorMessage('fail - ' + errorThrown);
        },
        function() {
            /* always */
        });
    }   // CryptoWithdrawal_RefreshReport

    function btnWithdraw_Click(event) {
        let $btn = $(event.target);
    
        let $row = $btn.closest('tr');
    
        let year = $row.data('year');
        let month = $row.data('month');
        let transaction_type_id = $row.data('transaction_type');
        let transaction_type = $('.transaction_type', $row).html();
        let symbol = $row.data('symbol');
        let crypto_available = $row.data('available');
        //let withdrawal_address = $row.data('withdrawal_address');
        //let withdrawal_memo = $row.data('withdrawal_memo');

        let currency = null;
        if (typeof dictionary_currencies !== undefined) {
            currency = dictionary_currencies[symbol];
        }

        if (currency !== undefined && currency !== null && currency.useScatter === 1) {
            Form_Withdraw_Scatter_Show(year, month, transaction_type_id, symbol, crypto_available);
        } else {
            Form_Withdraw_Show(year, month, transaction_type_id, symbol, crypto_available);
        }
    }   // btnWithdraw_Click

    function showScatterModal() {
        $('#CryptoWithdrawal_Scatter_Wait_Modal').modal('show');
    }
    
    function hideScatterModal() {
        $('#CryptoWithdrawal_Scatter_Wait_Modal').modal('hide');
    }

    async function EOSrequest_Show(year, month, transaction_type_id, symbol, amount, withdraw_address, withdraw_memo) {
        'use strict';
        let response = null;
        
        showScatterModal();
    
        response = await scatterTransferEOS(symbol, amount, withdraw_address, withdraw_memo);
    
        //console.log('response', response);
    
        hideScatterModal();
    
        if (response !== null && response.success) {
            let fee_spent = 0.0;
            
            showSuccessMessage('Transfer Successful\r\n' + "Txn ID: " + response.transaction_id);
            
            WithdrawCrypto_Insert(year, month, transaction_type_id, symbol, amount, fee_spent, withdraw_address, withdraw_memo, response.transaction_id
                , function (data) {
                    if (data.result.code === ResponseCodeEnum.SUCCESS) {
                        RefreshReport();
                        Hide_Form_Withdraw();
                        showSuccessMessage('Withdrawal Saved Successfully');
                    } else {
                        switch (data.result.code) {
                            default: {
                                //showErrorMessage(data.result.message);
                                alert(data.result.message);
                                break;
                            }
                        }
                    }
            });
        } else {
            if (response.errorMessage.includes('User Cancelled')) {
                // Do nothing
            }
            else if (response.errorMessage.includes('Insufficient Funds')) {
                showErrorMessage('Insufficient Funds');
            } else {
                showErrorMessage('Transfer Failed\r\n' + response.errorMessage);
            }
        }
    }   // EOSrequest_Show

    function Form_Withdraw_Scatter_Show(year, month, transaction_type_id, symbol, crypto_available) {
        'use strict';
        $('#CryptoWithdrawal_Withdraw_Scatter_Year').val(year);
        $('#CryptoWithdrawal_Withdraw_Scatter_Month').val(month);
        $('#CryptoWithdrawal_Withdraw_Scatter_TransactionTypeID').val(transaction_type_id);
        $('#CryptoWithdrawal_Withdraw_Scatter_Amount').val(crypto_available);
        $('#CryptoWithdrawal_Withdraw_Scatter_Symbol').val(symbol);
    
        $('#CryptoWithdrawal_Form_Withdraw_Scatter_Modal').modal( { show: true, keyboard: false, backdrop: 'static' } );
    }

    function Form_Withdraw_Show(year, month, transaction_type_id, symbol, crypto_available) {
        'use strict';
    
        $('#CryptoWithdrawal_Withdraw_Year').val(year);
        $('#CryptoWithdrawal_Withdraw_Month').val(month);
        $('#CryptoWithdrawal_Withdraw_TransactionTypeID').val(transaction_type_id);
    
        $('#CryptoWithdrawal_Withdraw_Amount_Cell').text(formatTokenBalance(symbol, crypto_available));
        $('#CryptoWithdrawal_Withdraw_Symbol_Cell').html('<img src="/images/wallets/' + symbol.toUpperCase() + '.png" style="height:20px;width: auto;margin-right: 10px;"></img>' + symbol);
        $('#CryptoWithdrawal_Withdraw_Address').val('');
        
    
        // http://davidshimjs.github.io/qrcodejs/
        $('#CryptoWithdrawal_Withdraw_QRPlaceHolder').html('');
        $('#CryptoWithdrawal_Withdraw_WarningRow').addClass('hidden');
        $('#CryptoWithdrawal_Withdraw_WarningText').html('');
        $('#CryptoWithdrawal_Withdraw_FeeSpent').val('');
        $('#CryptoWithdrawal_Withdraw_FeeSpent').trigger('input');
        $('#CryptoWithdrawal_Withdraw_TransactionID').val('');
    
        $('#CryptoWithdrawal_Form_Withdraw_Modal').modal( { show: true, keyboard: false, backdrop: 'static' } );
    }

    function CryptoWithdrawal_Withdraw_FeeSpent_Input(event) {
        'use strict';
        let qrCodeData = null;
        let amountLabel = 'amount';
        let amount = $('#CryptoWithdrawal_Withdraw_Amount_Cell').text();
        let fee_withdrawal = $('#CryptoWithdrawal_Withdraw_FeeSpent').val().trim();
        let symbol = $('#CryptoWithdrawal_Withdraw_Symbol_Cell').text().trim();
        let withdraw_address = $('#CryptoWithdrawal_Withdraw_Address').val().trim();
        let amountLessFee = 0.0;
        let warningMessage = '';
    
        amount = parseFloat(amount);
        if (isNaN(amount)) {
            amount = 0;
        }
    
        fee_withdrawal = parseFloat(fee_withdrawal);
        if (isNaN(fee_withdrawal)) {
            fee_withdrawal = 0;
        }
    
        amountLessFee = amount - fee_withdrawal;
    
        if (typeof dictionary_currencies !== undefined && dictionary_currencies[symbol.toUpperCase()] !== undefined) {
            let currency = dictionary_currencies[symbol];
            qrCodeData = currency.qr_code_prefix;
            amountLessFee = formatTokenBalance(symbol, amountLessFee);
            
        } else {
            proceed = false;
            amountLessFee = amountLessFee.toFixed(8);
            showErrorMessage('Unsupported Currency "' + symbol + '"');
        }
    
        $('#CryptoWithdrawal_Withdraw_AmountLessFee_Cell').text(amountLessFee);
    
        if (symbol === 'ETH') {
            let web3 = new Web3(Web3.givenProvider);    // Need Web3 to correctly convert ETH to Wei
            warningMessage = 'Only use this QR Code with the Blockchain or Exodus Wallet Apps.<br/><br/>The amount is incompatible with other apps.'
        }
    
        if (qrCodeData !== null) {
            if (qrCodeData.length > 0) {
                qrCodeData += ':';
            }
            qrCodeData += withdraw_address + '?' + amountLabel + '=' + amountLessFee;
        
            $('#CryptoWithdrawal_Withdraw_QRPlaceHolder').html('');
        
            let qr = new QRCode('CryptoWithdrawal_Withdraw_QRPlaceHolder', {
                text: qrCodeData,
                width: 200,
                height: 200,
                colorDark : "#000000",
                colorLight : "#ffffff",
                correctLevel : QRCode.CorrectLevel.M    // L 7%, M 15%, Q 25%, H 30%
            });
        }
        
    
        $('#CryptoWithdrawal_Withdraw_QRPlaceHolder img').css('margin-left', 'auto').css('margin-right', 'auto');
    
        if (warningMessage.length > 0) {
            $('#CryptoWithdrawal_Withdraw_WarningText').html(warningMessage);
            $('#CryptoWithdrawal_Withdraw_WarningRow').removeClass('hidden');
        }
    } // txtWithdrawFeesFeeSpent_Change
    
    function Form_Withdraw_Submit(event) {
        'use strict';
        event.target.blur();
        event.preventDefault();

        let proceed = true;

        $('#CryptoWithdrawal_Withdraw_Address_Error').hide();
        $('#CryptoWithdrawal_Withdraw_FeeSpent_Error').hide();
        $('#CryptoWithdrawal_Withdraw_TransactionID_Error').hide();

        let year = $('#CryptoWithdrawal_Withdraw_Year').val().trim();
        let month = $('#CryptoWithdrawal_Withdraw_Month').val().trim();
        let transaction_type_id = $('#CryptoWithdrawal_Withdraw_TransactionTypeID').val().trim();

        let symbol = $('#CryptoWithdrawal_Withdraw_Symbol_Cell').text().trim();
        let amount = $('#CryptoWithdrawal_Withdraw_Amount_Cell').text().trim();
        let fee_spent = $('#CryptoWithdrawal_Withdraw_FeeSpent').val().trim();
        let withdraw_address = $('#CryptoWithdrawal_Withdraw_Address').val().trim();
        let withdraw_memo = '';
        let txn_reference = $('#CryptoWithdrawal_Withdraw_TransactionID').val().trim();
    
        if (proceed) {
            if (withdraw_address === undefined || withdraw_address.length === 0) {
                $('#CryptoWithdrawal_Withdraw_Address_Error').show();
                proceed = false;
            }

            if (fee_spent === undefined || fee_spent.length === 0 || isNaN(fee_spent)) {
                $('#CryptoWithdrawal_Withdraw_FeeSpent_Error').show();
                proceed = false;
            }

            if (txn_reference === undefined || txn_reference.length === 0) {
                $('#CryptoWithdrawal_Withdraw_TransactionID_Error').show();
                proceed = false;
            }
        }

        if (proceed) {
            WithdrawCrypto_Insert(year, month, transaction_type_id, symbol, amount, fee_spent, withdraw_address, withdraw_memo, txn_reference
                , function (data) {
                    if (data.result.code === ResponseCodeEnum.SUCCESS) {
                        RefreshReport();
                        Hide_Form_Withdraw();
                        showSuccessMessage('Withdrawal Saved Successfully');
                    } else {
                        switch (data.result.code) {
                            default: {
                                //showErrorMessage(data.result.message);
                                alert(data.result.message);
                                break;
                            }
                        }
                    }
            });
        }
    
        return false;
    }

    function Form_Withdraw_Scatter_Submit(event) {
        'use strict';
        event.target.blur();
        event.preventDefault();
        
        let year = $('#CryptoWithdrawal_Withdraw_Scatter_Year').val().trim();
        let month = $('#CryptoWithdrawal_Withdraw_Scatter_Month').val().trim();
        let transaction_type_id = $('#CryptoWithdrawal_Withdraw_Scatter_TransactionTypeID').val();
        let symbol = $('#CryptoWithdrawal_Withdraw_Scatter_Symbol').val();
        let amount = $('#CryptoWithdrawal_Withdraw_Scatter_Amount').val();
        let withdraw_address = 'mytreosworld';  // Always go here
        let withdraw_memo = '';

        switch (transaction_type_id) {
            case '3' : {
                // Account Creation
                withdraw_memo = 'account service top up';
                break;
            }
            default : {
                break;
            }
        }
    
        $('#CryptoWithdrawal_Form_Withdraw_Scatter_Modal').modal('hide');
    
        if (!scatter) {
            showErrorMessage('Scatter is not running.');
        } else if (!scatter.identity) {
            showErrorMessage('Please attach Scatter account.');
        } else {
            EOSrequest_Show(year, month, transaction_type_id, symbol, amount, withdraw_address, withdraw_memo)
        }
    
        return false;
    }

    function WithdrawCrypto_Insert(year, month, transaction_type_id, symbol, amount, fee_spent, withdraw_address, withdraw_memo, txn_reference, successFn) {
        'use strict';
        let formData = new FormData();
    
        formData.append('year', year);
        formData.append('month', month);
        formData.append('transaction_type_id', transaction_type_id);
        formData.append('symbol', symbol);
        formData.append('amount', amount);
        formData.append('fee_spent', fee_spent);
        formData.append('withdraw_address', withdraw_address);
        formData.append('withdraw_memo', withdraw_memo);
        formData.append('txn_reference', txn_reference);
    
        $.ajax({
            //url: '/api/admin/completeWithdrawalRequest',
            url: '/api.php?target=withdrawal_requests&endpoint=' + encodeURIComponent('admin/withdrawCrypto_Insert'),
            method: 'post',
            dataType: 'json',
            processData: false,
            contentType: false,
            cache: false,
            timeout: 0,
            data: formData,
            // beforeSend: function() {
            //     $('#product-images-spinner').removeClass('hidden');
            // }
        })
        .done(function (data) {
            if (successFn) {
                successFn(data);
            }
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            alert('fail - ' + errorThrown);
        })
        .always(function () {
            //$('#product-images-spinner').addClass('hidden');
        });
    }

    function Hide_Form_Withdraw() {
        $('#CryptoWithdrawal_Form_Withdraw_Modal').modal('hide');
    }

    function Withdraw_btnCancel_Click(event) {
        'use strict';
        event.target.blur();
        event.preventDefault();
    
        Hide_Form_Withdraw();
        return false;
    }

    function CryptoWithdrawal_btnScatterClose_Click(event) {
        'use strict';
        event.target.blur();
        event.preventDefault();

        $('#CryptoWithdrawal_Form_Withdraw_Scatter_Modal').modal('hide');
        return false;
    }

    function Report_BuildData(forExport) {
        'use strict';
        let formData = new FormData();
        formData.append('symbol', document.getElementById("CryptoWithdrawal_Symbol").value);
        formData.append('transaction_type_id', document.getElementById("CryptoWithdrawal_Transaction_Type").value);
        formData.append('date_from', document.getElementById("CryptoWithdrawal_DateFrom").value);
        formData.append('date_to', document.getElementById("CryptoWithdrawal_DateTo").value);
        formData.append('record_offset', document.getElementById("CryptoWithdrawal_RecordOffset").value);
        formData.append('record_counter', document.getElementById("CryptoWithdrawal_RecordCounter").value);
        formData.append('forExport', forExport);
    
        return formData;
    }   // Report_BuildData
    
    function GetReport(forExport, successFn, failFn, alwaysFn) {
        'use strict';
        $.ajax({
            url: '/api.php?target=withdrawal_requests&endpoint=' + encodeURIComponent('admin/getCryptoWithdrawals_Report'),
            method: 'post',
            dataType: 'json',
            processData: false,
            contentType: false,
            cache: false,
            timeout: 0,
            data: Report_BuildData(forExport)
        })
        .done(function (data) {
            'use strict';
            if (successFn !== undefined && successFn !== null) {
                successFn(data);
            }
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            'use strict';
            if (failFn !== undefined && failFn !== null) {
                failFn(jqXHR, textStatus, errorThrown);
            }
        })
        .always(function (data) {
            'use strict';
            if (alwaysFn !== undefined && alwaysFn !== null) {
                alwaysFn();
            }
        });
    }   // CryptoWithdrawal_GetReport
    
    function initialiseDatePickers() {
        /*
        bootstrap-datepicker
        https://bootstrap-datepicker.readthedocs.io/en/latest/options.html
         */
        var dp_from = $('#CryptoWithdrawal_DateFrom');
        var dp_to = $('#CryptoWithdrawal_DateTo');
        var container = $('#CryptoWithdrawal_Form').length > 0 ? $('#CryptoWithdrawal_Form').parent() : "body";
        var options = {
            format: 'yyyy-mm-dd',
            container: container,
            todayHighlight: true,
            autoclose: true,
            //startView: 'months',  // Disabled as disables ability to update the date to the last day of the month.
            minViewMode: 'months'
        };
    
        dp_from.datepicker(options);
        dp_to.datepicker(options);
    
        var date_now = new Date();
        var date_from = new Date(date_now.getUTCFullYear(), date_now.getUTCMonth() - 11, 1);     // First day of UTC 12 months ago
        var date_to = new Date(date_now.getUTCFullYear(), date_now.getUTCMonth() + 1, 0);   // Last day of current UTC month
    
        dp_from.datepicker('update', date_from);
        dp_to.datepicker('update', date_to);
    
        dp_to.datepicker().on('changeDate', function(event) {
            // `event` contains the extra attributes
            let dateSelected = event.date;
            let lastDayOfMonth = new Date(dateSelected.getFullYear(), dateSelected.getMonth() + 1, 0);   // Last day of the month
            $(event.target).datepicker('update', lastDayOfMonth);
            $(event.target).val(event.format('yyyy-mm-dd'));
        });
    }


    

    function wireUpElements() {
        $('#CryptoWithdrawal_Form_Report').on('submit', Form_Report_Submit);
        $('#CryptoWithdrawal_Form_Withdrawal').on('submit', Form_Withdraw_Submit);
        $('#CryptoWithdrawal_Form_Withdraw_Scatter').on('submit', Form_Withdraw_Scatter_Submit)
        $('#CryptoWithdrawal_Withdraw_Address').on('input', CryptoWithdrawal_Withdraw_FeeSpent_Input);
        $('#CryptoWithdrawal_Withdraw_FeeSpent').on('input', CryptoWithdrawal_Withdraw_FeeSpent_Input);
        $('#CryptoWithdrawal_Withdraw_Withdraw_btnCancel').on('click', Withdraw_btnCancel_Click);
        $('#CryptoWithdrawal_btnApplyFilter').on('click', CryptoWithdrawal_btnApplyFilter_Click);
        $('#CryptoWithdrawal_btnScatterClose').on('click', CryptoWithdrawal_btnScatterClose_Click);
        $('#CryptoWithdrawal_btnExport').on('click', CryptoWithdrawal_btnExport_Click);
        $('#CryptoWithdrawal_Withdraw_Withdraw_btnSubmit').on('click', CryptoWithdrawal_Withdraw_Withdraw_btnSubmit_Click);
        $('#btnScatterModalHide').on('click', hideScatterModal);
    }

    function initialise() {
        initialiseDatePickers();
        wireUpElements();
    }

    return {
        initialise,
        RefreshReport
    }

})();


// ##### Common to all tabs

function getPlatformCurrencies(successFn, failFn) {
    "use strict";
    var request = $.ajax({
        url: '/api.php?target=withdrawal_requests&endpoint=' + encodeURIComponent('vendor/getPlatformCurrencies'),
        method: 'post',
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 0
    });

    request.done(function (data) {
        if (successFn !== undefined) {
            successFn(data);
        }
    });

    request.fail(function (jqXHR, textStatus, errorThrown) {
        if (failFn !== undefined) {
            failFn(jqXHR, textStatus, errorThrown);
        }
    });
}

function getPlatformTransactionTypes(successFn, failFn) {
    "use strict";

    let formData = new FormData();
    
    formData.append('only_active', false);

    var request = $.ajax({
        url: '/api.php?target=withdrawal_requests&endpoint=' + encodeURIComponent('vendor/getPlatformTransactionTypes'),
        method: 'post',
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 0,
        data: formData
    });

    request.done(function (data) {
        if (successFn !== undefined) {
            successFn(data);
        }
    });

    request.fail(function (jqXHR, textStatus, errorThrown) {
        if (failFn !== undefined) {
            failFn(jqXHR, textStatus, errorThrown);
        }
    });
}

function loadCurrencies(successFn) {
    getPlatformCurrencies(function(data) {
        // Success
        if (data.result.code === ResponseCodeEnum.SUCCESS) {
            dictionary_currencies = data.currencies;

            populateCryptoDropDowns();

            if (successFn) {
                successFn(data);
            }
        } else {
            console.error(data.result.message);
        }
    },
    function(jqXHR, textStatus, errorThrown) {
        // Fail
        console.error(errorThrown);
    });
}

function loadTransactionTypes(successFn) {
    getPlatformTransactionTypes(function(data) {
        // Success
        if (data.result.code === ResponseCodeEnum.SUCCESS) {

            populateTransactionTypeDropDowns(data.transaction_types);

            if (successFn) {
                successFn(data);
            }
        } else {
            console.error(data.result.message);
        }
    },
    function(jqXHR, textStatus, errorThrown) {
        // Fail
        console.error(errorThrown);
    });
}

function populateCryptoDropDowns() {
    // Remove all options except for the first one.
    $('#CryptoWithdrawal_Symbol').children('option:not(:first)').remove();
    $('#History_Symbol').children('option:not(:first)').remove();

    for (let symbol in dictionary_currencies) {
        if (dictionary_currencies[symbol].isFiat === 0) {
            $('#CryptoWithdrawal_Symbol').append($('<option></option>')
                .val(dictionary_currencies[symbol].symbol)
                .html(dictionary_currencies[symbol].symbol + ' - ' + dictionary_currencies[symbol].name));

            $('#History_Symbol').append($('<option></option>')
                .val(dictionary_currencies[symbol].symbol)
                .html(dictionary_currencies[symbol].symbol + ' - ' + dictionary_currencies[symbol].name));
        }
    }

    $('#CryptoWithdrawal_Symbol').prop('selectedIndex', 0);
    $('#History_Symbol').prop('selectedIndex', 0);
}

function populateTransactionTypeDropDowns(transaction_types) {
    // Remove all options except for the first one.
    $('#CryptoWithdrawal_Transaction_Type').children('option:not(:first)').remove();
    $('#History_Transaction_Type').children('option:not(:first)').remove();

    for (let transaction_type_id in transaction_types) {
        $('#CryptoWithdrawal_Transaction_Type').append($('<option></option>')
            .val(transaction_type_id)
            .html(transaction_types[transaction_type_id].transaction_type));

        $('#History_Transaction_Type').append($('<option></option>')
            .val(transaction_type_id)
            .html(transaction_types[transaction_type_id].transaction_type));
    }

    $('#CryptoWithdrawal_Transaction_Type').prop('selectedIndex', 0);
    $('#History_Transaction_Type').prop('selectedIndex', 0);
}



$(document).ready(function () {
    'use strict';

    CryptoWithdrawal.initialise();

    loadCurrencies();

    loadTransactionTypes();

    CryptoWithdrawal.RefreshReport();

    
   
    // $('#frmWithdrawFees').on('submit', frmWithdrawFees_Submit);

    
    // $('#btnWithdrawFeesExport').on('click', btnWithdrawFeesExport_Click);
   
});
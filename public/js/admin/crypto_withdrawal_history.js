

var CryptoWithdrawal_History = (function () {

    function Form_Report_Submit(event) {
        'use strict';
        $('#History_btnApplyFilter').blur();
        event.preventDefault();
        clearNotifyMessages();
    
        RefreshReport();
        return false;
    }   // Form_Report_Submit

    function History_btnExport_Click() {
        'use strict';
        event.target.blur();
        event.preventDefault();

        GetReport(true, function(data) {
            /* success */
            if (data.result.code === ResponseCodeEnum.SUCCESS) {
                if (data.withdrawals.length > 0) {
                    let columnHeadings = ['Withdrawal ID',
                        'Date',
                        'Year',
                        'Month',
                        'Transaction Type',
                        'Currency',
                        'Amount',
                        'Fee Spent',
                        'Withdraw Address',
                        'Withdraw Memo',
                        'Transaction ID' ];
                    let exportArray = [];

                    for (let i = 0; i < data.withdrawals.length; i++) {
                        let withdrawal = data.withdrawals[i];

                        let item = {
                            withdrawal_id: withdrawal.withdrawal_id,
                            date: withdrawal.date,
                            year: withdrawal.year,
                            month: withdrawal.month,
                            transaction_type: withdrawal.transaction_type,
                            symbol: withdrawal.symbol,
                            amount: withdrawal.amount,
                            fee_spent: withdrawal.fee_spent,
                            withdraw_address: withdrawal.withdraw_address,
                            withdraw_memo: withdrawal.withdraw_memo,
                            txn_reference: withdrawal.txn_reference
                        }
                        exportArray.push(item);
                    }
                    downloadArrayAsCSV(exportArray, 'crypto_withdrawal_history.csv', columnHeadings);
                } else {
                    alert('no records found.');
                }
            } else {
                switch (data.result.code) {
                    default: {
                        showErrorMessage(data.result.message);
                        break;
                    }
                }
            }
        },
        function(jqXHR, textStatus, errorThrown) {
            /* fail */
            showErrorMessage('fail - ' + errorThrown);
        },
        function() {
            /* always */
        });
    } // History_btnExport_Click

    function RefreshReport() {
        'use strict';
        $('#History_ReportTable tbody td.truncate').off('click');

        GetReport(false, function(data) {
            /* success */
            if (data.result.code === ResponseCodeEnum.SUCCESS) {
                let total_records = data.total_records;
    
                let table = $('#History_ReportTable table');
                table.find('tbody tr').remove();
    
                if (data.withdrawals.length > 0) {
                    for (let i = 0; i < data.withdrawals.length; i++) {
                        let withdrawal = data.withdrawals[i];
    
                        let imageFilename = withdrawal.symbol.toUpperCase() + '.png';
    
                        let $row = $('<tr></tr>');
    
                        $row.attr('data-withdrawal_id', withdrawal.withdrawal_id).data('year', withdrawal.withdrawal_id);
                        $row.attr('data-symbol', withdrawal.symbol).data('symbol', withdrawal.symbol);
                        $row.attr('data-txn_reference', withdrawal.txn_reference).data('txn_reference', withdrawal.txn_reference);
                        $row.attr('data-fee_spent', withdrawal.fee_spent).data('fee_spent', withdrawal.fee_spent);

                        let $cell_date = $('<td></td>').addClass('date');
                        let $cell_year = $('<td></td>').addClass('year');
                        let $cell_month = $('<td></td>').addClass('month');
                        let $cell_transaction_type = $('<td></td>').addClass('transaction_type');
                        let $cell_symbol = $('<td></td>').addClass('symbol');
                        let $cell_amount = $('<td></td>').addClass('amount');
                        let $cell_fee_spent = $('<td></td>').addClass('fee_spent');
                        let $cell_withdraw_address = $('<td></td>').addClass('withdraw_address');
                        let $cell_txn_reference = $('<td></td>').addClass('txn_reference');
                        //let $cell_buttons = $('<td></td>');

                        $cell_date.html(formatUTCDateTime(withdrawal.date));

                        $cell_year.html(withdrawal.year);
                        $cell_month.html(withdrawal.month);

                        $cell_transaction_type.html(withdrawal.transaction_type).attr('style', 'text-align: left;');

                        let $img_symbol = $('<img/>').attr('src', '/images/wallets/' + imageFilename).attr('style', 'height:20px; width: auth; margin-right: 10px;');
                        $cell_symbol.append($img_symbol);
                        $cell_symbol.append(withdrawal.symbol);

                        $cell_amount.html(formatTokenBalance(withdrawal.symbol, withdrawal.amount)).attr('style', 'text-align: right;');

                        $cell_fee_spent.html(formatTokenBalance(withdrawal.symbol, withdrawal.fee_spent)).attr('style', 'text-align: right;');

                        $cell_withdraw_address.html(withdrawal.withdraw_address).attr('style', 'text-align: right;').attr('class', 'truncate truncateContents');

                        $cell_txn_reference.html(withdrawal.txn_reference).attr('style', 'text-align: right;').attr('class', 'truncate truncateContents');

                        $row.append($cell_date);
                        $row.append($cell_year);
                        $row.append($cell_month);
                        $row.append($cell_transaction_type);
                        $row.append($cell_symbol);
                        $row.append($cell_amount);
                        $row.append($cell_fee_spent);
                        $row.append($cell_withdraw_address);
                        $row.append($cell_txn_reference);
                        //$row.append($cell_buttons);

                        table.find('tbody').append($row);
                    }
    
                    generateReportPagination($('#History_NavTop'), $('#History_RecordOffset'), $('#History_RecordCounter'), total_records);
                    generateReportPagination($('#History_NavBottom'), $('#History_RecordOffset'), $('#History_RecordCounter'), total_records);
                } else {
                    let row = '<tr>';
                    row += '<td colspan="9">No Matching Withdrawal History Found</td>';
                    table.find('tbody').append(row);
                }
            } else {
                switch (data.result.code) {
                    default: {
                        showErrorMessage(data.result.message);
                        break;
                    }
                }
            }
        },
        function(jqXHR, textStatus, errorThrown) {
            /* fail */
            showErrorMessage('fail - ' + errorThrown);
        },
        function() {
            /* always */
            $('#History_ReportTable tbody td.truncate').on('click', function(event) {
                $(event.target).toggleClass('truncateContents');
           });
        });
    }   // RefreshReport

    function Report_BuildData(forExport) {
        'use strict';
        let formData = new FormData();

        formData.append('symbol', document.getElementById("History_Symbol").value);
        formData.append('transaction_type_id', document.getElementById("History_Transaction_Type").value);
        formData.append('date_from', document.getElementById("History_DateFrom").value);
        formData.append('date_to', document.getElementById("History_DateTo").value);
        formData.append('record_offset', document.getElementById("History_RecordOffset").value);
        formData.append('record_counter', document.getElementById("History_RecordCounter").value);
        formData.append('forExport', forExport);
    
        return formData;
    }

    function GetReport(forExport, successFn, failFn, alwaysFn) {
        'use strict';
        $.ajax({
            url: '/api.php?target=withdrawal_requests&endpoint=' + encodeURIComponent('admin/getCryptoWithdrawalHistory_Report'),
            method: 'post',
            dataType: 'json',
            processData: false,
            contentType: false,
            cache: false,
            timeout: 0,
            data: Report_BuildData(forExport)
        })
        .done(function (data) {
            'use strict';
            if (successFn !== undefined && successFn !== null) {
                successFn(data);
            }
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            'use strict';
            if (failFn !== undefined && failFn !== null) {
                failFn(jqXHR, textStatus, errorThrown);
            }
        })
        .always(function (data) {
            'use strict';
            if (alwaysFn !== undefined && alwaysFn !== null) {
                alwaysFn();
            }
        });
    }   // CryptoWithdrawal_GetReport

    function initialiseDatePickers() {
        /*
        bootstrap-datepicker
        https://bootstrap-datepicker.readthedocs.io/en/latest/options.html
        */
        var dp_from = $('#History_DateFrom');
        var dp_to = $('#History_DateTo');
        //var container = $('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
        var container = $('#History_Form_Report').length>0 ? $('#History_Form_Report').parent() : "body";
        var options = {
            format: 'yyyy-mm-dd',
            container: container,
            todayHighlight: true,
            autoclose: true,
            //startView: 'months',
            //minViewMode: 'months'
        };

        dp_from.datepicker(options);
        dp_to.datepicker(options);

        var date_now = new Date();
        var date_from = new Date(date_now.getUTCFullYear(), date_now.getUTCMonth(), 1);     // First day of current UTC month
        var date_to = new Date(date_now.getUTCFullYear(), date_now.getUTCMonth() + 1, 0);   // Last day of current UTC month

        dp_from.datepicker('update', date_from);
        dp_to.datepicker('update', date_to);
    }

    function wireUpElements() {
        $('#History_Form_Report').on('submit', Form_Report_Submit);
        $('#History_btnExport').on('click', History_btnExport_Click);
        // $('#CryptoWithdrawal_Form_Withdrawal').on('submit', Form_Withdraw_Submit);
        // $('#CryptoWithdrawal_Form_Withdraw_Scatter').on('submit', Form_Withdraw_Scatter_Submit)
        // $('#CryptoWithdrawal_Withdraw_FeeSpent').on('input', CryptoWithdrawal_Withdraw_FeeSpent_Input);
        // $('#CryptoWithdrawal_Withdraw_Withdraw_btnCancel').on('click', Withdraw_btnCancel_Click);
        // $('#CryptoWithdrawal_btnApplyFilter').on('click', CryptoWithdrawal_btnApplyFilter_Click);
        // $('#CryptoWithdrawal_btnScatterClose').on('click', CryptoWithdrawal_btnScatterClose_Click);
    }

    function initialise() {
        initialiseDatePickers();
        wireUpElements();
    }

    return {
        initialise
    }

})();


$(document).ready(function () {
    'use strict';

    CryptoWithdrawal_History.initialise();
});
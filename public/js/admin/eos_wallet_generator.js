var dictionary_currencies = undefined;

function CreationRequests_Form_Report_Submit(event) {
    'use strict';
    $('#CreationRequests_btnApplyFilter').blur();
    event.preventDefault();
    clearNotifyMessages();
    CreationRequests_RefreshReport(null);
    return false;
}

function CreationRequests_btnApplyFilter_Click(event) {
    'use strict';
    $('#CreationRequests_RecordOffset').val('0');   // Reset offest to 0 every time a new search is performed.
}

function CreationRequests_btnNext_Click(event) {
    'use strict';
    $(event.target).blur();

    let request_id = $('#CreationRequests_RequestID').val().trim();
    let email = $('#CreationRequests_Email').val().trim();
    let txn_reference = $('#CreationRequests_TxnReference').val().trim();

    $('#CreationRequests_AccountHasBeenCreated_Error').hide();
    $('#CreationRequests_TxnReference_Error').hide();
    $('#CreationRequests_EmailHasBeenSent_Error').hide();

    let txnReferenceIsValid = false;
    let accountHasBeenCreated = $('#CreationRequests_AccountHasBeenCreated').is(':checked');
    let emailHasBeenSent = $('#CreationRequests_EmailHasBeenSent').is(':checked');
    
    if (!accountHasBeenCreated) {
        $('#CreationRequests_AccountHasBeenCreated_Error').html('You must create the EOS Account.').show();
    }

    if (!emailHasBeenSent) {
        $('#CreationRequests_EmailHasBeenSent_Error').html('You must create the EOS Account.').show();
    }

    if (txn_reference === undefined && txn_reference === null || txn_reference.length === 0) {
        $('#CreationRequests_TxnReference_Error').html('You must supply a Transaction ID.').show();
        txnReferenceIsValid = false;
    } else {
        txnReferenceIsValid = true;
    }

    if (accountHasBeenCreated && emailHasBeenSent && txnReferenceIsValid) {
        CreationRequests_CompleteRequest(request_id, email, txn_reference);
    }

}

function parseTransactionReference(txn_reference) {
    'use strict';
    // https://bloks.io/transaction/34bf8a168006265a6195f484b21a0de5ecf82549fdae737a4f54a5e37b42815b
    let result = '';
    let arr = txn_reference.split('/');
    if (arr.length > 0) {
        result = arr[arr.length -1];
    }
    return result;
}

function buildTransactionReference(txn_reference) {
    'use strict';
    let url = 'https://bloks.io/transaction/';
    let result = parseTransactionReference(txn_reference);

    if (result.length > 0) {
        result = url + result;
    }
    
    return result;
}

function CreationRequests_btnShowEmail_Click(event) {
    'use strict';
    $(event.target).blur();

    let email = $('#CreationRequests_Email').val();
    let account_name = $('#CreationRequests_AccountName').val();
    let owner_public_key = $('#CreationRequests_OwnerPublicKey').val();
    let active_public_key = $('#CreationRequests_ActivePublicKey').val();
    let txn_reference = $('#CreationRequests_TxnReference').val();

    $('#CreationRequests_EmailCopy').val(email);
    $('#CreationRequests_EmailBody').html('Your new EOS account has been successfully created with the details that you provided:\r\n' +
        '\r\n' +
        'ACCOUNT NAME:\r\n' +
        account_name + '\r\n\r\n' +
        'OWNER PUBLIC KEY:\r\n' +
        owner_public_key + '\r\n\r\n' +
        'ACTIVE PUBLIC KEY:\r\n' +
        active_public_key + '\r\n\r\n' +
        'Please make sure you have stored your EOS private keys in a safe place. Loosing your private keys could result in loss of control of your EOS account and any assets held in your EOS account.\r\n\r\n' +
        'TRANSACTION ID: \r\n' +
        buildTransactionReference(txn_reference) + '\r\n');

    $('#CreationRequests_Show_Email_Modal').modal( { show: true, keyboard: false, backdrop: 'static' } );
}

async function CreationRequests_btnCreateWithScatter_Click(event) {
    'use strict';
    let response = null;
    let proceed = true;

    let account_name = $('#CreationRequests_AccountName').val();
    let owner_public_key = $('#CreationRequests_OwnerPublicKey').val();
    let active_public_key = $('#CreationRequests_ActivePublicKey').val();

    let cpu = 0.2;
    let net = 0.2;
    let ram = 6000;
    let tro = 0.01;

    if (proceed) {
        if (!scatter) {
            //showErrorMessage('Scatter is not running.');
            alert('Scatter is not running;');
            proceed = false;
        } else if (!scatter.identity) {
            //showErrorMessage('Please attach Scatter account.');
            alert('Please attach Scatter account.');
            proceed = false;
        }
    }

    if (proceed) {

        //showScatter_Wait_Modal();

        //response = await scatterTransferEOS(symbol, amount, withdraw_address, withdraw_memo);
        response = await scatterCreateAccount(account_name, owner_public_key, active_public_key, cpu, net, ram, tro, 'Welcome to EOS!');

        //console.log('response', response);

        //hideScatter_Wait_Modal();

        if (response !== null && response.success) {
            let fee_spent = 0.0;
            
            //showSuccessMessage('Transfer Successful\r\n' + "Txn ID: " + response.transaction_id);
            alert('Transfer Successful\r\n' + "Txn ID: " + response.transaction_id);
            
            $('#CreationRequests_TxnReference').val(response.transaction_id);
            $('#CreationRequests_AccountHasBeenCreated').prop('checked', true);
            
        } else {
            if (response.errorMessage.includes('User Cancelled')) {
                // Do nothing
            }
            else if (response.errorMessage.includes('Insufficient Funds')) {
                //showErrorMessage('Insufficient Funds');
                alert('Insufficient Funds');
            } else {
                //showErrorMessage('Transfer Failed\r\n' + response.errorMessage);
                alert('Transfer Failed\r\n' + response.errorMessage);
            }
        }
    }
}

function CreationRequests_btnCancelRequest_Click(event) {
    'use strict';
    event.target.blur();
    event.preventDefault();

    let request_id = $('#CreationRequests_RequestID').val().trim();
    let email = $('#CreationRequests_Email').val().trim();

    $('#CreationRequests_Form_Request_Edit_Modal').modal('hide');

    CreationRequests_ShowForm_Request_Cancel();

    return false;
}

function CreationRequests_btnCancelRequest_Confirm_Click(event) {
    'use strict';
    event.target.blur();
    event.preventDefault();
    
    let request_id = $('#CreationRequests_RequestID').val().trim();
    let email = $('#CreationRequests_Email').val().trim();
    let reason = $('#CreationRequests_CancelReason').val().trim();

    CreationRequests_CancelRequest(request_id, reason, email);

    return false;
}

function CreationRequests_CancelRequest(request_id, reason, email) {
    'use strict';
    let formData = new FormData();
    
    formData.append('request_id', request_id);
    formData.append('reason', reason);
    formData.append('email', email);

    $.ajax({
        //url: '/api/admin/unreserveWithdrawalRequest',
        url: '/api.php?target=withdrawal_requests&endpoint=' + encodeURIComponent('admin/cancelEosAccountCreationRequest'),
        method: 'post',
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 0,
        data: formData,
        // beforeSend: function() {
        //     $('#product-images-spinner').removeClass('hidden');
        // }
    })
    .done(function (data) {
        if (data.result.code === ResponseCodeEnum.SUCCESS) {
            CreationRequests_RefreshReport(null);

            $('#CreationRequests_Form_Request_Edit_Modal').modal('hide');
            $('#CreationRequests_Form_Request_Cancel_Modal').modal('hide');

            showSuccessMessage('Account Creation Request has been Cancelled');
        } else {
            switch (data.result.code) {
                default: {
                    //showErrorMessage(data.result.message);
                    alert(data.result.message);
                    break;
                }
            }
        }
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
        alert('fail - ' + errorThrown);
    })
    .always(function () {
        //$('#product-images-spinner').addClass('hidden');
    });
} // CreationRequests_CancelRequest

function CreationRequests_CompleteRequest(request_id, email, txn_reference) {
    'use strict';
    let formData = new FormData();
    
    formData.append('request_id', request_id);
    formData.append('email', email);
    formData.append('txn_reference', parseTransactionReference(txn_reference));

    $.ajax({
        //url: '/api/admin/unreserveWithdrawalRequest',
        url: '/api.php?target=withdrawal_requests&endpoint=' + encodeURIComponent('admin/completeEosAccountCreationRequest'),
        method: 'post',
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 0,
        data: formData,
        // beforeSend: function() {
        //     $('#product-images-spinner').removeClass('hidden');
        // }
    })
    .done(function (data) {
        if (data.result.code === ResponseCodeEnum.SUCCESS) {
            CreationRequests_RefreshReport(null);

            $('#CreationRequests_Form_Request_Edit_Modal').modal('hide');
            $('#CreationRequests_Form_Request_Cancel_Modal').modal('hide');

            showSuccessMessage('Account Creation Request has been Completed');
        } else {
            switch (data.result.code) {
                default: {
                    //showErrorMessage(data.result.message);
                    alert(data.result.message);
                    break;
                }
            }
        }
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
        alert('fail - ' + errorThrown);
    })
    .always(function () {
        //$('#product-images-spinner').addClass('hidden');
    });
} // CreationRequests_CompleteRequest

function getPlatformCurrencies(successFn, failFn) {
    'use strict';
    var request = $.ajax({
        url: '/api.php?target=withdrawal_requests&endpoint=' + encodeURIComponent('vendor/getPlatformCurrencies'),
        method: 'post',
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 0
    });

    request.done(function (data) {
        if (data !== undefined && data !== null) {
            if (data.result.code === 0) {
                if (successFn !== undefined) {
                    successFn(data);
                }
            }
        }
    });

    request.fail(function (jqXHR, textStatus, errorThrown) {
        if (failFn !== undefined) {
            failFn(jqXHR, textStatus, errorThrown);
        }
    });
}

function loadCurrencies() {
    'use strict';
    getPlatformCurrencies(function(data) {
        'use strict';
        // Success
        if (data.result.code === ResponseCodeEnum.SUCCESS) {
            dictionary_currencies = data.currencies;

            buildCryptoDropDowns();

            CreationRequests_RefreshReport(null);
        } else {
            console.error(data.result.message);
        }
    },
    function() {
        // Fail
        jqXHR, textStatus;
        console.error(textStatus);
    });
}

function buildCryptoDropDowns() {
    //console.log(dictionary_currencies);

    $('#CreationRequests_Symbol').children('option:not(:first)').remove();   // Remove all options except for the first one.
    $('#CreationHistory_Symbol').children('option:not(:first)').remove();   // Remove all options except for the first one.
    // $('#cbWithdrawFeesCurrency').children('option:not(:first)').remove();   // Remove all options except for the first one.
    // $('#cbFeesWithdrawalHistoryCurrency').children('option:not(:first)').remove();   // Remove all options except for the first one.

    for (let symbol in dictionary_currencies) {
        if (dictionary_currencies[symbol].isFiat === 0) {
            $('#CreationRequests_Symbol').append($('<option></option>')
                .val(dictionary_currencies[symbol].symbol)
                .html(dictionary_currencies[symbol].symbol + ' - ' + dictionary_currencies[symbol].name));

            $('#CreationHistory_Symbol').append($('<option></option>')
                .val(dictionary_currencies[symbol].symbol)
                .html(dictionary_currencies[symbol].symbol + ' - ' + dictionary_currencies[symbol].name));
        }
    }

    $('#CreationRequests_Symbol').prop('selectedIndex', 0);
    $('#CreationHistory_Symbol').prop('selectedIndex', 0);
}

function CreationRequest_RowEdit_Click(event) {
    clearNotifyMessages();
    event.preventDefault();
    let row = $(event.target).closest('tr')[0];
    
    if (row !== undefined) {
        let request_id = $(row).attr('id');

        CreationRequests_GetReport(request_id, function(data) {
            /* success */
            if (data.result.code === ResponseCodeEnum.SUCCESS) {
                console.log(data.requests);
                if (data.requests.length > 0) {

                    let request = data.requests[0];

                    let account_name = request.account_name;
                    let owner_public_key = request.owner_public_key;
                    let active_public_key = request.active_public_key;
                    let email = request.email;
                    
                    CreationRequests_ShowForm_Request_Edit(request_id, account_name, owner_public_key, active_public_key, email);
                } else {
                    showErrorMessage('No records returned');
                }
            } else {
                switch (data.result.code) {
                    default: {
                        showErrorMessage(data.result.message);
                        break;
                    }
                }
            }
        },
        function(jqXHR, textStatus, errorThrown) {
            /* fail */
            showErrorMessage('fail - ' + errorThrown);
        },
        function(data) {
            /* always */
        });
    }
}


function CreationRequests_ShowForm_Request_Edit(request_id, account_name, owner_public_key, active_public_key, email) {
    'use strict';

    $('#CreationRequests_AccountHasBeenCreated').prop('checked', false);
    $('#CreationRequests_EmailHasBeenSent').prop('checked', false);

    $('#CreationRequests_AccountHasBeenCreated_Error').hide();
    $('#CreationRequests_EmailHasBeenSent_Error').hide();

    $('#CreationRequests_RequestID').val(request_id);
    $('#CreationRequests_AccountName').val(account_name);
    $('#CreationRequests_OwnerPublicKey').val(owner_public_key);
    $('#CreationRequests_ActivePublicKey').val(active_public_key);
    $('#CreationRequests_TxnReference').val('');
    $('#CreationRequests_Email').val(email);

    $('#CreationRequests_Form_Request_Edit_Modal').modal( { show: true, keyboard: false, backdrop: 'static' } );
}

function CreationRequests_ShowForm_Request_Cancel() {
    'use strict';

    $('#CreationRequests_Form_Request_Cancel_Modal').modal( { show: true, keyboard: false, backdrop: 'static' } );
}


function CreationRequests_RefreshReport(request_id) {
    'use strict';

    $('.creation-requests-edit').off('click');
    $('#CreationRequests_ReportTable tbody td.truncate').off('click');
    //$('#withdrawal_requests tbody td.truncate').off('click');

    CreationRequests_GetReport(request_id, function(data) {
        /* success */
        if (data.result.code === ResponseCodeEnum.SUCCESS) {
            let total_records = data.total_records;

            let table = $('#CreationRequests_ReportTable');
            table.find('tbody tr').remove();

            if (data.requests.length > 0) {
                
                for (let i = 0; i < data.requests.length; i++) {
                    let request = data.requests[i];

                    let imageFilename = request.symbol.toUpperCase() + '.png';

                    let row = '<tr id="' + request.request_id + '">';
                    row += '<td>' + formatUTCDateTime(request.date) + '</td>';
                    row += '<td>' + request.status + '</td>';
                    row += '<td>' + request.profile_id + '</td>';
                    row += '<td>' + request.profile_name + '</td>';
                    row += '<td>' + request.account_name + '</td>';
                    row += '<td><img src="/images/wallets/' + imageFilename + '" style="height:20px;width: auto;margin-right: 10px;">' + request.symbol + '</td>';
                    row += '<td style="text-align:right;">' + formatTokenBalance(request.symbol, request.fee) + '</td>';
                    row += '<td><a href="#" class="creation-requests-edit"><img src="/images/wallets/edit-icon.png" style="width:20px; height:20px;" /></a></td>';
                    row += '</tr>';
                    table.find('tbody').append(row);
                }

                generateReportPagination($('#CreationRequests_NavTop'), $('#CreationRequests_RecordOffset'), $('#CreationRequests_RecordCounter'), total_records);
                generateReportPagination($('#CreationRequests_NavBottom'), $('#CreationRequests_RecordOffset'), $('#CreationRequests_RecordCounter'), total_records);
            } else {
                let row = '<tr>';
                row += '<td colspan="8">No Matching EOS Wallet Creation Requests Found</td>';
                table.find('tbody').append(row);
            }
        } else {
            switch (data.result.code) {
                default: {
                    showErrorMessage(data.result.message);
                    break;
                }
            }
        }
    },
    function(jqXHR, textStatus, errorThrown) {
        /* fail */
        showErrorMessage('fail - ' + errorThrown);
    },
    function(data) {
        /* always */
        $('.creation-requests-edit').on('click', CreationRequest_RowEdit_Click);
        $('#CreationRequests_ReportTable tbody td.truncate').on('click', function(event) {
            $(event.target).toggleClass('truncateContents');
       });
    });
}

function CreationRequestsReport_BuildData(request_id) {
    'use strict';
    let formData = new FormData();
    formData.append('request_id', request_id);
    formData.append('symbol', document.getElementById("CreationRequests_Symbol").value);
    formData.append('member_name', document.getElementById("CreationRequests_Member").value);
    formData.append('record_offset', document.getElementById("CreationRequests_RecordOffset").value);
    formData.append('record_counter', document.getElementById("CreationRequests_RecordCounter").value);

    return formData;
}

function CreationRequests_GetReport(request_id, successFn, failFn, alwaysFn) {
    $.ajax({
        url: '/api.php?target=withdrawal_requests&endpoint=' + encodeURIComponent('admin/getEosWalletCreationRequests'),
        method: 'post',
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 0,
        data: CreationRequestsReport_BuildData(request_id)
    })
    .done(function (data) {
        if (successFn !== undefined && successFn !== null) {
            successFn(data);
        }
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
        if (failFn !== undefined && failFn !== null) {
            failFn(jqXHR, textStatus, errorThrown);
        }
    })
    .always(function (data) {
        if (alwaysFn !== undefined && alwaysFn !== null) {
            alwaysFn(data);
        }
    });
}

function copyTextBoxToClipboard_Click(event) {
    let $textbox = $(event.target).closest('.input-group').find('input');
    
    if ($textbox.length == 0) {
        $textbox = $(event.target).closest('.input-group').find('textarea');
    }

    if ($textbox.length == 0) {
        $textbox = $(event.target).closest('.form-group').find('textarea');

        // if ($textbox.length == 0) {
        //     alert('couldn\'t find textarea');
        // }
        
    }

    if ($textbox.length > 0) {
        let copyText = $textbox[0];

        /* Select the text field */
        copyText.select();
        copyText.setSelectionRange(0, 99999); /*For mobile devices*/

        /* Copy the text inside the text field */
        document.execCommand("copy");

        copyText.selectionStart = -1;
        copyText.selectionEnd = -1;

        alert('Copied to clipboard.');
    }
}


$(document).ready(function () {
    'use strict';
    
    /* Withdrawal Requests */
    // $('#frmScatter').on('submit', frmScatter_Submit);
    // $('#frmProcessWithdrawal').on('submit', frmProcessWithdrawal_Submit);
    $("#CreationRequests_Form_Report").on('submit', CreationRequests_Form_Report_Submit);
    // $('#frmWithdrawalCancel').on('submit', frmWithdrawalCancel_Submit);

    // $('#btnScatterCancelWithdrawal').on('click', btnScatterCancelWithdrawal_Click);
    $("#CreationRequests_btnCancelRequest").on('click', CreationRequests_btnCancelRequest_Click);
    $("#CreationRequests_btnCancelRequest_Confirm").on('click', CreationRequests_btnCancelRequest_Confirm_Click);
    // $("#btnScatterClose").on('click', btnScatterClose_Click);
    // $('#btnScatterModalHide').on('click', hideScatterModal);
    // $('#btnProcessLookupFee').on('click', btnProcessLookupFee_Click);
    // $('#txtProcessFee').on('change', txtProcessFee_Change).on('keyup', txtProcessFee_Change).on('paste', txtProcessFee_Change);
    $('#CreationRequests_btnApplyFilter').on('click', CreationRequests_btnApplyFilter_Click);
    $('#CreationRequests_btnNext').on('click', CreationRequests_btnNext_Click);
    $('#CreationRequests_btnShowEmail').on('click', CreationRequests_btnShowEmail_Click);
    $('#CreationRequests_btnCreateWithScatter').on('click', CreationRequests_btnCreateWithScatter_Click);
    

    // $('#btnWithdrawalCancelCancel').on('click', btnWithdrawalCancelCancel_Click);

    $('.copy-textbox').on('click', copyTextBoxToClipboard_Click)


    $( document ).ajaxStart(function() {
        $('.ajaxSpinner').removeClass('hidden');
    });

    $( document ).ajaxStop(function() {
        $('.ajaxSpinner').addClass('hidden');
    });

    loadCurrencies();
});
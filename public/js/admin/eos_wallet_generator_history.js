function CreationHistoryReport_BuildData(forExport, request_id) {
    'use strict';
    let formData = new FormData();
    formData.append('request_id', request_id);
    formData.append('symbol', document.getElementById("CreationHistory_Symbol").value);
    formData.append('member_name', document.getElementById("CreationHistory_Member").value);
    formData.append('date_from', document.getElementById("CreationHistory_DateFrom").value);
    formData.append('date_to', document.getElementById("CreationHistory_DateTo").value);
    formData.append('record_offset', document.getElementById("CreationHistory_RecordOffset").value);
    formData.append('record_counter', document.getElementById("CreationHistory_RecordCounter").value);
    formData.append('forExport', forExport);

    return formData;
}   // CreationHistoryReport_BuildData


function CreationHistory_GetReport(forExport, request_id, successFn, failFn, alwaysFn) {
    'use strict';
    $.ajax({
        url: '/api.php?target=withdrawal_requests&endpoint=' + encodeURIComponent('admin/getEosWalletCreationHistory'),
        method: 'post',
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 0,
        data: CreationHistoryReport_BuildData(forExport, request_id)
    })
    .done(function (data) {
        'use strict';
        if (successFn !== undefined && successFn !== null) {
            successFn(data);
        }
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
        'use strict';
        if (failFn !== undefined && failFn !== null) {
            failFn(jqXHR, textStatus, errorThrown);
        }
    })
    .always(function (data) {
        'use strict';
        if (alwaysFn !== undefined && alwaysFn !== null) {
            alwaysFn();
        }
    });
}   // CreationHistory_GetReport


function CreationHistory_RefreshReport() {
    'use strict';

    //$('#withdrawal_history tbody td').off('click');
    $('.creation-history-view').off('click');
    $('#CreationHistory_ReportTable tbody td.truncate').off('click');

    CreationHistory_GetReport(false, null, function(data) {
        /* success */
        if (data.result.code === ResponseCodeEnum.SUCCESS) {
            let total_records = data.total_records;

            let table = $('#CreationHistory_ReportTable');
            table.find('tbody tr').remove();

            if (data.requests.length > 0) {
                for (let i = 0; i < data.requests.length; i++) {
                    let request = data.requests[i];

                    let imageFilename = request.symbol.toUpperCase() + '.png';

                    let row = '<tr id="' + request.request_id + '">';
                    row += '<td>' + formatUTCDateTime(request.date) + '</td>';
                    row += '<td>' + request.status + '</td>';
                    row += '<td>' + request.profile_id + '</td>';
                    row += '<td>' + request.profile_name + '</td>';
                    row += '<td>' + request.account_name + '</td>';
                    row += '<td><img src="/images/wallets/' + imageFilename + '" style="height:20px;width: auto;margin-right: 10px;">' + request.symbol + '</td>';
                    row += '<td style="text-align:right;">' + formatTokenBalance(request.symbol, request.fee) + '</td>';
                    row += '<td style="text-align: left;" class="truncate truncateContents">' + cellText(request.reason) + '</td>';
                    row += '<td><a href="#" class="creation-history-view"><img src="/images/wallets/edit-icon.png" style="width:20px; height:20px;" /></a></td>';
                    row += '</tr>';
                    table.find('tbody').append(row);
                }

                generateReportPagination($('#CreationHistory_NavTop'), $('#CreationHistory_RecordOffset'), $('#CreationHistory_RecordCounter'), total_records);
                generateReportPagination($('#CreationHistory_NavBottom'), $('#CreationHistory_RecordOffset'), $('#CreationHistory_RecordCounter'), total_records);
            } else {
                let row = '<tr>';
                row += '<td colspan="9">No Matching Creation History Found</td>';
                table.find('tbody').append(row);
            }
        } else {
            switch (data.result.code) {
                default: {
                    showErrorMessage(data.result.message);
                    break;
                }
            }
        }

    },
    function(jqXHR, textStatus, errorThrown) {
        /* fail */
        showErrorMessage('fail - ' + errorThrown);
    },
    function() {
        /* always */
        $('.creation-history-view').on('click', CreationHistory_ReportRow_Click);
        $('#CreationHistory_ReportTable tbody td.truncate').on('click', function(event) {
             $(event.target).toggleClass('truncateContents');
        });
    });
}   // CreationHistory_RefreshReport

function CreationHistory_btnExport_Click(event) {
    'use strict';
    event.target.blur();
    event.preventDefault();

    CreationHistory_GetReport(true, null, function(data) {
        /* success */
        if (data.result.code === ResponseCodeEnum.SUCCESS) {
            if (data.requests.length > 0) {
                let columnHeadings = ['Request ID',
                    'Date Requested',
                    'Date Actioned',
                    'Status',
                    'Profile ID',
                    'Name',
                    'EOS Account Name',
                    'Owner Public Key',
                    'Active Public Key',
                    'Currency',
                    'Fee',
                    'Email',
                    'Transaction ID',
                    'Cancellation Reason' ];
                let exportArray = [];

                for (let i = 0; i < data.requests.length; i++) {
                    let request = data.requests[i];

                    let item = {
                        request_id: request.request_id,
                        date_request: request.date_request,
                        date: request.date,
                        status: request.status,
                        profile_id: request.profile_id,
                        profile_name: request.profile_name,
                        account_name: request.account_name,
                        owner_public_key: request.owner_public_key,
                        active_public_key: request.active_public_key,
                        symbol: request.symbol,
                        fee: request.fee,
                        email: request.email,
                        txn_reference: request.txn_reference,
                        reason: request.reason
                    }
                    exportArray.push(item);
                }
                downloadArrayAsCSV(exportArray, 'eos_wallet_creation_history.csv', columnHeadings);
            } else {
                alert('no records found.');
            }
        } else {
            switch (data.result.code) {
                default: {
                    showErrorMessage(data.result.message);
                    break;
                }
            }
        }
    },
    function(jqXHR, textStatus, errorThrown) {
        /* fail */
        showErrorMessage('fail - ' + errorThrown);
    },
    function() {
        /* always */
    });
} // CreationHistory_Export_Click


function CreationHistory_InitialiseDatePickers() {
    /*
    bootstrap-datepicker
    https://bootstrap-datepicker.readthedocs.io/en/latest/options.html
     */
    var dp_from = $('#CreationHistory_DateFrom');
    var dp_to = $('#CreationHistory_DateTo');
    var container = $('#CreationHistory_Form_Report').length > 0 ? $('#CreationHistory_Form_Report').parent() : "body";
    var options = {
        format: 'yyyy-mm-dd',
        container: container,
        todayHighlight: true,
        autoclose: true,
        //startView: 'months',
        //minViewMode: 'months'
    };

    dp_from.datepicker(options);
    dp_to.datepicker(options);

    var date_now = new Date();
    var date_from = new Date(date_now.getUTCFullYear(), date_now.getUTCMonth(), 1);     // First day of current UTC month
    var date_to = new Date(date_now.getUTCFullYear(), date_now.getUTCMonth() + 1, 0);   // Last day of current UTC month

    dp_from.datepicker('update', date_from);
    dp_to.datepicker('update', date_to);
}

function CreationHistory_ReportRow_Click(event) {
    clearNotifyMessages();
    event.preventDefault();
    let row = $(event.target).closest('tr')[0];
    
    if (row !== undefined) {
        let request_id = $(row).attr('id');
        
        CreationHistory_GetReport(false, request_id, function(data) {
            /* success */
            if (data.result.code === ResponseCodeEnum.SUCCESS) {
    
                if (data.requests.length > 0) {
                    let request = data.requests[0];

                    let date_request = request.date_request;
                    let date = request.date;
                    let status = request.status;
                    let profile_id = request.profile_id;
                    let profile_name = request.profile_name;
                    let account_name = request.account_name;
                    let owner_public_key = request.owner_public_key;
                    let active_public_key = request.active_public_key;
                    let symbol = request.symbol;
                    let fee = request.fee;
                    let email = request.email;
                    let txn_reference = request.txn_reference;
                    let reason = request.reason;

                    CreationHistory_ShowForm_Show_Details(request_id, date_request, date, status, profile_id, profile_name, account_name, owner_public_key, active_public_key, symbol, fee, email, txn_reference, reason);
                } else {
                    showErrorMessage('No records returned');
                }
            } else {
                switch (data.result.code) {
                    default: {
                        showErrorMessage(data.result.message);
                        break;
                    }
                }
            }
    
        },
        function(jqXHR, textStatus, errorThrown) {
            /* fail */
            showErrorMessage('fail - ' + errorThrown);
        },
        function() {
            /* always */
            $('.creation-history-view').on('click', CreationHistory_ReportRow_Click);
            $('#CreationHistory_ReportTable tbody td.truncate').on('click', function(event) {
                 $(event.target).toggleClass('truncateContents');
            });
        });
    }
}


function CreationHistory_ShowForm_Show_Details(request_id, date_request, date, status, profile_id, profile_name, account_name, owner_public_key, active_public_key, symbol, fee, email, txn_reference, reason) {
    'use strict';

    $('#CreationHistory_Show_DateRequest').html(formatUTCDateTime(date_request));
    $('#CreationHistory_Show_Date').html(formatUTCDateTime(date));
    $('#CreationHistory_Show_Status').html(cellText(status));
    $('#CreationHistory_Show_ProfileID').html(cellText(profile_id));
    $('#CreationHistory_Show_ProfileName').html(cellText(profile_name));
    $('#CreationHistory_Show_AccountName').html(cellText(account_name));
    $('#CreationHistory_Show_Symbol').html('<img src="/images/wallets/' + symbol.toUpperCase() + '.png" style="height:20px;width: auto;margin-right: 10px;">' + symbol);
    $('#CreationHistory_Show_Fee').html(formatTokenBalance(symbol, fee));
    $('#CreationHistory_Show_OwnerPublicKey').html(cellText(owner_public_key));
    $('#CreationHistory_Show_ActivePublicKey').html(cellText(active_public_key));
    $('#CreationHistory_Show_Email').html(cellText(email));
    $('#CreationHistory_Show_TxnReference').html(cellText(txn_reference));
    $('#CreationHistory_Show_Reason').html(cellText(reason));

    $('#CreationHistory_Show_Details_Modal').modal( { show: true, keyboard: false, backdrop: 'static' } );
}


function CreationHistory_btnApplyFilter_Click(event) {
    'use strict';
    $('#CreationHistory_RecordOffset').val('0');   // Reset offest to 0 every time a new search is performed.
} // CreationHistory_btnApplyFilter_Click


function CreationHistory_Form_Report_Submit(event) {
    'use strict';
    $('#CreationHistory_btnApplyFilter').blur();
    event.preventDefault();
    clearNotifyMessages();

    CreationHistory_RefreshReport();
    return false;
} // CreationHistory_Form_Report_Submit


$(document).ready(function () {
    'use strict';
    
    // $('#frmWithdrawalEdit').on('submit', frmWithdrawalEdit_Submit);
    $("#CreationHistory_Form_Report").on('submit', CreationHistory_Form_Report_Submit);
    // $("#btnWithdrawalEditCancel").on('click', btnWithdrawalEditCancel_Click);
    $('#CreationHistory_btnApplyFilter').on('click', CreationHistory_btnApplyFilter_Click);
    // $('#btnWithdrawalEditLookupFee').on('click', btnWithdrawalEditLookupFee_Click);
    $('#CreationHistory_btnExport').on('click', CreationHistory_btnExport_Click);
    
    CreationHistory_InitialiseDatePickers();
});

function btnApplyFeesWithdrawalHistoryFilter_Click(event) {
    'use strict';
    $('#WithdrawFeesHistoryRecordOffset').val('0');   // Reset offest to 0 every time a new search is performed.
} // btnApplyFeesWithdrawalHistoryFilter_Click


function frmFeesWithdrawalHistory_Submit(event) {
    'use strict';
    $('#btnApplyFeesWithdrawalHistoryFilter').blur();
    event.preventDefault();
    clearNotifyMessages();
    refreshFeesWithdrawalHistory();
    return false;
} // frmFeesWithdrawalHistory_Submit


function btnFeesWithdrawalHistoryExport_Click(event) {
    'use strict';
    event.target.blur();
    event.preventDefault();

    getFeesWithdrawalHistoryReport(true, function(data) {
        /* success */
        if (data.result.code === ResponseCodeEnum.SUCCESS) {
            if (data.withdrawals.length > 0) {
                let columnHeadings = ['Withdrawal ID', 'Date (UTC)', 'Year', 'Month', 'Currency', 'Amount', 'Fee Spent', 'Recipient Address', 'Txn ID' ];
                let exportArray = [];

                for (let i = 0; i < data.withdrawals.length; i++) {
                    let withdrawal = data.withdrawals[i];

                    let item = {
                        withdrawal_id: withdrawal.withdrawal_id,
                        date: withdrawal.date,
                        //date: formatUTCDateTimeForExport(withdrawal.date),
                        year: withdrawal.year,
                        month: withdrawal.month,
                        currency: withdrawal.currency,
                        amount: withdrawal.amount,
                        fee_spent: withdrawal.fee_spent,
                        address_to: withdrawal.address_to,
                        txn_reference: withdrawal.txn_reference
                    }
                    exportArray.push(item);
                }
                downloadArrayAsCSV(exportArray, 'fees_withdrawal_history.csv', columnHeadings);
            } else {
                alert('no records found.');
            }
        } else {
            switch (data.result.code) {
                default: {
                    showErrorMessage(data.result.message);
                    break;
                }
            }
        }
    },
    function(jqXHR, textStatus, errorThrown) {
        /* fail */
        showErrorMessage('fail - ' + errorThrown);
    },
    function() {
        /* always */
    });
} // btnFeesWithdrawalHistoryExport_Click


function FeesWithdrawalHistoryBuildData(forExport) {
    'use strict';
    let formData = new FormData();
    formData.append('currency', document.getElementById("cbFeesWithdrawalHistoryCurrency").value);
    formData.append('date_from', document.getElementById("txtFeesWithdrawalHistoryDateFrom").value);
    formData.append('date_to', document.getElementById("txtFeesWithdrawalHistoryDateTo").value);
    formData.append('record_offset', document.getElementById("FeesWithdrawalHistoryRecordOffset").value);
    formData.append('record_counter', document.getElementById("FeesWithdrawalHistoryRecordCounter").value);
    formData.append('forExport', forExport);

    return formData;
}


function getFeesWithdrawalHistoryReport(forExport, successFn, failFn, alwaysFn) {
    'use strict';
    $.ajax({
        url: '/api.php?target=withdrawal_requests&endpoint=' + encodeURIComponent('admin/getFeesWithdrawalHistory'),
        method: 'post',
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 0,
        data: FeesWithdrawalHistoryBuildData(forExport)
    })
    .done(function (data) {
        'use strict';
        if (successFn !== undefined && successFn !== null) {
            successFn(data);
        }
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
        'use strict';
        if (failFn !== undefined && failFn !== null) {
            failFn(jqXHR, textStatus, errorThrown);
        }
    })
    .always(function () {
        'use strict';
        if (alwaysFn !== undefined && alwaysFn !== null) {
            alwaysFn();
        }
    });
} // getWithdrawFeesHistoryReport


function refreshFeesWithdrawalHistory() {
    'use strict';

    $('#withdraw_fees_withdrawal_history tbody td.truncate').off('click');

    getFeesWithdrawalHistoryReport(false, function(data) {
        /* success */
        if (data.result.code === ResponseCodeEnum.SUCCESS) {
            let total_records = data.total_records;

            let table = $('#withdraw_fees_withdrawal_history table');

            table.find('tbody tr').remove();

            if (data.withdrawals.length > 0) {
                for (let i = 0; i < data.withdrawals.length; i++) {
                    let withdrawal = data.withdrawals[i];

                    let imageFilename = withdrawal.currency.toUpperCase() + '.png';
                    if (withdrawal.currency.toUpperCase() === 'TRO') {
                        imageFilename = withdrawal.currency.toLowerCase() + '.png';
                    }

                    let row = '<tr id="' + withdrawal.withdrawal_id + '"' +
                        ' data-currency="' + withdrawal.currency + '"' +
                        ' data-txnid="' + withdrawal.txn_reference + '"' +
                        ' data-feespent="' + withdrawal.fee_spent.toFixed(8) + '">';
                    row += '<td>' + formatUTCDateTime(withdrawal.date) + '</td>';
                    row += '<td>' + withdrawal.year + '</td>';
                    row += '<td>' + withdrawal.month + '</td>';
                    row += '<td><img src="/images/wallets/' + imageFilename + '" style="height:20px;width: auto;margin-right: 10px;">' + withdrawal.currency + '</td>';
                    row += '<td style="text-align:right;">' + formatTokenBalance(withdrawal.currency, withdrawal.amount) + '</td>';
                    row += '<td style="text-align:right;">' + formatTokenBalance(withdrawal.currency, withdrawal.fee_spent) + '</td>';
                    row += '<td style="text-align:right;" class="truncate truncateContents">' + withdrawal.address_to + '</td>';
                    row += '<td style="max-width: 200px; overflow: hidden; text-align: left;" class="truncate truncateContents">' + withdrawal.txn_reference + '</td>';
                    row += '</tr>';
                    table.find('tbody').append(row);
                }

                generateReportPagination($('#FeesWithdrawalHistoryNavTop'), $('#FeesWithdrawalHistoryRecordOffset'), $('#FeesWithdrawalHistoryRecordCounter'), total_records);
                generateReportPagination($('#FeesWithdrawalHistoryNavBottom'), $('#FeesWithdrawalHistoryRecordOffset'), $('#FeesWithdrawalHistoryRecordCounter'), total_records);
            } else {
                let row = '<tr>';
                row += '<td colspan="8">No Matching Fees Withdrawal History Found</td>';
                table.find('tbody').append(row);
            }
        } else {
            switch (data.result.code) {
                default: {
                    showErrorMessage(data.result.message);
                    break;
                }
            }
        }

    },
    function(jqXHR, textStatus, errorThrown) {
        /* fail */
        showErrorMessage('fail - ' + errorThrown);
    },
    function() {
        /* always */
        $('#withdraw_fees_withdrawal_history tbody td.truncate').on('click', function(event) {
            $(event.target).removeClass('truncateContents');
       });
    });
} // refreshWithdrawFeesHistory


function initialiseFeesWithdrawalHistoryDatePickers() {
    /*
    bootstrap-datepicker
    https://bootstrap-datepicker.readthedocs.io/en/latest/options.html
     */
    var dp_from = $('#txtFeesWithdrawalHistoryDateFrom');
    var dp_to = $('#txtFeesWithdrawalHistoryDateTo');
    //var container = $('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
    var container = $('#frmFeesWithdrawalHistory').length>0 ? $('#frmFeesWithdrawalHistory').parent() : "body";
    var options = {
        format: 'yyyy-mm-dd',
        container: container,
        todayHighlight: true,
        autoclose: true,
        //startView: 'months',
        //minViewMode: 'months'
    };

    dp_from.datepicker(options);
    dp_to.datepicker(options);

    var date_now = new Date();
    var date_from = new Date(date_now.getUTCFullYear(), date_now.getUTCMonth(), 1);     // First day of current UTC month
    var date_to = new Date(date_now.getUTCFullYear(), date_now.getUTCMonth() + 1, 0);   // Last day of current UTC month

    dp_from.datepicker('update', date_from);
    dp_to.datepicker('update', date_to);
}


$(document).ready(function () {
    'use strict';
    
    $("#frmFeesWithdrawalHistory").on('submit', frmFeesWithdrawalHistory_Submit);
    $('#btnApplyFeesWithdrawalHistoryFilter').on('click', btnApplyFeesWithdrawalHistoryFilter_Click);
    $('#btnFeesWithdrawalHistoryExport').on('click', btnFeesWithdrawalHistoryExport_Click);
    
    initialiseFeesWithdrawalHistoryDatePickers();
});
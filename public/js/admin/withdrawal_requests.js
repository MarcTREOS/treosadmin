//import TransportU2F from "@ledgerhq/hw-transport-u2f/src/TransportU2F";

var dictionary_currencies = undefined;

function reserveWithdrawalRequest(withdrawalID, overrideInProgress) {
    'use strict';
    let formData = new FormData();

    formData.append('withdrawalID', withdrawalID);
    formData.append('overrideInProgress', overrideInProgress);

    $.ajax({
        //url: '/api/admin/reserveWithdrawalRequest',
        url: '/api.php?target=withdrawal_requests&endpoint=admin%2FreserveWithdrawalRequest',
        method: 'post',
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 0,
        data: formData,
        // beforeSend: function() {
        //     $('#product-images-spinner').removeClass('hidden');
        // }
    })
    .done(function (data) {
        if (data.result.code === ResponseCodeEnum.SUCCESS) {
            let requestAmount = data.amount;
            let requestFee_Withdrawal = data.fee_withdrawal;
            let requestCurrency = data.currency;
            let requestAddressTo = data.address_to;
            let requestMemo = data.memo;

            if (requestCurrency === 'EOS' || requestCurrency === 'TRO') {
                //showEOSrequest(withdrawalID, requestAmount, requestFee_Withdrawal, requestCurrency, requestAddressTo);
                showForm_Scatter(withdrawalID, requestAmount, requestFee_Withdrawal, requestCurrency, requestAddressTo, requestMemo);
            } else {
                showForm(withdrawalID, requestAmount, requestFee_Withdrawal, requestCurrency, requestAddressTo, requestMemo);
            }
        } else {
            switch (data.result.code) {
                default: {
                    showErrorMessage(data.result.message);
                    break;
                }
            }
        }
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
        showErrorMessage('fail - ' + errorThrown);
    })
    .always(function () {
        //$('#product-images-spinner').addClass('hidden');
    });
}


function unreserveWithdrawalRequest(withdrawalID) {
    'use strict';
    let formData = new FormData();
    
    formData.append('withdrawalID', withdrawalID);

    $.ajax({
        //url: '/api/admin/unreserveWithdrawalRequest',
        url: '/api.php?target=withdrawal_requests&endpoint=admin%2FunreserveWithdrawalRequest',
        type: 'post',
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 0,
        data: formData,
        // beforeSend: function() {
        //     $('#product-images-spinner').removeClass('hidden');
        // }
    })
    .done(function (data) {
        if (data.result.code === ResponseCodeEnum.SUCCESS) {
            // Carry on
        } else {
            switch (data.result.code) {
                default: {
                    showErrorMessage(data.result.message);
                    break;
                }
            }
        }
        $('#form-scatter-modal').modal('hide');
        $('#form-process-modal').modal('hide');
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
        showErrorMessage('fail - ' + errorThrown);
    })
    .always(function () {
        //$('#product-images-spinner').addClass('hidden');
    });
} // unreserveWithdrawalRequest


function cancelWithdrawalRequest(withdrawalID, reason) {
    'use strict';
    let formData = new FormData();
    
    formData.append('withdrawalID', withdrawalID);
    formData.append('reason', reason);

    $.ajax({
        //url: '/api/admin/unreserveWithdrawalRequest',
        url: '/api.php?target=withdrawal_requests&endpoint=' + encodeURIComponent('admin/cancelWithdrawalRequest'),
        method: 'post',
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 0,
        data: formData,
        // beforeSend: function() {
        //     $('#product-images-spinner').removeClass('hidden');
        // }
    })
    .done(function (data) {
        if (data.result.code === ResponseCodeEnum.SUCCESS) {
            refreshWithdrawalRequests();
            $('#form-scatter-modal').modal('hide');
            $('#form-process-modal').modal('hide');

            $('#form-withdrawal-cancel-modal').modal('hide');

            showSuccessMessage('Withdrawal Request has been Cancelled');
        } else {
            switch (data.result.code) {
                default: {
                    //showErrorMessage(data.result.message);
                    alert(data.result.message);
                    break;
                }
            }
        }
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
        alert('fail - ' + errorThrown);
    })
    .always(function () {
        //$('#product-images-spinner').addClass('hidden');
    });
} // cancelWithdrawalRequest


function populateTokens() {
    'use strict';
    let formData = new FormData();

    $.ajax({
        //url: '/api/admin/getWithdrawalRequests',
        url: '/api.php?target=withdrawal_requests&endpoint=admin%2FgetWithdrawalRequests',
        type: 'post',
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 0,
        data: formData,
        // beforeSend: function() {
        //     $('#product-images-spinner').removeClass('hidden');
        // }
    })
    .done(function (data) {
        if (data.result.code === ResponseCodeEnum.SUCCESS) {
            let table = $('#withdrawal_requests');
            table.find('tbody tr').remove();

            for (let i = 0; i < data.requests.length; i++) {
                let request = data.requests[i];

                //https://stackoverflow.com/questions/2388115/get-locale-short-date-format-using-javascript/7740464
                let rqDate = new Date(request.date);    // Convert JSON Date string to Date object
                //toLocaleDateString("en", { weekday: 'short', year: 'numeric', month: '2-digit', day: 'numeric' })

                let dateOptions = {
                    weekday: "short",
                    year: "numeric",
                    month: "short",
                    day: "numeric",
                    hour12: false,
                    hour: '2-digit',
                    minute: '2-digit',
                    second: '2-digit'
                }
                let row = '<tr id="' + request.withdrawal_id + '" data-status="' + request.status + '" data-amount="' + request.amount.toFixed(8) + '" data-currency="' + request.currency + '" data-addressto="' + request.address_to + '">';
                row += '<td>' + rqDate.toLocaleString("en", dateOptions) + '</td>';
                row += '<td>' + request.status + '</td>';
                row += '<td>' + request.pending_hours + '</td>';
                row += '<td>' + request.profile_name + '</td>';
                row += '<td><img src="/images/wallets/' + request.currency.toUpperCase() + '.png" style="height:20px;width: auto;margin-right: 10px;">' + request.currency + '</td>';
                row += '<td style="text-align:right;">' + formatTokenBalance(request.currency, request.amount) + '</td>';
                row += '<td style="text-align:right;">' + request.address_to + '</td>';
                row += '</tr>';
                table.find('tbody').append(row);
            }

        } else {
            switch (data.result.code) {
                default: {
                    showErrorMessage(data.result.message);
                    break;
                }
            }
        }
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
        showErrorMessage('fail - ' + errorThrown);
    })
    .always(function () {
        //$('#product-images-spinner').addClass('hidden');
        $('#withdrawal_requests tbody tr').on('click', requestRow_Click);
    });
}


function WithdrawalRequestsBuildData() {
    'use strict';
    let formData = new FormData();
    formData.append('timeframe', document.getElementById("cbTimeframe").value);
    formData.append('currency', document.getElementById("cbCurrency").value);
    formData.append('member_name', document.getElementById("txtSearchMember").value);
    formData.append('record_offset', document.getElementById("WithdrawalRequestsRecordOffset").value);
    formData.append('record_counter', document.getElementById("WithdrawalRequestsRecordCounter").value);

    return formData;
}


function getWithdrawalRequestsReport(successFn, failFn, alwaysFn) {
    $.ajax({
        url: '/api.php?target=withdrawal_requests&endpoint=' + encodeURIComponent('admin/getWithdrawalRequests'),
        method: 'post',
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 0,
        data: WithdrawalRequestsBuildData()
    })
    .done(function (data) {
        if (successFn !== undefined && successFn !== null) {
            successFn(data);
        }
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
        if (failFn !== undefined && failFn !== null) {
            failFn(jqXHR, textStatus, errorThrown);
        }
    })
    .always(function (data) {
        if (alwaysFn !== undefined && alwaysFn !== null) {
            alwaysFn(data);
        }
    });
}


function refreshWithdrawalRequests() {
    'use strict';

    $('#withdrawal_requests tbody tr').off('click');
    //$('#withdrawal_requests tbody td.truncate').off('click');

    getWithdrawalRequestsReport(function(data) {
        /* success */
        if (data.result.code === ResponseCodeEnum.SUCCESS) {
            let total_records = data.total_records;

            let table = $('#withdrawal_requests');
            table.find('tbody tr').remove();

            if (data.requests.length > 0) {
                for (let i = 0; i < data.requests.length; i++) {
                    let request = data.requests[i];

                    let imageFilename = request.currency.toUpperCase() + '.png';

                    let row = '<tr id="' + request.withdrawal_id + '" data-status="' + request.status + '" data-amount="' + request.amount.toFixed(8) + '" data-currency="' + request.currency + '" data-addressto="' + request.address_to + '">';
                    row += '<td>' + formatUTCDateTime(request.date) + '</td>';
                    row += '<td>' + request.status + '</td>';
                    row += '<td>' + request.pending_hours + '</td>';
                    row += '<td>' + request.profile_id + '</td>';
                    row += '<td>' + request.profile_name + '</td>';
                    row += '<td><img src="/images/wallets/' + imageFilename + '" style="height:20px;width: auto;margin-right: 10px;">' + request.currency + '</td>';
                    row += '<td style="text-align:right;">' + formatTokenBalance(request.currency, request.amount) + '</td>';
                    row += '<td style="text-align:right;">' + formatTokenBalance(request.currency, request.current_balance) + '</td>';
                    row += '<td style="text-align:right;">' + request.address_to + '</td>';
                    row += '<td>' + request.memo + '</td>';
                    row += '</tr>';
                    table.find('tbody').append(row);
                }

                generateReportPagination($('#WithdrawalRequestsNavTop'), $('#WithdrawalRequestsRecordOffset'), $('#WithdrawalRequestsRecordCounter'), total_records);
                generateReportPagination($('#WithdrawalRequestsNavBottom'), $('#WithdrawalRequestsRecordOffset'), $('#WithdrawalRequestsRecordCounter'), total_records);
            } else {
                let row = '<tr>';
                row += '<td colspan="10">No Matching Withdrawal Requests Found</td>';
                table.find('tbody').append(row);
            }
        } else {
            switch (data.result.code) {
                default: {
                    showErrorMessage(data.result.message);
                    break;
                }
            }
        }
    },
    function(jqXHR, textStatus, errorThrown) {
        /* fail */
        showErrorMessage('fail - ' + errorThrown);
    },
    function() {
        /* always */
        $('#withdrawal_requests tbody tr td').on('click', requestRow_Click);
    //     $('#withdrawal_requests tbody td.truncate').on('click', function(event) {
    //         $(event.target).toggleClass('truncateContents');
    //    });
    });
}


function requestRow_Click(event) {
    clearNotifyMessages();
    let row = $(event.target).closest('tr')[0];
    
    if (row !== undefined) {
        let withdrawalID = $(row).attr('id');
        let requestStatus = $(row).data('status');
        let requestAmount = $(row).data('amount');
        let requestCurrency = $(row).data('currency');
        let requestAddressTo = $(row).data('addressto');
        
        let showForm = false;
        let overrideInProgress = false;

        if (requestStatus.toUpperCase() === 'PENDING') {   // PENDING, DONE
            showForm = true;
        } else if (requestStatus.toUpperCase() === 'IN_PROGRESS') {
            if (confirm('This request is already IN PROGRESS and may already be being processed by someone else.\r\n\r\nDo you still want to process this request?')) {
                overrideInProgress = true;
                showForm = true;
            }
        }

        if (showForm) {
            // if ((requestCurrency === 'EOS' || requestCurrency === 'TRO') && !scatter) {
            //     showErrorMessage('Scatter is not running.');
            // } else if ((requestCurrency === 'EOS' || requestCurrency === 'TRO') && !scatter.identity) {
            //     showErrorMessage('Please attach Scatter account.');
            // } else {
            //     //showForm(withdrawalID, requestAmount, requestCurrency, requestAddressTo);
                 reserveWithdrawalRequest(withdrawalID, overrideInProgress);
            // }
        }
    }
}


function feeLookup(transactionID, currencyCode, destinationID) {
    'use strict';

    //let transactionID = $('#txtProcessTransactionID').val().trim();
    //let currencyCode = $('#cellProcessCurrency').text().trim();

    if (transactionID !== undefined && transactionID !== null && transactionID.length > 0) {
        let formData = new FormData();

        formData.append('currencyCode', currencyCode);
        formData.append('transactionID', transactionID);

        $.ajax({
            url: '/api.php?target=withdrawal_requests&endpoint=' + encodeURIComponent('admin/getBlockchainDetails_Txn'),
            type: 'post',
            dataType: 'json',
            processData: false,
            contentType: false,
            cache: false,
            timeout: 0,
            data: formData,
            // beforeSend: function() {
            //     $('#product-images-spinner').removeClass('hidden');
            // }
        })
        .done(function (data) {
            console.log(data);
            if (data.result.code === ResponseCodeEnum.SUCCESS) {
                //$('#txtProcessFeeSpent').val(data.fee);
                $('#' + destinationID).val(data.fee);
            } else {
                switch (data.result.code) {
                    default: {
                        alert(data.result.message);
                        break;
                    }
                }
            }
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            alert('fail - ' + errorThrown);
        })
        .always(function () {
            //$('#product-images-spinner').addClass('hidden');
        });

    } else {
        alert('Transaction ID Must be Specified');
    }

}

function txtProcessFee_Change(event) {
    'use strict';
    let qrCodeData = '';
    let amountLabel = 'amount';
    let amount = $('#cellProcessAmount').text();
    let fee_withdrawal = $('#txtProcessFee').val().trim();
    let currencyCode = $('#cellProcessCurrency').text().trim();
    let addressTo = $('#cellProcessAddressTo').text().trim();
    let memo = $('#cellProcessMemo').text().trim();
    let amountLessFee = 0.0;
    let warningMessage = '';

    amount = parseFloat(amount);
    if (isNaN(amount)) {
        amount = 0;
    }

    fee_withdrawal = parseFloat(fee_withdrawal);
    if (isNaN(fee_withdrawal)) {
        fee_withdrawal = 0;
    }

    amountLessFee = amount - fee_withdrawal;

    $('#cellProcessAmountLessFee').text(amountLessFee.toFixed(8));

    switch (currencyCode.toLowerCase()) {
        case 'bch': {
            qrCodeData = 'bitcoincash';
            amountLessFee = amountLessFee.toFixed(8);
            break;
        }
        case 'bnb': {
            //qrCodeData = 'binancecoin';   // Didn't work on Exodus
            amountLessFee = amountLessFee.toFixed(8);
            break;
        }
        case 'btc': {
            qrCodeData = 'bitcoin';
            amountLessFee = amountLessFee.toFixed(8);
            break;
        }
        case 'eth': {
            qrCodeData = 'ethereum';
            //amountLabel = 'value';
            let web3 = new Web3(Web3.givenProvider);    // Need Web3 to correctly convert ETH to Wei
            //amount = web3.utils.toWei(requestAmount, 'ether');    // Blockchain wallet uses ETH instead of Wei
            amountLessFee = amountLessFee.toFixed(8);
            warningMessage = 'Only use this QR Code with the Blockchain or Exodus Wallet Apps.<br/><br/>The amount is incompatible with other apps.'
            break;
        }
        case 'ltc': {
            qrCodeData = 'litecoin';
            amountLessFee = amountLessFee.toFixed(8);
            break;
        }
        default: {
            proceed = false;
            amountLessFee = amountLessFee.toFixed(8);
            showErrorMessage('Unsupported Currency "' + requestCurrency + '"');
            break;
        }

        //qrCodeData = '';
    }

    qrCodeData += ':' + addressTo + '?' + amountLabel + '=' + amountLessFee;

    $('#qrPlaceHolder').html('');

    let qr_address = new QRCode('qrPlaceHolder', {
        text: qrCodeData,
        width: 150,
        height: 150,
        colorDark : "#000000",
        colorLight : "#ffffff",
        correctLevel : QRCode.CorrectLevel.M    // L 7%, M 15%, Q 25%, H 30%
    });

    $('#qrPlaceHolder img').css('margin-left', 'auto').css('margin-right', 'auto');

    if (memo.length > 0) {
        $('#qrPlaceHolder_Memo').html('');

        let qr_memo = new QRCode('qrPlaceHolder_Memo', {
            text: memo,
            width: 150,
            height: 150,
            colorDark : "#000000",
            colorLight : "#ffffff",
            correctLevel : QRCode.CorrectLevel.M    // L 7%, M 15%, Q 25%, H 30%
        });
        $('#qrPlaceHolder_Memo img').css('margin-left', 'auto').css('margin-right', 'auto');

        $('#row_memo').show();
        $('#row_memo_qr').show();
    } else {
        $('#row_memo').hide();
        $('#row_memo_qr').hide();
    }

    
    

    if (warningMessage.length > 0) {
        $('#warningRow').removeClass('hidden');
        $('#warningText').html(warningMessage);
    }
}


function showScatterModal() {
    $('#scatter-wait-modal').modal('show');
}

function hideScatterModal() {
    $('#scatter-wait-modal').modal('hide');
}

async function showEOSrequest(withdrawalID, requestAmount, requestFee_Withdrawal, requestCurrency, requestAddressTo, requestMemo) {
    let memo = requestMemo;
    let response = null;
    let amount = requestAmount;

    if (requestFee_Withdrawal !== undefined && requestFee_Withdrawal !== null && !isNaN(requestFee_Withdrawal)) {
        amount = amount - requestFee_Withdrawal;
    }

    if (requestMemo === undefined || requestMemo === null) {
        memo = '';
    }

    showScatterModal();

    // if (requestCurrency.toLowerCase() === 'eth') {
    //     console.log('Showing Scatter for ETH');
    //     response = await scatterTransferETH(requestAmount, requestAddressTo);
    // } else {
    response = await scatterTransferEOS(requestCurrency, amount, requestAddressTo, memo);
    // }

    //console.log('response', response);

    hideScatterModal();

    if (response.success) {
        let fee = 0.0;
        let fee_spent = 0.0;
        
        showSuccessMessage('Transfer Successful\r\n' + "Txn ID: " + response.transaction_id);
        completeWithdrawalRequest(withdrawalID, response.transaction_id, fee, fee_spent);
    } else {
        unreserveWithdrawalRequest(withdrawalID);

        if (response.errorMessage.includes('User Cancelled')) {
            // Do nothing
        }
        else if (response.errorMessage.includes('Insufficient Funds')) {
            showErrorMessage('Insufficient Funds');
        } else {
            showErrorMessage('Transfer Failed\r\n' + response.errorMessage);
        }
    }
}


function showForm_Scatter(withdrawalID, requestAmount, requestFee_Withdrawal, requestCurrency, requestAddressTo, requestMemo) {
    'use strict';
    $('#ScatterWithdrawalID').val(withdrawalID);
    $('#ScatterRequestAmount').val(requestAmount);
    $('#ScatterRequestFee_Withdrawal').val(requestFee_Withdrawal);
    $('#ScatterRequestCurrency').val(requestCurrency);
    $('#ScatterRequestAddressTo').val(requestAddressTo);
    $('#ScatterRequestMemo').val(requestMemo);

    $('#form-scatter-modal').modal( { show: true, keyboard: false, backdrop: 'static' } );
}


function showForm(withdrawalID, requestAmount, requestFee_Withdrawal, requestCurrency, requestAddressTo, requestMemo) {
    'use strict';

    $('#ProcessWithdrawalID').val(withdrawalID);
    $('#cellProcessAmount').text(requestAmount);
    $('#cellProcessCurrency').html('<img src="/images/wallets/' + requestCurrency.toUpperCase() + '.png" style="height:20px;width: auto;margin-right: 10px;"></img>' + requestCurrency);
    $('#cellProcessAddressTo').text(requestAddressTo);
    $('#cellProcessMemo').text(requestMemo);
    

    // http://davidshimjs.github.io/qrcodejs/
    $('#qrPlaceHolder').html('');
    $('#warningText').html('');
    $('#warningRow').addClass('hidden');
    $('#txtProcessTransactionID').val('');
    $('#txtProcessFeeSpent').val('');
    $('#txtProcessFee').val(parseFloat(requestFee_Withdrawal));
    $('#txtProcessFee').trigger('change');

    $('#form-process-modal').modal( { show: true, keyboard: false, backdrop: 'static' } );
}


function showForm_Withdrawal_Edit(withdrawalID, currency, txn_id, fee, fee_spent) {
    'use strict';

    $('#WithdrawalEditWithdrawalID').val(withdrawalID);
    $('#cellWithdrawalEditCurrency').html('<img src="/images/wallets/' + currency.toUpperCase() + '.png" style="height:20px;width: auto;margin-right: 10px;"></img>' + currency);
    $('#txtWithdrawalEditTransactionID').val(txn_id);
    $('#txtWithdrawalEditFee').val(formatTokenBalance(currency, fee));
    $('#txtWithdrawalEditFeeSpent').val(formatTokenBalance(currency, fee_spent));
    // $('#txtProcessFee').val(parseFloat(requestFee_Withdrawal));
    // $('#txtProcessFee').trigger('change');

    $('#form-withdrawal-edit-modal').modal( { show: true, keyboard: false, backdrop: 'static' } );
}

function showForm_Withdrawal_Cancel(withdrawalID) {
    'use strict';

    $('#txtWithdrawalCancelReason').val('');
    $('#WithdrawalCancelWithdrawalID').val(withdrawalID);

    $('#form-withdrawal-cancel-modal').modal( { show: true, keyboard: false, backdrop: 'static' } );
}


function btnProcessCancel_Click(event) {
    'use strict';
    event.target.blur();
    event.preventDefault();

    let withdrawalID = $('#ProcessWithdrawalID').val().trim();
    unreserveWithdrawalRequest(withdrawalID);
    return false;
}



function btnScatterClose_Click(event) {
    'use strict';
    event.target.blur();
    event.preventDefault();

    let withdrawalID = $('#ScatterWithdrawalID').val().trim();
    unreserveWithdrawalRequest(withdrawalID);
    return false;
}

function btnScatterCancelWithdrawal_Click(event) {
    'use strict';
    event.target.blur();
    event.preventDefault();

    let withdrawalID = $('#ScatterWithdrawalID').val().trim();

    $('#form-scatter-modal').modal('hide');

    showForm_Withdrawal_Cancel(withdrawalID);

    return false;
}

function btnProcessCancelWithdrawal_Click(event) {
    'use strict';
    event.target.blur();
    event.preventDefault();

    let withdrawalID = $('#ProcessWithdrawalID').val().trim();

    $('#form-process-modal').modal('hide');

    showForm_Withdrawal_Cancel(withdrawalID);

    return false;
}

function btnWithdrawalCancelCancel_Click(event) {
    'use strict';
    event.target.blur();
    event.preventDefault();

    let withdrawalID = $('#WithdrawalCancelWithdrawalID').val().trim();
    unreserveWithdrawalRequest(withdrawalID);

    $('#form-withdrawal-cancel-modal').modal('hide');

    return false;
}

function frmSearch_Submit(event) {
    'use strict';
    $('#btnApplyFilter').blur();
    event.preventDefault();
    clearNotifyMessages();
    refreshWithdrawalRequests();
    return false;
}

function btnApplyFilter_Click(event) {
    'use strict';
    $('#WithdrawalRequestsRecordOffset').val('0');   // Reset offest to 0 every time a new search is performed.
}


function btnProcessLookupFee_Click(event) {
    'use strict';
    $(event.target).blur();

    let transactionID = $('#txtProcessTransactionID').val().trim();
    let currencyCode = $('#cellProcessCurrency').text().trim();
    feeLookup(transactionID, currencyCode, 'txtProcessFeeSpent');
}


function completeWithdrawalRequest(withdrawalID, transactionID, fee, fee_spent) {
    'use strict';
    let formData = new FormData();

    formData.append('withdrawalID', withdrawalID);
    formData.append('transactionID', transactionID);
    formData.append('fee', fee);
    formData.append('fee_spent', fee_spent);

    $.ajax({
        //url: '/api/admin/completeWithdrawalRequest',
        url: '/api.php?target=withdrawal_requests&endpoint=admin%2FcompleteWithdrawalRequest',
        type: 'post',
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 0,
        data: formData,
        // beforeSend: function() {
        //     $('#product-images-spinner').removeClass('hidden');
        // }
    })
    .done(function (data) {
        if (data.result.code === ResponseCodeEnum.SUCCESS) {
            refreshWithdrawalRequests();
            $('#form-process-modal').modal('hide');
            showSuccessMessage('Withdrawal Submitted Successfully');
        } else {
            switch (data.result.code) {
                default: {
                    //showErrorMessage(data.result.message);
                    alert(data.result.message);
                    break;
                }
            }
        }
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
        alert('fail - ' + errorThrown);
    })
    .always(function () {
        //$('#product-images-spinner').addClass('hidden');
    });
}


function frmProcessWithdrawal_Submit(event) {
    'use strict';
    event.target.blur();
    event.preventDefault();
    
    let withdrawalID = $('#ProcessWithdrawalID').val().trim();

    let transactionID = $('#txtProcessTransactionID').val().trim();

    let fee = $('#txtProcessFee').val().trim();

    let fee_spent = $('#txtProcessFeeSpent').val().trim();

    completeWithdrawalRequest(withdrawalID, transactionID, fee, fee_spent);

    //$('#form-process-modal').modal('hide');
    return false;
}


function frmWithdrawalCancel_Submit(event) {
    'use strict';
    event.target.blur();
    event.preventDefault();
    
    let withdrawalID = $('#WithdrawalCancelWithdrawalID').val().trim();
    let reason = $('#txtWithdrawalCancelReason').val().trim();

    cancelWithdrawalRequest(withdrawalID, reason);

    return false;
}



function frmScatter_Submit(event) {
    'use strict';
    event.target.blur();
    event.preventDefault();
    
    let withdrawalID = $('#ScatterWithdrawalID').val().trim();
    let requestAmount = $('#ScatterRequestAmount').val().trim();
    let requestFee_Withdrawal = $('#ScatterRequestFee_Withdrawal').val().trim();
    let requestCurrency = $('#ScatterRequestCurrency').val().trim();
    let requestAddressTo = $('#ScatterRequestAddressTo').val().trim();
    let requestMemo = $('#ScatterRequestMemo').val().trim();

    $('#form-scatter-modal').modal('hide');

    if (!scatter) {
        showErrorMessage('Scatter is not running.');
    } else if (!scatter.identity) {
        showErrorMessage('Please attach Scatter account.');
    } else {
        showEOSrequest(withdrawalID, requestAmount, requestFee_Withdrawal, requestCurrency, requestAddressTo, requestMemo);
    }

    return false;
}

function getPlatformCurrencies(successFn, failFn) {
    "use strict";
    var request = $.ajax({
        url: '/api.php?target=withdrawal_requests&endpoint=' + encodeURIComponent('vendor/getPlatformCurrencies'),
        method: 'post',
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 0
    });

    request.done(function (data) {
        if (data !== undefined && data !== null) {
            if (data.result.code === 0) {
                if (successFn !== undefined) {
                    successFn(data);
                }
            }
        }
    });

    request.fail(function (jqXHR, textStatus, errorThrown) {
        if (failFn !== undefined) {
            failFn(jqXHR, textStatus, errorThrown);
        }
    });
}

function loadCurrencies() {
    getPlatformCurrencies(function(data) {
        // Success
        if (data.result.code === ResponseCodeEnum.SUCCESS) {
            dictionary_currencies = data.currencies;

            buildCryptoDropDowns();

            refreshWithdrawalRequests();
        } else {
            console.error(data.result.message);
        }
    },
    function(jqXHR, textStatus, errorThrown) {
        // Fail
        console.error(errorThrown);
    });
}


function buildCryptoDropDowns() {
    //console.log(dictionary_currencies);

    $('#cbCurrency').children('option:not(:first)').remove();   // Remove all options except for the first one.
    $('#cbWithdrawalHistoryCurrency').children('option:not(:first)').remove();   // Remove all options except for the first one.
    $('#cbWithdrawFeesCurrency').children('option:not(:first)').remove();   // Remove all options except for the first one.
    $('#cbFeesWithdrawalHistoryCurrency').children('option:not(:first)').remove();   // Remove all options except for the first one.

    for (let currency_code in dictionary_currencies) {
        if (dictionary_currencies[currency_code].isFiat === 0) {
            $('#cbCurrency').append($('<option></option>')
                .val(dictionary_currencies[currency_code].symbol)
                .html(dictionary_currencies[currency_code].symbol + ' - ' + dictionary_currencies[currency_code].name));
            
            $('#cbWithdrawalHistoryCurrency').append($('<option></option>')
                .val(dictionary_currencies[currency_code].symbol)
                .html(dictionary_currencies[currency_code].symbol + ' - ' + dictionary_currencies[currency_code].name));

            $('#cbWithdrawFeesCurrency').append($('<option></option>')
                .val(dictionary_currencies[currency_code].symbol)
                .html(dictionary_currencies[currency_code].symbol + ' - ' + dictionary_currencies[currency_code].name));

            $('#cbFeesWithdrawalHistoryCurrency').append($('<option></option>')
                .val(dictionary_currencies[currency_code].symbol)
                .html(dictionary_currencies[currency_code].symbol + ' - ' + dictionary_currencies[currency_code].name));
        }
    }

    $('#cbCurrency').prop('selectedIndex', 0);
    $('#cbWithdrawalHistoryCurrency').prop('selectedIndex', 0);
    $('#cbWithdrawFeesCurrency').prop('selectedIndex', 0);
}


$(document).ready(function () {
    'use strict';
    
    /* Withdrawal Requests */
    $('#frmScatter').on('submit', frmScatter_Submit);
    $('#frmProcessWithdrawal').on('submit', frmProcessWithdrawal_Submit);
    $("#frmSearch").on('submit', frmSearch_Submit);
    $('#frmWithdrawalCancel').on('submit', frmWithdrawalCancel_Submit);

    $('#btnScatterCancelWithdrawal').on('click', btnScatterCancelWithdrawal_Click);
    $("#btnProcessCancelWithdrawal").on('click', btnProcessCancelWithdrawal_Click);
    $("#btnProcessCancel").on('click', btnProcessCancel_Click);
    $("#btnScatterClose").on('click', btnScatterClose_Click);
    $('#btnScatterModalHide').on('click', hideScatterModal);
    $('#btnProcessLookupFee').on('click', btnProcessLookupFee_Click);
    $('#txtProcessFee').on('change', txtProcessFee_Change).on('keyup', txtProcessFee_Change).on('paste', txtProcessFee_Change);
    $('#btnApplyFilter').on('click', btnApplyFilter_Click);

    $('#btnWithdrawalCancelCancel').on('click', btnWithdrawalCancelCancel_Click);


    $( document ).ajaxStart(function() {
        $('.ajaxSpinner').removeClass('hidden');
    });

    $( document ).ajaxStop(function() {
        $('.ajaxSpinner').addClass('hidden');
    });

    loadCurrencies();
});
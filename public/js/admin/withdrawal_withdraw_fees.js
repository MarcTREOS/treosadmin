
function frmWithdrawFeesHistory_Submit(event) {
    'use strict';
    $('#btnApplyWithdrawFeesHistoryFilter').blur();
    event.preventDefault();
    clearNotifyMessages();

    refreshWithdrawFeesHistory();
    return false;
}

function frmWithdrawFees_Submit(event) {
    'use strict';
    event.target.blur();
    event.preventDefault();
    
    let fees_year = $('#WithdrawFeesYear').val().trim();
    let fees_month = $('#WithdrawFeesMonth').val().trim();
    let currency = $('#cellWithdrawFeesCurrency').text().trim();
    let amount = $('#cellWithdrawFeesAmount').text().trim();
    let fee_spent = $('#txtWithdrawFeesFeeSpent').val().trim();
    let address_to = $('#cellWithdrawFeesAddressTo').text().trim();
    let txn_reference = $('#txtWithdrawFeesTransactionID').val().trim();

    withdrawFees_Insert(fees_year, fees_month, currency, amount, fee_spent, address_to, txn_reference);

    //$('#form-process-modal').modal('hide');
    return false;
}


function withdrawFees_Insert(fees_year, fees_month, currency, amount, fee_spent, address_to, txn_reference) {
    'use strict';
    let formData = new FormData();

    formData.append('fees_year', fees_year);
    formData.append('fees_month', fees_month);
    formData.append('currency', currency);
    formData.append('amount', amount);
    formData.append('fee_spent', fee_spent);
    formData.append('address_to', address_to);
    formData.append('txn_reference', txn_reference);

    $.ajax({
        //url: '/api/admin/completeWithdrawalRequest',
        url: '/api.php?target=withdrawal_requests&endpoint=' + encodeURIComponent('admin/withdrawFees_Insert'),
        method: 'post',
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 0,
        data: formData
        // beforeSend: function() {
        //     $('#product-images-spinner').removeClass('hidden');
        // }
    })
    .done(function (data) {
        if (data.result.code === ResponseCodeEnum.SUCCESS) {
            refreshWithdrawFeesHistory();
            hideWithdrawFeesModal();
            showSuccessMessage('Fee Withdrawal Saved Successfully');
        } else {
            switch (data.result.code) {
                default: {
                    //showErrorMessage(data.result.message);
                    alert(data.result.message);
                    break;
                }
            }
        }
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
        alert('fail - ' + errorThrown);
    })
    .always(function () {
        //$('#product-images-spinner').addClass('hidden');
    });
}


function btnWithdrawFees_Withdraw_Click(event) {
    let $btn = $(event.target);

    let $row = $btn.closest('tr');

    let year = $row.data('year');
    let month = $row.data('month');
    let fee_available = $row.find('.fee_available').text();
    let currency = $row.data('currency');
    let addressTo = $row.data('addressto');

    showForm_WithdrawFees(year, month, fee_available, currency, addressTo);
}


function showForm_WithdrawFees(year, month, amount, currency, addressTo) {
    'use strict';

    $('#WithdrawFeesYear').val(year);
    $('#WithdrawFeesMonth').val(month);

    $('#cellWithdrawFeesAmount').text(amount);
    $('#cellWithdrawFeesCurrency').html('<img src="/images/wallets/' + currency.toUpperCase() + '.png" style="height:20px;width: auto;margin-right: 10px;"></img>' + currency);
    $('#cellWithdrawFeesAddressTo').text(addressTo);
    

    // http://davidshimjs.github.io/qrcodejs/
    $('#qrWithdrawFeesPlaceHolder').html('');
    $('#withdrawFeesWarningText').html('');
    $('#withdrawFeesWarningRow').addClass('hidden');
    $('#txtWithdrawFeesTransactionID').val('');
    $('#txtWithdrawFeesFeeSpent').val('');
    $('#txtWithdrawFeesFeeSpent').trigger('change');

    $('#form-withdrawfees-modal').modal( { show: true, keyboard: false, backdrop: 'static' } );
}


function hideWithdrawFeesModal() {
    $('#form-withdrawfees-modal').modal('hide');
}


function btnWithdrawFeesCancel_Click(event) {
    'use strict';
    event.target.blur();
    event.preventDefault();

    hideWithdrawFeesModal();
    return false;
}


function btnWithdrawFeesExport_Click(event) {
    'use strict';
    event.target.blur();
    event.preventDefault();

    getWithdrawFeesReport(true, function(data) {
        /* success */
        if (data.result.code === ResponseCodeEnum.SUCCESS) {
            if (data.months.length > 0) {
                let columnHeadings = ['Year', 'Month', 'Currency', 'Members Fee', 'Fee Spent', 'Difference', 'Available to Withdraw' ];
                let exportArray = [];

                for (let i = 0; i < data.months.length; i++) {
                    let feeMonth = data.months[i];
                    for (let j = 0; j < feeMonth.currencies.length; j++) {
                        let feeMonthCurrency = feeMonth.currencies[j];
                        let item = {
                            year: feeMonth.year,
                            month: feeMonth.month,
                            currency: feeMonthCurrency.currency,
                            fee: feeMonthCurrency.fee,
                            fee_spent: feeMonthCurrency.fee_spent,
                            fee_diff: feeMonthCurrency.fee_diff,
                            fee_available: feeMonthCurrency.fee_available
                            //fee_withdrawal_address: feeMonthCurrency.fee_withdrawal_address
                        }
                        exportArray.push(item);
                    }
                    
                }
                downloadArrayAsCSV(exportArray, 'withdraw_fees.csv', columnHeadings);
            } else {
                alert('no records found.');
            }
        } else {
            switch (data.result.code) {
                default: {
                    showErrorMessage(data.result.message);
                    break;
                }
            }
        }
    },
    function(jqXHR, textStatus, errorThrown) {
        /* fail */
        showErrorMessage('fail - ' + errorThrown);
    },
    function() {
        /* always */
    });
}


function btnApplyWithdrawFeesHistoryFilter_Click(event) {
    'use strict';
    $('#WithdrawFeesRecordOffset').val('0');   // Reset offest to 0 every time a new search is performed.
}


function txtWithdrawFeesFeeSpent_Change(event) {
    'use strict';
    let qrCodeData = null;
    let amountLabel = 'amount';
    let amount = $('#cellWithdrawFeesAmount').text();
    let fee_withdrawal = $('#txtWithdrawFeesFeeSpent').val().trim();
    let currencyCode = $('#cellWithdrawFeesCurrency').text().trim();
    let addressTo = $('#cellWithdrawFeesAddressTo').text().trim();
    let amountLessFee = 0.0;
    let warningMessage = '';

    amount = parseFloat(amount);
    if (isNaN(amount)) {
        amount = 0;
    }

    fee_withdrawal = parseFloat(fee_withdrawal);
    if (isNaN(fee_withdrawal)) {
        fee_withdrawal = 0;
    }

    amountLessFee = amount - fee_withdrawal;

    if (typeof dictionary_currencies !== undefined && dictionary_currencies[currencyCode.toUpperCase()] !== undefined) {
        let currency = dictionary_currencies[currencyCode];
        qrCodeData = currency.qr_code_prefix;
        amountLessFee = formatTokenBalance(currencyCode, amountLessFee);
        
    } else {
        proceed = false;
        amountLessFee = amountLessFee.toFixed(8);
        showErrorMessage('Unsupported Currency "' + requestCurrency + '"');
    }

    $('#cellWithdrawFeesAmountLessFee').text(amountLessFee);

    if (currencyCode === 'ETH') {
        let web3 = new Web3(Web3.givenProvider);    // Need Web3 to correctly convert ETH to Wei
        warningMessage = 'Only use this QR Code with the Blockchain or Exodus Wallet Apps.<br/><br/>The amount is incompatible with other apps.'
    }

    /*switch (currencyCode.toLowerCase()) {
        case 'bch': {
            qrCodeData = 'bitcoincash';
            amountLessFee = amountLessFee.toFixed(8);
            break;
        }
        case 'bnb': {
            qrCodeData = 'binancecoin';
            amountLessFee = amountLessFee.toFixed(8);
            break;
        }
        case 'btc': {
            qrCodeData = 'bitcoin';
            amountLessFee = amountLessFee.toFixed(8);
            break;
        }
        case 'eth': {
            qrCodeData = 'ethereum';
            //amountLabel = 'value';
            let web3 = new Web3(Web3.givenProvider);    // Need Web3 to correctly convert ETH to Wei
            //amount = web3.utils.toWei(requestAmount, 'ether');    // Blockchain wallet uses ETH instead of Wei
            amountLessFee = amountLessFee.toFixed(8);
            warningMessage = 'Only use this QR Code with the Blockchain or Exodus Wallet Apps.<br/><br/>The amount is incompatible with other apps.'
            break;
        }
        case 'ltc': {
            qrCodeData = 'litecoin';
            amountLessFee = amountLessFee.toFixed(8);
            break;
        }
        default: {
            proceed = false;
            amountLessFee = amountLessFee.toFixed(8);
            showErrorMessage('Unsupported Currency "' + requestCurrency + '"');
            break;
        }

        //qrCodeData = '';
    }*/

    if (qrCodeData !== null) {
        if (qrCodeData.length > 0) {
            qrCodeData += ':';
        }
        qrCodeData += addressTo + '?' + amountLabel + '=' + amountLessFee;
    
        $('#qrWithdrawFeesPlaceHolder').html('');
    
        let qr = new QRCode('qrWithdrawFeesPlaceHolder', {
            text: qrCodeData,
            width: 200,
            height: 200,
            colorDark : "#000000",
            colorLight : "#ffffff",
            correctLevel : QRCode.CorrectLevel.M    // L 7%, M 15%, Q 25%, H 30%
        });
    }
    

    $('#qrWithdrawFeesPlaceHolder img').css('margin-left', 'auto').css('margin-right', 'auto');

    if (warningMessage.length > 0) {
        $('#withdrawFeesWarningRow').removeClass('hidden');
        $('#withdrawFeesWarningText').html(warningMessage);
    }
} // txtWithdrawFeesFeeSpent_Change


function WithdrawFeesBuildData(forExport) {
    'use strict';
    let formData = new FormData();
    formData.append('currency', document.getElementById("cbWithdrawFeesCurrency").value);
    formData.append('date_from', document.getElementById("txtWithdrawFeesDateFrom").value);
    formData.append('date_to', document.getElementById("txtWithdrawFeesDateTo").value);
    formData.append('record_offset', document.getElementById("WithdrawFeesRecordOffset").value);
    formData.append('record_counter', document.getElementById("WithdrawFeesRecordCounter").value);
    formData.append('forExport', forExport);

    return formData;
}


function getWithdrawFeesReport(forExport, successFn, failFn, alwaysFn) {
    'use strict';
    $.ajax({
        url: '/api.php?target=withdrawal_requests&endpoint=' + encodeURIComponent('admin/getWithdrawFeesHistory'),
        method: 'post',
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 0,
        data: WithdrawFeesBuildData(forExport)
    })
    .done(function (data) {
        'use strict';
        if (successFn !== undefined && successFn !== null) {
            successFn(data);
        }
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
        'use strict';
        if (failFn !== undefined && failFn !== null) {
            failFn(jqXHR, textStatus, errorThrown);
        }
    })
    .always(function () {
        'use strict';
        if (alwaysFn !== undefined && alwaysFn !== null) {
            alwaysFn();
        }
    });
}


function refreshWithdrawFeesHistory() {
    'use strict';

    getWithdrawFeesReport(false, function(data) {
        /* success */
        if (data.result.code === ResponseCodeEnum.SUCCESS) {
            let total_records = data.total_records;

            let table = $('#withdraw_fees');
            table.find('tbody tr').remove();

            if (data.months.length > 0) {
                for (let i = 0; i < data.months.length; i++) {
                    let feesmonth = data.months[i];

                    for (let j = 0; j < feesmonth.currencies.length; j++) {
                        let feecurrency = feesmonth.currencies[j];


                        let imageFilename = feecurrency.currency.toUpperCase() + '.png';
                        // if (feecurrency.currency.toUpperCase() === 'TRO') {
                        //     imageFilename = feecurrency.currency.toLowerCase() + '.png';
                        // }


                        let $row = $('<tr></tr>');

                        $row.attr('data-year', feesmonth.year).data('year', feesmonth.year);
                        $row.attr('data-month', feesmonth.month).data('month', feesmonth.month);
                        $row.attr('data-currency', feecurrency.currency).data('currency', feecurrency.currency);
                        $row.attr('data-addressto', feecurrency.fee_withdrawal_address).data('addressto', feecurrency.fee_withdrawal_address);

                        let $cell_year = $('<td></td>').addClass('year');
                        let $cell_month = $('<td></td>').addClass('month');
                        let $cell_currency = $('<td></td>').addClass('currency');
                        let $cell_fee = $('<td></td>').addClass('fee');
                        let $cell_fee_spent = $('<td></td>').addClass('fee_spent');
                        let $cell_fee_diff = $('<td></td>').addClass('fee_diff');
                        let $cell_fee_available = $('<td></td>').addClass('fee_available');
                        let $cell_buttons = $('<td></td>');

                        if (j > 0) {
                            $cell_year.html('&nbsp;');
                            $cell_month.html('&nbsp;');
                        } else {
                            $cell_year.text(feesmonth.year);
                            $cell_month.text(feesmonth.month);
                        }
                        
                        let $img_currency = $('<img/>').attr('src', '/images/wallets/' + imageFilename).attr('style', 'height:20px; width: auth; margin-right: 10px;');
                        $cell_currency.append($img_currency);
                        $cell_currency.append(feecurrency.currency);

                        $cell_fee.html(formatTokenBalance(feecurrency.currency, feecurrency.fee)).attr('style', 'text-align: right;');

                        $cell_fee_spent.html(formatTokenBalance(feecurrency.currency, feecurrency.fee_spent)).attr('style', 'text-align: right;');

                        $cell_fee_diff.html(formatTokenBalance(feecurrency.currency, feecurrency.fee_diff)).attr('style', 'text-align: right;');

                        $cell_fee_available.html(formatTokenBalance(feecurrency.currency, feecurrency.fee_available)).attr('style', 'text-align: right;');

                        //if (!feesmonth.current_month && feecurrency.fee_available > 0.0) {
                        if (feecurrency.fee_available > 0.0) {
                            let $btn_withdraw = $('<button/>').text('Withdraw').on('click', btnWithdrawFees_Withdraw_Click);

                            $cell_buttons.append($btn_withdraw);
                        } else {
                            $cell_buttons.html('&nbsp;');
                        }

                        $row.append($cell_year);
                        $row.append($cell_month);
                        $row.append($cell_currency);
                        $row.append($cell_fee);
                        $row.append($cell_fee_spent);
                        $row.append($cell_fee_diff);
                        $row.append($cell_fee_available);
                        $row.append($cell_buttons);

                        table.find('tbody').append($row);
                    }
                }

                generateReportPagination($('#WithdrawFeesNavTop'), $('#WithdrawFeesRecordOffset'), $('#WithdrawFeesRecordCounter'), total_records);
                generateReportPagination($('#WithdrawFeesNavBottom'), $('#WithdrawFeesRecordOffset'), $('#WithdrawFeesRecordCounter'), total_records);
            } else {
                let row = '<tr>';
                row += '<td colspan="8">No Records Found</td>';
                table.find('tbody').append(row);
            }
        } else {
            switch (data.result.code) {
                default: {
                    showErrorMessage(data.result.message);
                    break;
                }
            }
        }
    },
    function(jqXHR, textStatus, errorThrown) {
        /* fail */
        showErrorMessage('fail - ' + errorThrown);
    },
    function() {
        /* always */
    });
}   // refreshWithdrawFeesHistory


function initialiseWithdrawFeesDatePickers() {
    /*
    bootstrap-datepicker
    https://bootstrap-datepicker.readthedocs.io/en/latest/options.html
     */
    var dp_from = $('#txtWithdrawFeesDateFrom');
    var dp_to = $('#txtWithdrawFeesDateTo');
    var container = $('#frmWithdrawFeesHistory').length>0 ? $('#frmWithdrawFeesHistory').parent() : "body";
    var options = {
        format: 'yyyy-mm-dd',
        container: container,
        todayHighlight: true,
        autoclose: true,
        //startView: 'months',  // Disabled as disables ability to update the date to the last day of the month.
        minViewMode: 'months'
    };

    dp_from.datepicker(options);
    dp_to.datepicker(options);

    var date_now = new Date();
    var date_from = new Date(date_now.getUTCFullYear(), date_now.getUTCMonth() - 11, 1);     // First day of UTC 12 months ago
    var date_to = new Date(date_now.getUTCFullYear(), date_now.getUTCMonth() + 1, 0);   // Last day of current UTC month

    dp_from.datepicker('update', date_from);
    dp_to.datepicker('update', date_to);

    dp_to.datepicker().on('changeDate', function(event) {
        // `event` contains the extra attributes
        let dateSelected = event.date;
        let lastDayOfMonth = new Date(dateSelected.getFullYear(), dateSelected.getMonth() + 1, 0);   // Last day of the month
        $(event.target).datepicker('update', lastDayOfMonth);
        $(event.target).val(event.format('yyyy-mm-dd'));
        //console.log(event.format(0, 'yyyy-mm-dd'));
        //console.log(event.format('yyyy-mm-dd'));
    });
}


$(document).ready(function () {
    'use strict';
    
    $("#frmWithdrawFeesHistory").on('submit', frmWithdrawFeesHistory_Submit);
    $('#frmWithdrawFees').on('submit', frmWithdrawFees_Submit);

    $('#btnApplyWithdrawFeesHistoryFilter').on('click', btnApplyWithdrawFeesHistoryFilter_Click);
    $('#btnWithdrawFeesCancel').on('click', btnWithdrawFeesCancel_Click);
    $('#btnWithdrawFeesExport').on('click', btnWithdrawFeesExport_Click);

    $('#txtWithdrawFeesFeeSpent').on('change', txtWithdrawFeesFeeSpent_Change).on('keyup', txtWithdrawFeesFeeSpent_Change).on('paste', txtWithdrawFeesFeeSpent_Change);
    
    initialiseWithdrawFeesDatePickers();

});
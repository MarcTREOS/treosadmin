function withdrawalEdit_Update(withdrawalID, transactionID, fee, fee_spent) {
    'use strict';
    let formData = new FormData();

    formData.append('withdrawalID', withdrawalID);
    formData.append('transactionID', transactionID);
    formData.append('fee', fee);
    formData.append('fee_spent', fee_spent);

    $.ajax({
        //url: '/api/admin/completeWithdrawalRequest',
        url: '/api.php?target=withdrawal_requests&endpoint=' + encodeURIComponent('admin/withdrawalEdit_Update'),
        type: 'post',
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 0,
        data: formData,
        // beforeSend: function() {
        //     $('#product-images-spinner').removeClass('hidden');
        // }
    })
    .done(function (data) {
        if (data.result.code === ResponseCodeEnum.SUCCESS) {
            refreshWithdrawalHistory();
            $('#form-withdrawal-edit-modal').modal('hide');
            showSuccessMessage('Withdrawal Saved Successfully');
        } else {
            switch (data.result.code) {
                default: {
                    //showErrorMessage(data.result.message);
                    alert(data.result.message);
                    break;
                }
            }
        }
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
        alert('fail - ' + errorThrown);
    })
    .always(function () {
        //$('#product-images-spinner').addClass('hidden');
    });
}


function frmWithdrawalEdit_Submit(event) {
    'use strict';
    event.target.blur();
    event.preventDefault();
    
    let withdrawalID = $('#WithdrawalEditWithdrawalID').val().trim();

    let transactionID = $('#txtWithdrawalEditTransactionID').val().trim();

    let fee = $('#txtWithdrawalEditFee').val().trim();

    let fee_spent = $('#txtWithdrawalEditFeeSpent').val().trim();

    withdrawalEdit_Update(withdrawalID, transactionID, fee, fee_spent);

    return false;
} // frmWithdrawalEdit_Submit


function frmSearchHistory_Submit(event) {
    'use strict';
    $('#btnApplyHistoryFilter').blur();
    event.preventDefault();
    clearNotifyMessages();

    refreshWithdrawalHistory();
    return false;
} // frmSearchHistory_Submit


function btnWithdrawalEditCancel_Click(event) {
    'use strict';
    event.target.blur();
    event.preventDefault();

    $('#form-withdrawal-edit-modal').modal('hide');
    return false;
} // btnWithdrawalEditCancel_Click


function btnApplyHistoryFilter_Click(event) {
    'use strict';
    $('#WithdrawalHistoryRecordOffset').val('0');   // Reset offest to 0 every time a new search is performed.
} // btnApplyHistoryFilter_Click


function btnWithdrawalEditLookupFee_Click(event) {
    'use strict';
    $(event.target).blur();
    
    let transactionID = $('#txtWithdrawalEditTransactionID').val().trim();
    let currencyCode = $('#cellWithdrawalEditCurrency').text().trim();

    feeLookup(transactionID, currencyCode, 'txtWithdrawalEditFeeSpent');
} // btnWithdrawalEditLookupFee_Click


function btnWithdrawalHistoryExport_Click(event) {
    'use strict';
    event.target.blur();
    event.preventDefault();

    getWithdrawHistoryReport(true, function(data) {
        /* success */
        if (data.result.code === ResponseCodeEnum.SUCCESS) {
            if (data.withdrawals.length > 0) {
                let columnHeadings = ['Withdrawal ID', 'Date', 'State', 'Profile ID', 'Name', 'Currency', 'Amount', 'Members Fee', 'Fee Spent', 'Recipient Address', 'Memo', 'Txn ID' ];
                let exportArray = [];

                for (let i = 0; i < data.withdrawals.length; i++) {
                    let withdrawal = data.withdrawals[i];

                    let item = {
                        withdrawal_id: withdrawal.withdrawal_id,
                        date: withdrawal.date,
                        status: withdrawal.status,
                        profile_id: withdrawal.profile_id,
                        profile_name: withdrawal.profile_name,
                        currency: withdrawal.currency,
                        amount: withdrawal.amount,

                        fee: withdrawal.fee,
                        fee_spent: withdrawal.fee_spent,
                        address_to: withdrawal.address_to,
                        memo: withdrawal.memo,
                        txn_reference: withdrawal.txn_reference,
                    }
                    exportArray.push(item);
                }
                downloadArrayAsCSV(exportArray, 'withdrawal_history.csv', columnHeadings);
            } else {
                alert('no records found.');
            }
        } else {
            switch (data.result.code) {
                default: {
                    showErrorMessage(data.result.message);
                    break;
                }
            }
        }
    },
    function(jqXHR, textStatus, errorThrown) {
        /* fail */
        showErrorMessage('fail - ' + errorThrown);
    },
    function() {
        /* always */
    });
} // btnWithdrawalEditLookupFee_Click


function historyRowEdit_Click(event) {
    clearNotifyMessages();
    event.preventDefault();
    let row = $(event.target).closest('tr')[0];
    
    if (row !== undefined) {
        let withdrawalID = $(row).attr('id');
        let currency = $(row).data('currency');
        let txn_id = $(row).data('txnid');
        let fee = $(row).data('fee');
        let fee_spent = $(row).data('feespent');
        
        showForm_Withdrawal_Edit(withdrawalID, currency, txn_id, fee, fee_spent);
    }
}


function WithdrawHistoryBuildData(forExport) {
    'use strict';
    let formData = new FormData();
    formData.append('currency', document.getElementById("cbWithdrawalHistoryCurrency").value);
    formData.append('member_name', document.getElementById("txtWithdrawalHistorySearchMember").value);
    formData.append('date_from', document.getElementById("txtWithdrawalHistoryDateFrom").value);
    formData.append('date_to', document.getElementById("txtWithdrawalHistoryDateTo").value);
    formData.append('record_offset', document.getElementById("WithdrawalHistoryRecordOffset").value);
    formData.append('record_counter', document.getElementById("WithdrawalHistoryRecordCounter").value);
    formData.append('forExport', forExport);

    return formData;
}


function getWithdrawHistoryReport(forExport, successFn, failFn, alwaysFn) {
    'use strict';
    $.ajax({
        url: '/api.php?target=withdrawal_requests&endpoint=' + encodeURIComponent('admin/getWithdrawalHistory'),
        method: 'post',
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 0,
        data: WithdrawHistoryBuildData(forExport)
    })
    .done(function (data) {
        'use strict';
        if (successFn !== undefined && successFn !== null) {
            successFn(data);
        }
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
        'use strict';
        if (failFn !== undefined && failFn !== null) {
            failFn(jqXHR, textStatus, errorThrown);
        }
    })
    .always(function () {
        'use strict';
        if (alwaysFn !== undefined && alwaysFn !== null) {
            alwaysFn();
        }
    });
}


function refreshWithdrawalHistory() {
    'use strict';

    //$('#withdrawal_history tbody td').off('click');
    $('.withdrawal-history-edit').off('click');
    $('#withdrawal_history tbody td.truncate').off('click');

    getWithdrawHistoryReport(false, function(data) {
        /* success */
        if (data.result.code === ResponseCodeEnum.SUCCESS) {
            let total_records = data.total_records;

            let table = $('#withdrawal_history');
            table.find('tbody tr').remove();

            if (data.withdrawals.length > 0) {
                for (let i = 0; i < data.withdrawals.length; i++) {
                    let withdrawal = data.withdrawals[i];

                    let imageFilename = withdrawal.currency.toUpperCase() + '.png';
                    if (withdrawal.currency.toUpperCase() === 'TRO') {
                        imageFilename = withdrawal.currency.toLowerCase() + '.png';
                    }

                    let row = '<tr id="' + withdrawal.withdrawal_id + '"' +
                        ' data-currency="' + withdrawal.currency + '"' +
                        ' data-txnid="' + withdrawal.txn_reference + '"' +
                        ' data-fee="' + withdrawal.fee.toFixed(8) + '"' +
                        ' data-feespent="' + withdrawal.fee_spent.toFixed(8) + '">';
                    //row += '<td>' + rqDate.toLocaleString("en", dateOptions) + '</td>';
                    //row += '<td>' + formatLocalDateTime(withdrawal.date) + '</td>';
                    row += '<td>' + formatUTCDateTime(withdrawal.date) + '</td>';
                    row += '<td>' + withdrawal.status + '</td>';
                    row += '<td>' + withdrawal.profile_id + '</td>';
                    row += '<td>' + withdrawal.profile_name + '</td>';
                    row += '<td><img src="/images/wallets/' + imageFilename + '" style="height:20px;width: auto;margin-right: 10px;">' + withdrawal.currency + '</td>';
                    row += '<td style="text-align:right;">' + formatTokenBalance(withdrawal.currency, withdrawal.amount) + '</td>';
                    row += '<td style="text-align:right;">' + formatTokenBalance(withdrawal.currency, withdrawal.fee) + '</td>';
                    row += '<td style="text-align:right;">' + formatTokenBalance(withdrawal.currency, withdrawal.fee_spent) + '</td>';
                    row += '<td style="text-align:right;" class="truncate truncateContents">' + withdrawal.address_to + '</td>';
                    row += '<td style="text-align: left;" class="truncate truncateContents">' + cellText(withdrawal.memo) + '</td>';
                    row += '<td style="text-align: left;" class="truncate truncateContents">' + cellText(withdrawal.txn_reference) + '</td>';
                    row += '<td style="text-align: left;" class="truncate truncateContents">' + cellText(withdrawal.reason) + '</td>';
                    row += '<td><a href="#" class="withdrawal-history-edit"><img src="/images/wallets/edit-icon.png" style="width:20px; height:20px;" /></a></td>';
                    row += '</tr>';
                    table.find('tbody').append(row);
                }

                generateReportPagination($('#WithdrawalHistoryNavTop'), $('#WithdrawalHistoryRecordOffset'), $('#WithdrawalHistoryRecordCounter'), total_records);
                generateReportPagination($('#WithdrawalHistoryNavBottom'), $('#WithdrawalHistoryRecordOffset'), $('#WithdrawalHistoryRecordCounter'), total_records);
            } else {
                let row = '<tr>';
                row += '<td colspan="12">No Matching Withdrawal History Found</td>';
                table.find('tbody').append(row);
            }
        } else {
            switch (data.result.code) {
                default: {
                    showErrorMessage(data.result.message);
                    break;
                }
            }
        }

    },
    function(jqXHR, textStatus, errorThrown) {
        /* fail */
        showErrorMessage('fail - ' + errorThrown);
    },
    function() {
        /* always */
        $('.withdrawal-history-edit').on('click', historyRowEdit_Click);
        $('#withdrawal_history tbody td.truncate').on('click', function(event) {
             $(event.target).removeClass('truncateContents');
        });
    });
}


function initialiseWithdrawalHistoryDatePickers() {
    /*
    bootstrap-datepicker
    https://bootstrap-datepicker.readthedocs.io/en/latest/options.html
     */
    var dp_from = $('#txtWithdrawalHistoryDateFrom');
    var dp_to = $('#txtWithdrawalHistoryDateTo');
    //var container = $('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
    var container = $('#frmSearchHistory').length>0 ? $('#frmSearchHistory').parent() : "body";
    var options = {
        format: 'yyyy-mm-dd',
        container: container,
        todayHighlight: true,
        autoclose: true,
        //startView: 'months',
        //minViewMode: 'months'
    };

    dp_from.datepicker(options);
    dp_to.datepicker(options);

    var date_now = new Date();
    var date_from = new Date(date_now.getUTCFullYear(), date_now.getUTCMonth(), 1);     // First day of current UTC month
    var date_to = new Date(date_now.getUTCFullYear(), date_now.getUTCMonth() + 1, 0);   // Last day of current UTC month

    dp_from.datepicker('update', date_from);
    dp_to.datepicker('update', date_to);
}


$(document).ready(function () {
    'use strict';
    
    $('#frmWithdrawalEdit').on('submit', frmWithdrawalEdit_Submit);
    $("#frmSearchHistory").on('submit', frmSearchHistory_Submit);
    $("#btnWithdrawalEditCancel").on('click', btnWithdrawalEditCancel_Click);
    $('#btnApplyHistoryFilter').on('click', btnApplyHistoryFilter_Click);
    $('#btnWithdrawalEditLookupFee').on('click', btnWithdrawalEditLookupFee_Click);
    $('#btnWithdrawalHistoryExport').on('click', btnWithdrawalHistoryExport_Click);
    
    initialiseWithdrawalHistoryDatePickers();
});
/*
    columnsToInclude:
    Not required, but advisable so as to exclude any values returned that you may not want in the exported file.
    An array of the actual field names contained in csvArray.  Not the names that you want to export.
*/
function downloadArrayAsCSV(csvArray, filename, columnHeadings) {
    'use strict';

    var csvHeader = null;

    var unparseConfig = {
        quotes: false, //or array of booleans
        quoteChar: '"',
        escapeChar: '"',
        delimiter: ",",
        header: true,
        newline: "\r\n",
        skipEmptyLines: false, //or 'greedy',
        columns: null //or array of strings
    }
    
    if (columnHeadings !== undefined && columnHeadings !== null) {
        unparseConfig.header = false;   // Do not include header row is CSV so that we can replace it with the value in 'columnHeadings'
        csvHeader = Papa.unparse({ fields: columnHeadings, data: [] });
    }

    var csv = Papa.unparse(csvArray, unparseConfig);

    if (csvHeader !== null) {
        csv = csvHeader + csv;
    }

    var csvData = new Blob([csv], {type: 'text/csv;charset=utf-8;'});
    var csvURL =  null;
    if (navigator.msSaveBlob) {
        csvURL = navigator.msSaveBlob(csvData, 'filename');
    } else {
        csvURL = window.URL.createObjectURL(csvData);
    }
    var tempLink = document.createElement('a');
    tempLink.href = csvURL;
    tempLink.setAttribute('download', filename);
    tempLink.click();
}
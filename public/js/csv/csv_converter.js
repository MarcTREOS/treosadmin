"use strict";

var categories_treos = [];
var categories_known = {};
var columns_generic = [];

var columnTypes = [
    { name : '', id : 0 },
    { name : 'BriefDescription', id : 1 },
    { name : 'Categoy', id : 2 },
    { name : 'Description', id : 3 },
    { name : 'ImageURLs', id : 4 },
    { name : 'Name', id : 5 },
    { name : 'Price', id : 6 },
    { name : 'ShippingCost', id : 7 },
    { name : 'SKU', id : 8 },
    { name : 'StockLevel', id : 9 },
    { name : 'Weight', id : 10 }
];


function btnFileTypeNext_Click(event) {
    'use strict';
    $('#file-type').addClass('hidden');
    $('#currency').removeClass('hidden');
}

function btnCurrencyBack_Click(event) {
    'use strict';
    $('#currency').addClass('hidden');
    $('#file-type').removeClass('hidden');
}

function btnCurrencyNext_Click(event) {
    'use strict';
    $('#currency').addClass('hidden');
    $('#file-upload').removeClass('hidden');
}

function btnUploadFileBack_Click(event) {
    'use strict';
    $('#file-upload').addClass('hidden');
    //$('#file-upload-generic').addClass('hidden');
    $('#currency').removeClass('hidden');
}

function btnUploadFile_Click(event) {
    'use strict';
    clearNotifyMessages();
    //parseCSVFile(1);
    submitCSVfile(1);
}

function fileInput_Change(event) {
    $('#tmpFilename').val('');
    clearObject(categories_known);
}


// function onGenericColumnsBackClick() {
//     $('#generic-columns-columns .field-type').off('change');        // Remove any existing events.
//     $('#generic-columns-columns .generic-column-detail').remove();   // Remove any existing entries

//     $('#generic-columns').addClass('hidden');
//     $('#file-upload-generic').removeClass('hidden');
// }


// function onGenericColumnsNextClick() {
//     // Basic validation.
//     var allRegions = $('#generic-columns .generic-column-detail');

//     // Update the array
//     $(allRegions).each(function (index, region) {
//         var fieldIndex = $(region).data('fieldIndex');
//         var fieldName = $(region).data('fieldName');

//         var fieldType = $('.field-type', region).val();
//         var delimeter = $('.delimeter', region).val();

//         columns_generic[fieldIndex].type = fieldType;
//         columns_generic[fieldIndex].delimeter = delimeter;
//     });

//     processFile_Generic(function () {
//         alert('success');
//         //$('#generic-columns-columns .field-type').off('change');        // Remove any existing events.
//         //$('#generic-columns-columns .generic-column-detail').remove();   // Remove any existing entries
//     },
//         function () {
//             alert('fail');
//     });   
// }


// function uploadFile() {
//     var files = $("#fileInput").get(0).files;
//     var fileData = new FormData();

//     for (var i = 0; i < files.length; i++) {
//         fileData.append("fileInput", files[i]);
//     }

//     var request = $.ajax({
//         type: "POST",
//         url: "./services/csvconverter.asmx/UploadCSVFile",
//         dataType: "json",
//         cache: false,
//         contentType: false, // Not to set any content header
//         //contentType: "application/json; charset=utf-8",
//         processData: false, // Not to process data
//         data: fileData
//     });

//     request.done(function (data) {
//         if (data !== null) {
//             if (data.Success) {
//                 processFile();
//             } else {
//                 alert(data.ErrorMessage);
//             }

//         }
//     });

//     request.fail(function (jqXHR, textStatus) {
//         alert('uploadFile : ' + textStatus);
//     });
// }


// function uploadFileGeneric() {
//     var files = $("#fileInput-generic").get(0).files;
//     var fileData = new FormData();

//     for (var i = 0; i < files.length; i++) {
//         fileData.append("fileInput-generic", files[i]);
//     }

//     var request = $.ajax({
//         type: "POST",
//         url: "./services/csvconverter.asmx/UploadCSVFile",
//         dataType: "json",
//         cache: false,
//         contentType: false, // Not to set any content header
//         //contentType: "application/json; charset=utf-8",
//         processData: false, // Not to process data
//         data: fileData
//     });

//     request.done(function (data) {
//         if (data !== null) {
//             if (data.Success) {
//                 getFileColumnNames(function (data) {
//                     columns_generic = data.Columns;
//                     buildGenericColumns();

//                     $('#generic-columns-columns .field-type').on('change', onGenericFieldTypeChanged);

//                     $('#file-upload-generic').addClass('hidden');
//                     $('#generic-columns').removeClass('hidden');
//                 });
//             } else {
//                 alert(data.ErrorMessage);
//             }

//         }
//     });

//     request.fail(function (jqXHR, textStatus) {
//         alert('uploadFile : ' + textStatus);
//     });
// }


function parseCSVFile(stage) {
    'use strict';
    var files = $("#fileInput").get(0).files;
    
    if (files.length > 0) {
        Papa.parse(files[0], {
            delimiter: "",	// auto-detect
            newline: "",	// auto-detect
            quoteChar: '"',
            escapeChar: '"',
            //header: false,
            header: true,
            transformHeader: undefined,
            //dynamicTyping: false,
            dynamicTyping: true,
            preview: 0,
            encoding: "",
            worker: false,
            comments: false,
            step: undefined,
            //download: false,
            download: true,
            downloadRequestHeaders: undefined,
            skipEmptyLines: true,
            chunk: undefined,
            fastMode: undefined,
            beforeFirstChunk: undefined,
            withCredentials: undefined,
            transform: undefined,
            delimitersToGuess: [',', '\t', '|', ';', Papa.RECORD_SEP, Papa.UNIT_SEP],
            //complete: undefined,
            complete: function(results) {
                //CSV_Doba.ProcessCSVFile(results.data);
                console.log('Display CSV');
                console.log(results);
                //CSV_Doba.DownloadCSVFile();
                if (results.errors.length === 0) {
                    submitCSVfile(stage);
                } else {
                    let errorMessage = 'There were ' + results.errors.length.toString() + ' errors parsing the file.';
                    for (let i = 0; i < results.errors.length; i++) {
                        errorMessage += '\r\n' + results.errors[i].message;
                        if (results.errors[i].row !== undefined) {
                            errorMessage += ' Row ' + results.errors[i].row + '.';
                        }
                        if (i >= 10) {
                            break;
                        }
                    }
                    alert(errorMessage);
                    
                }
            },
            //error: undefined,
            error: function() {
                alert('error parsing csv file');
            }
        });
    } else {
        alert('Please select a .csv file to process.');
    }
}


function submitCSVfile(stage) {
    'use strict';
    let formData = new FormData();

    let fileType = $('input[type=radio][name=file-type]:checked').val();
    let currency = $('#cbFiatCurrency').val();
    let tmpFilename = $('#tmpFilename').val();

    formData.append('fileType', fileType);
    formData.append('currency', currency);
    formData.append('knownCategories', JSON.stringify(categories_known));

    if (tmpFilename.trim().length > 0) {
        formData.append('tmpFilename', tmpFilename);
    } else {
        if (document.getElementById('fileInput').files.length > 0) {
            formData.append('csvFile', document.getElementById('fileInput').files[0]);
        }
    }
    

    $.ajax({
        //url: '/api/admin/completeWithdrawalRequest',
        url: '/api.php?target=withdrawal_requests&endpoint=vendor%2FconvertCSV',
        type: 'post',
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 0,
        data: formData,
        // beforeSend: function() {
        //     $('#product-images-spinner').removeClass('hidden');
        // }
    })
    .done(function (data) {
        //console.log(data);
        if (data.result.code === ResponseCodeEnum.SUCCESS) {
            //refreshWithdrawalRequests();
            //$('#form-process-modal').modal('hide');
            $('#tmpFilename').val(data.tmpFilename);
            showSuccessMessage('Success');

            if (data.csv !== undefined && data.csv !== null) {
                downloadArrayAsCSV(data.csv, 'products.csv', null);
            }
        } else {
            switch (data.result.code) {
                default: {
                    //showErrorMessage(data.result.message);
                    alert(data.result.message);
                    break;
                }
            }
        }
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
        alert('fail - ' + errorThrown);
    })
    .always(function () {
        //$('#product-images-spinner').addClass('hidden');
    });
}


function loadFiatCurrencies() {
    'use strict';
    getPlatformCurrencies(function(data) {
        // Success
        'use strict';
        if (data.result.code === ResponseCodeEnum.SUCCESS) {

            buildFiatDropDowns(data.currencies);

        } else {
            console.error(data.result.message);
        }
    },
    function() {
        // Fail
        'use strict';
        jqXHR, textStatus;
        console.error(textStatus);
    });
}


function getPlatformCurrencies(successFn, failFn) {
    "use strict";
    var request = $.ajax({
        url: '/api.php?target=withdrawal_requests&endpoint=' + encodeURIComponent('vendor/getPlatformCurrencies'),
        //method: 'post',
        //dataType: "json",
        //contentType: "application/json; charset=utf-8"
        method: 'post',
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 0
    });

    request.done(function (data) {
        if (data !== undefined && data !== null) {
            if (data.result.code === 0) {
                if (successFn !== undefined) {
                    successFn(data);
                }
            }
        }
    });

    request.fail(function (jqXHR, textStatus) {
        if (failFn !== undefined) {
            failFn(jqXHR, textStatus);
        }
    });
}


function buildFiatDropDowns(currencies) {
    'use strict';
    //console.log(currencies);
    let selectedIndex = 0;

    $('#cbFiatCurrency').children().remove();   // Remove all options except for the first one.

    let index = 0;
    for (let currency_code in currencies) {
        if (currencies[currency_code].isFiat === 1) {
            if (currency_code === 'USD') {
                selectedIndex = index;
            }
            $('#cbFiatCurrency').append($('<option></option>')
                .val(currencies[currency_code].symbol)
                .html(currencies[currency_code].symbol + ' - ' + currencies[currency_code].name));
            index++;
        }
    }

    // Default to USD
    $('#cbFiatCurrency').prop('selectedIndex', selectedIndex);
}


$(document).ready(function () {
    'use strict';
    // ### File Selection
    $('input[type=radio][name=file-type]').on('change', function () {
        switch ($(this).val()) {
            case '0':
                // Generic
                $("#btnFileTypeNext").prop('disabled', false);
                break;
            case '1':
                // Doba
                $("#btnFileTypeNext").prop('disabled', false);
                break;
            case '2':
                // Shopify
                $("#btnFileTypeNext").prop('disabled', false);
                break;
            case '3':
                // Mathew
                $("#btnFileTypeNext").prop('disabled', false);
                break;
        }
    });

    /*
    $('input[type=radio][name=cat-override]').on('change', function () {
        switch ($(this).val()) {
            case 'empty':
                $('#btnCategoryOverride-Next').prop('disabled', false);
                $("#category-override-categories").addClass('hidden');
                break;
            case 'choose':
                $('#txtCategoryFilter1').val('');
                $('#btnFilter-Category1').trigger('click');
                $("#category-override-categories").removeClass('hidden');
                break;
        }
    });
    */

    $("#btnFileTypeNext").on('click', btnFileTypeNext_Click);

    $("#btnCurrencyBack").on('click', btnCurrencyBack_Click);
    $("#btnCurrencyNext").on('click', btnCurrencyNext_Click);

    $('#btnUploadFileBack').on('click', btnUploadFileBack_Click);
    //$("#btnFile").on('click', uploadFile);
    $("#btnUploadFile").on('click', btnUploadFile_Click);

    //$('#btnUpload-Back-generic').on('click', onUploadFileBackClick);
    //$("#btnFile-generic").on('click', uploadFileGeneric);

    // $('#btnGenericColumns-Back').on('click', onGenericColumnsBackClick);
    // $('#btnGenericColumns-Next').on('click', onGenericColumnsNextClick);

    /*$("#txtCategoryFilter1").on('keyup', onCategoryFilterKeyUp1);
    $("#txtCategoryFilter2").on('keyup', onCategoryFilterKeyUp2);
    $("#btnFilter-Category1").on('click', onFilterCategoryClick1);
    $("#btnFilter-Category2").on('click', onFilterCategoryClick2);

    $('#ddlCategory-Override').on('change', onCategorySelectChange1);
    $('#ddlCategories-Valid').on('change', onCategorySelectChange2);

    $('#btnUnknownCat-Save').on('click', onUnknownCatSaveClick);

    $('#btnCategoryOverride-Back').on('click', onCategoryOverrideBackClick);
    $('#btnCategoryOverride-Next').on('click', onCategoryOverrideNextClick);

    $('#btnDownloadFile-Restart').on('click', onDownloadFileRestartClick);

    

    

    $(document).ajaxStart(function () {
        $("#loadingImg").show();
        $("#btnFile").prop('disabled', true);
    });

    $(document).ajaxStop(function () {
        $("#loadingImg").hide();
        $("#btnFile").prop('disabled', false);
        $("#fileInput").val("");
    });
    */

   
    // Wait until all of the events have been attached
    //$('#file-type-0').prop('disabled', false);  // Generic
    $('#file-type-1').prop('disabled', false);  // Doba
    $('#file-type-2').prop('disabled', false);  // Shopify
    $('#file-type-3').prop('disabled', false);  // Custom-1

    $('#fileInput').on('change', fileInput_Change);
    $('#fileInput').on('click', fileInput_Change);
    
    /*
    updateExchangeRate();

    // Load remote catefory information when page first loads
    if (categories_treos.length === 0) {
        getCategories(function () {
            reformatCategories();
        });
    }
    */
    
    // let width_sm = window.matchMedia('(max-width: 768px)');
    // responsiveDesign(width_sm); // Call listener at run time
    // width_sm.addListener(responsiveDesign); // Attach listener function on state changes

    loadFiatCurrencies();
});
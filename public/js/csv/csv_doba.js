var CSV_Doba = (function () {

    var csv;

    // Constructor
    // Inherit
    csv = CSV_Master();

    // csv.ParseCSVfile = async function ParseCSVfile(CSVfile) {
    //     console.log('parsing file');
    //     await Papa.parse(CSVfile, {
    //         delimiter: "",	// auto-detect
    //         newline: "",	// auto-detect
    //         quoteChar: '"',
    //         escapeChar: '"',
    //         //header: false,
    //         header: true,
    //         transformHeader: undefined,
    //         //dynamicTyping: false,
    //         dynamicTyping: true,
    //         preview: 0,
    //         encoding: "",
    //         worker: false,
    //         comments: false,
    //         step: undefined,
    //         //download: false,
    //         download: true,
    //         downloadRequestHeaders: undefined,
    //         skipEmptyLines: true,
    //         chunk: undefined,
    //         fastMode: undefined,
    //         beforeFirstChunk: undefined,
    //         withCredentials: undefined,
    //         transform: undefined,
    //         delimitersToGuess: [',', '\t', '|', ';', Papa.RECORD_SEP, Papa.UNIT_SEP],
    //         //complete: undefined,
    //         complete: function(results) {
    //             _items = results.data;
    //             csv.ProcessCSVFile();
    //         },
    //         //error: undefined,
    //         error: function() {
    //             alert('error parsing csv file');
    //         }
    //     });
    // }

    csv.ProcessCSVFile = function ProcessCSVFile(items) {
        csv.ClearCSVfile();
        console.log('items.length = ' + items.length);
        for (let row = 0; row < items.length; row++) {
            let item = items[row];
            
            if (item.product_id !== null) {
                csv.AddToXCartArray(
                    UniqueProductIdentifier = item.product_id.toString()
                );
            }
        }
    }

    

    // return {
    //     ParseCSVfile: ParseCSVfile,
    //     ProcessCSVFile: ProcessCSVFile
    // };
    return csv;
})();

/* ------------- END OF MODULE ------------- */
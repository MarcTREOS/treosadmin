function CSV_Master() {
    var XCartItems = [];

    return {
        ClearCSVfile: function() { XCartItems.length = 0; },
        //ParseCSVfile: async function (CSVfile) { },
        ProcessCSVfile: function() {},
        AddToXCartArray: function(UniqueProductIdentifier = null,
            SkuCommon = null,       // If there are variants, this is the common SKU between them
            Sku = null,
            Name = null,
            Price = null,
            MarketPrice = null,
            Description = null,
            BriefDescription = null,
            Weight = null,
            Shippable = null,
            Images = null,
            VariantImage = null,    // Only to be used when it is a Variant
            ImagesAlt = null,
            InventoryTrackingEnabled = null,
            StockLevel = null,
            Attributes = null,
            Sale = null,
            UpcISBN = null,
            Manufacturer = null,
            RelatedProducts = null,
            ShipForFree = null,     // Enables/disables the 'Free shipping' option for a product
            FreeShipping = null,    // Enables/disables the 'Exclude from shipping cost calculation' option for a product
            FreightFixedFee = null,
            UseSeparateBox = null,
            BoxWidth = null,        // Width of box in mm
            BoxHeight = null,       // Height of box in mm
            BoxLength = null,       // Length of box in mm
            ItemsPerBox = null) {
            let xCartItem = {};
    
            xCartItem.sku = UniqueProductIdentifier || '';
            xCartItem.price = Price || '';
            xCartItem.weight = Weight || '';
            xCartItem.shippable = Shippable || '';
            xCartItem.images = Images || '';
            xCartItem.imagesAlt = ImagesAlt || '';
            xCartItem.inventoryTrackingEnabled = InventoryTrackingEnabled || '';
            xCartItem.stockLevel = StockLevel || '';
            xCartItem.useSeparateBox = UseSeparateBox || '';
            xCartItem.boxWidth = BoxWidth || '';
            xCartItem.boxLength = BoxLength || '';
            xCartItem.boxHeight = BoxHeight || '';
            xCartItem.itemsPerBox = ItemsPerBox || '';
            xCartItem.name = Name || '';
            xCartItem.description = Description || '';
            xCartItem.briefDescription = BriefDescription || '';

            xCartItem.shipForFree = ShipForFree || '';
            xCartItem.freeShipping = FreeShipping || '';
            xCartItem.freightFixedFee = FreightFixedFee || '';
            xCartItem.sale = Sale || '';
            xCartItem.marketPrice = MarketPrice || '';
            
            
            
            
    
            XCartItems.push(xCartItem);
        },
        DownloadCSVFile: function() {
            //console.log('DownloadCSVFile');
            //console.log(XCartItems);

            var csv = Papa.unparse(XCartItems, {
                quotes: false, //or array of booleans
                quoteChar: '"',
                escapeChar: '"',
                delimiter: ",",
                header: true,
                newline: "\r\n",
                skipEmptyLines: true, //or 'greedy',
                columns: null //or array of strings
            });


            // Download the file
            var csvData = new Blob([csv], {type: 'text/csv;charset=utf-8;'});
            var csvURL =  null;
            if (navigator.msSaveBlob)
            {
                csvURL = navigator.msSaveBlob(csvData, 'products.csv');
            }
            else
            {
                csvURL = window.URL.createObjectURL(csvData);
            }

            var tempLink = document.createElement('a');
            tempLink.href = csvURL;
            tempLink.setAttribute('download', 'products.csv');
            tempLink.click();
        }
    } 

    
    // return {
    //     ParseCSVfile: ParseCSVfile,
    //     ProcessCSVFile: ProcessCSVFile,
    //     AddToXCartArray: AddToXCartArray
    // };
};
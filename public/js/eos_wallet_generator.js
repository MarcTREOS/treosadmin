var eosWalletGen = (function () {
    'use strict';
    // private variables
    var convTarget = null;
    var balances = null;
    var currencies = null;

    var test = 'test';

    function buildWalletGen(successFn) {
        "use strict";
        var containerDiv = null;
        var objContainerDiv = null;
        var keysDiv = null;
        var objKeysDiv = null;

        containerDiv = '<div id="eos-generator-modal" class="treos-modal-dialog modal fade" role="dialog" style="display: none;">' +
            '<div class="modal-dialog">' +
                '<div class="modal-content">' +
                    '<div class="modal-header">' +
                        '<button class="close" type="button" data-dismiss="modal">×</button>' +
                        '<h4 class="modal-title">' +
                            '<span>' +
                                '<img src="/images/wallets/EOS.png" style="height: 30px; width: auto; margin-bottom: 5px;">' +
                            '</span>' +
                            'Create EOS Account' +
                        '</h4>' +
                    '</div>' +
                    '<div class="modal-body">' +
                        '<div id="EosGenerator_Page1">' +
                            '<form id="EosGenerator_Form1" action="#">' +
                                '<h2>Account</h2>' +

                                '<p>This form will enable you to create and pay for your own EOS wallet to be created.</p>' +
                                '<p>The account will be created with the following resources:</p>' +
                                '<ul style="list-style: inside; margin-left: 30px;">' +
                                    '<li>CPU: 0.2 EOS</li>' +
                                    '<li>NET: 0.2 EOS</li>' +
                                    '<li>RAM: 6,000 bytes</li>' +
                                '</ul>' +

                                '<div class="form-group">' +
                                    '<label for="EosGenerator_AccountName">New account to create</label>' +
                                    '<input type="text" id="EosGenerator_AccountName" class="form-control" placeholder="Username of the new account" autocomplete="off">' +
                                    '<div id="divEosGenerator_AccountName_Error" class="alert alert-danger" role="alert" style="display:none;"></div>' +
                                    '<div id="divEosGenerator_AccountName_Available" class="alert alert-success" role="alert" style="display:none;"></div>' +
                                    '<small id="emailHelp" class="form-text text-muted">12 characters long, lowercase and only characters a-z or numbers 1-5 can be used in name.</small>' +
                                '</div>' +
                            '</form>' +
                        '</div>' +  // Page1

                        '<div id="EosGenerator_Page2" style="display: none;">' +
                            '<form id="EosGenerator_Form2" action="#">' +

                                '<h2>Keys</h2>' +
                                '<p>' +
                                    'If you have generated your own keys, enter the public keys below.  Otherwise, we can ' +
                                    ' generate the keys for you.' +
                                '</p>' +
                                '<p><a id="lnkEosGenerator_GenerateKeys" href="#">Generate Keys</a></p>' +
                                '<div class="form-group">' +
                                    '<label for="EosGenerator_PublicOwnerKey">New Accounts Public Owner Key</label>' +
                                    '<input type="text" id="EosGenerator_PublicOwnerKey" class="form-control" placeholder="Owner Public Key (Starts with EOS...)" autocomplete="off">' +
                                    '<div id="divEosGenerator_PublicOwnerKey_Error" class="alert alert-danger" role="alert" style="display:none;"></div>' +
                                    '<small id="emailHelp" class="form-text text-muted">Public key only, should be 54 characters long and start with EOS.</small>' +
                                '</div>' +
                                '<div class="form-group">' +
                                    '<label for="EosGenerator_PublicActiveKey">New Accounts Public Active Key</label>' +
                                    '<input type="text" id="EosGenerator_PublicActiveKey" class="form-control" placeholder="Active Public Key (Starts with EOS...)" autocomplete="off">' +
                                    '<div id="divEosGenerator_PublicActiveKey_Error" class="alert alert-danger" role="alert" style="display:none;"></div>' +
                                    '<small id="emailHelp" class="form-text text-muted">Public key only, should be 54 characters long and start with EOS.</small>' +
                                '</div>' +
                                '<p style="color: #d44950;">' +
                                'Warning<br>' +
                                'Failure to save a copy of your Private keys will result in no control or ownership of your new account. ' +
                                'TR<span style="text-decoration:underline;">E</span>OS does NOT store a copy of your private keys and will not be liable or responsible for the recovery of your account.' +
                                '</p>' +
                                '<p>' +
                                'Please save your public and private keys somewhere safe.' +
                                '</p>' +
                                '<div class="form-check" style="margin-top: 30px;">' +
                                    '<input type="checkbox" id="EosGenerator_cbKeysSaved" class="form-check-input">' +
                                    '<label for="EosGenerator_cbKeysSaved" class="form-check-label">I have saved my keys.</label>' +
                                    '<div id="divEosGenerator_cbKeysSaved_Error" class="alert alert-danger" role="alert" style="display:none;"></div>' +
                                '</div>' +
                            '</form>' +
                        '</div>' +  // Page2

                        '<div id="EosGenerator_Page3" style="display: none;">' +
                            '<form id="EosGenerator_Form3" action="#">' +
                                '<h2>Payment</h2>' +
                                '<p>' +
                                    'EOS accounts aren\'t free to create.  As such, a fee is required to create the account and set it up with enough resources to get you started.' +
                                '</p>' +
                                '<p>' +
                                    'Which currency would you like to pay with?' +
                                '</p>' +
                                '<div class="form-check" style="margin-top: 30px;">' +
                                    '<input type="checkbox" id="EosGenerator_cbShowAllSymbols" class="form-check-input">' +
                                    '<label for="EosGenerator_cbShowAllSymbols" class="form-check-label">Show all</label>' +
                                '</div>' +
                                '<p id="EosGenerator_Fee" style="text-align: center;display: none;"><strong>Fee:</strong>&nbsp;<span id="EosGenerator_FeeAmount"></span></p>' +
                                '<div id="EosGenerator_InsufficientBalance" class="alert alert-danger" style="text-align: center; margin-bottom: 10px; display: none;">Insufficient Balance</div>' +
                                '<div id="EosGenerator_Coins">' +
                                '</div>' +
                            '</form>' +
                        '</div>' +  // Page3

                        '<div id="EosGenerator_Page4" style="display: none;">' +
                            '<form id="EosGenerator_Form4" action="#">' +
                                '<h2>Confirmation</h2>' +
                                '<p>The account will be created using the following information that you have provided:</p>' +
                                '<div style="display: table; overflow: hidden;">' +
                                    '<div style="display: table-row;">' +
                                        '<div style="display: table-cell; text-align: right; padding: 10px; white-space: nowrap;">' +
                                            '<strong>Account</strong>' +
                                        '</div>' +
                                        '<div id="EosGenerator_AccountName_Confirm" style="display: table-cell; overflow: hidden;">' +
                                        '</div>' +
                                    '</div>' +
                                    '<div style="display: table-row">' +
                                        '<div style="display: table-cell; text-align: right; padding: 10px; white-space: nowrap;">' +
                                            '<strong>Public Owner Key</strong>' +
                                        '</div>' +
                                        '<div id="EosGenerator_PublicOwnerKey_Confirm" style="display: table-cell; overflow: hidden;">' +
                                        '</div>' +
                                    '</div>' +
                                    '<div style="display: table-row">' +
                                        '<div style="display: table-cell; text-align: right; padding: 10px; white-space: nowrap;">' +
                                            '<strong>Public Active Key</strong>' +
                                        '</div>' +
                                        '<div id="EosGenerator_PublicActiveKey_Confirm" style="display: table-cell; overflow: hidden;">' +
                                        '</div>' +
                                    '</div>' +
                                    '<div style="display: table-row">' +
                                        '<div style="display: table-cell; text-align: right; padding: 10px; white-space: nowrap;">' +
                                            '<strong>Fee</strong>' +
                                        '</div>' +
                                        '<div id="EosGenerator_FeeAmount_Confirm" style="display: table-cell; overflow: hidden;">' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</form>' +
                        '</div>' +  // Page4

                        '<div id="EosGenerator_Page5" style="display: none;">' +
                            '<form id="EosGenerator_Form5" action="#">' +
                                '<h2>Success</h2>' +
                                '<p>' +
                                    'EOS wallet creation is a manual process.  Your request has been stored and you will receive an email once your account has been created.' +
                                '</p>' +
                            '</form>' +
                        '</div>' +  // Page5

                    '</div>' +
                    '<div class="modal-footer">' +
                        '<div style="display: table; width: 100%;">' +
                            '<div style="display: table-row;">' +
                                '<div style="display: table-cell; text-align: left;">' +
                                    '<button type="button" id="btnEosGenerator_Back" class="btn btn-default" style="display: none;">Back</button>' +
                                '</div>' +
                                '<div style="display: table-cell; text-align: right;">' +
                                    '<button type="button" id="btnEosGenerator_Next" class="btn btn-success">Next</button>' +
                                    '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>';

        objContainerDiv = $(containerDiv);

        keysDiv = '<div id="eos-generator-keys-modal" class="treos-modal-dialog modal fade" role="dialog" style="display: none;">' +
            '<div class="modal-dialog">' +
                '<div class="modal-content">' +
                    '<div class="modal-header">' +
                        '<button class="close" type="button" data-dismiss="modal">×</button>' +
                        '<h4 class="modal-title">' +
                            'EOS Account Keys' +
                        '</h4>' +
                    '</div>' +
                    '<div class="modal-body">' +
                        '<p>' +
                        'Please save your keys somewhere safe.' +
                        '</p>' +
                        '<form method="post" action="#">' +
                            '<textarea id="txtEosGenerator_Keys" class="form-control" rows="10" readonly></textarea>' +
                            '<div style="text-align: right;">' +
                                '<img id="EosGenerator_btnDownloadKeys" src="/api.php?target=withdrawal_requests&endpoint=images%2Ficon_download_32x32.png" alt="Download file." style="height: 32px; width: 32px; margin-top: 10px; cursor: pointer;">' +
                                '<img id="EosGenerator_btnCopyKeys" src="/api.php?target=withdrawal_requests&endpoint=images%2Ficon_copy_32x32.png" alt="Copy keys to clipboard" style="height: 32px; width: 32px; margin-top: 10px; margin-left:20px; cursor: pointer;">' +
                            '</div>' +
                        '</form>' +
                    '</div>' +
                    '<div class="modal-footer">' +
                        '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' +
                    '</div>' +
                '</div>' +
            '</div>';

        objKeysDiv = $(keysDiv);

        successFn(objContainerDiv, objKeysDiv);
    }

    function reset() {
        hidePage2();
        $('#EosGenerator_PublicOwnerKey').val('');
        $('#EosGenerator_PublicActiveKey').val('');
        $('#divEosGenerator_PublicOwnerKey_Error').hide();
        $('#divEosGenerator_PublicActiveKey_Error').hide();
        $('#EosGenerator_cbKeysSaved').prop('checked', false);
        $('#divEosGenerator_cbKeysSaved_Error').hide();

        hidePage3();
        $('#EosGenerator_Fee').hide();
        $('#EosGenerator_FeeAmount').empty();
        $('#EosGenerator_Coins').empty();
        $('#EosGenerator_InsufficientBalance').hide();

        hidePage4();

        hidePage5();

        $('#EosGenerator_AccountName').val('');
        $('#divEosGenerator_AccountName_Error').hide();
        $('#divEosGenerator_AccountName_Available').hide();
        showPage1();

        //showPage3();
        //$('#EosGenerator_AccountName').val('junglemonkee').trigger('input'); // Remove this
    }

    function showPage1() {
        hidePage2();
        hidePage3();
        hidePage4();
        hidePage5();
        $('#btnEosGenerator_Back').hide();
        $('#btnEosGenerator_Next').show();
        $('#EosGenerator_Page1').show();
    }

    function showPage2() {
        hidePage1();
        hidePage3();
        hidePage4();
        hidePage5();
        $('#btnEosGenerator_Back').show();
        $('#btnEosGenerator_Next').show();
        $('#EosGenerator_Page2').show();
        $('#divEosGenerator_cbKeysSaved_Error').html('You must save your keys somewhere safe.');
    }

    function showPage3() {
        hidePage1();
        hidePage2();
        hidePage4();
        hidePage5();
        $('#btnEosGenerator_Back').show();
        $('#btnEosGenerator_Next').show();
        $('#EosGenerator_Page3').show();
    }

    function showPage4() {
        'use strict';
        hidePage1();
        hidePage2();
        hidePage3();
        hidePage5();
        $('#btnEosGenerator_Back').show();
        $('#btnEosGenerator_Next').show();
        $('#EosGenerator_Page4').show();
    }

    function showPage5() {
        'use strict';
        hidePage1();
        hidePage2();
        hidePage3();
        hidePage4();
        $('#btnEosGenerator_Back').hide();
        $('#btnEosGenerator_Next').hide();
        $('#EosGenerator_Page5').show();
    }

    function hidePage1() {
        $('#EosGenerator_Page1').hide();
    }

    function hidePage2() {
        $('#EosGenerator_Page2').hide();
    }

    function hidePage3() {
        $('#EosGenerator_Page3').hide();
    }

    function hidePage4() {
        $('#EosGenerator_Page4').hide();
    }

    function hidePage5() {
        $('#EosGenerator_Page5').hide();
    }

    function hide(event) {
        'use strict';
        $('#eos-generator-modal').modal('hide');
    }

    function show(event) {
        'use strict';
        reset();
        refreshBalances();
        $('#eos-generator-modal').modal('show');
    }

    function accountNameIsValid(account_name) {
        'use strict';
        let result = false;
        if (account_name) {
            let matched = account_name.match(/^[a-z1-5]{12}$/);

            if (matched !== null) {
                result = true;
            }
        } else {
            console.log('accountName = ' + account_name);
        }

        return result;
    }

    function accountExists(account_name, successFn, failFn) {
        'use strict';
        
        if (account_name && account_name.length === 12) {
            let formData = new FormData();
    
            formData.append('account_name', account_name);

            $.ajax({
                //url: '/api/admin/unreserveWithdrawalRequest',
                url: '/api.php?target=withdrawal_requests&endpoint=' + encodeURIComponent('vendor/eosAccountExists'),
                method: 'post',
                dataType: 'json',
                processData: false,
                contentType: false,
                cache: false,
                timeout: 0,
                data: formData,
                beforeSend: function() {
                    // $('#product-images-spinner').removeClass('hidden');
                }
            })
            .done(function (data) {
                if (data.result.code === ResponseCodeEnum.SUCCESS) {
                    if (data.accountExists) {
                        $('#divEosGenerator_AccountName_Error').html('Account already exists.').show();
                        $('#divEosGenerator_AccountName_Available').hide();
                    } else if (data.requestExists) {
                        $('#divEosGenerator_AccountName_Error').html('Account creation request already exists for that username.').show();
                        $('#divEosGenerator_AccountName_Available').hide();
                    } else {
                        $('#divEosGenerator_AccountName_Error').hide();
                        $('#divEosGenerator_AccountName_Available').html('Account is available.').show();
                    }
                } else {
                    // Do nothing
                    $('#divEosGenerator_AccountName_Error').html(data.result.message).show();
                    $('#divEosGenerator_AccountName_Available').hide();
                }

                if (successFn) {
                    successFn(data);
                }
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                if (failFn) {
                    failFn(jqXHR, textStatus, errorThrown);
                }
            })
            .always(function () {
                //$('#product-images-spinner').addClass('hidden');
            });
        } else {
            $('#divEosGenerator_AccountName_Error').html('Invalid Account Name.').show();
            $('#divEosGenerator_AccountName_Available').hide();
        }
    }

    function refreshBalances() {
        'use strict';
        
        getPlatformCurrencies(function(data) {
            'use strict';
            // successFn
            if (data.result.code === ResponseCodeEnum.SUCCESS) {

                currencies = data.currencies;

                getBalances(function(balanceData) {
                    'use strict';
                    // successFn
                    if (balanceData.result.code === ResponseCodeEnum.SUCCESS) {
        
                        balances = balanceData.profile.balances;
        
                        displayBalances();
        
                    } else {
                        // Do nothing
                    }
                }),
                function(jqXHR, textStatus, errorThrown) {
                    'use strict';
                    // failFn
                };
            } else {
                // Do nothing
            }
        }, function(jqXHR, textStatus, errorThrown) {
            'use strict';
            // failFn
        });
    }

    function displayBalances() {
        'use strict';

        $('#EosGenerator_Fee').hide();
        $('#EosGenerator_InsufficientBalance').hide();

        $('input[type=radio][name=eoswallet]').off('change');
        
        $('#EosGenerator_Coins').empty();

        if ($('#EosGenerator_cbShowAllSymbols').is(':checked')) {
            $.each(currencies, function(key, value) {
                if (value.isFiat === 0 && key != 'GCR' && key != 'TRO_UBI' && key !== 'TRO') {
                    let coin = ' '; // Space is important for justification
                    coin += '<span class="radio-buttons">';
                    coin += '<input id="' + key.toLowerCase() + '-eos-wallet-radio" type="radio" name="eoswallet" value="' + key.toUpperCase() + '">';
                    coin += '<label class="radio-label" for="' + key.toLowerCase() + '-eos-wallet-radio">';
                    coin += key;
                    coin += '<img src="/images/wallets/' + key.toUpperCase() + '.png" alt="" style="height:17px; margin-bottom: 2px; margin-left: 2px;">';
                    coin += '</label>';
                    coin += '</span>';
    
                    $('#EosGenerator_Coins').append(coin);
                }
            });
        } else {
            $.each(balances, function(key, value) {
                if (value.currency !== undefined && key != 'GCR' && key != 'TRO_UBI' && key !== 'TRO') {
                    let fee = calculateFee(key);

                    if (balances[key].balance >= fee) {
                        let coin = ' '; // Space is important for justification
                        coin += '<span class="radio-buttons">';
                        coin += '<input id="' + key.toLowerCase() + '-eos-wallet-radio" type="radio" name="eoswallet" value="' + key.toUpperCase() + '">';
                        coin += '<label class="radio-label" for="' + key.toLowerCase() + '-eos-wallet-radio">';
                        coin += key;
                        coin += '<img src="/images/wallets/' + key.toUpperCase() + '.png" alt="" style="height:17px; margin-bottom: 2px; margin-left: 2px;">';
                        coin += '</label>';
                        coin += '</span>';

                        $('#EosGenerator_Coins').append(coin);
                    }
                }
            });
        }
        

        if ($('#EosGenerator_Coins').html().length === 0) {
            $('#EosGenerator_InsufficientBalance').html('Insufficient balance to pay the fee.<br><br>Please deposit more funds into one of your wallets.').show();
        } else {
            $('#EosGenerator_Coins').append('<span class="stretch"></span>');   // Force justification
            $('input[type=radio][name=eoswallet]').on('change', symbol_onChange);
        }
    }

    function getBalances(successFn, failFn) {
        'use strict';
        
        let formData = new FormData();

        $.ajax({
            //url: '/api/admin/unreserveWithdrawalRequest',
            url: '/api.php?target=withdrawal_requests&endpoint=' + encodeURIComponent('vendor/getVendorProfile'),
            method: 'post',
            dataType: 'json',
            processData: false,
            contentType: false,
            cache: false,
            timeout: 0,
            data: formData,
            beforeSend: function() {
                // $('#product-images-spinner').removeClass('hidden');
            }
        })
        .done(function (data) {
            'use strict';
            if (successFn) {
                successFn(data);
            }
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            if (failFn) {
                failFn(jqXHR, textStatus, errorThrown);
            }
        })
        .always(function () {
            //$('#product-images-spinner').addClass('hidden');
        });
    }

    function getPlatformCurrencies(successFn, failFn) {
        'use strict';
        
        let formData = new FormData();

        $.ajax({
            //url: '/api/admin/unreserveWithdrawalRequest',
            url: '/api.php?target=withdrawal_requests&endpoint=' + encodeURIComponent('vendor/getPlatformCurrencies'),
            method: 'post',
            dataType: 'json',
            processData: false,
            contentType: false,
            cache: false,
            timeout: 0,
            data: formData,
            beforeSend: function() {
                // $('#product-images-spinner').removeClass('hidden');
            }
        })
        .done(function (data) {
            'use strict';
            if (successFn) {
                successFn(data);
            }
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            'use strict';
            if (failFn) {
                failFn();
            }
        })
        .always(function () {
            //$('#product-images-spinner').addClass('hidden');
        });
    }

    function calculateFee(symbol) {
        let result = 0.0;
        let usdFee = 4.0;   // $4

        let usdRate = currencies['USD'].tro_rate;    // 1 TRO = 49.12 USD
        let symbolRate = currencies[symbol].tro_rate; // 1 TRO = 24.07843137 EOS

        let usdMultiplier = usdFee / usdRate;   // 0.08143322  8.1% of 1 x TRO

        result = symbolRate * usdMultiplier;
        result = parseFloat(result.toFixed(currencies[symbol].exponent));

        return result;
    }

    async function generateKeys(successFn) {
        'use strict';
        // https://github.com/EOSIO/eosjs-ecc
        let privateKey = await eosjs_ecc.randomKey();
        let publicKey = eosjs_ecc.privateToPublic(privateKey);
        successFn(privateKey, publicKey);
    }

    async function btnEosGenerator_Next_onClick(event) {
        'use strict';
        event.target.blur();
        if ($("#EosGenerator_Page1").is(":visible")) {
            $('#EosGenerator_Form1').submit();
        } else if ($("#EosGenerator_Page2").is(":visible")) {
            $('#EosGenerator_Form2').submit();
        } else if ($("#EosGenerator_Page3").is(":visible")) {
            $('#EosGenerator_Form3').submit();
        } else if ($("#EosGenerator_Page4").is(":visible")) {
            $('#EosGenerator_Form4').submit();
        }

        
    }

    async function btnEosGenerator_Back_onClick(event) {
        'use strict';
        event.target.blur();
        if ($("#EosGenerator_Page2").is(":visible")) {
            showPage1();
        } else if ($("#EosGenerator_Page3").is(":visible")) {
            showPage2();
        } else if ($("#EosGenerator_Page4").is(":visible")) {
            showPage3();
        }
    }

    async function EosGenerator_cbKeysSaved_onChange(event) {
        let checkBox = $(event.target);
        if (checkBox.is(':checked')) {
            $('#divEosGenerator_cbKeysSaved_Error').hide();
        } else {
            $('#divEosGenerator_cbKeysSaved_Error').show();
        }
    }

    async function EosGenerator_btnCopyKeys_onClick(event) {
        let copyText = document.getElementById("txtEosGenerator_Keys");

        /* Select the text field */
        copyText.select();
        copyText.setSelectionRange(0, 99999); /*For mobile devices*/

        /* Copy the text inside the text field */
        document.execCommand("copy");

        copyText.selectionStart = -1;
        copyText.selectionEnd = -1;

        alert('The keys have been copied to the clipboard.');
    }

    async function EosGenerator_btnDownloadKeys_onClick(event) {
        let accountName = $('#EosGenerator_AccountName').val();
        let filename = 'EOS_Account_Keys-' + accountName + '.txt';

        let copyText = document.getElementById("txtEosGenerator_Keys").value;

        var csvData = new Blob([copyText], {type: 'text/plain;charset=utf-8;'});
        var csvURL =  null;
        if (navigator.msSaveBlob) {
            csvURL = navigator.msSaveBlob(csvData, 'filename');
        } else {
            csvURL = window.URL.createObjectURL(csvData);
        }
        var tempLink = document.createElement('a');
        tempLink.href = csvURL;
        tempLink.setAttribute('download', filename);
        tempLink.click();
    }

    function EosGenerator_Form1_onSubmit(event) {
        'use strict';
        event.preventDefault();

        let accountName = $('#EosGenerator_AccountName').val();

        if (accountNameIsValid(accountName)) {
            accountExists(accountName, function(data) {
                if (data.result.code === ResponseCodeEnum.SUCCESS && !data.accountExists && !data.requestExists) {
                    showPage2();
    
                    eosjs_ecc.initialize();
                }
            },
            function(jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            });
        } else {
            $('#divEosGenerator_AccountName_Error').html('Invalid Account Name.').show();
            $('#divEosGenerator_AccountName_Available').hide();
        }
        
        return false;
    }

    function EosGenerator_Form2_onSubmit(event) {
        'use strict';
        event.preventDefault();

        let validOwnerKey = false;
        let validActiveKey = false;
        let keysSaved = false;

        let owner_public_key = $('#EosGenerator_PublicOwnerKey').val();
        let active_public_key = $('#EosGenerator_PublicActiveKey').val();

        if (eosjs_ecc.isValidPublic(owner_public_key)) {
            validOwnerKey = true;
            $('#divEosGenerator_PublicOwnerKey_Error').hide();
        } else {
            $('#divEosGenerator_PublicOwnerKey_Error').html('Invalid Public Owner Key.').show();
        }

        if (eosjs_ecc.isValidPublic(active_public_key)) {
            validActiveKey = true;
            $('#divEosGenerator_PublicActiveKey_Error').hide();
        } else {
            $('#divEosGenerator_PublicActiveKey_Error').html('Invalid Public Active Key.').show();
        }

        if ($('#EosGenerator_cbKeysSaved').prop('checked')) {
            keysSaved = true;
            $('#divEosGenerator_cbKeysSaved_Error').hide();
        } else {
            $('#divEosGenerator_cbKeysSaved_Error').show();
        }

        if (validOwnerKey && validActiveKey && keysSaved) {
            showPage3();
        }
        
        return false;
    }

    function EosGenerator_Form3_onSubmit(event) {
        'use strict';
        let proceed = true;
        event.preventDefault();
        
        $('#EosGenerator_InsufficientBalance').hide();

        let fee = 0.0;
        let balance = 0.0;
        let symbol = $("input[name='eoswallet']:checked").val();

        if (proceed) {
            if (symbol === undefined) {
                $('#EosGenerator_InsufficientBalance').html('You must select a currency to pay with.').show();
                proceed = false;
            }
        }

        if (proceed) {
            fee = calculateFee(symbol);
            balance = 0.0;

            if (balances[symbol] !== undefined) {
                balance = parseFloat(balances[symbol].balance);
            }

            if (fee > balance) {
                $('#EosGenerator_InsufficientBalance').html('Insufficent Balance<br>' + balance.toFixed(currencies[symbol].exponent) + ' ' + symbol).show();
                proceed = false;
            }
        }
        
        if (proceed) {
            $('#EosGenerator_AccountName_Confirm').html($('#EosGenerator_AccountName').val());
            $('#EosGenerator_PublicOwnerKey_Confirm').html($('#EosGenerator_PublicOwnerKey').val());
            $('#EosGenerator_PublicActiveKey_Confirm').html($('#EosGenerator_PublicActiveKey').val());
            $('#EosGenerator_FeeAmount_Confirm').html('<image src="/images/wallets/' + symbol + '.png" style="height: 20px; width: auto;">&nbsp;' +
                fee.toFixed(currencies[symbol].exponent));
            showPage4();
        }

        return false;
    }

    function EosGenerator_Form4_onSubmit(event) {
        'use strict';
        event.preventDefault();

        let formData = new FormData();
    
        let symbol = $("input[name='eoswallet']:checked").val();

        formData.append('account_name', $('#EosGenerator_AccountName').val());
        formData.append('owner_public_key', $('#EosGenerator_PublicOwnerKey').val());
        formData.append('active_public_key', $('#EosGenerator_PublicActiveKey').val());
        formData.append('symbol', symbol);
        formData.append('fee', calculateFee(symbol));

        $.ajax({
            url: '/api.php?target=withdrawal_requests&endpoint=' + encodeURIComponent('vendor/eosCreateAccount'),
            method: 'post',
            dataType: 'json',
            processData: false,
            contentType: false,
            cache: false,
            timeout: 0,
            data: formData,
            beforeSend: function() {
                // $('#product-images-spinner').removeClass('hidden');
            }
        })
        .done(function (data) {
            if (data.result.code === ResponseCodeEnum.SUCCESS) {
                // Update balances on wallet page if on production
                if (typeof window.refreshBalances === "function") { 
                    // safe to use the function
                    window.refreshBalances();
                }

                showPage5();
            } else {
                // Do nothing
                alert(data.result.message);
            }
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            alert('Fail');
        })
        .always(function () {
            //$('#product-images-spinner').addClass('hidden');
        });

        return false;
    }

    function EosGenerator_AccountName_onInput(event) {
        let target = $(event.target);
        target.val(target.val().toLowerCase());

        if (target.val().length === 12) {
            if (accountNameIsValid(target.val())) {
                $('#divEosGenerator_AccountName_Error').hide();
                $('#divEosGenerator_AccountName_Available').hide();

                accountExists(target.val());
            } else {
                $('#divEosGenerator_AccountName_Error').html('Invalid Account Name.').show();
                $('#divEosGenerator_AccountName_Available').hide();
            }
        } else {
            $('#divEosGenerator_AccountName_Error').html('Account name must be 12 characters long.').show();
            $('#divEosGenerator_AccountName_Available').hide();
        }
    }

    function EosGenerator_PublicOwnerKey_onInput(event) {
        let publicKey = $(event.target).val();
        if (publicKey.length > 0 && !eosjs_ecc.isValidPublic(publicKey)) {
            $('#divEosGenerator_PublicOwnerKey_Error').html('Invalid Public Owner Key.').show();
        } else {
            $('#divEosGenerator_PublicOwnerKey_Error').hide();
        }
    }

    function EosGenerator_PublicActiveKey_onInput(event) {
        let publicKey = $(event.target).val();
        if (publicKey.length > 0 && !eosjs_ecc.isValidPublic(publicKey)) {
            $('#divEosGenerator_PublicActiveKey_Error').html('Invalid Public Active Key.').show();
        } else {
            $('#divEosGenerator_PublicActiveKey_Error').hide();
        }
    }

    function lnkEosGenerator_GenerateKeys_onClick(event) {
        generateKeys(function(owner_private_key, owner_public_key) {
            generateKeys(function(active_private_key, active_public_key) {
                $('#EosGenerator_PublicOwnerKey').val(owner_public_key).trigger('input');
                $('#EosGenerator_PublicActiveKey').val(active_public_key).trigger('input');

                let allKeys = 'Account: ' + $('#EosGenerator_AccountName').val() + '\r\n\r\n' +
                    'Owner Keys' + '\r\n' +
                    'Public: ' + owner_public_key + '\r\n' +
                    'Private: ' + owner_private_key + '\r\n\r\n' +
                    'Active Keys' + '\r\n' +
                    'Public: ' + active_public_key + '\r\n' +
                    'Private: ' + active_private_key;

                $('#txtEosGenerator_Keys').html(allKeys);

                $('#eos-generator-keys-modal').modal('show');
            });
        });
        return false;
    }

    function EosGenerator_cbShowAllSymbols_onChange(event) {
        'use strict';
        displayBalances();
    }

    function symbol_onChange(event) {
        let symbol = event.target.value;
        let fee = calculateFee(symbol);
        let balance = 0.0;

        if (balances[symbol] !== undefined) {
            balance = parseFloat(balances[symbol].balance);
        }

        $('#EosGenerator_FeeAmount').html('<image src="/images/wallets/' + symbol.toUpperCase() + '.png" style="height: 20px; width: auto;">&nbsp;' +
            fee.toFixed(currencies[symbol].exponent));

        $('#EosGenerator_Fee').show();

        if (fee > balance) {
            $('#EosGenerator_InsufficientBalance').html('Insufficent Balance<br>' + balance.toFixed(currencies[symbol].exponent) + ' ' + symbol).show();
        } else {
            $('#EosGenerator_InsufficientBalance').hide();
        }
        
    }


    return {
        buildWalletGen: buildWalletGen,
        hide: hide,
        show: show,
        accountExists: accountExists,
        EosGenerator_Form1_onSubmit: EosGenerator_Form1_onSubmit,
        EosGenerator_Form2_onSubmit: EosGenerator_Form2_onSubmit,
        EosGenerator_Form3_onSubmit: EosGenerator_Form3_onSubmit,
        EosGenerator_Form4_onSubmit: EosGenerator_Form4_onSubmit,
        EosGenerator_AccountName_onInput: EosGenerator_AccountName_onInput,
        EosGenerator_PublicOwnerKey_onInput: EosGenerator_PublicOwnerKey_onInput,
        EosGenerator_PublicActiveKey_onInput: EosGenerator_PublicActiveKey_onInput,
        lnkEosGenerator_GenerateKeys_onClick: lnkEosGenerator_GenerateKeys_onClick,
        btnEosGenerator_Next_onClick: btnEosGenerator_Next_onClick,
        btnEosGenerator_Back_onClick: btnEosGenerator_Back_onClick,
        EosGenerator_cbKeysSaved_onChange: EosGenerator_cbKeysSaved_onChange,
        EosGenerator_btnCopyKeys_onClick: EosGenerator_btnCopyKeys_onClick,
        EosGenerator_btnDownloadKeys_onClick: EosGenerator_btnDownloadKeys_onClick,
        EosGenerator_cbShowAllSymbols_onChange: EosGenerator_cbShowAllSymbols_onChange
    };
})();


$(document).ready(function () {
    "use strict";
    // Add CSS to head
    if ($('#css-eos-gen-stylesheet').length === 0) {
        let linkElement = document.createElement('link');
        linkElement.setAttribute('id', 'css-eos-gen-stylesheet');
        linkElement.setAttribute('rel', 'stylesheet');
        linkElement.setAttribute('type', 'text/css');
        linkElement.setAttribute('href', '/api.php?target=withdrawal_requests&endpoint=' + encodeURIComponent('css/eos_wallet_generator.css?t=20191208095400'));
        document.getElementsByTagName('head')[0].appendChild(linkElement);
    }


    // Add js to head
    if ($('#js-treos-include-enums').length === 0) {
        let jsElement = document.createElement('script');
        jsElement.setAttribute('id', 'js-treos-include-enums');
        jsElement.setAttribute('src', '/api.php?target=withdrawal_requests&endpoint=' + encodeURIComponent('js/enums.js?t=20191208095400'));
        document.getElementsByTagName('head')[0].appendChild(jsElement);
    }

    if ($('#js-eosjs-ecc').length === 0) {
        let jsElement = document.createElement('script');
        jsElement.setAttribute('id', 'js-eosjs-ecc');
        jsElement.setAttribute('src', 'https://cdn.jsdelivr.net/npm/eosjs-ecc@4.0.4/lib/eosjs-ecc.min.js');
        document.getElementsByTagName('head')[0].appendChild(jsElement);
    }

    if ($('#eos-generator-modal').length === 0) {
        eosWalletGen.buildWalletGen(function (containerDiv, keysDiv) {
            "use strict";
            $('body').append($(containerDiv));
            $('body').append($(keysDiv));

            $('#EosGenerator_Form1').on('submit', eosWalletGen.EosGenerator_Form1_onSubmit);
            $('#EosGenerator_Form2').on('submit', eosWalletGen.EosGenerator_Form2_onSubmit);
            $('#EosGenerator_Form3').on('submit', eosWalletGen.EosGenerator_Form3_onSubmit);
            $('#EosGenerator_Form4').on('submit', eosWalletGen.EosGenerator_Form4_onSubmit);
            $('#btnEosGenerator_Next').on('click', eosWalletGen.btnEosGenerator_Next_onClick);
            $('#btnEosGenerator_Back').on('click', eosWalletGen.btnEosGenerator_Back_onClick);
            $('#lnkEosGenerator_GenerateKeys').on('click', eosWalletGen.lnkEosGenerator_GenerateKeys_onClick);
            $('#EosGenerator_AccountName').on('input', eosWalletGen.EosGenerator_AccountName_onInput);
            $('#EosGenerator_PublicOwnerKey').on('input', eosWalletGen.EosGenerator_PublicOwnerKey_onInput);
            $('#EosGenerator_PublicActiveKey').on('input', eosWalletGen.EosGenerator_PublicActiveKey_onInput);
            $('#EosGenerator_cbKeysSaved').on('change', eosWalletGen.EosGenerator_cbKeysSaved_onChange);
            $('#EosGenerator_btnCopyKeys').on('click', eosWalletGen.EosGenerator_btnCopyKeys_onClick);
            $('#EosGenerator_btnDownloadKeys').on('click', eosWalletGen.EosGenerator_btnDownloadKeys_onClick);
            $('#EosGenerator_cbShowAllSymbols').on('change', eosWalletGen.EosGenerator_cbShowAllSymbols_onChange);
        });
    }
});
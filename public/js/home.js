function loadUsers(successFn) {
    'use strict';

    $.ajax({
        url: '/api/users/getUsers',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify( { } )
    })
    .done(function (data) {
        'use strict';
        
        if (data.success) {
            if (data.users !== null) {
                data.users.forEach(function(item) {
                    option = $('<option value="' + item + '">' + item + '</option>');
                    $('#ddlUserID').append(option);
                });

                successFn(data);
            } else {
                alert('No users returned');
            }
        } else {
            alert(data.errorMessage);
        }
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
        alert('fail');
    });
}

function createCookie(event) {
    'use strict';
    clearNotifyMessages();
    
    $.ajax({
        url: '/api/users/setUser',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify( { profile_id: $('#txtProfileID').val() } )
    })
    .done(function (data) {
        if (data.result.code === ResponseCodeEnum.SUCCESS) {
            showSuccessMessage('profile_id cookie has been stored.');
        } else {
            showErrorMessage(data.result.message);
        }
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
        showErrorMessage('fail - ' + textStatus);
    });

    return false;
}

function testCookie(event) {
    'use strict';
    
    clearNotifyMessages();

    showInfoMessage('All browser cookies = ' + document.cookie);
    let cookieValue = getCookie('UserToken');
    showInfoMessage('UserToken = ' + cookieValue);

    $.ajax({
        url: '/api/users/checkCookie',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify( { } )
    })
    .done(function (data) {
        'use strict';

        if (data.result.code === ResponseCodeEnum.SUCCESS) {
            showSuccessMessage(data.result.message);
        } else {
            showErrorMessage(data.result.message);
        }
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
        'use strict';

        showErrorMessage('fail');
    });
    return false;
}


function clearCookie(event) {
    'use strict';

    clearNotifyMessages();

    $.ajax({
        url: '/api/users/clearCookie',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify( { } )
    })
    .done(function (data) {
        if (data.result.code === ResponseCodeEnum.SUCCESS) {
            showSuccessMessage('Successfully cleared the UserToken');
        } else {
            showErrorMessage(data.result.message);
        }
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
        alert('fail');
    });
    return false;
}


function getCookie(cname) {
    'use strict';

    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }

$(document).ready(function() {
    'use strict';
    $('#frmLogin').on('submit', createCookie);
    $('#btnTestCookie').on('click', testCookie);
    $('#btnClearCookie').on('click', clearCookie);

    // Stops Bootstrap buttons from appearing white when focussed
    $(".btn").on('mouseup', function(event) {
        event.target.blur();
    });

    //loadUsers(function() {
        $('#btnSubmit').prop("disabled", false);
    //});
    
});

//
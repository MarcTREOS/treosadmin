
function getAccountBalances(account, successFn, failFn, alwaysFn) {
    'use strict';
    
    let formData = new FormData();
    formData.append('account', account);

    $.ajax({
        //url: '/api/admin/unreserveWithdrawalRequest',
        url: '/api.php?target=withdrawal_requests&endpoint=' + encodeURIComponent('playground/getDfuse_AccountBalances'),
        method: 'post',
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 0,
        data: formData,
        beforeSend: function() {
            // $('#product-images-spinner').removeClass('hidden');
        }
    })
    .done(function (data) {
        'use strict';
        if (successFn) {
            successFn(data);
        }
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
        'use strict';
        if (failFn) {
            failFn(jqXHR, textStatus, errorThrown);
        }
    })
    .always(function (data) {
        'use strict';
        if(alwaysFn) {
            alwaysFn(data);
        }
    });
}

function getTokenBalances(contract, symbol, successFn, failFn, alwaysFn) {
    'use strict';
    
    let formData = new FormData();
    formData.append('contract', contract);
    formData.append('symbol', symbol);

    $.ajax({
        //url: '/api/admin/unreserveWithdrawalRequest',
        url: '/api.php?target=withdrawal_requests&endpoint=' + encodeURIComponent('playground/getDfuse_TokenBalances'),
        method: 'post',
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 0,
        data: formData,
        beforeSend: function() {
            // $('#product-images-spinner').removeClass('hidden');
        }
    })
    .done(function (data) {
        'use strict';
        if (successFn) {
            successFn(data);
        }
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
        'use strict';
        if (failFn) {
            failFn(jqXHR, textStatus, errorThrown);
        }
    })
    .always(function (data) {
        'use strict';
        if(alwaysFn) {
            alwaysFn(data);
        }
    });
}

function sendToken(account_to, amount, successFn, failFn, alwaysFn) {
    'use strict';
    
    let formData = new FormData();
    formData.append('account_to', account_to);
    formData.append('amount', amount);

    $.ajax({
        //url: '/api/admin/unreserveWithdrawalRequest',
        url: '/api.php?target=withdrawal_requests&endpoint=' + encodeURIComponent('playground/dfuse_SendToken'),
        method: 'post',
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 0,
        data: formData,
        beforeSend: function() {
            // $('#product-images-spinner').removeClass('hidden');
        }
    })
    .done(function (data) {
        'use strict';
        if (successFn) {
            successFn(data);
        }
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
        'use strict';
        if (failFn) {
            failFn(jqXHR, textStatus, errorThrown);
        }
    })
    .always(function (data) {
        'use strict';
        if(alwaysFn) {
            alwaysFn(data);
        }
    });
}

function btnSendToken_Click(event) {
    $(event.target).blur();

    sendToken('mdixonwallet', 0.0001, function(data) {
        'use strict';
        // success
    }
    , function(jqXHR, textStatus, errorThrown) {
        'use strict';
        // fail
    }
    , function(data) {
        'use strict';
        // always
        console.log(data);
    });
}

function btnAccountBalances_Click(event) {
    $(event.target).blur();

    getAccountBalances('junglemonkey', function(data) {
        'use strict';
        // success
    }
    , function(jqXHR, textStatus, errorThrown) {
        'use strict';
        // fail
    }
    , function(data) {
        'use strict';
        // always
        console.log(data);
    });
}

function btnTokenBalances_Click(event) {
    $(event.target).blur();

    getTokenBalances('mytreosworld', 'TRO', function(data) {
        'use strict';
        // success
    }
    , function(jqXHR, textStatus, errorThrown) {
        'use strict';
        // fail
    }
    , function(data) {
        'use strict';
        // always
        console.log(data);
    });
}

$(document).ready(function () {
    "use strict";
    $('#btnAccountBalances').on('click', btnAccountBalances_Click);
    $('#btnTokenBalances').on('click', btnTokenBalances_Click);
    $('#btnSendToken').on('click', btnSendToken_Click);
});
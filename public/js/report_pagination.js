
function generateReportPagination(objContainer, objRecordOffset, objRecordCounter, total_records) {
    'use strict';
    let maxPages = 10;
    let recordOffset = parseInt(objRecordOffset.val());
    let recordCounter = parseInt(objRecordCounter.val());
    let totalRecords = parseInt(total_records);
    let currentPageIndex = parseInt(recordOffset / recordCounter);

    let lastPageIndex = Math.ceil(totalRecords / recordCounter);

    let minWindowIndex = currentPageIndex - Math.ceil(maxPages / 2);
    let maxWindowIndex = currentPageIndex + Math.floor(maxPages / 2);

    if (minWindowIndex < 0) {
        minWindowIndex = 0;
        maxWindowIndex = minWindowIndex + maxPages;
        if (maxWindowIndex > lastPageIndex) {
            maxWindowIndex = lastPageIndex;
        }
    } else if (maxWindowIndex > lastPageIndex) {
        maxWindowIndex = lastPageIndex;
        minWindowIndex = maxWindowIndex - maxPages;
        if (minWindowIndex < 0) {
            minWindowIndex = 0;
        }
    }


    // <nav aria-label="Withdrawal History">
    //     <ul class="pagination">
    //         <li class="page-item"><a class="page-link" href="#">Previous</a></li>
    //         <li class="page-item"><a class="page-link" href="#">1</a></li>
    //         <li class="page-item"><a class="page-link" href="#">2</a></li>
    //         <li class="page-item"><a class="page-link" href="#">3</a></li>
    //         <li class="page-item"><a class="page-link" href="#">Next</a></li>
    //     </ul>
    // </nav>

    let $nav = $('<nav>');

    let $ul = $('<ul>', {
        'class': 'pagination'
    });

    $nav.append($ul);

    if (recordOffset > 0) {
        let $li = $('<li>', {
            'class': 'page-item'
        });

        let $a = $('<a>', {
            'class': 'page-link',
            'href': '#'
        }).text('Previous').appendTo($li);

        $a.on('click', function() {
            objRecordOffset.val(recordOffset - recordCounter);
            objRecordOffset.closest('form').submit();
            return false;
        });

        $ul.append($li);
    }

    if (total_records > recordCounter) {
        for (let i = 0; i < Math.ceil(totalRecords / recordCounter); i++) {
            let pageIndex = i + 1;

            if (pageIndex >= minWindowIndex && pageIndex <= maxWindowIndex) {
                let $li = $('<li>', {
                    'class': 'page-item'
                });
    
                if ((i * recordCounter) === recordOffset) {
                    $li.addClass('disabled');
                }
        
                let $a = $('<a>', {
                    'class': 'page-link',
                    'href': '#'
                }).text((pageIndex).toString()).appendTo($li);
        
                $a.on('click', function() {
                    objRecordOffset.val(i * recordCounter);
                    objRecordOffset.closest('form').submit();
                    return false;
                });
    
                $ul.append($li);
            }
        }
        $(objContainer.removeClass('hidden'));
    } else {
        $(objContainer.addClass('hidden'));
    }

    if (recordOffset + recordCounter < totalRecords) {
        let $li = $('<li>', {
            'class': 'page-item'
        });

        let $a = $('<a>', {
            'class': 'page-link',
            'href': '#'
        }).text('Next').appendTo($li);

        $a.on('click', function() {
            objRecordOffset.val(recordOffset + recordCounter);
            objRecordOffset.closest('form').submit();
            return false;
        });
        
        $ul.append($li);
    }

    $('a', objContainer).off('click');  // remove onClick event handlers
    $('nav', objContainer).remove();

    objContainer.append($nav);
}

function responsiveDesignGlobal(x) {
    if (x.matches) {
        // screen width <= 768px
        if (!$('body').hasClass('left-menu-compressed')) {
            $('.left-menu-ctrl').trigger('click');
        }
        $('button.responsive').addClass('btn-block');
        $('a.responsive').addClass('btn-block');
    } else {
        // screen width > 768px
        if ($('body').hasClass('left-menu-compressed')) {
            $('.left-menu-ctrl').trigger('click');
        }
        $('button.responsive').removeClass('btn-block');
        $('a.responsive').removeClass('btn-block');
    }
}


$(document).ready(function () {
    let width_sm = window.matchMedia('(max-width: 768px)');
    responsiveDesignGlobal(width_sm); // Call listener at run time
    width_sm.addListener(responsiveDesignGlobal); // Attach listener function on state changes
})
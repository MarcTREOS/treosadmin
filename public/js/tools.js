function ajaxCall(pagePath, fn, paramArray, doneFn, failFn, alwaysFn) {
    'use strict';
    $.ajax({
        type: 'POST',
        url: pagePath + '/' + fn,
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(paramArray),
        dataType: 'json'
    }).done(function (data) {
        if (doneFn !== undefined && doneFn !== null) {
            doneFn(data);
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        if (failFn !== undefined && failFn !== null) {
            failFn(jqXHR, textStatus, errorThrown);
        }
    }).always (function () {
        if (alwaysFn !== undefined && alwaysFn !== null) {
            alwaysFn();
        }
    });
}

/*
If the text is null or undefined then returns a non breaking space to retain
the table formatting.
*/
function cellText(text) {
    let result = '';
    if (text === null || text === undefined || String(text).length === 0) {
        result = '&nbsp;';
    } else {
        result = replaceLineBreaks(text, '<br>');
    }
    return result;
}

function countDecimals(value) {
    let result = 0;
    if (value !== null && value !== undefined) {
        if (Math.floor(value) !== value) {
            result = value.toString().split(".")[1].length || 0;
        }
    }
    return result;
}

function formatTokenBalance(tokenCode, value) {
    let result = '';
    let exponent = 8;

    if (!isNaN(value)) {
        //let decimalPlaces = countDecimals(parseFloat(value));

        if (value !== undefined && value !== null) {
            result = parseFloat(value);
    
            if (typeof dictionary_currencies !== undefined && dictionary_currencies[tokenCode] !== undefined) {
                let currency = dictionary_currencies[tokenCode];
                
                exponent = currency.exponent;
            } else {
                switch (tokenCode) {
                    case 'TRO': {
                        exponent = 4;
                        break;
                    }
                    case 'TRO_UBI': {
                        exponent = 4;
                        break;
                    }
                    case 'TRE': {
                        exponent = 4;
                        break;
                    }
                    case 'TREOS': {
                        exponent = 4;
                        break;
                    }
                    case 'EOS': {
                        exponent = 4;
                        break;
                    }
                }
            }

            // if (decimalPlaces < exponent) {
            result = result.toFixed(exponent);
            // } else {
            //     result = result.toFixed(decimalPlaces);
            // }
        } else {
            result = value;
        }
    } else {
        result = value;
    }

    return result;
}

function formatLocalDateTime(date) {
    //https://stackoverflow.com/questions/2388115/get-locale-short-date-format-using-javascript/7740464
    let rqDate = new Date(date);    // Convert JSON Date string to Date object
    //toLocaleDateString("en", { weekday: 'short', year: 'numeric', month: '2-digit', day: 'numeric' })

    let dateOptions = {
        weekday: "short",
        year: "numeric",
        month: "short",
        day: "numeric",
        hour12: false,
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit'
    }

    return rqDate.toLocaleString("en", dateOptions)
}

function formatUTCDateTime(date) {
    let result = '';
    let monthsOfYear = [ 'Jan', 'Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov', 'Dec' ];
    //let monthsOfYear = [ 'January', 'February','March','April','May','June','July','August','September','October','November', 'December' ];
    let newDate = new Date(date);
    result = newDate.getUTCDate();
    result += ' ' + monthsOfYear[newDate.getUTCMonth()];
    result += ' ' + newDate.getUTCFullYear();
    result += ' ' + zeroPad(newDate.getUTCHours(), 2);
    result += ':' + zeroPad(newDate.getUTCMinutes(), 2);
    result += ':' + zeroPad(newDate.getUTCSeconds(), 2);
    return result;
}


function formatUTCDateTimeForExport(date) {
    let result = '';
    let newDate = new Date(date);
    result = ' ' + newDate.getUTCFullYear();
    result += '-' + zeroPad(newDate.getUTCMonth() + 1, 2);
    result += '-' + zeroPad(newDate.getUTCDate(), 2);

    result += ' ' + zeroPad(newDate.getUTCHours(), 2);
    result += ':' + zeroPad(newDate.getUTCMinutes(), 2);
    result += ':' + zeroPad(newDate.getUTCSeconds(), 2);
    return result;
}


function isNullOrEmpty(value) {
    let result = false;
    if (value === null || value === undefined || value.toString().trim() === '') {
        result = true;
    }
    return result;
}

function roundDown(value, decimalPlaces) {
    let result = value;
    result = value * Math.pow(10, decimalPlaces);
    result = Math.floor(result);
    result = result / Math.pow(10, decimalPlaces);
    return result;
}

function roundUp(value, decimalPlaces) {
    let result = value;
    result = value * Math.pow(10, decimalPlaces);
    result = Math.ceil(result);
    result = result / Math.pow(10, decimalPlaces);
    return result;
}


//load src and convert to a File instance object
//work for any type of src, not only image src.
//return a promise that resolves with a File instance
function srcToFileObject(src, filename) {
    return (fetch(src)
        .then(function(res){return res.arrayBuffer();})
        .then(function(buf){return new File([buf], filename);})
        //.then(function(buf){return new File([buf], filename, {type:mimeType});})
    );
}

function clearNotifyMessages() {
    $('#notifyMessagePanel').remove();
    $('.has-error').removeClass('has-error');
}

function saveObjectToFile(obj, filename) {
    let mimeType = 'text/plain;charset=utf-8;';

    if (filename === undefined || filename === null) {
        filename = 'object.txt';
    }

    let arr = filename.split('.');
    let extension = arr[arr.length -1];

    switch (extension.toLowerCase()) {
        case 'json' : {
            mimeType = 'application/json;charset=utf-8';
            break;
        }
        default: {
            mimeType = 'text/plain;charset=utf-8;';
            break;
        }
    }

    let copyText = JSON.stringify(obj, null, 3);    // Make it pretty

    var csvData = new Blob([copyText], {type: mimeType});

    var csvURL =  null;
    if (navigator.msSaveBlob) {
        csvURL = navigator.msSaveBlob(csvData, 'filename');
    } else {
        csvURL = window.URL.createObjectURL(csvData);
    }
    var tempLink = document.createElement('a');
    tempLink.href = csvURL;
    tempLink.setAttribute('download', filename);
    tempLink.click();
}

function showSuccessMessage(message) {
    showSuccessMessages([message]);
}

function showSuccessMessages(messages) {
    showNotifyMessages(messages, 'alert-success');
}

function showInfoMessage(message) {
    showInfoMessages([message]);
}

function showInfoMessages(messages) {
    showNotifyMessages(messages, 'alert-info');
}

function showWarningMessage(message) {
    showWarningMessages([message]);
}

function showWarningMessages(messages) {
    showNotifyMessages(messages, 'alert-warning');
}

function showErrorMessage(message) {
    showErrorMessages([message]);
}

function showErrorMessages(messages) {
    showNotifyMessages(messages, 'alert-danger');
}

function showNotifyMessage(message, messageType) {
    showNotifyMessages([message], messageType);
}

function showNotifyMessages(messages, messageType) {
    /*
        alert-success
        alert-info
        alert-warning
        alert-danger
    */

    let panel = $('#notifyMessagePanel');

    if (panel.length === 0) {
        // Create the panel
        let main = $('#main');
        if (main.length > 0) {
            panel = $('<div id="notifyMessagePanel" class="row"></div>');
            main.prepend(panel);
        } else {
            console.log('Could not find "main" div to create message box');
        }
    }

    if (panel.length > 0) {
        let message = '';
        for (let i = 0; i < messages.length; i++) {
            if (i > 0) {
                message += '<br/>';
            }
            message += messages[i];
        }

        $('<div class="alert ' + messageType + '" role="alert">' + message + '</div>').appendTo($('#notifyMessagePanel'));

        // Scroll to the top of the page
        $("html, body").animate({ scrollTop: 0 }, "fast");
    }
}

function replaceLineBreaks(textIn, replaceString) {
    'use strict';
    let result = textIn;
    try {
        if (result) {
            result = result.toString();
            result = result.replace(/(?:\r\n|\r|\n)/g, replaceString);
        }
        // https://stackoverflow.com/questions/784539/how-do-i-replace-all-line-breaks-in-a-string-with-br-tags
    } catch(err) {
        console.log(textIn);
        console.error(err);
    }
    return result;
}

function zeroPad(number, minLength) {
    let result = number.toString();
    while (result.length < minLength) {
        result = '0' + result;
    }
    return result;
}

function clearObject(obj) {
    // Remove each property in the dictionary
    for (var prop in obj) {
        if (obj.hasOwnProperty(prop)) {
            delete obj[prop];
        }
    }
}


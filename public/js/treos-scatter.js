// https://github.com/GetScatter/scatter-js/tree/master/mock-sites/eosjs2

// https://github.com/GetScatter/scatter-js
// <script src="https://cdn.scattercdn.com/file/scatter-cdn/js/latest/scatterjs-core.min.js"></script>
// <script src="https://cdn.scattercdn.com/file/scatter-cdn/js/latest/scatterjs-plugin-eosjs.min.js"></script>
// <script src="https://cdn.scattercdn.com/file/scatter-cdn/js/latest/scatterjs-plugin-eosjs2.min.js"></script>
// <script src="https://cdn.scattercdn.com/file/scatter-cdn/js/latest/scatterjs-plugin-web3.min.js"></script>
// <script src="https://cdn.scattercdn.com/file/scatter-cdn/js/latest/scatterjs-plugin-tron.min.js"></script>
// <script src="https://cdn.scattercdn.com/file/scatter-cdn/js/latest/scatterjs-plugin-fio.min.js"></script>
// <script src="https://cdn.scattercdn.com/file/scatter-cdn/js/latest/scatterjs-plugin-lynx.min.js"></script>


let scatter = null;
let rpc = null;

const network_eos = ScatterJS.Network.fromJson({
	blockchain:'eos',
	chainId:'aca376f206b8fc25a6ed44dbdc66547c36c6c33e3a119ffbeaef943642f0e906',
	host:'nodes.get-scatter.com',
	port:443,
	protocol:'https'
});

/*
Change this.  This is using Marc's personal infura link.
Kept getting 401 error.
*/
const network_eth = ScatterJS.Network.fromJson({
	blockchain:'eth',
	chainId:'1',
    host:'mainnet.infura.io/v3/e6695759ae684d859557358cf70ac81e',
	port:443,
	protocol:'https'
});


function getScatterEos() {
    'use strict';
    return scatter.eos(network_eos, eosjs_api.Api, {rpc});
}

function getScatterETH() {
    'use strict';
    return scatter.eth(network_eth, Web3);
}

async function setScatterStatus() {
    'use strict';
    let txtScatterStatus = document.getElementById('txtScatterStatus');
    let lnkScatterStatus = document.getElementById('lnkScatterStatus');
    //if(!scatter) {
    if(scatter === null) {
        let connected = await ScatterJS.scatter.connect('treos.io', {network_eos});

        if (connected) {
            if(connected) {
                scatter = ScatterJS.scatter;
                window.ScatterJS = null;
            }
        }

        txtScatterStatus.innerText = 'Please install Scatter';
        lnkScatterStatus.setAttribute('data-action', 'install');
    } else if(!scatter.identity) {
        txtScatterStatus.innerText = 'Attach Account';
        lnkScatterStatus.setAttribute('data-action', 'login');
    } else {
        if (scatter.account('eos') !== undefined) {
            let account = scatter.account('eos');
            txtScatterStatus.innerText = account.name + '@' + account.authority;
        //}
        // else if (scatter.account('eth') !== undefined) {
        //     let account = scatter.account('eth');
        //     txtScatterStatus.innerText = account.address;
        } else {
            txtScatterStatus.innerText = 'No EOS Account';
        }
        lnkScatterStatus.setAttribute('data-action', 'logout');
    }
};

async function scatterLogout() {
    if (scatter !== null && scatter !== undefined && scatter.identity !== null) {
        await scatter.forgetIdentity();
    }
}

async function scatterTransferETH(amount, addressTo) {
    'use strict';
    let response = {
        success: false,
        errorMessage: null,
        error: null,
        apiResponse: null,
        transaction_id: null
    }
    try {
        let api = getScatterETH();
        const web3 = scatter.eth( network_eth, Web3 );

        const fromAccount = scatter.account('eth').address;
        console.log('fromAccount = ' + fromAccount);

        //let web3 = new Web3(Web3.givenProvider);    // Need Web3 to correctly convert ETH to Wei
        let amountWei = web3.utils.toWei(amount, 'ether');

        // https://github.com/GetScatter/scatter-js
        // Look for web3 example

        let result = await web3.eth.sendTransaction({
            //from: account.address,
            from: fromAccount,
            to: addressTo,
            value: amountWei
        });

        console.log(result);
    } catch (e) {
        console.error(e);
        response.error = e;
        response.success = false;
        response.errorMessage = 'Internal Error';
    }
    return response;
}

async function scatterTransferEOS(token, amount, accountNameTo, memo) {
    'use strict';
    let response = {
        success: false,
        errorMessage: null,
        error: null,
        apiResponse: null,
        transaction_id: null
    }

    const contractNames = {
        'EOS': 'eosio.token',
        'DICE': 'betdicetoken',
        'GIL': 'gilsertience',
        'KARMA': 'therealkarma',
        'MAIL': 'd.mail',
        'POOR': 'poormantoken',
        'SENSE': 'sensegenesis',
        'TRO': 'mytreosworld',
        'TRE': 'mytreosworld',
        'TREOS': 'mytreosworld'
    }

    let contractName;
    let fromAccount;
    let proceed = true;
    try {
        
        let api = getScatterEos();
        
        //console.log('api', api);
        //console.log('token', token);

        if (proceed) {
            contractName = contractNames[token.toUpperCase()];
            //console.log('contractName', contractName);

            if (contractName === undefined || contractName === null || contractName.length === 0) {
                response.success = false;
                response.errorMessage = 'Invalid Contract Name for token "' + token + '"';
                proceed = false;
            }
        }
        
        if (proceed) {
            //const fromAccount = scatter.account(token.toLowerCase());
            fromAccount = scatter.account('eos');
            //console.log('from', fromAccount);

            if (fromAccount === null || fromAccount === undefined) {
                response.success = false;
                response.errorMessage = "No Account Found";
                proceed = false;
            }
        }
        
        
        // if (proceed) {
        //     try {
        //         // Authenticate takes no parameters. 
        //         // It will fail if there is no identity bound to Scatter.
        //         const getRandom = () => Math.round(Math.random() * 8 + 1).toString();
        //         let randomString = '';
        //         for(let i = 0; i < 12; i++) randomString += getRandom();
        //         //console.log('randomString', randomString);
        //         alert('begin2');
        //         let sig = await scatter.authenticate(randomString);
        //         //console.log('sig', sig);
        //         alert('begin3');
        //     } catch(err) {
        //         alert(err);
        //         console.error(err);
        //         response.success = false;
        //         response.errorMessage = 'Identity Authentication Failure';
        //         proceed = false;
        //     }
        // }

        if (proceed) {
            try {
                let result = null;

                let transaction = {
                    actions: [{
                        account: contractName, // EOS = eosio.token, TRO = treosworld
                        //account: 'eosio.token', // EOS = eosio.token, TRO = treosworld
                        name: 'transfer',
                        authorization: [{
                            actor: fromAccount.name,
                            permission: fromAccount.authority,
                        }],
                        data: {
                            from: fromAccount.name,
                            to: accountNameTo,
                            quantity: parseFloat(amount).toFixed(4) + ' ' + token.toUpperCase(),
                            memo: memo,
                        },
                    }]
                }


                if (isUsingLynxWallet()) {
                    // result = await window.lynxMobile.transfer({ 
                    //     contract: contractName,
                    //     symbol: token.toUpperCase(), 
                    //     toAccount: accountNameTo,
                    //     amount: parseFloat(amount).toFixed(4),
                    //     memo: memo
                    // });
                    result = await lynxMobile.transact(transaction, {
                        blocksBehind: 3,
                        expireSeconds: 30,
                    });
                } else {
                    result = await api.transact(transaction, {
                        blocksBehind: 3,
                        expireSeconds: 30,
                    });
                }
    
                //console.log('result', result);
                response.apiResponse = result;
                
                if (result !== null && result.transaction_id !== null && result.transaction_id !== undefined) {
                    response.success = true;
                    response.errorMessage = null;
                    response.transaction_id = result.transaction_id;
                }
            } catch(err) {
                console.error(err);
                response.error = err;
                response.success = false;
    
                if (err.message.includes('no balance object found')) {
                    response.errorMessage = 'Insufficient Funds';
                } else if (err.message.includes('overdrawn balance')) {
                    response.errorMessage = 'Insufficient Funds';
                } else if (err.message.includes('User rejected the signature request')) {
                    response.errorMessage = 'User Cancelled';
                } else {
                    response.errorMessage = err.message;
                }
    
                //User rejected the signature request
            }
        } // proceed
    } catch (e) {
        console.error(e);
        response.error = e;
        response.success = false;
        response.errorMessage = 'Internal Error';
    }
    return response;
} // tryTransferEOS


async function scatterCreateAccount(accountNameTo, ownerPublicKey, activePublicKey, cpu, net, ram, tro, memo) {
    'use strict';
    let response = {
        success: false,
        errorMessage: null,
        error: null,
        apiResponse: null,
        transaction_id: null
    }

    const contractNames = {
        'EOS': 'eosio.token',
        'DICE': 'betdicetoken',
        'GIL': 'gilsertience',
        'KARMA': 'therealkarma',
        'MAIL': 'd.mail',
        'POOR': 'poormantoken',
        'SENSE': 'sensegenesis',
        'TRO': 'mytreosworld',
        'TRE': 'mytreosworld',
        'TREOS': 'mytreosworld'
    }

    //let contractName;
    let fromAccount;
    let proceed = true;
    try {
        
        let api = getScatterEos();
        
        // if (proceed) {
        //     contractName = contractNames[token.toUpperCase()];
        //     //console.log('contractName', contractName);

        //     if (contractName === undefined || contractName === null || contractName.length === 0) {
        //         response.success = false;
        //         response.errorMessage = 'Invalid Contract Name for token "' + token + '"';
        //         proceed = false;
        //     }
        // }
        
        if (proceed) {
            fromAccount = scatter.account('eos');

            if (fromAccount === null || fromAccount === undefined) {
                response.success = false;
                response.errorMessage = "No Account Found";
                proceed = false;
            }
        }
        
        
        if (proceed) {
            try {
                let result = null;

                // let transaction = {
                //     actions: [{
                //         account: contractName, // EOS = eosio.token, TRO = treosworld
                //         //account: 'eosio.token', // EOS = eosio.token, TRO = treosworld
                //         name: 'transfer',
                //         authorization: [{
                //             actor: fromAccount.name,
                //             permission: fromAccount.authority,
                //         }],
                //         data: {
                //             from: fromAccount.name,
                //             to: accountNameTo,
                //             quantity: parseFloat(amount).toFixed(4) + ' ' + token.toUpperCase(),
                //             memo: memo,
                //         },
                //     }]
                // }

                // https://github.com/EOSIO/eosjs/blob/master/docs/2.-Transaction-Examples.md
                let transaction = {
                    actions: [
                        {
                            account: 'eosio',
                            name: 'newaccount',
                            authorization: [{
                                actor: fromAccount.name, // 'useraaaaaaaa',
                                permission: fromAccount.authority, // 'active',
                            }],
                            data: {
                                creator: fromAccount.name, // 'useraaaaaaaa',
                                name: accountNameTo, // 'mynewaccount',
                                owner: {
                                    threshold: 1,
                                    keys: [{
                                        key: ownerPublicKey, // 'PUB_R1_6FPFZqw5ahYrR9jD96yDbbDNTdKtNqRbze6oTDLntrsANgQKZu',
                                        weight: 1
                                    }],
                                    accounts: [],
                                    waits: []
                                },
                                active: {
                                    threshold: 1,
                                    keys: [{
                                        key: activePublicKey, // 'PUB_R1_6FPFZqw5ahYrR9jD96yDbbDNTdKtNqRbze6oTDLntrsANgQKZu',
                                        weight: 1
                                    }],
                                    accounts: [],
                                    waits: []
                                },
                            },
                        },
                    {
                        account: 'eosio',
                        name: 'buyrambytes',
                        authorization: [{
                            actor: fromAccount.name, // 'useraaaaaaaa',
                            permission: fromAccount.authority, // 'active',
                        }],
                        data: {
                            payer: fromAccount.name, // 'useraaaaaaaa',
                            receiver: accountNameTo, // 'mynewaccount',
                            bytes: ram, // 8192,
                        },
                    },
                    {
                        account: 'eosio',
                        name: 'delegatebw',
                        authorization: [{
                            actor: fromAccount.name, // 'useraaaaaaaa',
                            permission: fromAccount.authority, // 'active',
                        }],
                        data: {
                            from: fromAccount.name, // 'useraaaaaaaa',
                            receiver: accountNameTo, // 'mynewaccount',
                            stake_net_quantity: net.toFixed(4) + ' EOS',
                            stake_cpu_quantity: cpu.toFixed(4) + ' EOS',
                            transfer: true,
                        }
                    },
                    {
                        account: 'mytreosworld', // EOS = eosio.token, TRO = mytreosworld
                        //account: 'eosio.token', // EOS = eosio.token, TRO = mytreosworld
                        name: 'transfer',
                        authorization: [{
                            actor: fromAccount.name,
                            permission: fromAccount.authority,
                        }],
                        data: {
                            from: fromAccount.name,
                            to: accountNameTo,
                            quantity: tro.toFixed(4) + ' TRO',
                            memo: memo,
                        },
                    }]
                };

                //console.log(transaction);

                showScatter_Wait_Modal();

                if (isUsingLynxWallet()) {
                    result = await lynxMobile.transact(transaction, {
                        blocksBehind: 3,
                        expireSeconds: 30,
                    });
                } else {
                    result = await api.transact(transaction, {
                        blocksBehind: 3,
                        expireSeconds: 30,
                    });
                }

                response.apiResponse = result;

                //saveObjectToFile(result, 'scatter_result.txt');
                
                if (result !== null && result.transaction_id !== null && result.transaction_id !== undefined) {
                    response.success = true;
                    response.errorMessage = null;
                    response.transaction_id = result.transaction_id;
                }
            } catch(err) {
                console.error(err);
                response.error = err;
                response.success = false;
    
                if (err.message.includes('no balance object found')) {
                    response.errorMessage = 'Insufficient Funds';
                } else if (err.message.includes('overdrawn balance')) {
                    response.errorMessage = 'Insufficient Funds';
                } else if (err.message.includes('User rejected the signature request')) {
                    response.errorMessage = 'User Cancelled';
                } else {
                    response.errorMessage = err.message;
                }
            } finally {
                hideScatter_Wait_Modal();
            }
    
        } // proceed
    } catch (e) {
        console.error(e);
        response.error = e;
        response.success = false;
        response.errorMessage = 'Internal Error';
    }
    return response;
} // scatterCreateAccount


async function injectScatterMenu() {
    'use strict';
    let $menu = $('ul.menu.main-menu')[0];

    if ($menu !== null && $menu !== undefined) {
        let scatterMenu = '<li class="menu-item icon collapsed" data-state="install">';
        scatterMenu += '<div class="line">';
        scatterMenu += '<a id="lnkScatterStatus" class="link" title="Scatter">';
        scatterMenu += '<span class="icon">';
        scatterMenu += '<img src="/images/scatter-logo.png" style="width: 25px; height: 25px;" />'
        scatterMenu += '</span>';
        scatterMenu += '<span id="txtScatterStatus" class="title">Please install Scatter</span>';
        scatterMenu += '</a>';
        scatterMenu += '</div>';
        scatterMenu += '</li>';

        let $listItems = $.parseHTML(scatterMenu);

        for (let i = $listItems.length -1; i >= 0; i--) {
            $menu.prepend($listItems[i]);
        }
        
    }
}

async function lnkScatterStatus_Click(event) {
    let lnkScatterStatus = document.getElementById('lnkScatterStatus');
    let action = lnkScatterStatus.getAttribute('data-action');

    event.preventDefault();

    if (action === 'install') {
        window.open('https://get-scatter.com', '_blank');
    } else if (action === 'login') {
        const requiredFields = { accounts:[network_eos] };
        //const requiredFields = { accounts:[network_eth] };
        let identity = await scatter.getIdentity( requiredFields );
    } else {
        // Default to logout
        await scatterLogout();
    }

    return false;
}

function isUsingLynxWallet() {
    'use strict';
    let result = false;

    switch (navigator.userAgent) {
        case 'EOSLynx IOS': { result = true; break };
        case 'EOSLynx Android': { result = true; break };
        case 'EOSLynx Desktop': { result = true; break };
    }

    return result;
}

function showScatter_Wait_Modal() {
    $('#Scatter_Wait_Modal').modal('show');
}

function hideScatter_Wait_Modal() {
    $('#Scatter_Wait_Modal').modal('hide');
}

function injectModals() {
    var modal_wait = `<div id="Scatter_Wait_Modal" class="modal fade" role="dialog" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content" style="border-radius: 7px;">
                <div class="modal-body">
                    <div class="bootbox-body">
                        <h1>Please Wait...</h1>
                        <p>
                            Scatter will appear shortly to confirm your transaction.
                        </p>
                        <button type="button" id="btnScatterModalHide" class="btn btn-primary responsive" data-dismiss="modal">Hide</button>
                    </div>
                </div>
            </div>
        </div>
    </div>`

    if ($('#Scatter_Wait_Modal').length == 0) {
        $('body').append($(modal_wait));
    }
}

async function page_Loaded() {
    'use strict';
    // let connected = await ScatterJS.scatter.connect('treos.io', {network_eos});

    // if (connected) {
    //     if(connected) {
    //         scatter = ScatterJS.scatter;
    //         window.ScatterJS = null;
    //     }
    // }

    injectModals();

    await injectScatterMenu();

    $('#lnkScatterStatus').on('click', lnkScatterStatus_Click);

    setScatterStatus();

    setInterval(function() {
        setScatterStatus();
    }, 3000);
}

// document.ready without inconsistent JQuery
//document.addEventListener("DOMContentLoaded", async function(){
$(document).ready(function () {
    // Handler when the DOM is fully loaded

    ScatterJS.plugins( new ScatterEOS(), new ScatterLynx({Api:eosjs_api.Api, JsonRpc:eosjs_jsonrpc.JsonRpc, textEncoder:() => {}}) );
    //ScatterJS.plugins( new ScatterEOS(), new ScatterETH(), new ScatterLynx({Api:eosjs_api.Api, JsonRpc:eosjs_jsonrpc.JsonRpc, textEncoder:() => {}}) );
    //ScatterJS.useRelay();
    rpc = new eosjs_jsonrpc.JsonRpc(network_eos.fullhost());

    page_Loaded();

    // will inject the lynxMobile SDK object 
    // into the page.
    // Your app will have access to the global lynxMobile 
    // Add an event listner on window like below to be notified
    // once the lynxMobile object has been added to window.
    window.addEventListener("lynxMobileLoaded", function() {
        // lynx is on the window and ready!
        //alert('lynxMobileLoaded loaded');
        window.lynxMobile.requestFullscreen(true);
    });


});



﻿var troConv = (function () {
    "use strict";
    var convTarget = null;
    var exponents = {};

    function buildConverter(successFn) {
        "use strict";
        var convDiv = null;
        var objConvDiv = null;
        var launchDiv = null;
        var objLaunchDiv = null;

        launchDiv = '<div id="conv-launch">' +
            '<button id="conv-launchButton" type="button">TRO Converter</button>' +
            '</div>';

        objLaunchDiv = $(launchDiv);
    
        convDiv = '<div id="conv-container" style="display: none;">' +
            '<form id="calc-form" method="post" action="#" onsubmit="$(\'#calc-btnApply\').trigger(\'click\');return false;">' +
                '<p style="text-align: center;">TRO Converter</p>' +
                '<div id="conv-inputs">' +
                    '<label id="calc-tro-label" for="calc-tro">TRO</label>' +
                    '<input id="calc-tro" type="number" step="0.0001" class="form-control valid untouched pristine" value="1">' +
                    '<br>' +
                    '<select id="conv-select-fiat" class="form-control valid untouched pristine"></select>' +
                    '<input id="calc-fiat" type="number" step="0.0001" class="form-control valid untouched pristine">' +
                    '<br>' +
                    '<select id="conv-select-crypto" class="form-control valid untouched pristine"></select>' +
                    '<input id="calc-crypto" type="number" step="0.00000001" class="form-control valid untouched pristine">' +
                '</div>' +
                '<div id="conv-buttons">';
                //'<button id="calc-btnApply" type="button" class="btn regular-main-button"><span>Apply</span></button>' +
        convDiv += '<button id="conv-btnCancel" type="button" class="btn regular-button main-button"><span>Close</span></button>' +
                '</div>' +
            '</form>' +
            '</div>';

        objConvDiv = $(convDiv);


        // Should use getRates for production and serve the rates from a local web service.
        getRates(function (data) {
            "use strict";
            // Retrieve stored setting from session cookie
            //var selectedFiat = sessionStorage.getItem("conv-select-fiat");
            var selectedFiat = localStorage.getItem("conv-select-fiat");
            var selectedCrypto = localStorage.getItem("conv-select-crypto");

            for (var currency in data.currencies) {
                let option = '<option value="' + data.currencies[currency].tro_rate + '">' + currency + '</option>';

                if (data.currencies[currency].isFiat === 1) {
                    $('#conv-select-fiat', objConvDiv).append(option);
                } else {
                    if (currency !== 'TRO' && currency !== 'TRE' && currency !== 'TREOS') {
                        $('#conv-select-crypto', objConvDiv).append(option);
                    }
                }
            }

            // Set the default selected option
            if (selectedFiat !== undefined && selectedFiat !== null) {
                $('#conv-select-fiat', objConvDiv).find('option:contains("' + selectedFiat + '")').prop('selected', true);
            } else {
                // Default to USD
                $('#conv-select-fiat', objConvDiv).find('option:contains("USD")').prop('selected', true);
            }

            if (selectedCrypto !== undefined && selectedCrypto !== null) {
                $('#conv-select-crypto', objConvDiv).find('option:contains("' + selectedCrypto + '")').prop('selected', true);
            } else {
                // Default to BTC
                $('#conv-select-crypto', objConvDiv).find('option:contains("BTC")').prop('selected', true);
            }

            $('#conv-btnCancel', objConvDiv).on('click', troConv.onCancelClick);
            $('#calc-btnApply', objConvDiv).on('click', troConv.onApplyClick);
            $('#calc-tro', objConvDiv).on('keyup', troConv.onTROkeyup);
            $('#calc-tro', objConvDiv).on('change', troConv.onTROchange);
            
            $('#calc-fiat', objConvDiv).on('keyup', troConv.onFIATkeyup);
            $('#calc-fiat', objConvDiv).on('change', troConv.onFIATchange);

            $('#calc-crypto', objConvDiv).on('keyup', troConv.onCryptoKeyup);
            $('#calc-crypto', objConvDiv).on('change', troConv.onCryptoChange);

            $('#conv-select-fiat', objConvDiv).on('change', troConv.onFIATchanged);
            $('#conv-select-crypto', objConvDiv).on('change', troConv.onCryptoChanged);

            //$('#conv-select-fiat', objConvDiv).trigger('change');
            $('#calc-tro', objConvDiv).trigger('keyup');

            successFn(objLaunchDiv, objConvDiv, data);
        });
    }

    function getRates(successFn, failFn) {
        "use strict";
        var request = $.ajax({
            url: '/api.php?target=withdrawal_requests&endpoint=' + encodeURIComponent('vendor/getPlatformCurrencies'),
            method: 'post',
            dataType: "json",
            contentType: "application/json; charset=utf-8"
        });

        request.done(function (data) {
            if (data !== undefined && data !== null) {
                if (data.result.code === 0) {
                    if (successFn !== undefined) {
                        successFn(data);
                    }
                }
            }
        });

        request.fail(function (jqXHR, textStatus) {
            if (failFn !== undefined) {
                failFn(jqXHR, textStatus);
            }
        });
    }


    function updateRates(successFn) {
        getRates(function (data) {
            "use strict";
            if (data.currencies !== undefined && data.currencies !== null) {
                for (let currency_code in data.currencies) {
                    exponents[currency_code] = data.currencies[currency_code].exponent;
                    let rate = data.currencies[currency_code].tro_rate;

                    if (data.currencies[currency_code].isFiat === 1) {
                        for (let i = 0; i < document.getElementById('conv-select-fiat').options.length; i++ ) {
                            if (document.getElementById('conv-select-fiat').options[i].text === currency_code) {
                                document.getElementById('conv-select-fiat').options[i].value = rate;
                                break;
                            }
                        }
                    } else {
                        for (let i = 0; i < document.getElementById('conv-select-crypto').options.length; i++ ) {
                            if (document.getElementById('conv-select-crypto').options[i].text === currency_code) {
                                document.getElementById('conv-select-crypto').options[i].value = rate;
                                break;
                            }
                        }
                    }
                }
                //$('#calc-fiat').trigger('keyup');
                if (successFn !== undefined) {
                    successFn(data.currencies);
                }
            }
        });
    }

    function hide(event) {
        "use strict";
        $('#conv-container').hide();
    }

    function show(event) {
        updateRates(function(currencies) {
            $('#calc-tro').trigger('keyup');

            $('#conv-container').show();

            $('#calc-fiat').select();
        });
    }

    function updateFiat() {
        "use strict";
        var value = $('#calc-tro').val();
        var rate = parseFloat($('#conv-select-fiat').val());
        var symbol = $('#conv-select-fiat option:selected').text();
        var fiatValue = 0;

        if (!isNaN(parseFloat(value)) && $.isNumeric(value)) {
            fiatValue = parseFloat(value) * rate;
            $('#calc-fiat').val(fiatValue.toFixed(exponents[symbol]));
            $('#calc-fiat').removeClass('nan');
            $('#calc-tro').removeClass('nan');
            $('#calc-btnApply').prop('disabled', false);
        }
        else {
            $('#calc-fiat').val('');
            $('#calc-tro').addClass('nan');
            $('#calc-btnApply').prop('disabled', true);
        }
    }

    function updateTRO(value, rate) {
        "use strict";
        var troValue = 0;

        if (!isNaN(parseFloat(value)) && $.isNumeric(value)) {
            troValue = parseFloat(value) / rate;
            $('#calc-tro').val(troValue.toFixed(4));
            $('#calc-fiat').removeClass('nan');
            $('#calc-tro').removeClass('nan');
        }
        else {
            $('#calc-tro').val('');
            $('#calc-fiat').addClass('nan');
        }
    }

    function updateCrypto() {
        "use strict";
        
        var value = $('#calc-tro').val();
        var rate = parseFloat($('#conv-select-crypto').val());// Fiat to crypt rate
        var cryptoValue = 0;
        var exponent = 8;

        var selectedCrypto = localStorage.getItem("conv-select-crypto");

        if (exponents[selectedCrypto] !== undefined) {
            exponent = exponents[selectedCrypto];
        }

        if (!isNaN(parseFloat(value)) && $.isNumeric(value)) {
            cryptoValue = parseFloat(value) * rate;
            $('#calc-crypto').val(cryptoValue.toFixed(exponent));
            $('#calc-fiat').removeClass('nan');
            $('#calc-tro').removeClass('nan');
            $('#calc-crypto').removeClass('nan');
        }
        else {
            $('#calc-tro').val('');
            $('#calc-crypto').val('');
            $('#calc-fiat').addClass('nan');
        }
    }

    function onFIATchanged(event) {
        "use strict";
        var selectedText = $("#conv-select-fiat option:selected").text();

        // Store the currency temporarily so that the selection is consistent between pages.
        if (selectedText.length > 0) {
            $('#calc-fiat-label').text(selectedText);
            localStorage.setItem("conv-select-fiat", selectedText);
            $('#calc-tro').trigger('keyup');
        }
    }


    function onCryptoChanged(event) {
        "use strict";
        var selectedText = $("#conv-select-crypto option:selected").text();

        // Store the currency temporarily so that the selection is consistent between pages.
        if (selectedText.length > 0) {
            localStorage.setItem("conv-select-crypto", selectedText);
            $('#calc-tro').trigger('keyup');
        }
    }

    function onFIATkeyup(event) {
        "use strict";
        if (event.keyCode === 27) {    // Escape
            $('#conv-btnCancel').trigger('click');
        } else {
            updateTRO($('#calc-fiat').val(), parseFloat($('#conv-select-fiat').val()));
            updateCrypto();
        }
    }

    function onFIATchange(event) {
        "use strict";
        updateTRO($('#calc-fiat').val(), parseFloat($('#conv-select-fiat').val()));
        updateCrypto();
    }

    function onTROkeyup(event) {
        "use strict";
        if (event.keyCode === 27) {    // Escape
            $('#conv-btnCancel').trigger('click');
        } else {
            updateFiat();
            updateCrypto();
        }
    }

    function onTROchange(event) {
        "use strict";
        updateFiat();
        updateCrypto();
    }



    function onCryptoKeyup(event) {
        "use strict";
        if (event.keyCode === 27) {    // Escape
            $('#conv-btnCancel').trigger('click');
        } else {
            updateTRO($('#calc-crypto').val(), parseFloat($('#conv-select-crypto').val()));
            updateFiat();
        }
    }

    function onCryptoChange(event) {
        "use strict";
        updateTRO($('#calc-crypto').val(), parseFloat($('#conv-select-crypto').val()));
        updateFiat();
    }






    function onCancelClick(event) {
        "use strict";
        hide();
    }

    function onApplyClick(event) {
        "use strict";
        var troValue = $('#calc-tro').val();

        if (!$('#calc-tro').hasClass('nan')) {
            var troOutput = Math.round(parseFloat(troValue) * 100) / 100;
            $(convTarget).val(troOutput.toFixed(2));
            convTarget = null;
            hide();
        }
    }

    return {
        buildConverter: buildConverter,
        hide: hide,
        show: show,
        updateRates: updateRates,
        onFIATchanged: onFIATchanged,
        onFIATkeyup: onFIATkeyup,
        onFIATchange: onFIATchange,
        onTROkeyup: onTROkeyup,
        onTROchange: onTROchange,
        onCryptoKeyup: onCryptoKeyup,
        onCryptoChange: onCryptoChange,
        onCryptoChanged: onCryptoChanged,
        onCancelClick: onCancelClick,
        onApplyClick: onApplyClick
    };
})();

/* ------------- END OF MODULE ------------- */

// $(document).mouseup(function (e) {
//     "use strict";
//     var container = $('#conv-container');

//     if ($(container).length > 0 && $(container).is(":visible")) {
//         // if the target of the click isn't the container nor a descendant of the container
//         if (!$('.usetroConv').is(e.target) && !container.is(e.target) && container.has(e.target).length === 0) {
//             troConv.hide();
//         }
//     }
// });

$(window).resize(function () {
    "use strict";
    var container = $('#conv-container');

    if ($(container).length > 0 && $(container).is(":visible")) {
        troConv.hide();
    }
});

$(document).ready(function () {
    "use strict";
    // Add CSS to head
    if ($('#conv-stylesheet').length === 0) {
        let linkElement = document.createElement('link');
        linkElement.setAttribute('id', 'conv-stylesheet');
        linkElement.setAttribute('rel', 'stylesheet');
        linkElement.setAttribute('type', 'text/css');
        linkElement.setAttribute('href', '/api.php?target=withdrawal_requests&endpoint=' + encodeURIComponent('css/tro_converter.css?t=20191208095400'));
        document.getElementsByTagName('head')[0].appendChild(linkElement);

        //console.log(linkElement);
    }

    if ($('#conv-container').length === 0) {
        troConv.buildConverter(function (launchDiv, calcDiv, data) {
            "use strict";
            $('body').append($(launchDiv));
            $('body').append($(calcDiv));

            //$('.usetroConv').on('focus', troConv.show);
            $('#conv-launchButton').on('click', troConv.show);
        });

        // Update the rates every 10 minutes
        setInterval(function() {
            troConv.updateRates();
        }, (10 * 60000) );
    }
});







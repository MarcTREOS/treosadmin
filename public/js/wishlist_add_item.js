var vendorProfile = null;
var fiatCurrencies = {};
var shippingAddresses = [];

function loadCurrencies(updateList) {
    'use strict';
    $.ajax({
        url: '/api/vendor/getFiatCurrencies',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify( { } )
    })
    .done(function (data) {
        console.log('data: ', data);
        if (data.result.code === ResponseCodeEnum.SUCCESS) {
            if (data.currencies !== null && data.currencies !== undefined) {
                fiatCurrencies = data.currencies;
                if (updateList !== null && updateList !== undefined && updateList) {
                    let selectIndex = 0;
                    $('#ddlPriceCurrency').empty();
                    for (let i = 0; i < Object.keys(fiatCurrencies).length; i++) {
                        let key = Object.keys(fiatCurrencies)[i];
                        if (key === 'USD') {
                            selectIndex = i;
                        }
                        $('#ddlPriceCurrency').append('<option value="' + fiatCurrencies[key].code + '">' + fiatCurrencies[key].code + ' - ' + fiatCurrencies[key].name + '</option>');
                    }
                    $('#ddlPriceCurrency').prop('selectedIndex', selectIndex);
                    $('#ddlPriceCurrency').trigger('change');   // Fire, just in case
                }
            } else {
                showErrorMessage('No currencies returned');
            }
        } else {
            showErrorMessage(data.result.message);
        }
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
        showErrorMessage('fail - ' + textStatus);
    });
}

function loadShippingAddress() {
    'use strict';
    $.ajax({
        url: '/api/vendor/getAddresses',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify( { isBilling: true, isShipping: false, isWork: true } )
    })
    .done(function (data) {
        if (data.result.code === ResponseCodeEnum.SUCCESS) {
            if (data.addresses !== null) {
                if (data.addresses.length > 0) {
                    shippingAddresses = data.addresses;
                    for (let i = 0; i < data.addresses.length; i++) {
                        let address = data.addresses[i];
                        let selected = '';

                        if (i === 0) {
                            selected = ' checked="checked"';
                        }

                        let obj = '<div class="item">';
                        obj += '<label class="box-address">';
                        obj += '<input type="radio" name="addressSelected" value="' + address.address_id + '"' + selected + ' />';
                        obj += '<div>';
                        obj += '<ul>';

                        if (address.firstname !== null && address.lastname !== null) {
                            obj +=  '<li>' + cellText(address.firstname + ' ' + address.lastname) + '</li>';
                        } else if (address.firstname !== null) {
                            obj +=  '<li>' + cellText(address.firstname) + '</li>';
                        } else if (address.lastname !== null) {
                            obj +=  '<li>' + cellText(address.lastname) + '</li>';
                        } else {
                            obj +=  '<li>&nbsp;</li>';
                        }

                        obj +=  '<li>' + cellText(address.street) + '</li>';
                        obj +=  '<li>' + cellText(address.city) + '</li>';
                        obj +=  '<li>' + cellText(address.custom_state) + '</li>';
                        obj +=  '<li>' + cellText(address.zipcode) + '</li>';
                        obj +=  '<li>' + cellText(address.country) + '</li>';
                        obj +=  '<li>' + cellText(address.phone) + '</li>';

                        obj +=  '<li></li>';
                        obj += '</ul>';
                        obj += '</div>';
                        obj += '</label>';
                        obj += '</div>'; // item

                        //$('#addresses').append($(obj));
                        $('#carousel-shipping-addresses .carousel-inner').append($(obj));
                        $('<li data-target="#carousel-shipping-addresses" data-slide-to="'+i+'"></li>').appendTo('#carousel-shipping-addresses .carousel-indicators')
                    }

                    $('#carousel-shipping-addresses .carousel-inner > .item').first().addClass('active');
                    $('#carousel-shipping-addresses .carousel-indicators > li').first().addClass('active');

                    $('#carousel-shipping-addresses').carousel();
                }
                else {
                    
                }
                // data.users.forEach(function(item) {
                //     option = $('<option value="' + item + '">' + item + '</option>');
                //     $('#ddlUserID').append(option);
                // });
            } else {
                showErrorMessage('No addresses returned');
            }
        } else {
            showErrorMessage(data.result.message);
        }
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
        showErrorMessage('fail - ' + errorThrown);
    });
}


function loadVendorProfile() {
    'use strict';
    $.ajax({
        url: '/api/vendor/getVendorProfile',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify( { } )
    })
    .done(function (data) {
        'use strict';
        if (data.result.code === ResponseCodeEnum.SUCCESS) {
            if (data.profile !== null && data.profile !== undefined) {
                vendorProfile = data.profile;

                let troSpendableBalance = vendorProfile.balances['TRO'] || 0.0;
                let troUBIBalance = vendorProfile.balances['TRO_UBI'] || 0.0;

                $('#txtTROSpendableBalance').text(formatTokenBalance('TRO', troSpendableBalance));
                $('#txtTROUBIBalance').text(formatTokenBalance('TRO_UBI', troUBIBalance));

                $('#txtDigitalEmail').val(vendorProfile.login);

                let listingFeePercentage = vendorProfile.settings['listing_fee_wishlist'];
                if (isNullOrEmpty(listingFeePercentage)) {
                    listingFeePercentage = 0.02;
                }
                $('#txtItemListingFeePercentage').val(roundDown(listingFeePercentage * 100, 0) + '%');
            } else {
                showErrorMessage('No addresses returned');
            }
        } else {
            showErrorMessage(data.result.message);
        }
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
        'use strict';
        showErrorMessage('fail - ' + textStatus);
    });
}

function calculateBonus() {
    'use strict';
    var currencyCode = $("#ddlPriceCurrency").val();
    var itemPrice = parseFloat($("#txtItemPrice").val());
    var shippingPrice = parseFloat($("#txtItemShipping").val());
    var bonusPercentage = $("#ddlBonusPercentage").val();
    var bonus = (itemPrice + shippingPrice) * (bonusPercentage / 100.0);
    if (!isNaN(bonus)) {
        $("#txtItemBonus").val(formatCurrency(currencyCode, bonus));
    } else {
        bonus = 0;
        $("#txtItemBonus").val('');
    }
    return bonus;
}

function calculateListingFee() {
    'use strict';
    var currencyCode = $("#ddlPriceCurrency").val();
    var itemPrice = parseFloat($("#txtItemPrice").val());
    var shippingPrice = parseFloat($("#txtItemShipping").val());
    var listingFeePercentage = 2; // 2%
    var listingFee = (itemPrice + shippingPrice) * (listingFeePercentage / 100.0);
    if (!isNaN(listingFee)) {
        $("#txtItemListingFee").val(formatCurrency(currencyCode, listingFee));
    } else {
        listingFee = 0;
        $("#txtItemListingFee").val('');
    }
    return listingFee;
}

function formatCurrency(currencyCode, currencyValue) {
    'use strict';
    var result = 0.0;
    var currency = undefined;

    switch (currencyCode) {
        case 'TRO': {
            currency = {
                code: 'TRO',
                exponent: 4
            }
            break;
        }
        default: {
            currency = fiatCurrencies[currencyCode];    // Now using Dictionary
            break;
        }
    }

    if (currency !== undefined) {
        var exponent = currency.exponent;   // no of decimal places

        result = roundDown(currencyValue, exponent);

        //result = currencyValue * Math.pow(10, exponent);
        //result = Math.floor(result);
        //result = result / Math.pow(10, exponent);
        result = result.toFixed(exponent);
    } else {
        console.log('Did not find currency ' + currencyCode + ' in fiatCurrencies.');
    }

    return result;
}

function calculateTRO(currencyCode, currencyValue) {
    'use strict';
    var result = 0.0;
    var currency = null;

    currency = fiatCurrencies[currencyCode];

    if (currency !== undefined) {
        var tro_rate = currency.tro_rate;
        result = currencyValue * (1.0/tro_rate);
        result = roundUp(result, 4);
    } else {
        console.log('Did not find currency ' + currencyCode + ' in fiatCurrencies.');
    }

    return result;
}

function calculateTotal() {
    'use strict';
    let currencyCode = $("#ddlPriceCurrency").val();
    let itemPrice = parseFloat($("#txtItemPrice").val());
    let shippingPrice = parseFloat($("#txtItemShipping").val());
    let bonusPercent = parseInt($('#ddlBonusPercentage').val());
    let listingFeePercent = 2.0;
    if (!isNullOrEmpty(vendorProfile)) {
        console.log(vendorProfile.settings);
        listingFeePercent = vendorProfile.settings['listing_fee_wishlist'];
        if (!isNullOrEmpty(listingFeePercent)) {
            listingFeePercent = roundDown(parseFloat(listingFeePercent) * 100.0, 0);
        } else {
            listingFeePercent = 2.0;
        }
    }
    
    let currency = fiatCurrencies[currencyCode];
    
    //var bonus = parseFloat($("#txtItemBonus").val());
    //var listingFee = parseFloat($("#txtItemListingFee").val());
    let totalPercentage = 100.0 + bonusPercent + listingFeePercent;
    let totalFiat = 0.0;
    let totalTRO = 0.0;
    let totalFees = 0.0;
    let listingFee = 0.0;
    let bonus = 0.0;
    let tro100Percent = 0.0;
    
    if (isNaN(itemPrice)) { itemPrice = 0.0; }
    if (isNaN(shippingPrice)) { shippingPrice = 0.0; }
    if (isNaN(bonusPercent)) { bonusPercent = 0.0; }
    if (isNaN(listingFeePercent)) { listingFeePercent = 0.0; }

    totalFiat = (itemPrice + shippingPrice) * (totalPercentage / 100);

    totalFiat = roundUp(totalFiat, currency.exponent);

    totalTRO = calculateTRO(currencyCode, totalFiat);

    tro100Percent = totalTRO / (totalPercentage / 100);
    tro100Percent = roundUp(tro100Percent, 4);
    
    totalFees = totalTRO - tro100Percent;
    totalFees = roundDown(totalFees, 4);    // roundDown to avoid float errors
    
    listingFee = tro100Percent * (listingFeePercent / 100);
    listingFee = roundUp(listingFee, 4);    // Round up to make sure we always get our listing fee
    
    bonus = totalFees - listingFee;
    bonus = roundDown(bonus, 4);    // roundDown to avoid float errors

    if (!isNaN(tro100Percent)) {
        $('#txtItemPlusShipping').val(formatCurrency('TRO', tro100Percent));
    } else {
        $('#txtItemPlusShipping').val('');
    }

    if (!isNaN(totalTRO)) {
        $('#txtItemTRO').val(formatCurrency('TRO', totalTRO));
    } else {
        $('#txtItemTRO').val('');
    }

    if (!isNaN(bonus)) {
        $('#txtItemBonus').val(formatCurrency('TRO', bonus));
    } else {
        $('#txtItemBonus').val('');
    }

    if (!isNaN(listingFee)) {
        $('#txtItemListingFee').val(formatCurrency('TRO', listingFee));
    } else {
        $('#txtItemListingFee').val('');
    }


    // total = itemPrice + shippingPrice + bonus + listingFee;

    // $("#txtItemFinalPrice").val(formatCurrency(currencyCode, total));

    // var tro_value = calculateTRO(currencyCode, total);

    
}


function ddlPriceCurrency_Change(event) {
    'use strict';
    let currencyCode = $("#ddlPriceCurrency").val();
    $('.input-group-addon.currency').html(currencyCode);

    calculateTotal();
}

function txtItemPrice_Keyup(event) {
    'use strict';
    calculateTotal();

    //calculateBonus();
    //calculateListingFee();
}

function txtItemShipping_Keyup(event) {
    'use strict';
    calculateTotal();
    //calculateBonus();
    //calculateListingFee();
    //calculateTotal();
}

function ddlBonusPercentage_Change(event) {
    'use strict';
    calculateTotal();
    //calculateBonus();
    //calculateListingFee();
    //calculateTotal();
}

function btnProcessURL_Click(event) {
    'use strict';
    event.target.blur();

    document.querySelector('#btnFileSelector').value = '';  // Reset the file upload button

    let proceed = true;
    let txtItemURL = $('#txtItemURL').val();

    clearNotifyMessages();

    if (proceed) {
        $('#product-info-form').addClass('hidden');
        $('#txtItemTitle').val('');
        $('#ddlItemTitle').empty();
        $('#txtItemPrice').val('0').trigger('change');
        $('#carousel-product-images .carousel-inner .item').remove();
        $('#carousel-product-images .carousel-indicators li').remove();

        if (txtItemURL === null || txtItemURL === undefined || txtItemURL.trim().length === 0) {
            proceed = false;
            showErrorMessage('Invalid URL');
            $('#txtItemURL').closest('.form-group').addClass('has-error');
        }
    }

    if (proceed) {

        $.ajax({
            url: '/api/vendor/webScrapeProductInfo',
            method: 'post',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify( { url: txtItemURL } ),
            beforeSend: function() {
                $('#product-images-spinner').removeClass('hidden');
            }
        })
        .done(function (data) {
            if (data.result.code === ResponseCodeEnum.SUCCESS) {
                $('#rbtnImageSourceURL').prop('checked', true);
                //console.log(data);
                for (let i = 0; i < data.scrapeResult.imageURLs.length; i++) {
                    let imageURL = data.scrapeResult.imageURLs[i];
                    console.log(imageURL);

                    let selected = '';

                    if (i === 0) {
                        selected = ' checked="checked"';
                    }

                    let obj = '<div class="item">';
                    obj += '<label class="box-product-image">';
                    obj += '<input type="radio" name="productImageSelected" value="' + encodeURI(imageURL) + '"' + selected + ' />';
                    obj += '<img src="' + encodeURI(imageURL) + '" >';
                    obj += '</label>';
                    obj += '</div>'; // item

                    //$('#addresses').append($(obj));
                    $('#carousel-product-images .carousel-inner').append($(obj));
                    $('<li data-target="#carousel-product-images" data-slide-to="'+i+'"></li>').appendTo('#carousel-product-images .carousel-indicators')
                }

                $('#carousel-product-images .carousel-inner > .item').first().addClass('active');
                $('#carousel-product-images .carousel-indicators > li').first().addClass('active');

                $('#carousel-product-images').carousel();

                for (let i = 0; i < data.scrapeResult.headings.length; i++) {
                    $('#ddlItemTitle').append('<option value="' + data.scrapeResult.headings[i] + '">' + data.scrapeResult.headings[i] + '</option>');
                }

                if (data.scrapeResult.prices.length > 0) {
                    document.getElementById('txtItemPrice').value = data.scrapeResult.prices[0];
                    $('#txtItemPrice').trigger('change');
                }

                $('#ddlItemTitle').trigger('click').trigger('change');

                $('#product-info-form').removeClass('hidden');
            } else {
                switch (data.result.code) {
                    case ResponseCodeEnum.INVALID_URL: {
                        showErrorMessage('There was a problem with the URL.<br/>' +
                            data.result.message +
                            '<br/><br/>' +
                            'Please check the URL is correct and try gain.');
                        $('#txtItemURL').closest('.form-group').addClass('has-error');
                        break;
                    }
                    case ResponseCodeEnum.TIMEOUT_ERROR: {
                        showErrorMessage('Timed out while processing the URL.<br/>' +
                            data.result.message);
                        $('#txtItemURL').closest('.form-group').addClass('has-error');
                        break;
                    }
                    default: {
                        showErrorMessage(data.result.message);
                        break;
                    }
                }
                $('#main')[0].scrollIntoView(); // Scroll to top of page
            }
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            showErrorMessage('fail - ' + errorThrown);
        })
        .always(function () {
            $('#product-images-spinner').addClass('hidden');
        });
    }
    
}

function lnkTermsAndConditions_Click(event) {
    'use strict';
    event.preventDefault();
    $('#agreement-modal').modal('show');
    return false;
}

function btnAgreeTermsAndConditions_Click(event) {
    'use strict';
    event.target.blur();
    event.preventDefault();
    $('#cbAcceptTerms').prop('checked', true);
    $('#agreement-modal').modal('hide');
    return false;
}

function btnCloseTermsAndConditions_Click(event) {
    'use strict';
    event.target.blur();
    event.preventDefault();
    $('#agreement-modal').modal('hide');
    return false;
}

function CarouselShippingAddresses_Slid(event) {
    'use strict';
    let carousel = event.target;
    $('.carousel-inner .active input', carousel).trigger('click');
}

function CarouselProductImages_Slid(event) {
    'use strict';
    let carousel = event.target;
    $('.carousel-inner .active input', carousel).trigger('click');
}

function deliveryOptions_Change(event) {
    'use strict';
    switch (event.target.value) {
        case 'standard': {
            $('#shipping-standard-section').removeClass('hidden');
            $('#shipping-digital-section').addClass('hidden');
            $('#shipping-collection-section').addClass('hidden');
            $('#shipping-exchange-section').addClass('hidden');
            break;
        }
        case 'digital': {
            $('#shipping-standard-section').addClass('hidden');
            $('#shipping-digital-section').removeClass('hidden');
            $('#shipping-collection-section').addClass('hidden');
            $('#shipping-exchange-section').addClass('hidden');
            break;
        }
        case 'collection': {
            $('#shipping-standard-section').addClass('hidden');
            $('#shipping-digital-section').addClass('hidden');
            $('#shipping-collection-section').removeClass('hidden');
            $('#shipping-exchange-section').addClass('hidden');
            break;
        }
        case 'exchange': {
            $('#shipping-standard-section').addClass('hidden');
            $('#shipping-digital-section').addClass('hidden');
            $('#shipping-collection-section').addClass('hidden');
            $('#shipping-exchange-section').removeClass('hidden');
            break;
        }
    }
}

function fundingSource_Change(event) {
    'use strict';
    switch ($(this).val()) {
        case 'TRO':{
            $('.funding-info.tro-spendable').removeClass('hidden');
            $('.funding-info.tro-ubi').addClass('hidden');
            break;
        }
        case 'TRO_UBI' : {
            $('.funding-info.tro-spendable').addClass('hidden');
            $('.funding-info.tro-ubi').removeClass('hidden');
            break;
        }
    }
}

function ddlItemTitle_Change(event) {
    console.log(event);
    //document.getElementById('txtItemTitle').value = event.target.options[event.target.selectedIndex].value;
    if (event.target.options.length > 0) {
        document.getElementById('txtItemTitle').value = event.target.selectedOptions[0].value.trim();
    }
}


async function frmAddWishlistItem_Submit(event) {
    'use strict';
    try {
        event.preventDefault();

        clearNotifyMessages();

        //let validateResponse = frmAddWishlistItem_Validate();

        //if (validateResponse.success) {
            await frmAddWishlistItem_SubmitForm();
        //}
        //else {
        //    if (validateResponse.errorMessages.length > 0) {
        //        showErrorMessages(validateResponse.errorMessages);
        //        $('#main')[0].scrollIntoView(); // Scroll to top of page
        //    }
        //}
    } catch (err) {
        console.log(err);
    }
    
    return false;
}


async function frmAddWishlistItem_SubmitForm() {
    'use strict';
    let proceed = true;
    let formData = new FormData();

    let deliveryMethod = null;
    let shippingAddressId = null;
    let digitalEmailAddress = null;
    let collectionLocation = null;
    let exchangeDetails = null;
    let itemWebsiteURL = null;
    let imageSource = null;
    let itemImageURL = null;
    let itemImageUploaded = null;
    let itemTitle = null;
    let fiatCurrencyCode = null;
    let fiatItemPrice = null;
    let fiatShipping = null;
    let bonusPercentage = null;
    let totalTRO = null;
    let fundingWallet = null;
    let deliveryComment = null;
    let acceptTerms = null;

    //    formData.append('wishlistImageFile', file);

    if (proceed) {
        deliveryMethod = $('input[type=radio][name=deliveryOptions]:checked').val();
        formData.append('deliveryMethod', deliveryMethod);

        switch (deliveryMethod) {
            case 'standard' : {
                shippingAddressId = $('input[type=radio][name=addressSelected]:checked').val();
                formData.append('shippingAddressId', shippingAddressId);
                break;
            }
            case 'digital' : {
                digitalEmailAddress = document.getElementById('txtDigitalEmail').value.trim();
                formData.append('digitalEmailAddress', digitalEmailAddress);
                break;
            }
            case 'collection' : {
                collectionLocation = document.getElementById('txtCollectionLocation').value.trim();
                formData.append('collectionLocation', collectionLocation);
                break;
            }
            case 'exchange' : {
                exchangeDetails = document.getElementById('txtExchangeDetails').value.trim();
                formData.append('exchangeDetails', exchangeDetails);
                break;
            }
            default: {
                proceed = false;
                showErrorMessage('Unknown Delivery Method "' + deliveryMethod + '"');
                $('#main')[0].scrollIntoView(); // Scroll to top of page
                break;
            }
        }
    }

    if (proceed) {
        itemWebsiteURL = document.getElementById('txtItemURL').value.trim();
        formData.append('itemWebsiteURL', itemWebsiteURL);

        imageSource = $('input[type=radio][name=imageSource]:checked').val();
        //formData.append('imageSource', imageSource);

        if (imageSource === 'file') {
            if (document.getElementById('btnFileSelector').files.length > 0) {
                itemImageUploaded = document.getElementById('btnFileSelector').files[0];
                formData.append('itemImageUploaded', itemImageUploaded);
            }
        } else {
            // Assume URL
            //itemImageURL = $('input[type=radio][name=productImageSelected]:checked').val();
            // if (isNullOrEmpty(itemImageURL)) {
            //     itemImageURL = null;
            // } else {
            //     formData.append('itemImageURL', itemImageURL);
            // }

            let itemImage = $('input[type=radio][name=productImageSelected]:checked').val();
            let blob = await srcToFileObject(itemImage, 'unknown.jpg');
            formData.append('itemImageUploaded', blob);
        }
        itemTitle = document.getElementById('txtItemTitle').value.trim();
        formData.append('itemTitle', itemTitle);

        fiatCurrencyCode = $("#ddlPriceCurrency").val();
        formData.append('fiatCurrencyCode', fiatCurrencyCode);

        fiatItemPrice = parseFloat(document.getElementById('txtItemPrice').value);
        formData.append('fiatItemPrice', fiatItemPrice);

        fiatShipping = parseFloat(document.getElementById('txtItemShipping').value);
        formData.append('fiatShipping', fiatShipping);

        bonusPercentage = parseInt($('#ddlBonusPercentage').val());
        formData.append('bonusPercentage', bonusPercentage);

        totalTRO = parseFloat(document.getElementById('txtItemTRO').value);
        formData.append('totalTRO', totalTRO);

        fundingWallet = $('input[type=radio][name=fundingSource]:checked').val();
        formData.append('fundingWallet', fundingWallet);

        deliveryComment = document.getElementById('txtDeliveryComment').value.trim();
        formData.append('deliveryComment', deliveryComment);

        if ($('#cbAcceptTerms').prop('checked')) {
            acceptTerms = $('#cbAcceptTerms').val();
        } else {
            acceptTerms = 'disagree';
        }
        formData.append('acceptTerms', acceptTerms);
        
    }

    if (proceed) {

        $.ajax({
            url: '/api/vendor/addWishlistItem',
            method: 'post',
            dataType: 'json',
            processData: false,
            contentType: false,
            cache: false,
            timeout: 0,
            data: formData,
            beforeSend: function() {
                $('#product-images-spinner').removeClass('hidden');
            }
        })
        .done(function (data) {
            if (data.result.code === ResponseCodeEnum.SUCCESS) {
                alert('success');
            } else {
                switch (data.result.code) {
                    case ResponseCodeEnum.INVALID_URL: {
                        showErrorMessage('There was a problem with the URL.<br/>' +
                            data.result.message +
                            '<br/><br/>' +
                            'Please check the URL is correct and try gain.');
                        $('#txtItemURL').closest('.form-group').addClass('has-error');
                        break;
                    }
                    case ResponseCodeEnum.TIMEOUT_ERROR: {
                        showErrorMessage('Timed out while processing the URL.<br/>' +
                            data.result.message);
                        $('#txtItemURL').closest('.form-group').addClass('has-error');
                        break;
                    }
                    case ResponseCodeEnum.FORM_FIELD_REQUIRED: {
                        showErrorMessage('<strong>Field Required</strong><p>' + data.result.message + '</p>');
                        break;
                    }
                    case ResponseCodeEnum.FORM_FIELD_INVALID: {
                        showErrorMessage('<strong>Field Invalid</strong><p>' + data.result.message + '</p>');
                        break;
                    }
                    default: {
                        showErrorMessage(data.result.message);
                        break;
                    }
                }
                $('#main')[0].scrollIntoView(); // Scroll to top of page
            }
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            showErrorMessage('fail - ' + errorThrown);
        })
        .always(function () {
            $('#product-images-spinner').addClass('hidden');
        });
    }
}


function btnChooseImage_Click(event) {
    event.target.blur();
    document.querySelector('#btnFileSelector').value = '';  // Reset the files
    $('#btnFileSelector').trigger('click');
}

function btnFileSelector_Change(event) {
    // https://usefulangle.com/post/67/pure-javascript-ajax-file-upload-showing-progess-percent
    // Allowed types
	var mime_types = [ 'image/jpg', 'image/jpeg', 'image/png', 'image/gif' ];
	
    if (event.target.files.length > 0) {
        $('#rbtnImageSourceFile').prop('checked', true);

        let file = event.target.files[0];
        let filename = file.name;
        let filesize = file.size;
        let filetype = file.type;   // image/gif
        
        // Validate MIME type
        if(mime_types.indexOf(file.type) == -1) {
            alert('Error : Incorrect file type');
        } else if(file.size > 2*1024*1024) {   // Max 2 Mb allowed
            alert('Error : Exceeded size 2MB');
        } else {
            // Validation is successful

            var reader = new FileReader();
            reader.onload = function(event) {
                //var $img = $('<img>');
                //$img.attr('src', event.target.result);

                // Clear images from carousel
                $('#carousel-product-images .carousel-inner .item').remove();
                $('#carousel-product-images .carousel-indicators li').remove();




                let obj = '<div class="item">';
                    obj += '<label class="box-product-image">';
                    //obj += '<input type="radio" name="productImageSelected" value="' + encodeURI(imageURL) + '"' + selected + ' />';
                    obj += '<img src="' + encodeURI(event.target.result) + '" >';
                    obj += '</label>';
                    obj += '</div>'; // item

                $('#carousel-product-images .carousel-inner').append($(obj));
                $('<li data-target="#carousel-product-images" data-slide-to="' + '0' + '"></li>').appendTo('#carousel-product-images .carousel-indicators')

                $('#carousel-product-images .carousel-inner > .item').first().addClass('active');
                $('#carousel-product-images .carousel-indicators > li').first().addClass('active');

                $('#carousel-product-images').carousel();




                // $('#file-preview').html('');    // Remove any previous images
                // $('#file-preview').append($img);
            }

            reader.readAsDataURL(file);

            // This is the name of the file

        //     var formData = new FormData();

        //    formData.append('wishlistImageFile', file);

        //    $.ajax({
        //         url: '/api/vendor/uploadWishlistImage',
        //         type: 'post',
        //         dataType: 'json',
        //         processData: false,
        //         contentType: false,
        //         cache: false,
        //         timeout: 0,
        //         data: formData,
        //         beforeSend: function() {
        //             //$('#product-images-spinner').removeClass('hidden');
        //         }
        //     })
        //     .done(function (data) {
        //         console.log(data);
        //         if (data.result.code === ResponseCodeEnum.SUCCESS) {
        //             alert('success');
        //         } else {
        //             switch (data.result.code) {
        //                 case ResponseCodeEnum.INVALID_URL: {
        //                     showErrorMessage('There was a problem with the URL.<br/>' +
        //                         data.result.message +
        //                         '<br/><br/>' +
        //                         'Please check the URL is correct and try gain.');
        //                     $('#txtItemURL').closest('.form-group').addClass('has-error');
        //                     break;
        //                 }
        //                 case ResponseCodeEnum.TIMEOUT_ERROR: {
        //                     showErrorMessage('Timed out while processing the URL.<br/>' +
        //                         data.result.message);
        //                     $('#txtItemURL').closest('.form-group').addClass('has-error');
        //                     break;
        //                 }
        //                 default: {
        //                     showErrorMessage(data.result.message);
        //                     break;
        //                 }
        //             }
        //             //$('#main')[0].scrollIntoView(); // Scroll to top of page
        //         }
        //     })
        //     .fail(function (jqXHR, textStatus, errorThrown) {
        //         showErrorMessage('fail - ' + errorThrown);
        //     })
        //     .always(function () {
        //         //$('#product-images-spinner').addClass('hidden');
        //     });
        }
    }
    
}


function frmAddWishlistItem_Validate() {
    'use strict';

    let response = {
        success: true,
        errorMessages: []
    }

    try {
        /* ### Delivery Options ###*/
        {
            let deliveryOption = $('input[type=radio][name=deliveryOptions]:checked').val();

            switch (deliveryOption) {
                case 'standard': {
                    let address_id = $('input[type=radio][name=addressSelected]:checked').val();
                    if (isNullOrEmpty(address_id)) {
                        $('#carousel-shipping-addresses').addClass('has-error');
                        response.errorMessages.push('No shipping address has been selected.');
                        response.success = false;
                    } else {
                        let selectedAddress = null;
                        for (let i = 0; i < shippingAddresses.length; i++) {
                            if (shippingAddresses[i].address_id == address_id) {
                                selectedAddress = shippingAddresses[i];
                                break;
                            }
                        }

                        if (selectedAddress === null) {
                            $('#carousel-shipping-addresses').addClass('has-error');
                            response.errorMessages.push('No shipping address has been selected.');
                            response.success = false;
                        }
                        else {
                            if (isNullOrEmpty(selectedAddress.firstname) && isNullOrEmpty(selectedAddress.lastname)) {
                                $('#carousel-shipping-addresses').addClass('has-error');
                                response.errorMessages.push('<strong>Name</strong> is a required field in the shipping address.');
                                response.success = false;
                            }

                            if (isNullOrEmpty(selectedAddress.street)) {
                                $('#carousel-shipping-addresses').addClass('has-error');
                                response.errorMessages.push('<strong>Address</strong> is a required field in the shipping address.');
                                response.success = false;
                            }

                            if (isNullOrEmpty(selectedAddress.city)) {
                                $('#carousel-shipping-addresses').addClass('has-error');
                                response.errorMessages.push('<strong>City</strong> is a required field in the shipping address.');
                                response.success = false;
                            }

                            if (isNullOrEmpty(selectedAddress.zipcode)) {
                                $('#carousel-shipping-addresses').addClass('has-error');
                                response.errorMessages.push('<strong>Zip Code</strong> is a required field in the shipping address.');
                                response.success = false;
                            }

                            if (isNullOrEmpty(selectedAddress.country)) {
                                $('#carousel-shipping-addresses').addClass('has-error');
                                response.errorMessages.push('<strong>Country</strong> is a required field in the shipping address.');
                                response.success = false;
                            }

                            if (isNullOrEmpty(selectedAddress.phone)) {
                                $('#carousel-shipping-addresses').addClass('has-error');
                                response.errorMessages.push('<strong>Phone Number</strong> is a required field in the shipping address.');
                                response.success = false;
                            }
                        }
                    }
                    break;
                }
                case 'digital': {
                    let emailAddress = document.getElementById('txtDigitalEmail').value;
                    if (emailAddress === null || emailAddress === undefined || emailAddress.trim().length === 0) {
                        $('#txtDigitalEmail').closest('.form-group').addClass('has-error');
                        response.errorMessages.push('Invalid Email Address.');
                        response.success = false;
                    }
                    break;
                }
                case 'collection': {
                    let location = document.getElementById('txtCollectionLocation').value;
                    if (location === null || location === undefined || location.trim().length === 0) {
                        $('#txtCollectionLocation').closest('.form-group').addClass('has-error');
                        response.errorMessages.push('A location must be specified.');
                        response.success = false;
                    }
                    break;
                }
                case 'exchange': {
                    let exchangeDetails = document.getElementById('txtExchangeDetails').value;
                    if (exchangeDetails === null || exchangeDetails === undefined || exchangeDetails.trim().length === 0) {
                        $('#txtExchangeDetails').closest('.form-group').addClass('has-error');
                        response.errorMessages.push('Exchange details must be specified.');
                        response.success = false;
                    }
                    break;
                }
            }

        } // End of Delivery Options


        /* ### Product Information ### */
        {
            let websiteURL = document.getElementById('txtItemURL').value;
            let imageSource = $('input[type=radio][name=imageSource]:checked').val();
            let productImageURL = $('input[type=radio][name=productImageSelected]:checked').val();
            let productTitle = document.getElementById('txtItemTitle').value;

            if (websiteURL === null || websiteURL === undefined || websiteURL.trim().length === 0) {
                $('#txtItemURL').closest('.form-group').addClass('has-error');
                response.errorMessages.push('The website URL of the purchase item must be specified.');
                response.success = false;
            }

            if (imageSource === 'url') {
                if (isNullOrEmpty(productImageURL)) {
                    $('#carousel-product-images').addClass('has-error');
                    response.errorMessages.push('A valid product image must be selected.');
                    response.success = false;
                }
            } else if (imageSource === 'file') {
                if (document.getElementById('btnFileSelector').files.length === 0) {
                    $('#carousel-product-images').addClass('has-error');
                    response.errorMessages.push('A valid product image must be selected.');
                    response.success = false;
                }
            }
            

            if (isNullOrEmpty(productTitle)) {
                $('#ddlItemTitle').closest('.form-group').addClass('has-error');
                $('#txtItemTitle').closest('.form-group').addClass('has-error');
                response.errorMessages.push('A product title must be specified.');
                response.success = false;
            }
            

        }   // End of Product Information


        /* ### Product Pricing ### */
        {
            let currencyCodeIndex = document.getElementById('ddlPriceCurrency').selectedIndex;

            if (currencyCodeIndex < 0) {
                $('#ddlPriceCurrency').closest('.form-group').addClass('has-error');
                response.errorMessages.push('A currency must be selected for Product Pricing.');
                response.success = false;
            }

            let itemPrice = parseFloat(document.getElementById('txtItemPrice').value);

            if (itemPrice === null || itemPrice === undefined || isNaN(itemPrice) ) {
                $('#txtItemPrice').closest('.form-group').addClass('has-error');
                response.errorMessages.push('A price for the item must be specified.');
                response.success = false;
            } else if (!isNaN(itemPrice) && itemPrice <= 0.0) {
                $('#txtItemPrice').closest('.form-group').addClass('has-error');
                response.errorMessages.push('A price greater than 0 must be specified.');
                response.success = false;
            }

            let shippingPrice = parseFloat(document.getElementById('txtItemShipping').value);

            if (shippingPrice === null || shippingPrice === undefined || isNaN(shippingPrice) ) {
                $('#txtItemShipping').closest('.form-group').addClass('has-error');
                response.errorMessages.push('A shipping price for the item must be specified.');
                response.success = false;
            } else if (!isNaN(shippingPrice) && shippingPrice < 0.0) {
                $('#txtItemShipping').closest('.form-group').addClass('has-error');
                response.errorMessages.push('A price of 0 or greater must be specified.');
                response.success = false;
            }

        }   // End of Product Pricing


        /* ### Funding Wallet ### */
        {
            let fundingSource = $('input[type=radio][name=fundingSource]:checked').val();
            let totalAmount = parseFloat(document.getElementById('txtItemTRO').value);

            if (!isNaN(totalAmount)) {
                switch (fundingSource) {
                    case 'TRO' : {
                        let amount = parseFloat(document.getElementById('txtTROSpendableBalance').innerText);
                        if (totalAmount > amount) {
                            $('input[type=radio][name=fundingSource]:checked + div').addClass('has-error');
                            response.errorMessages.push('Insufficient funds in TREOS Origin Wallet');
                            response.success = false;    
                        }
                        break;
                    }
                    case 'TRO_UBI' : {
                        let amount = parseFloat(document.getElementById('txtTROSpendableBalance').innerText);
                        if (totalAmount > amount) {
                            $('input[type=radio][name=fundingSource]:checked + div').addClass('has-error');
                            response.errorMessages.push('Insufficient funds in TREOS Origin UBI Wallet');
                            response.success = false;    
                        }
                        break;
                    }
                    default: {
                        $('input[type=radio][name=fundingSource]:checked + div').addClass('has-error');
                        response.errorMessages.push('Unknown Funding Wallet "' + fundingSource + '"');
                        response.success = false;
                        break;
                    }
                }
            } else {
                $('#txtItemTRO').closest('.form-group').addClass('has-error');
                response.errorMessages.push('Error retrieving total amount for funding.');
                response.success = false;
            }
            
        }   // Funding Wallet


        /* Terms and Condition */
        {
            if (!$('#cbAcceptTerms').prop('checked')) {
                $('#cbAcceptTerms').closest('.form-group').addClass('has-error');
                response.errorMessages.push('You must accept the terms and conditions.');
                response.success = false;
            }
        }
        
    } catch (err) {
        console.log(err);
        response.success = false;
        response.errorMessages.length = 0;
        response.errorMessages.push(err.description);
    }
    



    // if ($('#txtItemURL').val().trim().length === 0) {
    //     console.log('Error with txtItemURL');
    //     //$('#txtItemURL').addClass('has-error');
    //     $('#txtItemURL').alert('close');
    // }


    return response;
}


function responsiveDesign(x) {
    'use strict';
    if (x.matches) {
        // screen width <= 768px
        // $('div.responsiveContainer').addClass('containerMobile');
        // if (!$('body').hasClass('left-menu-compressed')) {
        //     $('.left-menu-ctrl').trigger('click');
        // }
        // $('button.responsive').addClass('btn-block');
        //$('.quick-menu-menu').hide();
        //$('.storefront-status').hide();
        $('#product-images-spinner').insertBefore('#btnProcessURL');
    } else {
        // screen width > 768px
        // $('div.responsiveContainer').removeClass('containerMobile');
        // if ($('body').hasClass('left-menu-compressed')) {
        //     $('.left-menu-ctrl').trigger('click');
        // }
        // $('button.responsive').removeClass('btn-block');
        //$('.quick-menu-menu').show();
        //$('.storefront-status').show();
        $('#product-images-spinner').insertAfter('#btnProcessURL');
    }
}




$(document).ready(function () {
    'use strict';

    $("#ddlPriceCurrency").on('change', ddlPriceCurrency_Change);
    $("#txtItemPrice").on('keyup', txtItemPrice_Keyup).on('change', txtItemPrice_Keyup);
    $("#txtItemShipping").on('keyup', txtItemShipping_Keyup).on('change', txtItemShipping_Keyup);
    $("#ddlBonusPercentage").on('change', ddlBonusPercentage_Change);
    $("#frmAddWishlistItem").on('submit', frmAddWishlistItem_Submit);
    $("#btnProcessURL").on('click', btnProcessURL_Click);
    $('#carousel-shipping-addresses').on('slid.bs.carousel', CarouselShippingAddresses_Slid);
    $('#carousel-product-images').on('slid.bs.carousel', CarouselProductImages_Slid);

    $('input[type=radio][name=deliveryOptions]').on('change', deliveryOptions_Change);
    $('#rbtnDelivStandard').trigger('click').trigger('change');

    $('input[type=radio][name=fundingSource]').on('change', fundingSource_Change);
    $('#rbtnTROSpendable').trigger('click').trigger('change');

    $('#ddlItemTitle').on('change', ddlItemTitle_Change);
    
    $('#btnChooseImage').on('click', btnChooseImage_Click);
    $('#btnFileSelector').on('change', btnFileSelector_Change);

    $('#lnkTermsAndConditions').on('click', lnkTermsAndConditions_Click);
    $('#btnCloseTermsAndConditions').on('click', btnCloseTermsAndConditions_Click);
    $('#btnAgreeTermsAndConditions').on('click', btnAgreeTermsAndConditions_Click);
    
    let width_sm = window.matchMedia('(max-width: 768px)');
    responsiveDesign(width_sm); // Call listener at run time
    width_sm.addListener(responsiveDesign); // Attach listener function on state changes

    loadVendorProfile();
    loadShippingAddress();
    loadCurrencies(true);
});
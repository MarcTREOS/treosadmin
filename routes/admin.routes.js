const express = require('express');
const Router = require('express-promise-router');
//const router = express.Router();
const router = new Router();


// GET home page
router.get('/withdrawal_requests', function(req, res, next) {
    let params = {
        title: 'TREOS Admin - Withdrawal Requests',
        layout: 'falcon'
    }
    //console.log('get withdrawal_requests');

    res.render('admin/withdrawal_requests', params);
    //res.send('done');
});

router.get('/eos_wallet_generator', function(req, res, next) {
    let params = {
        title: 'TREOS Admin - EOS Wallet Generator',
        layout: 'falcon'
    }
    //console.log('get eos_wallet_generator');

    res.render('admin/eos_wallet_generator', params);
    //res.send('done');
});

router.get('/crypto_withdrawal', function(req, res, next) {
    let params = {
        title: 'TREOS Admin - Crypto Withdrawal',
        layout: 'falcon'
    }
    //console.log('get crypto_withdrawal');

    res.render('admin/crypto_withdrawal', params);
    //res.send('done');
});

module.exports = router;
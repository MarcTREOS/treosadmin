const express = require('express');
const Router = require('express-promise-router');
//const router = express.Router();
const router = new Router();
const dbpool = require('../middleware/dbpool');
const admin = require('../middleware/api.admin.functions');
const vendor = require('../middleware/api.vendor.functions');
const enums = require('../middleware/enums');
const tools = require('../middleware/tools');

router.use(express.json());

/*
Middleware to authenticate and authorize the user token created by X-Cart
*/
async function authorizeUser(req, res, next) {
    'use strict';
    try {
        let LoggedInProfileID = req.body.LoggedInProfileID;
    
        if (process.env.NODE_ENV === 'development') {
            //let ssn = req.session;
            //LoggedInProfileID = ssn.profile_id;

            let cookies = tools.parseCookies(req);

            if (cookies.treos_profile_id !== undefined) {
                LoggedInProfileID = cookies.treos_profile_id;
            }
        }

        if (LoggedInProfileID !== undefined && LoggedInProfileID !== null) {
            // Assume logged in, proceed on to the routes

            let roleResponse = await vendor.getRole(LoggedInProfileID);

            if (roleResponse.result.code === enums.ResponseCodeEnum.SUCCESS) {
                if (roleResponse.role_id === 1) {
                    res.locals.profile_id = LoggedInProfileID;
                    next();
                } else {
                    // Do not proceed on to the routes
                    let response = {
                        result: {
                            code: enums.ResponseCodeEnum.NOT_LOGGED_IN,
                            message: 'Not Logged In As Admin'
                        }
                    }
                    res.json(response);
                }
            } else {
                // Do not proceed on to the routes
                let response = {
                    result: {
                        code: enums.ResponseCodeEnum.NOT_LOGGED_IN,
                        message: 'Not Logged In'
                    }
                }
                res.json(response);
            }

            
        } else {
            // Do not proceed on to the routes
            let response = {
                result: {
                    code: enums.ResponseCodeEnum.NOT_LOGGED_IN,
                    message: 'Not Logged In'
                }
            }
            res.json(response);
        }
    } catch(err) {
        console.error(err);
    }
    
}


/*
Authorization must be placed BEFORE any other routes.
If authentication fails, the client will not be authorized to use any of the following API calls.
*/
router.use(authorizeUser);

router.post('/getCryptoWithdrawals_Report', async function(req, res) {
    'use strict';
    let response = {
        result: {
            code: enums.ResponseCodeEnum.INTERNAL_ERROR,
            message: 'Internal Error'
        },
        total_records: 0,
        months: []
    }

    try {
        let symbol = req.body.symbol;
        let transaction_type_id = req.body.transaction_type_id;
        let date_from = req.body.date_from;
        let date_to = req.body.date_to;
        let forExport = (req.body.forExport === 'true');
        let record_offset = req.body.record_offset;
        let record_counter = req.body.record_counter;

        if (!isNaN(record_offset)) {
            record_offset = parseInt(record_offset);
        } else {
            record_offset = 0;
        }

        if (!isNaN(record_counter)) {
            record_counter = parseInt(record_counter);
        } else {
            record_counter = 25;
        }

        response = await admin.getCryptoWithdrawals_Report(symbol, transaction_type_id, date_from, date_to, forExport, record_offset, record_counter);

        //console.log(response);
    } catch (err) {
        response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
        response.result.message = "Internal Error";
        console.log(err);
    }
    
    res.json(response);
}); // getCryptoWithdrawals_Report

router.post('/getEosWalletCreationHistory', async function(req, res) {
    'use strict';
    let response = {
        result: {
            code: enums.ResponseCodeEnum.INTERNAL_ERROR,
            message: 'Internal Error'
        },
        total_records: 0,
        requests: []
    }

    try {
        let request_id = req.body.request_id;
        let symbol = req.body.symbol;
        let member_name = req.body.member_name;
        let date_from = req.body.date_from;
        let date_to = req.body.date_to;
        let forExport = (req.body.forExport === 'true');
        let record_offset = req.body.record_offset;
        let record_counter = req.body.record_counter;

        if (!isNaN(record_offset)) {
            record_offset = parseInt(record_offset);
        } else {
            record_offset = 0;
        }

        if (!isNaN(record_counter)) {
            record_counter = parseInt(record_counter);
        } else {
            record_counter = 25;
        }



        response = await admin.getEosWalletCreationHistory(request_id, symbol, member_name, date_from, date_to, forExport, record_offset, record_counter);

        //console.log(response);
    } catch (err) {
        response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
        response.result.message = "Internal Error";
        console.log(err);
    }
    
    res.json(response);
}); // getEosWalletCreationHistory

router.post('/getEosWalletCreationRequests', async function(req, res) {
    'use strict';
    let response = {
        result: {
            code: enums.ResponseCodeEnum.INTERNAL_ERROR,
            message: 'Internal Error'
        },
        requests: []
    }

    try {
        let request_id = req.body.request_id;
        let symbol = req.body.symbol;
        let member_name = req.body.member_name;
        let record_offset = req.body.record_offset;
        let record_counter = req.body.record_counter;

        if (!isNaN(record_offset)) {
            record_offset = parseInt(record_offset);
        } else {
            record_offset = 0;
        }

        if (!isNaN(record_counter)) {
            record_counter = parseInt(record_counter);
        } else {
            record_counter = 25;
        }

        response = await admin.getEosWalletCreationRequests(request_id, symbol, member_name, record_offset, record_counter);

        //console.log(response);
    } catch (err) {
        response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
        response.result.message = "Internal Error";
        console.error(err);
    }
    
    res.json(response);
}); // getEosWalletCreationRequests

router.post('/cancelEosAccountCreationRequest', async function(req, res) {
    'use strict';
    let response = {
        result: {
            code: enums.ResponseCodeEnum.INTERNAL_ERROR,
            message: 'Internal Error'
        },
        requests: []
    }

    try {
        let request_id = req.body.request_id;
        let reason = req.body.reason;
        let email = req.body.email;

        response = await admin.cancelEosAccountCreationRequest(request_id, reason, email);

        //console.log(response);
    } catch (err) {
        response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
        response.result.message = "Internal Error";
        console.error(err);
    }
    
    res.json(response);
}); // cancelEosAccountCreationRequest

router.post('/completeEosAccountCreationRequest', async function(req, res) {
    'use strict';
    let response = {
        result: {
            code: enums.ResponseCodeEnum.INTERNAL_ERROR,
            message: 'Internal Error'
        },
        requests: []
    }

    try {
        let request_id = req.body.request_id;
        let email = req.body.email;
        let txn_reference = req.body.txn_reference;

        response = await admin.completeEosAccountCreationRequest(request_id, email, txn_reference);

        //console.log(response);
    } catch (err) {
        response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
        response.result.message = "Internal Error";
        console.error(err);
    }
    
    res.json(response);
}); // completeEosAccountCreationRequest

router.post('/getWithdrawalRequests', async function(req, res) {
    'use strict';
    let response = {
        result: {
            code: enums.ResponseCodeEnum.INTERNAL_ERROR,
            message: 'Internal Error'
        },
        requests: []
    }

    try {
        let timeframe = req.body.timeframe;
        let currency = req.body.currency;
        let member_name = req.body.member_name;
        let record_offset = req.body.record_offset;
        let record_counter = req.body.record_counter;

        if (!isNaN(record_offset)) {
            record_offset = parseInt(record_offset);
        } else {
            record_offset = 0;
        }

        if (!isNaN(record_counter)) {
            record_counter = parseInt(record_counter);
        } else {
            record_counter = 25;
        }

        response = await admin.getWithdrawalRequests(timeframe, currency, member_name, record_offset, record_counter);

        //console.log(response);
    } catch (err) {
        response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
        response.result.message = "Internal Error";
        console.log(err);
    }
    
    res.json(response);
}); // getWithdrawalRequests


router.post('/getWithdrawalHistory', async function(req, res) {
    'use strict';
    let response = {
        result: {
            code: enums.ResponseCodeEnum.INTERNAL_ERROR,
            message: 'Internal Error'
        },
        total_records: 0,
        withdrawals: []
    }

    try {
        let currency = req.body.currency;
        let member_name = req.body.member_name;
        let date_from = req.body.date_from;
        let date_to = req.body.date_to;
        let forExport = (req.body.forExport === 'true');
        let record_offset = req.body.record_offset;
        let record_counter = req.body.record_counter;

        if (!isNaN(record_offset)) {
            record_offset = parseInt(record_offset);
        } else {
            record_offset = 0;
        }

        if (!isNaN(record_counter)) {
            record_counter = parseInt(record_counter);
        } else {
            record_counter = 25;
        }

        response = await admin.getWithdrawalHistory(currency, member_name, date_from, date_to, forExport, record_offset, record_counter);

        //console.log(response);
    } catch (err) {
        response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
        response.result.message = "Internal Error";
        console.log(err);
    }
    
    res.json(response);
}); // getWithdrawalHistory


router.post('/getWithdrawFeesHistory', async function(req, res) {
    'use strict';
    let response = {
        result: {
            code: enums.ResponseCodeEnum.INTERNAL_ERROR,
            message: 'Internal Error'
        },
        total_records: 0,
        months: []
    }

    try {
        let currency = req.body.currency;
        let date_from = req.body.date_from;
        let date_to = req.body.date_to;
        let forExport = (req.body.forExport === 'true');
        let record_offset = req.body.record_offset;
        let record_counter = req.body.record_counter;

        if (!isNaN(record_offset)) {
            record_offset = parseInt(record_offset);
        } else {
            record_offset = 0;
        }

        if (!isNaN(record_counter)) {
            record_counter = parseInt(record_counter);
        } else {
            record_counter = 25;
        }

        response = await admin.getWithdrawFeesHistory(currency, date_from, date_to, forExport, record_offset, record_counter);

        //console.log(response);
    } catch (err) {
        response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
        response.result.message = "Internal Error";
        console.log(err);
    }
    
    res.json(response);
}); // getWithdrawFeesHistory


router.post('/getFeesWithdrawalHistory', async function(req, res) {
    'use strict';
    let response = {
        result: {
            code: enums.ResponseCodeEnum.INTERNAL_ERROR,
            message: 'Internal Error'
        },
        total_records: 0,
        withdrawals: []
    }

    try {
        let currency = req.body.currency;
        let date_from = req.body.date_from;
        let date_to = req.body.date_to;
        let forExport = (req.body.forExport === 'true');
        let record_offset = req.body.record_offset;
        let record_counter = req.body.record_counter;

        if (!isNaN(record_offset)) {
            record_offset = parseInt(record_offset);
        } else {
            record_offset = 0;
        }

        if (!isNaN(record_counter)) {
            record_counter = parseInt(record_counter);
        } else {
            record_counter = 25;
        }

        response = await admin.getFeesWithdrawalHistory(currency, date_from, date_to, forExport, record_offset, record_counter);

        //console.log(response);
    } catch (err) {
        response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
        response.result.message = "Internal Error";
        console.log(err);
    }
    
    res.json(response);
}); // getFeesWithdrawalHistory

router.post('/getCryptoWithdrawalHistory_Report', async function(req, res) {
    'use strict';
    let response = {
        result: {
            code: enums.ResponseCodeEnum.INTERNAL_ERROR,
            message: 'Internal Error'
        },
        total_records: 0,
        withdrawals: []
    }

    try {
        let symbol = req.body.symbol;
        let transaction_type_id = req.body.transaction_type_id;
        let date_from = req.body.date_from;
        let date_to = req.body.date_to;
        let record_offset = req.body.record_offset;
        let record_counter = req.body.record_counter;
        let forExport = (req.body.forExport === 'true');

        if (!isNaN(record_offset)) {
            record_offset = parseInt(record_offset);
        } else {
            record_offset = 0;
        }

        if (!isNaN(record_counter)) {
            record_counter = parseInt(record_counter);
        } else {
            record_counter = 25;
        }

        response = await admin.getCryptoWithdrawalHistory_Report(symbol, transaction_type_id, date_from, date_to, record_offset, record_counter, forExport);

        //console.log(response);
    } catch (err) {
        response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
        response.result.message = "Internal Error";
        console.log(err);
    }
    
    res.json(response);
}); // getCryptoWithdrawalHistory_Report


router.get('/test', async function(req, res) {
    'use strict';
    let response = {
        result: {
            code: enums.ResponseCodeEnum.INTERNAL_ERROR,
            message: 'Internal Error'
        },
        requests: []
    }

    try {
        
    } catch (err) {
        response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
        response.result.message = "Internal Error";
        console.log(err);
    }
    
    res.json(response);
}); // getWithdrawalRequests


router.post('/reserveWithdrawalRequest', async function(req, res) {
    'use strict';
    let response = {
        result: {
            code: enums.ResponseCodeEnum.INTERNAL_ERROR,
            message: 'Internal Error'
        },
        requests: []
    }

    try {
        let withdrawalID = req.body.withdrawalID;
        let overrideInProgress = req.body.overrideInProgress;

        response = await admin.reserveWithdrawalRequest(withdrawalID, overrideInProgress);

        //console.log(response);
    } catch (err) {
        response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
        response.result.message = "Internal Error";
        console.log(err);
    }
    
    res.json(response);
}); // reserveWithdrawalRequest


router.post('/unreserveWithdrawalRequest', async function(req, res) {
    'use strict';
    let response = {
        result: {
            code: enums.ResponseCodeEnum.INTERNAL_ERROR,
            message: 'Internal Error'
        },
        requests: []
    }

    try {
        let withdrawalID = req.body.withdrawalID;

        response = await admin.unreserveWithdrawalRequest(withdrawalID);

        //console.log(response);
    } catch (err) {
        response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
        response.result.message = "Internal Error";
        console.log(err);
    }
    
    res.json(response);
}); // unreserveWithdrawalRequest


router.post('/cancelWithdrawalRequest', async function(req, res) {
    'use strict';
    let response = {
        result: {
            code: enums.ResponseCodeEnum.INTERNAL_ERROR,
            message: 'Internal Error'
        },
        requests: []
    }

    try {
        let withdrawalID = req.body.withdrawalID;
        let reason = req.body.reason;

        response = await admin.cancelWithdrawalRequest(withdrawalID, reason);

        //console.log(response);
    } catch (err) {
        response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
        response.result.message = "Internal Error";
        console.log(err);
    }
    
    res.json(response);
}); // cancelWithdrawalRequest

router.post('/completeWithdrawalRequest', async function(req, res) {
    'use strict';
    let response = {
        result: {
            code: enums.ResponseCodeEnum.INTERNAL_ERROR,
            message: 'Internal Error'
        },
        requests: []
    }

    try {
        let withdrawalID = req.body.withdrawalID;
        let transactionID = req.body.transactionID;
        let fee = req.body.fee;
        let fee_spent = req.body.fee_spent;

        response = await admin.completeWithdrawalRequest(withdrawalID, transactionID, fee, fee_spent);

        //console.log(response);
    } catch (err) {
        response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
        response.result.message = "Internal Error";
        console.log(err);
    }
    
    res.json(response);
}); // completeWithdrawalRequest


router.post('/withdrawalEdit_Update', async function(req, res) {
    'use strict';
    let response = {
        result: {
            code: enums.ResponseCodeEnum.INTERNAL_ERROR,
            message: 'Internal Error'
        },
        requests: []
    }

    try {
        let withdrawalID = req.body.withdrawalID;
        let transactionID = req.body.transactionID;
        let fee = req.body.fee;
        let fee_spent = req.body.fee_spent;

        response = await admin.withdrawalEdit_Update(withdrawalID, transactionID, fee, fee_spent);

        //console.log(response);
    } catch (err) {
        response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
        response.result.message = "Internal Error";
        console.log(err);
    }
    
    res.json(response);
}); // withdrawalEdit_Update


router.post('/getBlockchainDetails_Txn', async function(req, res) {
    'use strict';
    let response = {
        result: {
            code: enums.ResponseCodeEnum.INTERNAL_ERROR,
            message: 'Internal Error'
        },
        fee: 0
    }

    try {
        let currencyCode = req.body.currencyCode;
        let transactionID = req.body.transactionID;

        response = await admin.getBlockchainDetails_Txn(currencyCode, transactionID);

        //console.log(response);
    } catch (err) {
        response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
        response.result.message = "Internal Error";
        console.log(err);
    }
    
    res.json(response);
}); // getBlockchainDetails_Txn


router.post('/withdrawFees_Insert', async function(req, res) {
    'use strict';
    let response = {
        result: {
            code: enums.ResponseCodeEnum.INTERNAL_ERROR,
            message: 'Internal Error'
        }
    }

    try {
        let fees_year = req.body.fees_year;
        let fees_month = req.body.fees_month;
        let currency = req.body.currency;
        let amount = req.body.amount;
        let fee_spent = req.body.fee_spent;
        let address_to = req.body.address_to;
        let txn_reference = req.body.txn_reference;

        response = await admin.withdrawFees_Insert(fees_year, fees_month, currency, amount, fee_spent, address_to, txn_reference);

        //console.log(response);
    } catch (err) {
        response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
        response.result.message = "Internal Error";
        console.log(err);
    }
    
    res.json(response);
}); // withdrawFees_Insert

router.post('/withdrawCrypto_Insert', async function(req, res) {
    'use strict';
    let response = {
        result: {
            code: enums.ResponseCodeEnum.INTERNAL_ERROR,
            message: 'Internal Error'
        }
    }

    try {
        let year = req.body.year;
        let month = req.body.month;
        let transaction_type_id = req.body.transaction_type_id;
        let symbol = req.body.symbol;
        let amount = req.body.amount;
        let fee_spent = req.body.fee_spent;
        let withdraw_address = req.body.withdraw_address;
        let withdraw_memo = req.body.withdraw_memo;
        let txn_reference = req.body.txn_reference;

        response = await admin.withdrawCrypto_Insert(year, month, transaction_type_id, symbol, amount, fee_spent, withdraw_address, withdraw_memo, txn_reference);

        //console.log(response);
    } catch (err) {
        response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
        response.result.message = "Internal Error";
        console.log(err);
    }
    
    res.json(response);
}); // withdrawCrypto_Insert


module.exports = router;
const express = require('express');
//const Router = require('express-promise-router');
const router = express.Router();
//const router = new Router();
const request = require('request-promise');
const fs = require('fs');

router.use(express.json());

/*
This is used for development to allow the use of ajax calls to api.php
and redirect to the local api's.
*/
router.post('/', function(req, res) {
    'use strict';
    try {
        let endpoint = req.query.endpoint;
        res.redirect(307, '/api/' + endpoint);
    } catch(err) {
        console.error(err);
    }
});


/*
for delivering js, css etc
*/
router.get('/', function(req, res) {
    'use strict';
    let suffix = null;
    let endpoint = null;
    let dt = '';
    let filename = null;
    try {
        endpoint = req.query.endpoint;
        if (endpoint !== undefined && endpoint !== null) {
            let dots = endpoint.split('.');
            let parts = dots[dots.length - 1].split('?');
            suffix = parts[0];
            if (parts.length > 1) {
                dt = '?' + parts[parts.length -1];
            }
            filename = 'public/' + endpoint.substring(0, endpoint.length - dt.length);

            if (fs.existsSync(filename)) {
                switch (suffix.toLowerCase()) {
                    case 'css' : {
                        res.writeHead(200, {'Content-Type': 'text/css'});
                        break;
                    }
                    case 'js' : {
                        res.writeHead(200, {'Content-Type': 'text/javascript'});
                        break;
                    }
                    default: {
                        res.writeHead(200, {'Content-Type': 'text/html'});
                        break;
                    }
                }
    
                let fileStream = fs.createReadStream(filename);
                fileStream.pipe(res);
            } else {
                console.log('api.php - ' + filename + ' not found.');
                res.writeHead(404, {'Content-Type': 'text/plain'});
                res.write('404 Not Found\n');
                res.end();
            }
            
        }
    } catch(err) {
        console.error(err);
    }
});


module.exports = router;
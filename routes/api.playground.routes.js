const express = require('express');
const Router = require('express-promise-router');
const vendor = require('../middleware/api.vendor.functions');
const enums = require('../middleware/enums');
const tools = require('../middleware/tools');
const dfuse = require('../middleware/dfuse_api');

const router = new Router();

router.use(express.json());

async function authorizeUser(req, res, next) {
    'use strict';
    let proceed = true;
    try {
        let LoggedInProfileID = req.body.LoggedInProfileID;
    
        if (proceed) {
            if (process.env.NODE_ENV === 'development') {
                let cookies = tools.parseCookies(req);
    
                if (cookies.treos_profile_id !== undefined) {
                    LoggedInProfileID = cookies.treos_profile_id;
                }
            } else {
                // Do not proceed on to the routes
                let response = {
                    result: {
                        code: enums.ResponseCodeEnum.NOT_LOGGED_IN,
                        message: 'Not Development'
                    }
                }
                res.json(response);
                proceed = false;
            }
        }
        
        if (proceed) {
            if (LoggedInProfileID !== undefined && LoggedInProfileID !== null) {
                // Assume logged in, proceed on to the routes
    
                let roleResponse = await vendor.getRole(LoggedInProfileID);
    
                if (roleResponse.result.code === enums.ResponseCodeEnum.SUCCESS) {
                    res.locals.profile_id = LoggedInProfileID;
                    next();
                } else {
                    // Do not proceed on to the routes
                    let response = {
                        result: {
                            code: enums.ResponseCodeEnum.NOT_LOGGED_IN,
                            message: 'Not Logged In vendor - 2'
                        }
                    }
                    res.json(response);
                }
    
                
            } else {
                // Do not proceed on to the routes
                let response = {
                    result: {
                        code: enums.ResponseCodeEnum.NOT_LOGGED_IN,
                        message: 'Not Logged In vendor - 1'
                    }
                }
                res.json(response);
            }
        }
        
    } catch(err) {
        console.error(err);
    }
    
}

router.use(authorizeUser);

router.post('/getDfuse_AccountBalances', async function(req, res) {
    'use strict';
    let profile_id = null;

    let response = {
        result: {
            code: enums.ResponseCodeEnum.INTERNAL_ERROR,
            message: 'Internal Error'
        }
    };
    
    try {
        profile_id = res.locals.profile_id;

        let account = req.body.account;

        let accountExists = await dfuse.accountBalances(account);

        console.log(accountExists);
        //response = await vendor.getAddresses(profile_id, isBilling, isShipping, isWork);

        //console.log(response);
    } catch (err) {
        response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
        response.result.message = "Internal Error";
        console.error(err);
    }
    
    res.json(response);
}); // getDfuse_AccountBalances

router.post('/getDfuse_TokenBalances', async function(req, res) {
    'use strict';
    let profile_id = null;

    let response = {
        result: {
            code: enums.ResponseCodeEnum.INTERNAL_ERROR,
            message: 'Internal Error'
        }
    };
    
    try {
        profile_id = res.locals.profile_id;

        let contract = req.body.contract;
        let symbol = req.body.symbol;

        let accountExists = await dfuse.tokenBalances(contract, symbol);

        console.log(accountExists);
        //response = await vendor.getAddresses(profile_id, isBilling, isShipping, isWork);

        //console.log(response);
    } catch (err) {
        response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
        response.result.message = "Internal Error";
        console.error(err);
    }
    
    res.json(response);
}); // getDfuse_AccountBalances

router.post('/dfuse_SendToken', async function(req, res) {
    'use strict';
    let profile_id = null;

    let response = {
        result: {
            code: enums.ResponseCodeEnum.INTERNAL_ERROR,
            message: 'Internal Error'
        }
    };
    
    try {
        profile_id = res.locals.profile_id;

        let account_to = req.body.account_to;
        let amount = req.body.amount;

        if (!amount || isNaN(amount)) {
            amount = 0.0;
        }

        let accountExists = await dfuse.sendToken(account_to, amount);

        console.log(accountExists);
        //response = await vendor.getAddresses(profile_id, isBilling, isShipping, isWork);

        //console.log(response);
    } catch (err) {
        response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
        response.result.message = "Internal Error";
        console.error(err);
    }
    
    res.json(response);
}); // getDfuse_AccountBalances

module.exports = router;
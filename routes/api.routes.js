const express = require('express');
const Router = require('express-promise-router');
//const router = express.Router();
const router = new Router();

router.use(express.json());

router.get('/', async function(req, res) {
    'use strict';
    let response = {
        success: true,
        message: 'Inside API'
    }
    
    res.json(response);
});

router.get('/set', async function(req, res) {
    'use strict';

    res.locals.test = 'Testing';

    let response = {
        success: true,
        message: 'SET res.locals.test = ' + res.locals.test
    }
    
    res.json(response);
});

router.get('/get', async function(req, res) {
    'use strict';
    let response = {
        success: true,
        message: 'GET res.locals.test = ' + res.locals.test
    }
    
    res.json(response);
});

module.exports = router;
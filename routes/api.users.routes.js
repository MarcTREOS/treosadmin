const express = require('express');
const Router = require('express-promise-router');
//const router = express.Router();
const router = new Router();
//const bodyParser = require('body-parser');  // enables parsing of JSON from request
const dbpool = require('../middleware/dbpool');
const enums = require('../middleware/enums');
const uuidv4 = require('uuid/v4'); // random

//router.use(bodyParser.json({ type: 'application/json' }));

// Body Parser Middleware
// parse application/x-www-form-urlencoded
//router.use(express.urlencoded({extended:true}));
// parse application/json

router.use(express.json());


// For Passport

router.get('/', async (req, res) => {
    //res.json(members);
    res.send('<p>In the API</p>');
});


router.post('/getUsers', async function(req, res) {
    'use strict';
    let response = {
        result: {
            code: ResponseCodeEnum.INTERNAL_ERROR,
            message: 'Internal Error'
        },
        users: []
    }
    
    try {
        
        let dbResult = await dbpool.query(
            "SELECT profile_id, login" +
            " FROM xc_profiles",
            []);

        if (dbResult.success) {

            dbResult.result[0].forEach(function(item) {
                response.users.push(item.profile_id);
            });

            response.result.code = enums.ResponseCodeEnum.SUCCESS;
            response.result.message = null;
        } else {
            console.log(dbResult.error);
            response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
            response.result.message = dbResult.error;
        }
    } catch (err) {
        response.errorMessage = "Internal Error";
        console.log(err);
    }
    
    res.json(response);
});

router.post('/setUser', async function(req, res) {
    'use strict';

    let response = {
        result: {
            code: enums.ResponseCodeEnum.INTERNAL_ERROR,
            message: 'Internal Error'
        }
    }

    let commandText = '';
    let parameters = [];
    let dbResult = null;
    
    try {
        let profile_id = req.body.profile_id;
        console.log('profile_id = ' + profile_id);

        //let ssn = req.session;
        //ssn.profile_id = profile_id;
        res.cookie('treos_profile_id', profile_id, { encode: String, httpOnly: false, overwrite: true });

        response.result.code = enums.ResponseCodeEnum.SUCCESS;
        response.result.message = "Success";

        /*
        // Create cookie that can only be read by the server.
        let uid = uuidv4();
        let token = profile_id + ':' + uid;

        commandText = "INSERT INTO treos_profile_session_token" +
                " SET profile_id = ?, token = ?, date_created = NOW()";

        parameters.push(profile_id);
        parameters.push(uid);

        dbResult = await dbpool.query(commandText, parameters);

        if (dbResult.success) {
            res.cookie('treos_jwt', token, { encode: String, httpOnly: true, overwrite: true });
            response.result.code = enums.ResponseCodeEnum.SUCCESS;
            response.result.message = null;
        } else {
            response.result.code = enums.ResponseCodeEnum.DATABASE_ERROR;
            response.result.message = 'Database Error';
        }
        */

        
    } catch (err) {
        response.result.message = "Internal Error";
        console.log(err);
    }
    
    res.json(response);
});

module.exports = router;
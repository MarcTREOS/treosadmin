const express = require('express');
const Router = require('express-promise-router');
//const router = express.Router();
const router = new Router();
const dbpool = require('../middleware/dbpool');
const vendor = require('../middleware/api.vendor.functions');
const enums = require('../middleware/enums');
const tools = require('../middleware/tools');

router.use(express.json());

async function authorizeUser(req, res, next) {
    'use strict';
    try {
        let LoggedInProfileID = req.body.LoggedInProfileID;
    
        if (process.env.NODE_ENV === 'development') {
            let cookies = tools.parseCookies(req);

            if (cookies.treos_profile_id !== undefined) {
                LoggedInProfileID = cookies.treos_profile_id;
            }
        }

        if (LoggedInProfileID !== undefined && LoggedInProfileID !== null) {
            // Assume logged in, proceed on to the routes

            let roleResponse = await vendor.getRole(LoggedInProfileID);

            if (roleResponse.result.code === enums.ResponseCodeEnum.SUCCESS) {
                res.locals.profile_id = LoggedInProfileID;
                next();
            } else {
                // Do not proceed on to the routes
                let response = {
                    result: {
                        code: enums.ResponseCodeEnum.NOT_LOGGED_IN,
                        message: 'Not Logged In vendor - 2'
                    }
                }
                res.json(response);
            }

            
        } else {
            // Do not proceed on to the routes
            let response = {
                result: {
                    code: enums.ResponseCodeEnum.NOT_LOGGED_IN,
                    message: 'Not Logged In vendor - 1'
                }
            }
            res.json(response);
        }
    } catch(err) {
        console.error(err);
    }
    
}

router.use(authorizeUser);

router.post('/eosAccountExists', async function(req, res) {
    'use strict';
    let profile_id = null;
    let account_name = null;

    let response = {
        result: {
            code: enums.ResponseCodeEnum.INTERNAL_ERROR,
            message: 'Internal Error'
        },
        accountExists: false,
        requestExists: false
    }

    try {
        profile_id = res.locals.profile_id;

        account_name = req.body.account_name;

        response = await vendor.eosAccountExists(account_name);

        //console.log(response);
    } catch (err) {
        response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
        response.result.message = "Internal Error";
        console.log(err);
    }
    res.json(response);
}); // eosAccountExists

router.post('/eosCreateAccount', async function(req, res) {
    'use strict';
    let profile_id = null;
    let account_name = null;
    let owner_public_key = null;
    let active_public_key = null;
    let symbol = null;
    let fee = null;

    let response = {
        result: {
            code: enums.ResponseCodeEnum.INTERNAL_ERROR,
            message: 'Internal Error'
        }
    }

    try {
        profile_id = res.locals.profile_id;

        account_name = req.body.account_name;
        owner_public_key = req.body.owner_public_key;
        active_public_key = req.body.active_public_key;
        symbol = req.body.symbol;
        fee = parseFloat(req.body.fee);

        response = await vendor.eosCreateAccount(profile_id, account_name, owner_public_key, active_public_key, symbol, fee);

        //console.log(response);
    } catch (err) {
        response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
        response.result.message = "Internal Error";
        console.log(err);
    }
    res.json(response);
}); // eosCreateAccount

router.post('/getAddresses', async function(req, res) {
    'use strict';
    let profile_id = null;
    let isBilling = null;
    let isShipping = null;
    let isWork = null;

    let response = {
        result: {
            code: enums.ResponseCodeEnum.INTERNAL_ERROR,
            message: 'Internal Error'
        }
    };
    
    try {
        profile_id = res.locals.profile_id;

        isBilling = req.body.isBilling;
        isShipping = req.body.isShipping;
        isWork = req.body.isWork;

        response = await vendor.getAddresses(profile_id, isBilling, isShipping, isWork);

        //console.log(response);
    } catch (err) {
        response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
        response.result.message = "Internal Error";
        console.log(err);
    }
    
    res.json(response);
}); // getAddresses


router.post('/getFiatCurrencies', async function(req, res) {
    'use strict';
    let profile_id = null;

    let response = {
        result: {
            code: enums.ResponseCodeEnum.INTERNAL_ERROR,
            message: 'Internal Error'
        }
    };
    
    try {
        profile_id = res.locals.profile_id;

        response = await vendor.getFiatCurrencies();

        console.log(response);
    } catch (err) {
        response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
        response.result.message = "Internal Error";
        console.log(err);
    }
    
    res.json(response);
}); // getAddresses


router.post('/getPlatformCurrencies', async function(req, res) {
    'use strict';
    let profile_id = null;

    let response = {
        result: {
            code: enums.ResponseCodeEnum.INTERNAL_ERROR,
            message: 'Internal Error'
        }
    };
    
    try {
        profile_id = res.locals.profile_id;

        response = await vendor.getPlatformCurrencies();

        //console.log(response);
    } catch (err) {
        response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
        response.result.message = "Internal Error";
        console.log(err);
    }
    
    res.json(response);
}); // getPlatformCurrencies

router.post('/getPlatformTransactionTypes', async function(req, res) {
    'use strict';
    let profile_id = null;
    let only_active = null;

    let response = {
        result: {
            code: enums.ResponseCodeEnum.INTERNAL_ERROR,
            message: 'Internal Error'
        },
        transaction_types: {}
    }
    
    try {
        profile_id = res.locals.profile_id;

        only_active = req.body.only_active;

        if (only_active === undefined || only_active === null) {
            only_active = true;
        }

        response = await vendor.getPlatformTransactionTypes(only_active);

        //console.log(response);
    } catch (err) {
        response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
        response.result.message = "Internal Error";
        console.log(err);
    }
    
    res.json(response);
}); // getPlatformCurrencies


router.post('/getVendorProfile', async function(req, res) {
    'use strict';
    let profile_id = null;

    let response = {
        result: {
            code: enums.ResponseCodeEnum.INTERNAL_ERROR,
            message: 'Internal Error'
        }
    };
    
    try {
        profile_id = res.locals.profile_id;

        response = await vendor.getVendorProfile(profile_id);

        //console.log(response);
    } catch (err) {
        response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
        response.result.message = "Internal Error";
        console.log(err);
    }
    
    res.json(response);
}); // getAddresses


router.post('/webScrapeProductInfo', async function(req, res) {
    'use strict';
    let profile_id = null;
    let url = null;

    let response = {
        result: {
            code: enums.ResponseCodeEnum.INTERNAL_ERROR,
            message: 'Internal Error'
        }
    };
    
    try {
        profile_id = res.locals.profile_id;
        url = req.body.url;

        response = await vendor.webScrapeProductInfo(url);
    } catch (err) {
        response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
        response.result.message = "Internal Error";
        console.log(err);
    }
    
    res.json(response);
}); // webScrapeProductInfo


router.post('/uploadWishlistImage', async function(req, res) {
    'use strict';
    let profile_id = null;
    let url = null;

    let response = {
        result: {
            code: enums.ResponseCodeEnum.INTERNAL_ERROR,
            message: 'Internal Error'
        }
    };
    
    try {
        profile_id = res.locals.profile_id;
        console.log('req.files', req.files);

        response = await vendor.uploadWishlistImage(profile_id, req.files);
    } catch (err) {
        response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
        response.result.message = "Internal Error";
        console.log(err);
    }
    
    res.json(response);
}); // webScrapeProductInfo


router.post('/addWishlistItem', async function(req, res) {
    'use strict';
    let profile_id = null;
    let url = null;

    let response = {
        result: {
            code: enums.ResponseCodeEnum.INTERNAL_ERROR,
            message: 'Internal Error'
        }
    };
    
    try {
        profile_id = res.locals.profile_id;
        console.log('req.files', req.files);

        response = await vendor.addWishlistItem(profile_id, req);
    } catch (err) {
        response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
        response.result.message = "Internal Error";
        console.log(err);
    }
    
    res.json(response);
}); // addWishlistItem


router.post('/convertCSV', async function(req, res) {
    'use strict';
    let profile_id = null;

    let response = {
        result: {
            code: enums.ResponseCodeEnum.INTERNAL_ERROR,
            message: 'Internal Error'
        }
    };
    
    try {
        profile_id = res.locals.profile_id;
        response = await vendor.convertCSV(profile_id, req);
    } catch (err) {
        response.result.code = enums.ResponseCodeEnum.INTERNAL_ERROR;
        response.result.message = "Internal Error";
        console.log(err);
    }
    
    res.json(response);
}); // convertCSV

module.exports = router;
const express = require('express');
const Router = require('express-promise-router');
//const router = express.Router();
const router = new Router();

// GET home page
router.get('/', function(req, res, next) {
    let params = {
        title: 'TREOS Admin - Login',
        //layout: 'loggedOut'
        layout: 'falcon'
    }

    res.render('home', params);
});

router.post('/', function(req, res, next) {
    
});

module.exports = router;
const express = require('express');
const Router = require('express-promise-router');
//const router = express.Router();
const router = new Router();

router.use('/', require('./home.routes.js'));

router.use('/api/users', require('./api.users.routes'));
router.use('/api/playground', require('./api.playground.routes'));
router.use('/api/vendor', require('./api.vendor.routes'));
router.use('/api/admin', require('./api.admin.routes'));
router.use('/api.php', require('./api.php.routes'));
router.use('/api', require('./api.routes'));

router.use('/playground', require('./playground.routes'));
router.use('/vendor', require('./vendor.routes'));
router.use('/admin', require('./admin.routes'));
router.use('/ipfs', require('./ipfs.routes'));

module.exports = router;
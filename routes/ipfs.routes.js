/* https://www.npmjs.com/package/ipfs-http-client */
const express = require('express');
const Router = require('express-promise-router');
//const router = express.Router();
const router = new Router();
const ipfsClient = require('ipfs-http-client');

let ipfs = ipfsClient({ host: 'localhost', port: '5001', protocol: 'http' });

router.post("/upload", async function(req, res, next) {
    var files = [
        {
            path: '/tmp/myfile.txt',
            content: ipfs.Buffer.from('ABC')
        }
    ]
    var content = ipfs.Buffer.from('ABC');
    var results = await ipfs.add(content);
    var hash = results[0].hash; // "m..WW"
});

module.exports = router;
const express = require('express');
const Router = require('express-promise-router');
//const router = express.Router();
const router = new Router();

router.get('/dfuse', function(req, res, next) {
    let params = {
        title: 'TREOS Admin - DFuse Playground',
        //layout: 'loggedOut'
        layout: 'falcon'
    }
    //console.log('get eos_wallet_generator');

    res.render('playground/dfuse', params);
    //res.send('done');
});


module.exports = router;
const express = require('express');
const Router = require('express-promise-router');
//const router = express.Router();
const router = new Router();


// GET home page
router.get('/wishlist_add_item', function(req, res, next) {
    let params = {
        title: 'TREOS Admin - WishList',
        //layout: 'loggedOut'
        layout: 'falcon'
    }
    //console.log('get wishlist_add_item');

    //res.render('vendor/wishlist_add_item', params);
    //res.send('done');
});

router.get('/csv_converter', function(req, res, next) {
    let params = {
        title: 'TREOS Admin - CSV Converter',
        //layout: 'loggedOut'
        layout: 'falcon'
    }
    //console.log('get csv_converter');

    res.render('vendor/csv_converter', params);
    //res.send('done');
});

router.get('/eos_wallet_generator', function(req, res, next) {
    let params = {
        title: 'TREOS Admin - EOS Wallet Generator',
        //layout: 'loggedOut'
        layout: 'falcon'
    }
    //console.log('get eos_wallet_generator');

    res.render('vendor/eos_wallet_generator', params);
    //res.send('done');
});

router.get('/inflation_credits', function(req, res, next) {
    let params = {
        title: 'TREOS Admin - Inflation Credits',
        //layout: 'loggedOut'
        layout: 'falcon'
    }
    //console.log('get eos_wallet_generator');

    res.render('vendor/inflation_credits', params);
    //res.send('done');
});


module.exports = router;
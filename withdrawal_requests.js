const express = require('express');
const app = express();
const exphbs = require('express-handlebars');
const path = require('path');
const dotenv = require('dotenv');
const fs = require('fs');
const fileupload = require('express-fileupload');

// Global variable to tell me where the root directory of the project is.
global.appRoot = path.resolve(__dirname);
global.tempFileDir = path.join(global.appRoot,'tmp');

// Dfuse
// npm install @dfuse/client
// npm install node-fetch ws
global.fetch = require('node-fetch')
global.WebSocket = require('ws')

dotenv.config();

//app.use(fileupload());
app.use(fileupload({
    useTempFiles: true,
    tempFileDir: global.tempFileDir
}));

// Parse URL-encoded bodies (as sent by HTML forms)
app.use(express.urlencoded({extended: true}));

// Parse JSON bodies (as sent by API clients)
app.use(express.json());

// Handlebars Middleware https://www.youtube.com/watch?v=1srD3Mdvf50
app.engine('handlebars', exphbs({
    extname: 'handlebars',
    defaultLayout: 'loggedIn',
    layoutsDir: path.join(__dirname, 'views/layouts'),
    partialsDir: path.join(__dirname, 'views/partials'),
    helpers: {
        section: function(name, options) { 
            if (!this._sections) this._sections = {};
            this._sections[name] = options.fn(this); 
            return null;
        },
        renderCSS: function(filename) {
            // {{{renderScript '/css/styles.css'}}}
            let result = '';
            try {
                let localFile = path.join(__dirname, 'public', filename)
                let epoch = parseInt(fs.statSync(localFile).mtimeMs);   // Last modified time of file
                result = '<link href="' + filename + '?t=' + epoch + '" rel="stylesheet" type="text/css" media="screen" />';
            } catch (err) {
                result = '<link href="' + filename + '" rel="stylesheet" type="text/css" media="screen" />';
            }
            return result;
        },
        renderScript: function(filename) {
            // {{{renderScript '/js/home.js'}}}
            let result = '';
            let attribType = '';
            try {
                // console.log(scriptType);
                // if (scriptType !== undefined && scriptType !== null) {
                //     attribType = ' type="' + scriptType + '"';
                // }
                let localFile = path.join(__dirname, 'public', filename)
                let epoch = parseInt(fs.statSync(localFile).mtimeMs);   // Last modified time of file
                result = '<script' + attribType + ' src="' + filename + '?t=' + epoch + '"></script>';
            } catch (err) {
                result = '<script' + attribType + ' src="' + filename + '"></script>';
            }
            return result;
        }
    }  
}));



app.set('view engine', 'handlebars');

// Move routes outside of server.js
const routes = require('./routes/index.routes');
app.use('/', routes);

// Serve everything from the public folder as static pages
//app.use(express.static(path.join(__dirname, '/public')));
app.use('/', express.static(path.join(__dirname, '/public')));

app.use('/uploads/wishlist', express.static(path.join(__dirname, 'public/var/www/html/treos/uploads/wishlist')));

//const serverPort = process.env.PORT || 8080;  // If environment server port not specified, use 8080
const serverPort = 5123;

app.listen(serverPort, () => {
    console.log(`Server started on port ${serverPort}`);
});  // callback as second parameter. note ` vs '

// 'npm run dev' to run with nodemon

// {user_id:0, logged_in: false} OR {user_id:7, logged_in: true}

// npm outdated                - Lists all packages that could be updated.
// npm update                  - Update the packages to the Wanted version
// npm update express-handlebars
// npm install express-handlebars@latest   - updates express-handlebars to the latest version
// npm install npm@latest -g   - Update npm